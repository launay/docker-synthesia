./cmd_launch.sh stop
./cmd_cleanup_engine.sh
./cmd_cleanup_www.sh

echo "Building Docker Image for Synthesia"
sudo docker build --rm -t synthesia .
echo "Done"

if [ "$1" = "start" ]
	then
		./cmd_launch.sh
fi
