
# print envars
printenv > ~/envars.txt

# get git data
git clone https://gitlab.com/launay/docker-synthesia.git docker-synthesia

# setup folders
sudo mkdir -p -m 777 /var/www/synthesia/

# setup synthesia
mkdir -p -m 777 /home/synthesia/synthesia/common/
cp -R ~/docker-synthesia/data/synthesia/engine/* /home/synthesia/synthesia/common/
mkdir -p -m 777 /var/www/synthesia/common/
cp -R ~/docker-synthesia/data/synthesia/www/* /var/www/synthesia/common/
sudo chown -R synthesia:synthesia /home/synthesia/synthesia/common
sudo chown -R synthesia:synthesia /var/www/synthesia/common

# red5 stuff
mkdir -p -m 777 /home/synthesia/red5/
cp -R ~/docker-synthesia/data/red5/* /home/synthesia/red5/
sudo chown -R synthesia:synthesia /home/synthesia/red5/
mkdir -p -m 777 /home/synthesia/red5/red5-server-1.0.9/log
mkdir -p -m 777 /home/synthesia/red5/red5-server-1.0.9/proc

# get binaries
cp ~/bin/LiveProviderV2.jar /home/synthesia/synthesia/common/bin/
cp ~/bin/CDNImport.jar /home/synthesia/synthesia/common/import/bin/
cp ~/bin/RtmpStreamer.jar /home/synthesia/synthesia/common/playlist/bin/
cp ~/bin/RtmpDump.jar /home/synthesia/synthesia/common/pige/bin/
cp ~/bin/RtmpDump.jar /var/www/synthesia/common/pige/bin/

#get .vizionr codec stuff
mkdir -p -m 777 ~/.vizionr/codec/
cp ~/bin/codec/* ~/.vizionr/codec/

#setup timezone
sudo ln -fs /usr/share/zoneinfo/Europe/Paris /etc/localtime
sudo dpkg-reconfigure -f noninteractive tzdata

# start apache
sudo service apache2 start

# red5 startup
cd ~/red5/red5-server-1.0.9/
./launch.sh

# engine startup
cd ~/synthesia/common/
./cleanup.sh
./configure.sh

# www cleanup
cd /var/www/synthesia/common/
./cleanup.sh

# demo stuff
cp ~/demo/playlist/master/* /var/www/synthesia/common/playlist/master/
cp ~/demo/playlist/out/* /var/www/synthesia/common/playlist/out/
cp ~/demo/playlist/images/* /var/www/synthesia/common/playlist/images/
cp ~/demo/playlist/current.m3u /var/www/synthesia/common/playlist/playlist/

# cleanup demo
sudo rm -fr ~/demo

# www rights config
./configure.sh

# configure input publish key
publishKey=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
sed -i -- 's/PUBLISH_KEY/'$publishKey'/g' /var/www/synthesia/common/config/startup.json

# synthesia startup
cd ~/synthesia/common/
./launch.sh

# playlist startup
cd ~/synthesia/common/playlist/
./launch.sh

#wait loop (to prevent container for shutting down)
while true ; do echo "main loop" ; sleep 10 ; done;
