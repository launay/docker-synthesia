<?php
require_once "php/info.php";

$message = "";
$error = false;


//handles remove request
if(isset($_POST["remove"])){
	$guid = $_POST["guid"];
	$name = $_POST["name"];
	$error = false;
	$message = "La vidéo " . $name . " a été supprimée";
	
	if(!removeVideo($guid)){
		$error = false;
		$message = "La vidéo " . $name . " n'a pas pu être supprimée";

	}

}

$videos = getAllVideos();
$videoNumber = count($videos);

//print_r($videos);

?>
<html>
	<head>
		<?php createHeader();?>

	</head>
	<style>
	.vzr_centered {
	   text-align: center;   
	}
	</style>

	<body>

	<script>
		function doConfirm(video) {
			return confirm("Etes-vous sûr de vouloir supprimer l'extrait '" +video + "' ?");
		}
	</script>


	<?php createNavBar("video",""); ?>

	<!-- ERROR PANEL -->
	<?php
	if(strlen($message)>0){
		$class="label label-default";
		if($error)
			$class="label label-danger";
		echo '<div id="errorMsg" class="' . $class . '" style="display:block">' . $message .'</div></br>';
	}
	?>

<div class="panel panel-default">


	<div class="panel-heading">
	  <h3 class="panel-title">Extraits disponibles (<?php echo $videoNumber;?>)</h3>
	</div>
		
	<div class="panel-body" _style="max-height: 400;overflow-y: scroll;">
		<div class="container">
			<table class="font_80 evenOdd" id="plTable" style="white-space: nowrap;">
			  <thead>
					<tr>
						<th>Fichier Vidéo&nbsp;</th>
						<th>Nom&nbsp;</th>
						<th class="vzr_centered">Horaires&nbsp;</th>
						<th class="vzr_centered">Durée&nbsp;</th>
						<th class="vzr_centered">Taille&nbsp;</th>
					</tr>
			  </thead>
			  <tbody id="sortable">
<?php

$count=0;
foreach($videos as $key=>$video){
	$count++;

	$link = $video["link"];
	$guid = $key;
	$from = $video["fromTs"];
	$to = $video["toTs"];
	$hour = prettyDate($from) . "<br/>" . prettyDate($to);
	$duration = $video["duration"];
	$size = $video["size"];
	$name = $video["name"];
	
	echo "<tr id='" . $guid . "'>";
	echo "<td><a href='". $link."' target='_blank'>" . $guid ."</a></td>";
	echo "<td>" . htmlspecialchars($name)  . "</td>";
	echo "<td class='vzr_centered'>" . $hour ."</td>";
	echo "<td class='vzr_centered'>" . printDuration($duration) ."</td>";
	echo "<td class='vzr_centered'>" . printSize($size) ."</td>";
	
	$submitText = "return doConfirm(\"" . $name ."\")";

	echo "
		<td>
			<form method='post' name='remove_" . $count . "' onsubmit='" . $submitText ."'>
				<input type='submit' name='remove' value='Supprimer' ></input>
				<input type='hidden' name='guid' value='" . $guid . "'></input>
				<input type='hidden' name='name' value='" . htmlspecialchars($name) . "'></input>
				<a href='" . SAVE_FOLDER_RELATIVE . $guid . "' download='" .htmlspecialchars($name) . ".mp4'>Télécharger</a>
			</form>
		</td>
		";
		

	echo "</tr>\n";
}

?>
				</tbody>
			</table>
	
		</div>
	</div>
		               
  </div>


<script>
  <?php if($videoNumber == 0){echo '$("#plTable").hide();';} ?>

		function reloadPage(){
			window.location = window.location.href;
		}

		setTimeout(function(){
			$("#errorMsg").hide(); 
		}, 5000);

</script>



</body>

</html>

