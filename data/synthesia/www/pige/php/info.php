<?php

require_once "config/config.php";

define("EXTRACTS_PERSISTENCY", 3600);
define("MAX_EXTRACT_DURATION", 20 * 60 * 1000);
define("EXTRACTS_FOLDER", "/var/www/synthesia/common/pige/extracts/temp/");
define("EXTRACTS_FOLDER_RELATIVE", "extracts/temp/");
define("SAVE_FOLDER", "/var/www/synthesia/common/pige/extracts/save/");
define("SAVE_FOLDER_RELATIVE", "extracts/save/");
define("DUMP_FOLDER", "/var/www/synthesia/common/pige/segments/");
define("FFMPEG_EXE", "/var/www/synthesia/common/pige/bin/ffmpeg");
define("AVCONV_EXE", "/var/www/synthesia/common/pige/bin/avconv");
define("EXTRACTOR_EXE", "/var/www/synthesia/common/pige/bin/RtmpDump.jar");


function isMediaFile($file){

	$MEDIA_EXTENSIONS = array(".MP4", ".MP3", ".AAC", ".ADTS");
	foreach($MEDIA_EXTENSIONS as $ext){
		if(endsWith(strtoupper(basename($file)), $ext)){
			return true;
		}
	}
	return false;
}

function getVideoDescription($file){
	$description = array();
	$descrFile = SAVE_FOLDER . $file . ".json";
	if(file_exists($descrFile)){
		$json = file_get_contents($descrFile);
		$description = json_decode($json,true);		
	}
	
	return $description;
	
}

function getAllVideos(){

	$videos = array();
	$result = array();

	$vids = scandir(SAVE_FOLDER);
	asort($vids);
	foreach($vids as $key=>$vid){
		if(isMediaFile($vid)){
			$guid = $vid;
			$descr = getVideoDescription($vid);
			$descr["link"] = SAVE_FOLDER_RELATIVE . $vid;
			$videos[$guid] = $descr;
		}
	}

	return $videos;
}

function removeVideo($path){

	$target = SAVE_FOLDER . $path;
	$targetJson = SAVE_FOLDER . $path . ".json";
	$ok = true;
	if(file_exists($target)){
			$ok &= unlink($target);
	}
	if(file_exists($targetJson)){
			$ok &= unlink($targetJson);
	}
	
	return $ok;

}

function prettyDate($timestamp){
	return date("d/m/Y H:i:s", $timestamp/1000);
}

function printSize($size){
	if(!$size) return "(inconnue)";

	if($size<1024) return ceil($size) . "o";
	if($size<1024*1024) return ceil($size/1024) . "Ko";
	return ceil($size / 1024 / 1024) . "Mo";
	
}

function printDuration($duration){
	if(!$duration) return "(inconnue)";


	$ms = $duration;
	$hours = 0;
	$minutes = 0;
	$seconds = 0;


	if ( $ms > 3600000 ){
		$hours = floor( $ms / 3600000 );
		$ms = $ms % 3600000;
	}
	if ( $ms > 60000 ){
		$minutes = floor( $ms / 60000 );
		$ms = $ms % 60000;
	}
	$seconds = floor( $ms / 1000 );


	return str_pad( $hours, 2, '0', STR_PAD_LEFT ) . "h"
		. str_pad( $minutes, 2, '0', STR_PAD_LEFT ) . "m"
		. str_pad( $seconds, 2, '0', STR_PAD_LEFT ) . "s";

	
}

function cleanupExtracts($sessionId){

	$extracts = scandir(EXTRACTS_FOLDER);
	foreach($extracts as $extract){
		$file = EXTRACTS_FOLDER . $extract;
		if(!is_file($file)) continue;
		if(startsWith($extract, ".")) continue;
		if(startsWith($extract, $sessionId)) continue;
		$lm = filemtime($file);
		$elapsed = time() - $lm;
		if($elapsed > EXTRACTS_PERSISTENCY) unlink ($file);
	}

}

function storeDescription($saveLink, $fromTs, $toTs, $saveName){

	$descriptionFile = $saveLink . ".json";
	$description = array();
	
	//load
	if(file_exists($descriptionFile)){
		$descriptionRaw = file_get_contents($descriptionFile);
		$descrition = json_decode($descriptionRaw, true);	
	}
	
	//update
	if($fromTs > 0)	$description["fromTs"] = $fromTs;
	if($toTs > 0)	$description["toTs"] = $toTs;
	if($fromTs > 0 && $toTs > 0){
		$description["duration"] = $toTs - $fromTs;
	}
	if($saveName){
		$description["name"] = $saveName;
	}
	if(file_exists($saveLink)){
		$description["size"] = filesize($saveLink);
	}
	
	//store back
	$newJson = json_encode($description);
	file_put_contents($descriptionFile, $newJson);

}

function save($fromTs, $toTs, $sessionId, $mp4Link, $saveName){
	
	$saveUid = $fromTs . "_" . $toTs . "_" . uniqid() . ".mp4";
	
	$saveLink = SAVE_FOLDER . $saveUid;
	$saveLinkRelative = SAVE_FOLDER_RELATIVE . $saveUid;
	
	copy($mp4Link, $saveLink);
	storeDescription($saveLink, $fromTs, $toTs, $saveName);
	
	return array(false, "L'extrait a été sauvegardé", $saveLink, $saveLinkRelative);

}

function getCapture($fromTs, $toTs, $sessionId){
	
	$capturePrefix = EXTRACTS_FOLDER . $sessionId . "_capture";
	$capturePrefixRelative = EXTRACTS_FOLDER_RELATIVE . $sessionId . "_capture";

	$captureNameTS = $capturePrefix . ".ts";
	$captureNameMP4 = $capturePrefix . ".mp4";
	$captureNameMP4Relative = $capturePrefixRelative . ".mp4";
	
//	$convertCL = FFMPEG_EXE . " -y -i $captureNameTS -acodec copy -vcodec copy -absf aac_adtstoasc -strict experimental -f mp4 $captureNameMP4";
	$convertCL = AVCONV_EXE . " -y -i $captureNameTS -acodec aac -ar 48k -ac 2 -vcodec copy -strict experimental -f mp4 $captureNameMP4";

	$captureCL = 
		'java -jar ' . EXTRACTOR_EXE 
		. ' EXTRACT -outputFile="' . $captureNameTS 
		. '" -dumpLocation="' . DUMP_FOLDER 
		. '" -segmentPrefix="segment_" -segmentDatePatternDump="yyyy_MM_dd_HH_mm_ss" -segmentDatePatternExtract="dd/MM/yyyy HH:mm:ss" -fromTs=' . $fromTs . ' -toTs=' . $toTs .'';

	$captureDuration = $toTs - $fromTs;
	if($captureDuration <= 0){
		return array(true, "La durée d'extraction doit être positive !", null, null);
	}
	if($captureDuration > MAX_EXTRACT_DURATION){
		$errored = true;
			return array(true, "La durée d'extraction est limitée à " . (MAX_EXTRACT_DURATION/1000/60) . "minutes !", null, null);
	}
	else{

		exec($captureCL, $output, $return);		
		//print_r($captureCL);echo "<br>";
		//print_r($output);echo "<br>";
		//print_r($return);echo "<br>";


		$output = array();
		$result = 0;
		exec($convertCL, $output, $return);
		//print_r($convertCL);echo "<br>";
		//print_r($output);echo "<br>";
		//print_r($return);echo "<br>";


		unlink($captureNameTS);
		
		if(!file_exists($captureNameMP4)){
			return array(true, "L'extraction a échoué (Vérifiez si vous ne vous trouvez pas en dehors de la période de validité de la capture...)", null,null);
		}
		
		return array(false, "L'extraction s'est déroulée avec succès.", $captureNameMP4, $captureNameMP4Relative);

	}	

}

function startsWith($haystack, $needle) {
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}


function createHeader(){
	echo '
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>' . MAIN_TITLE . '</title>
		<script src="js/info.js"></script>
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery-ui/jquery-ui.js"></script>				
		<link rel="icon" type="image/png" href="images/synthesia.png" />
		<link rel="stylesheet" type="text/css" href="js/jquery-ui/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
		
		<link href="include/video-js/video-js.css" rel="stylesheet">
		<script src="include/video-js/video.js"></script>
		<script> videojs.options.flash.swf = "include/video-js/video-js.swf";</script>
		
		<style>
		body {
			padding-top: 70px;	
		}
		.pad_10{
			padding: 10px;
		}
		
		.evenOdd tr:nth-child(even) {background: #EEE}
		.evenOdd tr:nth-child(odd) {background: #FFF}
		
		td { 
			padding: 10px;
		}
		
		.pad_4{
			padding: 4px;
		}
		.bordered_nok{
			border: 3px solid #DDD;
		}
		.bordered_ok{
			border: 3px solid red;
		}
		.font_80 {
			font-size: 85%;	
		}
		.btn-warning{
			background-color:#ff3d00;
		}
		.lbl-warning{
			background-color:#ff3d00;
		}
		.panel-heading a:after {
			font-family:"Glyphicons Halflings";
			content:"\e114";
			float: right;
			color: grey;
		}
		.panel-heading a.collapsed:after {
			content:"\e080";
		}
		.clickable{
			cursor: pointer;
		}
		.table-no-border td {
			border-top: none !important;
		}
		.table-label{
			font-weight:bold;
		}
		.row{
			margin-top:40px;
			padding: 0 10px;
		}
		.panel-heading span {
			margin-top: -20px;
			font-size: 15px;
		}
		</style>'
	;

}


function createNavBar($selectedOne, $selectedTwo){

	echo '

	<nav class="navbar navbar-default navbar-fixed-top">
		  <div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			<a class="navbar-brand" href="output.php"><span style="color:black;"><b>' . MAIN_TITLE . '</b></span></a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">
			  				<li><img src="images/synthesia.png" height="50"/>&nbsp;&nbsp;&nbsp;</li>

			  ';

				echo '				
					  <li ' . ($selectedOne == "pige" ? 'class="active"' : '') . '><a href="pige.php">Extraction</a></li>
					  <li ' . ($selectedOne == "video" ? 'class="active"' : '') . '><a href="video.php">Mes Extraits</a></li>

					<li class="dropdown' . (($selectedOne == "config" || $selectedOne == "status" || $selectedOne == "log") ? " active" : "") .'">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration<span class="caret"></span></a>
					<ul class="dropdown-menu">
					  <li ' . ($selectedOne == "config" ? 'class="active"' : '') . '><a href="config.php">Configuration</a></li>
				 <li ' . ($selectedOne == "status" ? 'class="active"' : '') . '><a href="status.php">Statut</a></li>
					  <li ' . ($selectedOne == "log" ? 'class="active"' : '') . '><a href="log.php">Log</a></li>
					</ul>				
					</li>


				';			
			  echo'
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li><a href="../main/">Synthésia © Vizion&apos;R</a></li>
			  </ul>
			</div>
		  </div>
		</nav>


	';

}
?>
