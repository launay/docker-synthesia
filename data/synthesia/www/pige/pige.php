<?php
require_once "php/info.php";

session_start();
//session_unset();

//print_r($_SESSION);

//print_r($_POST);


$error = false;
$message = "";

$oldFrom = -1;
$oldTo = -1;
if(isset($_POST["fromTime"])){
	$oldFrom = $_POST["fromTime"];
}
if(isset($_POST["toTime"])){
	$oldTo = $_POST["toTime"];
}
if(isset($_POST["fromTime2"])){
	$oldFrom = $_POST["fromTime2"];
}
if(isset($_POST["toTime2"])){
	$oldTo = $_POST["toTime2"];
}

$sessionId = "session_" . uniqid();
if(isset($_SESSION["id"])){
	$sessionId = $_SESSION["id"];
}else{
	$_SESSION["id"] = $sessionId;
}

cleanupExtracts($sessionId);


//echo "\n";
//echo $sessionId;

$mp4File = "";
$mp4FileRelative = "";
$extract = isset($_POST["extract"]) && $_POST["extract"] == 1;
if($extract){
	list($error, $message, $captureLink, $captureLinkRelative) = getCapture($oldFrom, $oldTo, $sessionId);
//echo "LINK: $captureLink  <--\n";
	if(!$error){
		$mp4File = $captureLink;
		$mp4FileRelative = $captureLinkRelative;
	}
	
}

$save = isset($_POST["save"]) && $_POST["save"] == 1;
$saveName="";
if($save){
	$saveName = $_POST["saveName"];
	$sessionId = $_POST["sessionId"];
	$mp4Link = $_POST["mp4File"];
	$mp4LinkRelative = $_POST["mp4FileRelative"];
	list($error, $message, $saveLink, $saveLinkRelative) = save($oldFrom, $oldTo, $sessionId, $mp4Link, $saveName);
	
	$mp4File = $mp4Link;
	$mp4FileRelative = $mp4LinkRelative;
	
	
}

//echo "\n" . $mp4File;
?>
<html>
	<head>
		<?php createHeader();?>

  <style>
  #sortable { cursor: pointer; list-style-type: none; margin: 0; padding: 0; width: 60%; }
  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
  #sortable li span { position: absolute; margin-left: -1.3em; }

  </style>


	</head>

	<body>

		<?php createNavBar("pige",""); ?>


	<!-- ERROR PANEL -->
	<?php
	if(strlen($message)>0){
		$class="label label-default";
		if($error)
			$class="label label-danger";
		echo '<div id="errorMsg" class="' . $class . '" style="display:block">' . $message .'</div></br>';
	}
	?>

	<!-- WAIT PANEL-->
	<center>
		<div id="pending" style="display:none;">
			<img id="pendingImage" src="images/wait.gif"/>
			<div id="pendingText">...Extraction de la vidéo en cours...</div>
		</div>	
	</center>


<div class="panel panel-default">

 <div class="panel-heading">
	  <h3 class="panel-title">Extraction de séquences</h3>
	</div>
	
	<div class="panel-body">
		<div class="container">
						
			
			<h4><b>Choisissez votre période d'extraction</b></h4><br/>
			<div>L'extraction est possible entre le <b><span id="captureFrom">(inconnu)</span></b> et le <b><span id="captureTo">(inconnu)</span></b></div><br/>

			<!-- CAPTURE START/END SELECTION -->
			<form method="POST" name="theForm" onSubmit="pleaseWait();" id="theForm">
				<table>
					<tr>
						<td style="font-size:80%">Début&nbsp;:&nbsp;</td>
						<td><select onchange="commitTimeChanges();applyChangesPending()" name="fromDay" id="fromDay"></select>/<!--/td-->
						<!--td--><select onchange="commitTimeChanges();applyChangesPending()" name="fromMonth" id="fromMonth"></select>/<!--/td-->
						<!--td--><select onchange="commitTimeChanges();applyChangesPending()" name="fromYear" id="fromYear"></select>-<!--/td-->
						<!--td--><select onchange="commitTimeChanges();applyChangesPending()" name="fromHour" id="fromHour"></select>:<!--/td-->
						<!--td--><select onchange="commitTimeChanges();applyChangesPending()" name="fromMinute" id="fromMinute"></select>:<!--/td-->
						<!--td--><select onchange="commitTimeChanges();applyChangesPending()" name="fromSecond" id="fromSecond"></select></td>
						<td><div style="width:30px;"></div></td>
						<td>
							<button type="button" onClick="addSeconds(true, -30)">-30s</button>
							<button type="button" onClick="addSeconds(true, 30)">+30s</button>
						</td>
						<td>
							<button type="button" onClick="addSeconds(true, -60)">-1min</button>
							<button type="button" onClick="addSeconds(true, 60)">+1min</button>
						</td>
						<td>
							<button type="button" onClick="setEndAsStart()">=fin</button>
						</td>
					</tr>
					<tr>
						<td style="font-size:80%">Fin&nbsp;:&nbsp;</td>
						<td><select onchange="commitTimeChanges();applyChangesPending()" name="toDay" id="toDay"></select>/<!--/td-->
						<!--/td--><select onchange="commitTimeChanges();applyChangesPending()" name="toMonth" id="toMonth"></select>/<!--/td-->
						<!--/td--><select onchange="commitTimeChanges();applyChangesPending()" name="toYear" id="toYear"></select>-<!--/td-->
						<!--/td--><select onchange="commitTimeChanges();applyChangesPending()" name="toHour" id="toHour"></select>:<!--/td-->
						<!--/td--><select onchange="commitTimeChanges();applyChangesPending()" name="toMinute" id="toMinute"></select>:<!--/td-->
						<!--/td--><select onchange="commitTimeChanges();applyChangesPending()" name="toSecond" id="toSecond"></select></td>
						<td><div style="width:30px;"></div></td>
						<td>
							<button type="button" onClick="addSeconds(false, -30)">-30s</button>
							<button type="button" onClick="addSeconds(false, 30)">+30s</button>
						</td>
						<td>
							<button type="button" onClick="addSeconds(false, -60)">-1min</button>
							<button type="button" onClick="addSeconds(false, 60)">+1min</button>
						</td>
						<td>
							<button type="button" onClick="setStartAsEnd()">=debut</button>
						</td>
					</tr>
				</table>
				<br/>
				<button type="submit" name="sub" id="sub" value="capture" onclick="beforeSubmit()">Extraire la vidéo</button>
				<button type="button" onClick="refreshTimes(true)">Rafraîchir la période d'extraction</button>
				<input type="hidden" id="fromTime" name="fromTime"></input>
				<input type="hidden" id="toTime" name="toTime"></input>
				<input type="hidden" id="extract" name="extract" value="1"></input>
				<input type="hidden" id="sessionId" name="sessionId" value="<?php echo $sessionId;?>"></input>
			</form>
		</div>
	</div>

 </div>


<div id="playerPanel" class="panel panel-default">

	<div class="panel-heading">
	  <h3 class="panel-title">Ajustement de la séquence extraite</h3>
	</div>
	
	<div class="panel-body">
		<div class="container">

			<span>Vous pouvez choisir de nouveaux instants de début/fin de votre séquence, puis cliquer sur <b>Retailler la vidéo</b>.</span>

			<!-- VIDEO PLAYER-->
			<div id="playerDiv">
				<form method="POST" name="theForm2" onsubmit="return checkName();"  id="theForm2">
					<table id="playerTable">
						<tr>
							<td>
								<table>
									<tr>
										<td colspan="3">
											<video align="left" id="videoObject" width="480" controls="controls">
											<source src="<?php echo $mp4FileRelative . "?rand=" . rand();?>" type="video/mp4" />
											</video>
										</td>
									</tr>
									<tr>
										<td>
											<button id="getPlayerStartButton" type="button" onClick="extractPlayerTs(true)">Nouvelle date de début</button>
											<button id="getPlayerEndButton" type="button" onClick="extractPlayerTs(false)">Nouvelle date de fin</button>
											<button type="button" onClick="beforeSubmit(); $('#theForm').submit();">Retailler la vidéo</button>
										</td>
									</tr>
								</table>
							</td>
							<td>			
								<input type="text" placeholder="Saisissez le nom à donner à l'extrait" id="saveName" name="saveName" value="<?php echo $saveName;?>"></input>
								<button type="submit" name="sub" id="sub" value="capture" onclick="beforeSubmit2()">Sauvegarder l'extrait</button>
							</td>
						</tr>
					</table>
					<input type="hidden" id="fromTime2" name="fromTime2"></input>
					<input type="hidden" id="toTime2" name="toTime2"></input>
					<input type="hidden" id="save" name="save" value="1"></input>
					<input type="hidden" id="sessionId2" name="sessionId2" value="<?php echo $sessionId;?>"></input>
					<input type="hidden" id="mp4File" name="mp4File" value="<?php echo $mp4File;?>"></input>
					<input type="hidden" id="mp4FileRelative" name="mp4FileRelative" value="<?php echo $mp4FileRelative;?>"></input>			
				</form>
			</div>
			
		</div>
	</div>

<script>
	
	<?php if(!$mp4File) echo "$('#playerPanel').hide();\n";?>
	
	oldFrom = <?php echo $oldFrom?>;
	
	function checkName(){
		var saveName = $("#saveName").val();
		if(saveName.trim().length == 0){
			alert("Vous devez saisir un nom à donner à l'extrait");
			return false;
		}
		return true;
	}
	
	function extractPlayerTs(isStart){

		var vidObject = document.getElementById("videoObject");
		var timestamp = Math.round(vidObject.currentTime);
		
		var newTs = oldFrom + timestamp*1000;

		setStartEndTime(isStart, newTs);

	}
	
	function beforeSubmit(){
		var from = getTime(true);
		var to = getTime(false);
		$("#fromTime").val(from);	
		$("#toTime").val(to);	
	}
	
	function beforeSubmit2(){
		var from = getTime(true);
		var to = getTime(false);
		$("#fromTime2").val(from);	
		$("#toTime2").val(to);	
	}

	function refreshTimes(refreshSelects){
		var statusFile = "status/status.json";
		$.getJSON( statusFile + "?rand=" + Math.random(), function( data ) {


			var now = new Date();
			var from = now.getTime();
			var to = from;
			
			var segmentsFrom = getJSONValueOrDefault(data, "segments_from", from);
			var segmentsTo = getJSONValueOrDefault(data, "segments_to", to);

			if(refreshSelects){
				setStartEndTime(true, segmentsFrom);
				setStartEndTime(false, segmentsTo);
			}			
			$("#captureFrom").html(getPrettyDate(segmentsFrom));
			$("#captureTo").html(getPrettyDate(segmentsTo));
		 
		});
	}

	function pleaseWait(){

		var pending = $('#pending');
		pending.show();
		
		$("#sub").attr('disabled','disabled');		
	}
	
	
	function fillSelect(sel, from, to){
		for (i = from; i <= to; i++) {
			sel.append($("<option>").attr('value',i).text(i<10 ? "0"+i : i));

		}
	}
	
	function setStartAsEnd(){
			setStartEndTime(false, getTime(true));
	}
	function setEndAsStart(){
			setStartEndTime(true, getTime(false));
	}

	function addSeconds(isStart, seconds){
		setStartEndTime(isStart, getTime(isStart) + seconds * 1000);
	}
	
	function getPrettyDate(inDate){

		var dateFrom = new Date();
		dateFrom.setTime(inDate);
		var dayFrom = dateFrom.getDate();
		var monthIndexFrom = dateFrom.getMonth() + 1;
		var yearFrom = dateFrom.getFullYear();
		var hourFrom = dateFrom.getHours();
		var minFrom = dateFrom.getMinutes();
		var secFrom = dateFrom.getSeconds();
		
		if (dayFrom < 10)dayFrom = '0' + dayFrom;
		if (monthIndexFrom < 10)monthIndexFrom = '0' + monthIndexFrom;
		if (hourFrom < 10)hourFrom = '0' + hourFrom;
		if (minFrom < 10)minFrom = '0' + minFrom;
		if (secFrom < 10)secFrom = '0' + secFrom;

		var result = dayFrom + "/" + monthIndexFrom + "/" + yearFrom + " " + hourFrom + ":"+minFrom+":"+secFrom;
		return result;

	}
	
	function setStartEndTime(isStart, timestamp){

		var date = new Date();
		date.setTime(timestamp);
		
		var month = date.getMonth() + 1;
		var day = date.getDate();
		var year = date.getFullYear();
		var hour = date.getHours();
		var minute = date.getMinutes();
		var second = date.getSeconds();

		var pfx = isStart ? "from" : "to";
		
		$('#' + pfx + 'Second option[value="' + second + '"]').prop('selected', true);
		$('#' + pfx + 'Minute option[value="' + minute + '"]').prop('selected', true);
		$('#' + pfx + 'Hour option[value="' + hour + '"]').prop('selected', true);
		$('#' + pfx + 'Day option[value="' + day + '"]').prop('selected', true);
		$('#' + pfx + 'Month option[value="' + month + '"]').prop('selected', true);
		$('#' + pfx + 'Year option[value="' + year + '"]').prop('selected', true);
		
		
		applyChangesPending();

	}
	
	function getTime(isStart){


		var pfx = isStart ? "from" : "to";

		var fromSecond = $("#" + pfx + "Second").val();
		var fromMinute = $("#" + pfx + "Minute").val();
		var fromHour = $("#" + pfx + "Hour").val();
		var fromDay = $("#" + pfx + "Day").val();
		var fromMonth = $("#" + pfx + "Month").val();
		var fromYear = $("#" + pfx + "Year").val();

		var fromDate = new Date();
		fromDate.setMilliseconds(0);		
		fromDate.setMonth(fromMonth-1);
		fromDate.setDate(fromDay);
		fromDate.setFullYear(fromYear);		
		fromDate.setHours(fromHour);		
		fromDate.setMinutes(fromMinute);		
		fromDate.setSeconds(fromSecond);		

		var fromTime = fromDate.getTime();
		return fromTime;

	}


	function applyChangesPending(){

		$("#sub").css("color", "red");
		$("#sub").css("font-weight", "bold");

	}

	function getJSONValueOrDefault(jsonObject, key, def){
		var value = jsonObject[key];
		if(!value) return def;
		return value;
		
	}
	

	var fromDay = $('#fromDay');
	var toDay = $('#toDay');
	var fromMonth = $('#fromMonth');
	var toMonth = $('#toMonth');
	var fromYear = $('#fromYear');
	var toYear = $('#toYear');
	var fromHour = $('#fromHour');
	var toHour = $('#toHour');
	var fromMinute = $('#fromMinute');
	var toMinute = $('#toMinute');
	var fromSecond = $('#fromSecond');
	var toSecond = $('#toSecond');
	fillSelect(fromDay, 1, 31);
	fillSelect(toDay, 1, 31);
	fillSelect(fromMonth, 1, 12);
	fillSelect(toMonth, 1, 12);
	fillSelect(fromYear, 2016, 2025);
	fillSelect(toYear, 2016, 2025);
	fillSelect(fromHour, 0, 23);
	fillSelect(toHour, 0, 23);
	fillSelect(fromMinute, 0, 59);
	fillSelect(toMinute, 0, 59);
	fillSelect(fromSecond, 0, 59);
	fillSelect(toSecond, 0, 59);
	
	<?php
		if($oldFrom>0 && $oldTo>0){
			echo "setStartEndTime(true, $oldFrom);\n";
			echo "setStartEndTime(false, $oldTo);\n";
			echo "refreshTimes(false);\n";
		}
		else {
			echo "refreshTimes(true);\n";
		}
	
	?>

		setTimeout(function(){
			$("#errorMsg").hide(); 
		}, 5000);

</script>

</body>

</html>

