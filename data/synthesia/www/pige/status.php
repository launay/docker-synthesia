<?php
require_once "php/info.php";

$numInputs = 1;

?>

<html>
	<head>
		<?php createHeader();?>

<script>
		
function getJSONValueOrDefault(jsonObject, key, def){
	var value = jsonObject[key];
	if(!value) return def;
	return value;
	
}

lm = 0;

function getPrettyDate(inDate){

	var dateFrom = new Date();
	dateFrom.setTime(inDate);
	var dayFrom = dateFrom.getDate();
	var monthIndexFrom = dateFrom.getMonth();
	var yearFrom = dateFrom.getFullYear();
	var hourFrom = dateFrom.getHours();
	var minFrom = dateFrom.getMinutes();
	var secFrom = dateFrom.getSeconds();
	
	if (dayFrom < 10)dayFrom = '0' + dayFrom;
	if (monthIndexFrom < 10)monthIndexFrom = '0' + monthIndexFrom;
	if (hourFrom < 10)hourFrom = '0' + hourFrom;
	if (minFrom < 10)minFrom = '0' + minFrom;
	if (secFrom < 10)secFrom = '0' + secFrom;

	var result = dayFrom + "/" + monthIndexFrom + "/" + yearFrom + " " + hourFrom + ":"+minFrom+":"+secFrom;
	return result;	  

}

function refreshStatus(){
		var statusFile = "status/status.json";
		$.getJSON( statusFile + "?rand=" + Math.random(), function( data ) {


			var engineRunning = true;
			var lastModified = getJSONValueOrDefault(data, "lastPublish", 0);
			var engineUptime = getJSONValueOrDefault(data, "uptime", "00:00:00.000");

			if(lastModified == lm){
				engineRunning = false;
			}
			

			var oldLm = lm;

			lm = lastModified;
			
			//first turn
			if(oldLm == 0) {
				return;
			}


			$("#engineRunning").removeClass("ok");
			$("#engineRunning").removeClass("nok");
			$("#engineRunning").addClass( engineRunning  ? "ok" : "nok");
			$("#engineRunning").html( engineRunning ? "Démarré" : "Stoppé");
			$("#engineUptime").html(engineRunning ? engineUptime : "");

			var numInputs = <?php echo $numInputs?>;
		
			var serverURLArray = new Array();
			var serverRunningArray = new Array();
			var serverBRArray = new Array();
			var serverUptimeArray = new Array();
			
			for (i = 0; i < numInputs; i++) {
				var sfx = numInputs < 2 ? "" : i;
				serverRunningArray[i] = getJSONValueOrDefault(data, "VZR_RTMP_SERVER_RUNNING"+sfx, false);
				serverBRArray[i] = Math.max(0, getJSONValueOrDefault(data, "VZR_RTMP_SERVER_BR_IN"+sfx, 0));
				serverUptimeArray[i] = getJSONValueOrDefault(data, "VZR_RTMP_SERVER_UPTIME"+sfx, "00:00:00.000");
				serverURLArray[i] = getJSONValueOrDefault(data, "VZR_RTMP_SERVER_URL"+sfx, "");
				
				$("#server"+i+"running").removeClass("ok");
				$("#server"+i+"running").removeClass("nok");
				$("#server"+i+"running").addClass( serverRunningArray[i] == "true" ? "ok" : "nok");
				$("#server"+i+"running").html(engineRunning ? (serverRunningArray[i] == "true" ? "connecté" : "non connecté") : "");
				$("#server"+i+"BR").html(engineRunning ? (serverBRArray[i] / 1000) + " kbps" : "");
				$("#server"+i+"uptime").html(engineRunning ?serverUptimeArray[i] : "");
				$("#server"+i+"url").html(engineRunning ? serverURLArray[i] : "");

			} 
		 
		  var dumpSize = getJSONValueOrDefault(data, "dump_size", 0);
		  var segmentsFrom = getJSONValueOrDefault(data, "segments_from", 0);
		  var segmentsTo = getJSONValueOrDefault(data, "segments_to", 0);
		  
    $("#dumpSize").html(engineRunning ?  Math.round(dumpSize/(1024*1024)) + " Mo" : "");
		  $("#segmentsFrom").html(engineRunning && dumpSize > 0 ?  getPrettyDate(segmentsFrom) : "");
		  $("#segmentsTo").html(engineRunning && dumpSize > 0 ?  "" + getPrettyDate(segmentsTo) : "");
		 
		});
   }
var timer=setInterval("refreshStatus()", 5000);
refreshStatus();
setTimeout("refreshStatus()", 3000);

</script>

	</head>

	<body>

		<?php createNavBar("status",""); ?>

<div class="panel panel-default">

<div class="panel-heading">
	  <h3 class="panel-title">Statut de la pige Synthésia</h3>
	</div>

<style>
 .bordered_table td{
	border: 1px solid black;
 }
 .ok{
	color: green;
	font-weight: bold;
 }
 .nok{
	color: red;
	font-weight: bold;
 }
</style>
<div class="panel-body">
		<div class="container">
			
<table width="100%">
	
		<tr class="bordered_table">
					<td><b>Statut de la pige</b></td>
					<td><span id="engineRunning"></span></td>
				</tr>
				<tr class="bordered_table">
					<td><b>Uptime</b></td>
					<td><span id="engineUptime"></span></td>
				</tr>
				<tr><td></td></tr>
	
<?php 
for ($i = 0; $i < $numInputs; $i++) {
	echo '
				<tr class="bordered_table">
					<td><b>Connexion Entrée ' . ($i+1) . '</b></td>
					<td><span id="server' . $i .'running"></span></td>
				</tr>
				<tr class="bordered_table">

					<td><b>URL</b></td>
					<td><span id="server' . $i .'url"></span></td>
				</tr>
				<tr class="bordered_table">

					<td><b>Débit</b></td>
					<td><span id="server' . $i .'BR"></span></td>
				</tr>
				<tr class="bordered_table">

					<td><b>Uptime</b></td>
					<td><span id="server' . $i .'uptime"></span></td>
				</tr>
				<tr><td></td></tr>
	';
}?>
				
				<tr class="bordered_table">
					<td><b>Taille de la pige</b></td>
					<td><span id="dumpSize"></span></td>
				</tr>
				<tr class="bordered_table">
					<td><b>Limite basse de pige</b></td>
					<td><span id="segmentsFrom"></span></td>
				</tr>
				<tr><br/></tr>
				<tr class="bordered_table">
					<td><b>Limite haute de pige</b></td>
					<td><span id="segmentsTo"></span></td>
				</tr>
				<tr><td></td></tr>

</table>
		</div>
	</div>

            
  </div>

</body>


</html>
