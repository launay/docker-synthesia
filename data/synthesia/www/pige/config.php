<?php
require_once "php/info.php";

$message = "";
$error = false;

         $cmdType = "";
         if(isset($_POST['startPige'])){
                 $cmdType = "start";
         }
         if(isset($_POST['stopPige'])){
                 $cmdType = "stop";
         }

         if($cmdType){
                 $command = 'sudo -u synthesia /home/synthesia/synthesia/common/pige/'  . $cmdType . 'Pige.sh';
                 exec($command, $output, $result);

                 if($result == 0){
                         if($cmdType == "start") $message = "La pige a été (re)démarrée avec succès";
                         if($cmdType == "stop") $message = "La pige a été stoppée avec succès";
																				$error = false;
                 }
                 else{
                         if($cmdType == "start") $message = "La pige n'a pas été (re)démarrée correctement.";
                         if($cmdType == "stop") $message = "La pige n'a pas été stoppée correctement.";
																				$error = true;
                 }

         }

	$sampling = 48000;
	$fps = 25;
	
	$port=8888;
	$app="live";
	$path="pige";
	$dumpDuration=1;


	$paramsFile = "conf/setup.json";
	if(!file_exists($paramsFile)){
		$error = true;
		$message = "Impossible de trouver le fichier de paramètres de la pige";
	}
	else{
		$paramsData = file_get_contents($paramsFile);
		if(!$paramsData){
			$error = true;
			$message = "Impossible de lire les paramètres de la pige";
		}
		else{
			$paramsJson = json_decode($paramsData,true);
			if(!$paramsJson){
				$error = true;
				$message = "Impossible de décoder les paramètres de la pige";
			}
			else{
				$sampling = $paramsJson['setup']['audioSamplingRate'];
				$fps = $paramsJson['setup']['fps'];

				$port = $paramsJson['setup']['rtmpPort'];
				$app = $paramsJson['setup']['rtmpApp'];
				$path = $paramsJson['setup']['rtmpPath'];
				$dumpDuration = $paramsJson['setup']['dumpExpirationHours'];

				
				if(isset($_POST['updateParameters'])){
							

					if(isset($_POST['sampling'])){
						$sampling = intval($_POST['sampling']);
						$paramsJson['setup']['audioSamplingRate'] = $sampling;
					}
					if(isset($_POST['fps'])){
						$fps = intval($_POST['fps']);
						$paramsJson['setup']['fps'] = $fps;
					}
					
					if(isset($_POST['port'])){
						$port = intval($_POST['port']);
						$paramsJson['setup']['rtmpPort'] = $port;
					}
					if(isset($_POST['app'])){
						$app = ($_POST['app']);
						$paramsJson['setup']['rtmpApp'] = $app;
					}
					if(isset($_POST['path'])){
						$path = ($_POST['path']);
						$paramsJson['setup']['rtmpPath'] = $path;
					}
					if(isset($_POST['dumpDuration'])){
						$dumpDuration = intval($_POST['dumpDuration']);
						$paramsJson['setup']['dumpExpirationHours'] = $dumpDuration;
					}
					
					
					//writes config back
					$newJson = json_encode($paramsJson);
					$res = file_put_contents($paramsFile, $newJson);
					if(!$res){
						$error = true;
						$message = "Impossible de mettre à jour les paramètres de la pige";
					}else{
						$error = false;
						$message = "Les paramètres de la pige ont été mis à jour";
					}
					
				
				 }
			}
		}

	}
	
	
	
?>



<html>
	<head>
		<?php createHeader();?>

	</head>

	<body>

		<?php createNavBar("config",""); ?>

<?php
if(strlen($message)>0){
	$class="label label-default";
	if($error)
		$class="label label-danger";
	echo '<div id="errorMsg" class="' . $class . '" style="display:block">' . $message .'</div></br>';
}
?>

<center>
	<div id="pendingON" style="display:none">
		<img class="pendingImage" src="images/wait.gif"/>
		<div class="pendingText">... Veuillez patienter pendant le (re)démarrage de la pige</div>
	</div>	
</center>
<center>
	<div id="pendingOFF" style="display:none">
		<img class="pendingImage" src="images/wait.gif"/>
		<div class="pendingText">... Veuillez patienter pendant l'arrêt de la pige</div>
	</div>	
</center>

<div class="panel panel-default">

	<div class="panel-heading">
	  <h3 class="panel-title">Commandes de la pige Synthésia</h3>
	</div>

	<div class="panel-body">
		<div class="container">
			<table><tr><td>
			 <form method="POST" onsubmit="if (!confirm('Etes-vous sûr de vouloir (re)démarrer la pige ?')) return false; displayPendingON();">
				 <input type="hidden" name="startPige" value="1"></input>
				 <input type="submit" value="(Re)démarrer la pige"></input>
			 </form></td><td>
			 <form method="POST" onsubmit="if (!confirm('Etes-vous sûr de vouloir arrêter la pige ?')) return false; displayPendingOFF();">
				 <input type="hidden" name="stopPige" value="1"></input>
				 <input type="submit" value="Arrêter la pige"></input>
			 </form>
		</td></tr></table>
		</div>
	</div>
</div>
<div class="panel panel-default">

	<div class="panel-heading">
	  <h3 class="panel-title">Paramètres de la pige</h3>
	</div>

	<div class="panel-body">
		<div class="container">
			 <form method="POST" onsubmit="return confirm('La modification des paramètres entraîne le redémarrage de la pige.\nEtes-vous sûr de vouloir sauvegarder les paramètres ?');">
				<input type="hidden" name="updateParameters" value="1"></input>
				<table>
					<tr>
						<td>Échantillonage audio</td>
						<td>
							<select name="sampling" id="sampling">
								<option value="44100" <?php if($sampling == 44100) echo "selected";?>>44.1 KHz</option>
								<option value="48000" <?php if($sampling == 48000) echo "selected";?>>48 KHz</option>
  							</select>
						</td>
					</tr>
					<tr>
						<td>Images par seconde</td>
						<td>
							<select name="fps" id="fps">
								<option value="25" <?php if($fps == 25) echo "selected";?>>25</option>
								<option value="30" <?php if($fps == 30) echo "selected";?>>30</option>
  							</select>
						</td>
					</tr>
					<tr>
						<td>Durée de la pige (heures)</td>
						<td>
							<select name="dumpDuration" id="dumpDuration">
								<option value="1" <?php if($dumpDuration == 1) echo "selected";?>>1 heure</option>
								<option value="2" <?php if($dumpDuration == 2) echo "selected";?>>2 heures</option>
								<option value="3" <?php if($dumpDuration == 3) echo "selected";?>>3 heures</option>
								<option value="4" <?php if($dumpDuration == 4) echo "selected";?>>4 heures</option>
								<option value="5" <?php if($dumpDuration == 5) echo "selected";?>>5 heures</option>
								<option value="6" <?php if($dumpDuration == 6) echo "selected";?>>6 heures</option>
  							</select>
						</td>
					</tr>
					<tr>
						<td>Entrée RTMP</td>
						<td>rtmp://localhost:
							<input type="text" name="port" size="4" value="<?php echo htmlspecialchars($port) ;?>"></input>/
							<input type="text" name="app" size="12" value="<?php echo htmlspecialchars($app) ;?>"></input>/
							<input type="text" name="path" size="12" value="<?php echo htmlspecialchars($path) ;?>"></input>
						</td>
					</tr>
					<tr>
						<td><input type="submit" value="Sauvegarder les paramètres"></input></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>

<script>
		function displayPendingON(){
			$("#pendingON").show();
		}
		function displayPendingOFF(){
			$("#pendingOFF").show();
		}
		
		setTimeout(function(){
			$("#errorMsg").hide(); 
		}, 5000);
</script>

</body>


</html>
