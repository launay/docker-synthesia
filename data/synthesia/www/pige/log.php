<?php
require_once "php/info.php";
$logs= file_get_contents("log/synthesia.log.0");
if(!$logs) $logs= "Aucun journal d'événements trouvé.";

?>



<html>
	<head>
		<?php createHeader();?>

	</head>

	<body>

		<?php createNavBar("log",""); ?>


<div class="panel panel-default">

<div class="panel-heading">
	  <h3 class="panel-title">Journaux d'événements Synthésia</h3>
	</div>

<div class="panel-body">
<div class="container">

         <form method="POST">
             <input type="hidden" name="refresh" value="1"></input>
             <input type="submit" value="Rafraîchir"></input>
         </form>
</div>


<div class="container">

       <textarea id="logsArea" style="width:100%;height:50%;resize: both;overflow:auto;"><?php echo $logs;?></textarea>
</div>


</div>
</div>

<script>
	var textarea = document.getElementById('logsArea');
	textarea.scrollTop = textarea.scrollHeight;
</script>
</body>


</html>
