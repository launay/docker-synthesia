<?php

         $cmdType = "";
         if(isset($_POST['startSynthesia'])){
                 $cmdType = "start";
         }
         if(isset($_POST['stopSynthesia'])){
                 $cmdType = "stop";
         }

         $controlResult = "";
         if($cmdType){
                 $command = 'sudo -u synthesia /home/synthesia/synthesia/common/' . $cmdType . 'Synthesia.sh';
                 exec($command, $output, $result);

                 if($result == 0){
                         if($cmdType == "start") $controlResult = "Synthésia a été (re)démarré avec succès";
                         if($cmdType == "stop") $controlResult = "Synthésia a été stoppé avec succès";
                 }
                 else{
                         if($cmdType == "start") $controlResult = "Synthésia n'a pas été (re)démarré correctement.";
                         if($cmdType == "stop") $controlResult = "Synthésia n'a pas été stoppé correctement.";
                 }

                 echo "<script>alert('". $controlResult ."')</script>";
         }
         ?>
         <?php


         //configuration
         $station = "Synthesia";
         $title = "Synthésia - Statut";
         $statusFile = "status.json";
         $numOutputs = 0;
         $numInputs = 1;


         $allStopped = false;
         $statusLastModif = filemtime($statusFile);
         if(time() - $statusLastModif > 10){
             $allStopped = true;
         }

         //gets status
         $statusData = json_decode(file_get_contents($statusFile));

         $status = $statusData->status;

         //print_r($status);


         //print_r($status);

         $data = array();

         $data['COMPOSITOR']['running'] = $allStopped ? false : translateBoolean($status->LIVE_COMPOSITOR_RUNNING);
         $data['COMPOSITOR']['uptime'] = $allStopped ? "" : $status->LIVE_COMPOSITOR_UPTIME;
         $data['COMPOSITOR']['last_start'] = $allStopped ? "" : translateDate($status->LIVE_COMPOSITOR_LAST_START);
         $data['COMPOSITOR']['last_stop'] = $allStopped ? "" : translateDate($status->LIVE_COMPOSITOR_LAST_STOP);

         $data['PROCESSOR']['running'] = $allStopped ? false : translateBoolean($status->VIDEO_PROCESSOR_RUNNING);
         $data['PROCESSOR']['uptime'] = $allStopped ? "" : $status->VIDEO_PROCESSOR_UPTIME;
         $data['PROCESSOR']['last_start'] = $allStopped ? "" : translateDate($status->VIDEO_PROCESSOR_LAST_START);
         $data['PROCESSOR']['last_stop'] = $allStopped ? "" : translateDate($status->VIDEO_PROCESSOR_LAST_STOP);
         //$data['PROCESSOR']['buffer'] = $allStopped ? "" : $status->VIDEO_PROCESSOR_BUFFER_MS;

         for($index=0 ; $index < $numOutputs ; $index++){
             $sfx = $numOutputs<2 ? "" : $index;
             $data['RTMP'][$index]['running'] = $allStopped ? false : translateBoolean($status->{VZR_RTMP_RUNNING . $sfx});
             $data['RTMP'][$index]['uptime'] = $allStopped ? "" : $status->{VZR_RTMP_UPTIME . $sfx};
             $data['RTMP'][$index]['last_start'] = $allStopped ? "" : translateDates($status->{VZR_RTMP_LAST_START . $sfx});
             $data['RTMP'][$index]['last_stop'] = $allStopped ? "" : translateDates($status->{VZR_RTMP_LAST_STOP . $sfx});
             $data['RTMP'][$index]['url'] = $allStopped ? "" : $status->{VZR_RTMP_URL . $sfx};
             $data['RTMP'][$index]['status'] = $allStopped ? "" : $status->{VZR_RTMP_STATUS_STATE . $sfx};
             $data['RTMP'][$index]['bitrate'] = $allStopped ? "" : $status->{VZR_RTMP_BR_OUT . $sfx}/1000;
         }

         for($index=0 ; $index < $numInputs ; $index++){
             $sfx = $numInputs<2 ? "" : $index;
             $data['RTMP_SERVER'][$index]['running'] = $allStopped ? false : translateBoolean($status->{VZR_RTMP_SERVER_RUNNING . $sfx});
             $data['RTMP_SERVER'][$index]['uptime'] = $allStopped ? "" : $status->{VZR_RTMP_SERVER_UPTIME . $sfx};
             $data['RTMP_SERVER'][$index]['last_start'] = $allStopped ? "" : translateDates($status->{VZR_RTMP_SERVER_LAST_START . $sfx});
             $data['RTMP_SERVER'][$index]['last_stop'] = $allStopped ? "" : translateDates($status->{VZR_RTMP_SERVER_LAST_STOP . $sfx});
             $data['RTMP_SERVER'][$index]['url'] = $allStopped ? "" : $status->{VZR_RTMP_SERVER_URL . $sfx};
             $data['RTMP_SERVER'][$index]['status'] = $allStopped ? "" : $status->{VZR_RTMP_SERVER_STATUS_STATE . $sfx};
             $data['RTMP_SERVER'][$index]['bitrate'] = $allStopped ? "" : $status->{VZR_RTMP_SERVER_BR_IN . $sfx}/1000;
         }

         //print_r($data);

         //misc functions
         function translateDate($date){
             
             if(!$date) return "";
             return date(DATE_ATOM, strtotime($date));

         }

         //misc functions
         function translateDates($dates){
             
             $datesArray = array();
             
             if($dates) {
                 $values = explode(", ", $dates);
                 foreach ($values as $val){
                     array_push($datesArray, translateDate($val));
                 }
             }
             return $datesArray;

         }

         function translateValues($values){
             
             $result = array();
             
             if($values) {
                 $values = explode(", ", $values);
                 foreach ($values as $val){
                     array_push($result, $val);
                 }
             }
             return $result;

         }


         function translateBoolean($bool){
             if("true" == $bool) return true;
             return false;

         }

         ?>

         <html>

         <head>
         <meta http-equiv="refresh" content="5" > 
         <meta charset="UTF-8">
         <title><?php echo $title;?></title>
         <style>

         body{
             font-family:arial;
         }

         table{


         }
         td{
             border: 1px solid black;
         }
         .tableItem{
             font-weight: bold;
         }
         .active{
             width:200px;
             background-color:green;
         }
         .inactive{
             width:200px;
             background-color:red;
         }

         </style>
         </head>
         <body>

         <h1><?php echo $title;?></h1>
         <div><i>Station : <?php echo $station;?></i></div>

         <h2>ENTRÉE(S) RTMP</h2>
         <?php foreach ($data['RTMP_SERVER'] as $RTMP){
             echo '
         <table>
             <tr>
                 <td class="tableItem">Actif</td>
                 <td class="' . ($RTMP['running'] ? "active" : "inactive") . '">&nbsp;</td>
             </tr>
             <tr>
                 <td class="tableItem">Uptime</td>
                 <td>' . $RTMP['uptime'] .'</td>
             </tr>
             <tr>
                 <td class="tableItem">Débit de sortie</td>
                 <td class="' . ($RTMP['bitrate'] ? "active" : "inactive") . '">' . $RTMP['bitrate'] .' kbps</td>
             </tr>
             <tr>
                 <td class="tableItem">URL</td>
                 <td>' . $RTMP['url'] .'</td>
             </tr>
             <tr>
                 <td class="tableItem">Statut RTMP</td>
                 <td>' . $RTMP['status'] .'</td>
             </tr>
             <tr>
                 <td class="tableItem">Dernier(s) démarrage(s)</td>
                 <td>' ; 
         foreach( $RTMP['last_start'] as $time) {echo $time; echo "<br/>";} ; 
         echo '</td>
             </tr>
             <tr>
                 <td class="tableItem">Dernier(s) arrêt(s)</td>
                 <td>' ;
         foreach( $RTMP['last_stop'] as $time) {echo $time; echo "<br/>";} ; 
         echo '</td>
             </tr>
         </table>
             ';
         }
         ?>

         <h2>PROCESSEUR VIDÉO</h2>
         <table>
             <tr>
                 <td class="tableItem">Actif</td>
                 <td class="<?php if($data['PROCESSOR']['running']) echo "active"; else echo "inactive";?>">&nbsp;</td>
             </tr>
             <tr>
                 <td class="tableItem">Uptime</td>
                 <td><?php echo $data['PROCESSOR']['uptime'];?></td>
             </tr>
             <tr>
                 <td class="tableItem">Dernier démarrage</td>
                 <td><?php echo $data['PROCESSOR']['last_start'];?></td>
             </tr>
             <tr>
                 <td class="tableItem">Dernier arrêt</td>
                 <td><?php echo $data['PROCESSOR']['last_stop'];?></td>
             </tr>

         </table>

         <h2>COMPOSITEUR</h2>
         <table>
             <tr>
                 <td class="tableItem">Actif</td>
                 <td class="<?php if($data['COMPOSITOR']['running']) echo "active"; else echo "inactive";?>">&nbsp;</td>
             </tr>
             <tr>
                 <td class="tableItem">Uptime</td>
                 <td><?php echo $data['COMPOSITOR']['uptime'];?></td>
             </tr>
             <tr>
                 <td class="tableItem">Dernier démarrage</td>
                 <td><?php echo $data['COMPOSITOR']['last_start'];?></td>
             </tr>
             <tr>
                 <td class="tableItem">Dernier arrêt</td>
                 <td><?php echo $data['COMPOSITOR']['last_stop'];?></td>
             </tr>


         </table>

         <h2>SORTIE(S) RTMP</h2>
         <?php foreach ($data['RTMP'] as $RTMP){
             echo '
         <table>
             <tr>
                 <td class="tableItem">Actif</td>
                 <td class="' . ($RTMP['running'] ? "active" : "inactive") . '">&nbsp;</td>
             </tr>
             <tr>
                 <td class="tableItem">Uptime</td>
                 <td>' . $RTMP['uptime'] .'</td>
             </tr>
             <tr>
                 <td class="tableItem">Débit de sortie</td>
                 <td class="' . ($RTMP['bitrate'] ? "active" : "inactive") . '">' . $RTMP['bitrate'] .' kbps</td>
             </tr>
             <tr>
                 <td class="tableItem">URL</td>
                 <td>' . $RTMP['url'] .'</td>
             </tr>
             <tr>
                 <td class="tableItem">Statut RTMP</td>
                 <td>' . $RTMP['status'] .'</td>
             </tr>
             <tr>
                 <td class="tableItem">Dernier(s) démarrage(s)</td>
                 <td>' ; 
         foreach( $RTMP['last_start'] as $time) {echo $time; echo "<br/>";} ; 
         echo '</td>
             </tr>
             <tr>
                 <td class="tableItem">Dernier(s) arrêt(s)</td>
                 <td>' ;
         foreach( $RTMP['last_stop'] as $time) {echo $time; echo "<br/>";} ; 
         echo '</td>
             </tr>
         </table>
             ';
         }
         ?>
         <hr/>
         <br/>

         <form method="POST">
             <input type="hidden" name="startSynthesia" value="1"></input>
             <input type="submit" value="(Re)démarrer Synthésia"></input>
         </form>
         <form method="POST">
             <input type="hidden" name="stopSynthesia" value="1"></input>
             <input type="submit" value="Arrêter Synthésia"></input>
         </form>

         </body>
         </html>
