## Execute this file as 'synthesia' to assign right properties on scripts and exes
echo 'Creation des dossiers'
mkdir -p log
mkdir -p data
mkdir -p webservices
mkdir -p images
mkdir -p main
mkdir -p main/php
mkdir -p main/images
mkdir -p main/config
mkdir -p main/css
mkdir -p status
mkdir -p pige
mkdir -p pige/php
mkdir -p pige/log
mkdir -p pige/images
mkdir -p pige/status
mkdir -p pige/extracts
mkdir -p pige/extracts/save
mkdir -p pige/extracts/temp
mkdir -p pige/config
mkdir -p pige/segments
mkdir -p pige/include
mkdir -p pige/include/video-js
mkdir -p pige/conf
mkdir -p pige/js
mkdir -p pige/css
mkdir -p pige/bin
mkdir -p playlist
mkdir -p playlist/log
mkdir -p playlist/images
mkdir -p playlist/out
mkdir -p playlist/playlist
mkdir -p playlist/in
mkdir -p playlist/convert
mkdir -p playlist/master
mkdir -p playlist/manager
mkdir -p playlist/manager/bin
mkdir -p playlist/manager/php
mkdir -p playlist/manager/images
mkdir -p playlist/manager/config
mkdir -p playlist/manager/js
mkdir -p playlist/manager/css
mkdir -p playlist/manager/dm_import
mkdir -p playlist/manager/yt_import
mkdir -p config
mkdir -p hls
mkdir -p source
mkdir -p control
mkdir -p control/php
mkdir -p control/images
mkdir -p control/config
mkdir -p control/js
mkdir -p control/css
mkdir -p bin/
echo 'Attribution des droits aux repertoires et fichiers'
chmod a+rw status
chmod a+rw source
chmod a+rw source/source.txt
chmod -R a+rw status
chmod -R a+rw images
chmod a+rw status
chmod a+rw config
chmod a+rw config/startup.json
chmod a+rw log
chmod -R a+rw playlist/master
chmod -R a+rw playlist/in
chmod -R a+rw playlist/out
chmod -R a+rw playlist/convert
chmod -R a+rw playlist/images
chmod a+x playlist/manager/*.sh
chmod a+rw playlist/log
chmod a+rw playlist/playlist.properties
chmod a+rw playlist/normalizer.properties
chmod a+rw playlist/import.properties
chmod a+rw playlist/playlist
chmod a+rw playlist/playlist/current.m3u
chmod a+rw playlist/playlist/next.m3u
chmod a+rw playlist/manager/dm_import
chmod a+rw playlist/manager/yt_import
chmod a+rwx playlist/manager/bin/*
chmod a+rw webservices
chmod -R a+rw webservices/*
chmod -R a+rw pige/extracts/*
chmod a+rwx pige/bin/*
chmod a+rw pige/conf/*
echo 'Installation de mediainfo'
sudo apt-get install mediainfo
echo 'Installation de libav-tools'
sudo apt-get install libav-tools
