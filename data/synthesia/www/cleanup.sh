## cleanup files
rm hls/*
rm log/*
sudo rm images/*
sudo rm status/anim.txt
sudo rm status/anim.state.json
sudo rm webservices/_*
echo "{}" > status/status.json
rm pige/log/*
sudo rm pige/extracts/save/*
sudo rm pige/extracts/temp/*
rm pige/segments/*
echo "{}" > pige/status/status.json
rm playlist/log/*
sudo rm playlist/convert/*
mv playlist/images/synthesia.png /tmp/
sudo rm playlist/images/*
mv /tmp/synthesia.png playlist/images/
sudo rm playlist/in/*
sudo rm playlist/out/*
sudo rm playlist/master/*
sudo rm playlist/playlist/*
sudo rm playlist/manager/dm_import/*
sudo rm playlist/manager/yt_import/*
touch playlist/playlist/current.m3u
touch playlist/playlist/next.m3u
echo "{}" > status/playlist_status.json
