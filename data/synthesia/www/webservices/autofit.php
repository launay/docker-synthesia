<?php

define("AUTO_FIT_CACHE_FOLDER", "autofit/");
define("AUTO_FIT_CACHE_PFX", "_autofit_cache");
define("AUTO_FIT_CACHE_SFX", ".json");

define("FONT_ESTIMATOR_JAR", "../bin/FontEstimatorV2.jar");
define("FONTS_FOLDER", "/home/synthesia/synthesia/common/fonts");

define("AUTO_FIT_CACHE_DURATION_SECONDS", 3600);
define("CACHE_RELOADED", "reloaded_fit_cache");

define("AUTOFIT_LABEL", "AUTOFIT");

header('Content-Type: application/json; charset=utf-8');

/*
$text = "CANNES CONTRE NETFLIX, COMBAT D'ARRIÈRE-GARDE ?\n20\/05\/2017 10:47\n\nEditorial. Entre l'inflexibilité de la plate-forme de vidéo et le conservatisme de certains secteurs du cinéma français, il faut préserver à la fois la diversité de la création et les nouvelles pratiques culturelles.\n\nLe Monde.fr - Actualités et Infos en France et dans le monde";
$textAttributes = "attributes=font:0:47:itcfranklingothic-bold.ttf:22::foreground:0:47:#000000::font:48:64:itcfranklingothic-bold.ttf:18::foreground:48:64:#AAAAAA::font:66:282:itcfranklingothic-plain.ttf:22::foreground:66:282:#000000::font:284:344:itcfranklingothic-bold.ttf:18::foreground:284:344:#888888::";
$align = 2;
$dimension = "934,167";
$auto_adjust = true;
$auto_adjust_min = 10;
$auto_adjust_max = 40;
$default_font = "itcfranklingothic-plain.ttf";
$default_size = 8;

print_r(fits($text, $textAttributes, $align, $dimension, $auto_adjust, $auto_adjust_min, $auto_adjust_max, $default_font, $default_size));
*/

function fits($text, $textAttributes, $align, $dimension, $auto_adjust, $auto_adjust_min, $auto_adjust_max, $default_font, $default_size){

	clearCache();
	
	$fitData = array();
	
	if($text && $textAttributes){
		$inData = array(
			"text"=>$text,
			"text_attributes"=>$textAttributes,
			"align"=>$align,
			"dimension"=>$dimension,
			"auto_adjust"=>$auto_adjust,
			"auto_adjust_max"=>$auto_adjust_max,
			"auto_adjust_min"=>$auto_adjust_min,
			"default_font"=>$default_font . ":" . $default_size,
			"font_folder"=>FONTS_FOLDER,
		);
		$fitData[AUTOFIT_LABEL] = $inData;	
	}
	

	$autoFitMD5 = md5(json_encode($fitData));
	$cacheFile = getCacheFile($autoFitMD5);
	
	//if already stored, we're done
	if(file_exists($cacheFile)){
		$resultJson = file_get_contents($cacheFile);
		$result = json_decode($resultJson, true);
		$result[CACHE_RELOADED] = true;
		return $result;
	}
	
	//if not stored, will compute fit
	$result = textFits(array("data"=>$fitData));
	$result[CACHE_RELOADED] = false;
	if(defined("JSON_PRETTY_PRINT") && defined("JSON_UNESCAPED_UNICODE"))
		$resultPretty = json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
	else
		$resultPretty = $result;
	file_put_contents($cacheFile, $resultPretty);

	return $result;
}

/**
 * MISC methods
 * 
 */
 
function textFits($data){

	//print_r($data);

	putenv('LANG=en_US.UTF-8');
	setlocale(LC_CTYPE, "en_US.UTF-8");
	
	$jsonData = json_encode($data);
	$text = base64_encode(trim($jsonData));
	$commandLine = "java -jar " . FONT_ESTIMATOR_JAR . " " . $text . " 2>&1";
	passthru(exec ($commandLine, $output));
	
	//print_r($output);
	
	if(!$output) return null;
	$output = join($output);

	//print_r($output);
	$jsonOut = json_decode($output, true);
	return $jsonOut;

}

function getCacheFile($guid){
	return AUTO_FIT_CACHE_FOLDER . AUTO_FIT_CACHE_PFX . $guid . AUTO_FIT_CACHE_SFX;
}

function clearCache(){
        $allCacheFiles = scandir(AUTO_FIT_CACHE_FOLDER);
        foreach($allCacheFiles as $cacheFile){
			$file = AUTO_FIT_CACHE_FOLDER . $cacheFile;
			if(!startsWith($cacheFile, AUTO_FIT_CACHE_PFX))
				continue;
			if(is_dir($file))
				continue;
			$lm = filemtime($file);
			if(time() - $lm < AUTO_FIT_CACHE_DURATION_SECONDS) {
				 continue;
			}
			unlink($file);
        }
}

function startsWith($haystack, $needle)
{
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}

?>
