<?php

define("SOURCE_FILE", "../source/source.txt");
define("SET_HIDER_URL", "http://localhost/synthesia/common/webservices/setLiveHider.php?mode=");
define("DEFAULT_SOURCE", "0");


//gets old source
$oldSource = DEFAULT_SOURCE;
if(file_exists(SOURCE_FILE)){
	$oldSource = file_get_contents(SOURCE_FILE);
}

$source = DEFAULT_SOURCE;

if(isset($_GET["source"])){
	$source = trim($_GET["source"]);
}

if($source == $oldSource) return;

file_put_contents(SOURCE_FILE, $source);

//sets hider
$mode = $source == 0 ? 2 : 1;
$url = SET_HIDER_URL . $mode;
file_get_contents($url);

?>
