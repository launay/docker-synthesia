<?php

define("NEWS_ACTIVE_FILE", "_news_active.json");

$rssActive = true;
$twitterActive = false;
$facebookActive = false;

//first reads from input config
if(file_exists(NEWS_ACTIVE_FILE)){
	$activeDataJson = file_get_contents(NEWS_ACTIVE_FILE);
	if($activeDataJson){
		$activeData = json_decode($activeDataJson, true);
		if($activeData){
			$rssActive = $activeData["rssActive"];
			$twitterActive = $activeData["twitterActive"];
			$facebookActive = $activeData["facebookActive"];
		}
	}
}

if(isset($_GET["rssActive"])){	$rssActive = trim($_GET["rssActive"]);}
if(isset($_GET["twitterActive"])){	$twitterActive = trim($_GET["twitterActive"]);}
if(isset($_GET["facebookActive"])){	$facebookActive = trim($_GET["facebookActive"]);}

$result = array(
	"rssActive" => ($rssActive ? 1:0),
	"twitterActive" => ($twitterActive ? 1:0),
	"facebookActive" => ($facebookActive ? 1:0)
);

$resultJson = json_encode($result);
file_put_contents(NEWS_ACTIVE_FILE, $resultJson);

?>
