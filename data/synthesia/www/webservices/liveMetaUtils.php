<?php

define('LIVE_META_FILE',  dirname(__FILE__) . "/_live_meta.json");
define('LIVE_TITLE_DEFAULT', "I'M LIVE !");
define('LIVE_SUBTITLE_DEFAULT', "Live on Synthesia");
define('LIVE_DESCRIPTION_DEFAULT', "I'm broadcasting enhanced live with synthesia video composer.");

function getLiveMeta(){
	$liveTitle = LIVE_TITLE_DEFAULT;
	$liveSubtitle = LIVE_SUBTITLE_DEFAULT;
	$liveDescription = LIVE_DESCRIPTION_DEFAULT;
	
	if(file_exists(LIVE_META_FILE)){
		$liveJson = file_get_contents(LIVE_META_FILE);
		if($liveJson){
			$liveConfig = json_decode($liveJson, true);
			$liveTitle = $liveConfig["liveTitle"];
			$liveSubtitle = $liveConfig["liveSubtitle"];
			$liveDescription = $liveConfig["liveDescription"];
		}
	}
	$liveConfig = array("liveTitle"=>$liveTitle, "liveSubtitle"=>$liveSubtitle, "liveDescription"=>$liveDescription);
	
	return $liveConfig;

}

function setLiveMeta($liveTitle, $liveSubtitle, $liveDescription){
	
	$liveConfig = array("liveTitle"=>$liveTitle, "liveSubtitle"=>$liveSubtitle, "liveDescription"=>$liveDescription);
	$liveJson = json_encode($liveConfig);
	file_put_contents(LIVE_META_FILE, $liveJson);

}


?>
