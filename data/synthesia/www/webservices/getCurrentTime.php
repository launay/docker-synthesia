<?php

include('timeUtils.php');

header('Content-Type: application/json; charset=utf-8');


$timeConfig = getTimeConfig();
$timezone = $timeConfig["timezone"];
$timezoneId = $TIMEZONES[$timezone];
$timeDisplayMode = $timeConfig["timeDisplayMode"];
$hourFormat = $timeConfig["hourFormat"];

$date = new DateTime('now', new DateTimeZone($timezoneId));
if($timeDisplayMode == 1){

	if($hourFormat == 0)
		$time = $date->format('h:i A') ;
	elseif($hourFormat == 1)
		$time = $date->format('h\hi A') ;
	elseif($hourFormat == 2)
		$time = $date->format('h:i:s A') ;
	else
		$time = $date->format('h\hi\ms\s A') ;

}else{
	if($hourFormat == 0)
		$time = $date->format('H:i') ;
	elseif($hourFormat == 1)
		$time = $date->format('H\hi') ;
	elseif($hourFormat == 2)
		$time = $date->format('H:i:s') ;
	else
		$time = $date->format('H\hi\ms\s') ;

}

if(false){
	//debug
	$MONTHES = array(
		"1"=>"janvier",
		"2"=>"février",
		"3"=>"mars",
		"4"=>"avril",
		"5"=>"mai",
		"6"=>"juin",
		"7"=>"juillet",
		"8"=>"août",
		"9"=>"septembre",
		"10"=>"octobre",
		"11"=>"novembre",
		"12"=>"décembre"

	);
	$DAYS = array(
		"1"=>"Lundi",
		"2"=>"Mardi",
		"3"=>"Mercredi",
		"4"=>"Jeudi",
		"5"=>"Vendredi",
		"6"=>"Samedi",
		"7"=>"Dimanche"

	);
	$now = time();
	$currentDatePfx = date('j', $now);
	$currentDateSfx = date('Y', $now);
	$currentMonthNum = date('n', $now);
	$currentMonth = $MONTHES[$currentMonthNum];
	$currentDayNum = date('N', $now);
	$currentDay = $DAYS[$currentDayNum];
	$currentDate = $currentDay . " " . $currentDatePfx . ' ' . $currentMonth . ' ' . $currentDateSfx;
	$time = $currentDate . " - " . $date->format('H:i');

}

echo json_encode( array("currentTime"=>$time));


?>
