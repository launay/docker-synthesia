<?php

define("LIVE_HIDER_FILE", "_live_hider.json");

define("LIVE_ON_IMAGE", "/home/synthesia/synthesia/common/images/live_on.png");
define("LIVE_OFF_IMAGE", "/home/synthesia/synthesia/common/images/live_off.png");
define("NO_LIVE_IMAGE", "");

define("MODE_NO_IMAGE", 0);
define("MODE_ON_IMAGE", 1);
define("MODE_OFF_IMAGE", 2);

define("LIVE_ON_VISIBLE", true);
define("LIVE_OFF_VISIBLE", true);
define("NO_LIVE_VISIBLE", false);



function getLiveHider(){
	
	$mode = MODE_NO_IMAGE;
	$switchTime = time();
	if(file_exists(LIVE_HIDER_FILE)){
		$oldImageJson = file_get_contents(LIVE_HIDER_FILE);
		if($oldImageJson){
			$oldImageData = json_decode($oldImageJson,true);
			if($oldImageData){
				$mode = $oldImageData["mode"];
				$switchTime = $oldImageData["switchTime"];
			}
		}
	}
	$image = getImage($mode);
	$visible = getVisible($mode);
	
	$liveHiderConfig = array("mode"=>$mode, "image"=>$image, "visible"=>$visible, "switchTime"=>$switchTime);
	
	return $liveHiderConfig;

}

function setLiveHider($mode){
	//gets old mode
	$oldMode = MODE_NO_IMAGE;
	if(file_exists(LIVE_HIDER_FILE)){
		$oldImageJson = file_get_contents(LIVE_HIDER_FILE);
		if($oldImageJson){
			$oldImageData = json_decode($oldImageJson,true);
			if($oldImageData) $oldMode = $oldImageData["mode"];
		}
	}
	
	if($mode == $oldMode) return;

	$image = getImage($mode);
	$visible = getVisible($mode);
	$switchTime = time();
	file_put_contents(LIVE_HIDER_FILE, json_encode(array("mode"=>$mode, "image"=>$image, "visible"=>$visible, "switchTime"=>$switchTime)));

}

function getImage($mode){
	if($mode == MODE_NO_IMAGE) return NO_LIVE_IMAGE;
	if($mode == MODE_ON_IMAGE) return LIVE_ON_IMAGE;
	if($mode == MODE_OFF_IMAGE) return LIVE_OFF_IMAGE;
	return NO_LIVE_IMAGE;
}

function getVisible($mode){
	if($mode == MODE_NO_IMAGE) return NO_LIVE_VISIBLE;
	if($mode == MODE_ON_IMAGE) return LIVE_ON_VISIBLE;
	if($mode == MODE_OFF_IMAGE) return LIVE_OFF_VISIBLE;
	return NO_LIVE_IMAGE;
}


?>
