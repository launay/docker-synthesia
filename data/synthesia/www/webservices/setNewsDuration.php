<?php

define("DEFAULT_NEWS_DURATION", 15);
define("NEWS_DURATION_FILE", "_news_duration.json");

$newsDuration = DEFAULT_NEWS_DURATION;

//first reads from input config
if(file_exists(NEWS_DURATION_FILE)){
	$numberDataJson = file_get_contents(NEWS_DURATION_FILE);
	if($numberDataJson){
		$numberData = json_decode($numberDataJson, true);
		if($numberData){
			$newsDuration = $numberData["newsDuration"];
		}
	}
}

if(isset($_GET["newsDuration"])){	$newsDuration = trim($_GET["newsDuration"]);}

$result = array(
	"newsDuration" => $newsDuration
);

$resultJson = json_encode($result);
file_put_contents(NEWS_DURATION_FILE, $resultJson);

?>
