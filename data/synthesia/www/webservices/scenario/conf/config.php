<?php

define('SCENARIOS_FOLDER', dirname(__FILE__) . '/../data/');
define('CURRENT_SCENARIO_FILE', dirname(__FILE__) . '/../current/current_scenario.json');
define('DEFAULT_SCENARIO_ID', '000_DEFAULT');

//for GET requests
define('SCENARIO_ID', 'scenario_id');
//for GET responses
define('SCENARIO_DATA', 'data');

define('JSON_EXT', '.json');

define('VALUE_SCENARIO_KIND_FRAMED', 'FRAMED');
define('VALUE_SCENARIO_KIND_FULLSCREEN', 'FULLSCREEN');
define('VALUE_SCENARIO_KIND_USER', "USER");

define('ATTR_SCENARIO_DESCRIPTION', 'scenario_description');
define('ATTR_SCENARIO_KIND', 'kind');
define('ATTR_SCENARIO_NAME', 'scenario_name');
define('ATTR_ELEMENTS', 'elements');
define('ATTR_ELEMENT_NAME', 'element_name');
define('ATTR_ELEMENT_POSITION', 'position');
define('ATTR_ELEMENT_SIZE', 'size');
define('ATTR_ELEMENT_VISIBLE', 'visible');
define('ATTR_ELEMENT_NO_IMAGE', 'no_image');

define('ATTR_NEWS', 'news');
define('ATTR_PLAYLIST', 'playlist');

define('ATTR_NEWS_RSS', 'rss');
define('ATTR_NEWS_TWITTER', 'twitter');
define('ATTR_NEWS_FACEBOOK', 'facebook');

define('ATTR_NEWS_TITLE', 'title');
define('ATTR_NEWS_SUBTITLE', 'subtitle');
define('ATTR_NEWS_DESCRIPTION', 'description');
define('ATTR_NEWS_FOOTER', 'footer');
define('ATTR_NEWS_HIGHLIGHT', 'highlight');

define('ATTR_NEWS_MAX_LEN', 'max_len');
define('ATTR_NEWS_FONT', 'font');
define('ATTR_NEWS_SIZE', 'size');
define('ATTR_NEWS_DISPLAY', 'display');
define('ATTR_NEWS_UPCASE', 'upcase');
define('ATTR_NEWS_COLOR', 'color');


define('OUT_ELEMENT_ID_PFX', 'element_');


?>
