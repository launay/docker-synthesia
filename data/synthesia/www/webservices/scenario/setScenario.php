<?php

require_once(dirname(__FILE__) . '/php/utils.php');

header('Content-Type: application/json; charset=utf-8');

$scenarioId = DEFAULT_SCENARIO_ID;
if(isset($_GET[SCENARIO_ID]))
	$scenarioId = $_GET[SCENARIO_ID];
	
setCurrentScenario($scenarioId);
prettyEchoJson(getCurrentScenarioData());



?>
