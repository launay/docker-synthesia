<?php

require_once(dirname(__FILE__) . '/../conf/config.php');

if (!function_exists('prettyEchoJson')) {
	function prettyEchoJson($data){
		if(defined("JSON_PRETTY_PRINT") && defined("JSON_UNESCAPED_UNICODE"))
			echo(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
		else
			echo(json_encode($data));

	}
}


function listScenarios(){
	$pattern = SCENARIOS_FOLDER . '*' . JSON_EXT;
	$scenarioFiles = glob($pattern);
	
	$scenarioData = array();
	foreach($scenarioFiles as $scenarioFile){
		$scenarioId = basename($scenarioFile);
		$scenarioId = explode(".", $scenarioId);
		$scenarioId =  $scenarioId[0];
		$scenarioData[$scenarioId]  = getScenarioData($scenarioId);
	}
	return $scenarioData;
	
}
function getScenarioPath($scenarioId){
	return SCENARIOS_FOLDER . $scenarioId . JSON_EXT;
}

function getScenarioData($scenarioId){
	$scenarioJson = file_get_contents(getScenarioPath($scenarioId));
	return json_decode($scenarioJson, true);
}

function getCurrentScenarioData(){
	$scenarioId = getCurrentScenario();
	$result = array(SCENARIO_ID => $scenarioId);
	$result[SCENARIO_DATA] = getScenarioData($scenarioId);
	return  $result;
}


function scenarioExists($scenarioId){
	return $scenarioId!= null && file_exists(getScenarioPath($scenarioId));
}

function getCurrentScenario(){
	$scenarioId = DEFAULT_SCENARIO_ID;
	if(file_exists(CURRENT_SCENARIO_FILE)){
		$currentJson = file_get_contents(CURRENT_SCENARIO_FILE);
		if($currentJson){
			$scenarioData = json_decode($currentJson, true);
			if($scenarioData[SCENARIO_ID]){
				$scenarioId = $scenarioData[SCENARIO_ID];
			}
		}
	}	
	return $scenarioId;
}


function setCurrentScenario($scenarioId){
	if(!scenarioExists($scenarioId))
		$scenarioId = DEFAULT_SCENARIO_ID;
		
	$result = array();
	$result[SCENARIO_ID] = $scenarioId;
	
	$resultJson = json_encode($result);
	file_put_contents(CURRENT_SCENARIO_FILE, $resultJson);
}

?>
