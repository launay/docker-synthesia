<?php

include("./php/utils.php");


header('Content-Type: application/json; charset=utf-8');

$sceneId = DEFAULT_SCENE;
if(isset($_GET["scene_id"])) $sceneId = trim($_GET["scene_id"]);
if(!sceneExists($sceneId)) $sceneId = DEFAULT_SCENE;

removeScene($sceneId);

prettyEchoJson(array("status"=>"ok"));

?>
