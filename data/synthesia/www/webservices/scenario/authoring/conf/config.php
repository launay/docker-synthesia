<?php

include(dirname(__FILE__) . '/../../conf/config.php');

define("ABSOLUTE_IMAGES_FOLDER_PATH", "/var/www/synthesia/common/webservices/scenario/authoring/images/");
define("RELATIVE_IMAGES_FOLDER_PATH", "../webservices/scenario/authoring/images/");
define('SCENES_FOLDER', dirname(__FILE__) . '/../scenes/');
define('BACKGROUND_IMAGES_FOLDER', dirname(__FILE__) . '/../images/');
define('DEFAULT_SCENE', 'DEFAULT_scene');
define('CURRENT_SCENARIO_FILE', dirname(__FILE__) . '/../../current/current_scenario.json');

define('REGULAR_SCENARIOS_FOLDER', dirname(__FILE__) . '/../../data/');


define("USER_SCENARIO_ID", "__user");


//for GET requests
define('SCENE_ID', 'scene_id');
//for GET responses
define('SCENE_DATA', 'data');

define('JSON_EXT', '.json');



?>
