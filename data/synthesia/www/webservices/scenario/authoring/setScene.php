<?php

require_once(dirname(__FILE__) . '/php/utils.php');

header('Content-Type: application/json; charset=utf-8');

$scene_id = DEFAULT_SCENE;
if(isset($_GET[SCENE_ID]))
	$scene_id = $_GET[SCENE_ID];
	
setCurrentScene($scene_id);
prettyEchoJson(getSceneData($scene_id));



?>
