<?php

require_once(dirname(__FILE__) . '/../conf/config.php');


function prettyEncodeJson($data){
	if(defined("JSON_PRETTY_PRINT") && defined("JSON_UNESCAPED_UNICODE"))
		return (json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
	else
		return(json_encode($data));

}

function prettyEchoJson($data){
	echo prettyEncodeJson($data);

}

function getImagePath($sceneId, $relative=false){
	$pattern = BACKGROUND_IMAGES_FOLDER . $sceneId . '*';
	$imageFiles = glob($pattern);
	foreach($imageFiles as $imageFile){
		$imageName = basename($imageFile);
		if(!$relative){
			return ABSOLUTE_IMAGES_FOLDER_PATH . $imageName;
		}else{
			return RELATIVE_IMAGES_FOLDER_PATH . $imageName;

		}
	}
	return "";

}

function createNewScene($sceneId, $sceneName){
	
	$defaultSceneJson = file_get_contents(getScenePath(DEFAULT_SCENE));
	$defaultScene = json_decode($defaultSceneJson, true);
	$defaultScene["scenario_name"] = $sceneName;
	$defaultScene["background"] = ABSOLUTE_IMAGES_FOLDER_PATH . $sceneId . ".png";

	$defaultSceneOut = prettyEncodeJson($defaultScene);
	file_put_contents(SCENES_FOLDER . $sceneId . ".json", $defaultSceneOut);
	copy(getImagePath(DEFAULT_SCENE), BACKGROUND_IMAGES_FOLDER . $sceneId . ".png");

}

function updateScene($sceneId, $sceneData){
	if(!sceneExists($sceneId) || $sceneId == DEFAULT_SCENE){
		return;
	}
	$sceneOut = prettyEncodeJson($sceneData);
	file_put_contents(SCENES_FOLDER . $sceneId . ".json", $sceneOut);

}


function removeScene($sceneId){
	if($sceneId == DEFAULT_SCENE) return;
	unlink(getScenePath($sceneId));
	unlink(getImagePath($sceneId));
}

function setPostImage($sceneId, $imageData){
	if($sceneId == DEFAULT_SCENE) return -1;

	//cleanup
	unlink(getImagePath($sceneId));

	$imageName = $imageData['name'];
	$random = rand();
	$imageName = preg_replace('/[^A-Za-z0-9_\-\.]/', '_', $imageName) ;
	$ext = pathinfo($imageName, PATHINFO_EXTENSION);
	$imageName = $sceneId . "_" . $imageName . rand() . "." . $ext;
	$imageTmpPath = $imageData['tmp_name'];
	$ok = ($imageData['error']==0);
	if($ok){
		move_uploaded_file($imageTmpPath, BACKGROUND_IMAGES_FOLDER . $imageName);
		
	}
}

function listScenes(){
	$pattern = SCENES_FOLDER . '*' . JSON_EXT;
	$sceneFiles = glob($pattern);
	
	$sceneData = array();
	foreach($sceneFiles as $sceneFile){
		$sceneId = basename($sceneFile);	
		$sceneId = explode(".", $sceneId);
		$sceneId =  $sceneId[0];
		$sceneName  = getSceneData($sceneId);
		$sceneName  = $sceneName["scenario_name"];
		$sceneData[$sceneId] = $sceneName;

		
	}
	return $sceneData;
	
}
function getScenePath($sceneId){
	return SCENES_FOLDER . $sceneId . JSON_EXT;
}

function getSceneData($sceneId){
	
	$sceneJson = file_get_contents(getScenePath($sceneId));
	return json_decode($sceneJson, true);
}


function sceneExists($sceneId){
	return $sceneId!= null && file_exists(getScenePath($sceneId));
}

function setCurrentScene($sceneId){
	if(!sceneExists($sceneId))
		$sceneId = DEFAULT_SCENE;
		
	$result = array();
	$result[SCENARIO_ID] = USER_SCENARIO_ID;
	
	//will push the scenario in the scenarios file
	$sceneJson = file_get_contents(getScenePath($sceneId));
	$scene = json_decode($sceneJson, true);
	$scene["background"] = getImagePath($sceneId);
	$sceneOut = prettyEncodeJson($scene);
	file_put_contents(REGULAR_SCENARIOS_FOLDER . USER_SCENARIO_ID . ".json", $sceneOut);
	
	$resultJson = json_encode($result);
	file_put_contents(CURRENT_SCENARIO_FILE, $resultJson);
}

function getCurrentSceneData(){
	$sceneId = getCurrentScene();
	$result = array(SCENE_ID => $sceneId);
	$result[SCENE_DATA] = getSceneData($sceneId);
	return  $result;
}

function getCurrentScene(){
	$sceneId = DEFAULT_SCENE;
	if(file_exists(CURRENT_SCENARIO_FILE)){
		$currentJson = file_get_contents(CURRENT_SCENARIO_FILE);
		if($currentJson){
			$sceneData = json_decode($currentJson, true);
			if($sceneData[SCENE_ID]){
				$sceneId = $sceneData[SCENE_ID];
			}
		}
	}	
	return $sceneId;
}

?>
