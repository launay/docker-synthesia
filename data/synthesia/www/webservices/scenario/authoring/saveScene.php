<?php

include("./php/utils.php");

function _g($postData, $field){
	return $postData[$field];
}

header('Content-Type: application/json; charset=utf-8');

$p = $_POST;
$sceneId = $p["sceneId"];

if(sceneExists($sceneId) && $sceneId != DEFAULT_SCENE){
	$sceneData = getSceneData($sceneId);

	$elements = &$sceneData["elements"];

	$elements["element_101"]["position"] = _g($p,"show_title_block_x") . "," . _g($p,"show_title_block_y");
	$elements["element_101"]["size"] = _g($p,"show_title_block_width") . "," . _g($p,"show_title_block_height");
	$elements["element_101"]["visible"] = "" . _g($p,"show_title_block_visible");

	$elements["element_1"]["position"] = _g($p,"clock_block_x") . "," . _g($p,"clock_block_y");
	$elements["element_1"]["size"] = _g($p,"clock_block_width") . "," . _g($p,"clock_block_height");
	$elements["element_1"]["visible"] = "" . _g($p,"clock_block_visible");
	
	$elements["element_10000"]["position"] = _g($p,"video_block_x") . "," . _g($p,"video_block_y");
	$elements["element_10000"]["size"] = _g($p,"video_block_width") . "," . _g($p,"video_block_height");
	$elements["element_10000"]["visible"] = "" . _g($p,"video_block_visible");
	
	$elements["element_201"]["position"] = _g($p,"now_playing_text_block_x") . "," . _g($p,"now_playing_text_block_y");
	$elements["element_201"]["size"] = _g($p,"now_playing_text_block_width") . "," . _g($p,"now_playing_text_block_height");
	$elements["element_201"]["visible"] = "" . _g($p,"now_playing_text_block_visible");
	
	$elements["element_301"]["position"] = _g($p,"news_text_block_x") . "," . _g($p,"news_text_block_y");
	$elements["element_301"]["size"] = _g($p,"news_text_block_width") . "," . _g($p,"news_text_block_height");
	$elements["element_301"]["visible"] = "" . _g($p,"news_text_block_visible");
	
	$elements["element_300"]["position"] = _g($p,"news_image_block_x") . "," . _g($p,"news_image_block_y");
	$elements["element_300"]["size"] = _g($p,"news_image_block_width") . "," . _g($p,"news_image_block_height");
	$elements["element_300"]["visible"] = "" . _g($p,"news_image_block_visible");

	$show_title = &$sceneData["show_title"];
	$show_title["upcase"] = _g($p,"show_title_block_upcase") == "true";
	$show_title["color"] = "#" . _g($p,"show_title_block_color");

	$clock = &$sceneData["clock"];
	$clock["upcase"] = _g($p,"clock_block_upcase") == "true";
	$clock["color"] = "#" . _g($p,"clock_block_color");
	
	//must be done twice each time (for live and playlist)

	$now_playing_text = &$sceneData["live"]["title"];
	$now_playing_text["display"] = _g($p,"now_playing_text_block_title_visible") == "true";
	$now_playing_text["upcase"] = _g($p,"now_playing_text_block_title_upcase") == "true";
	$now_playing_text["color"] = "#" . _g($p,"now_playing_text_block_title_color");
	$now_playing_text = &$sceneData["playlist"]["title"];
	$now_playing_text["display"] = _g($p,"now_playing_text_block_title_visible") == "true";
	$now_playing_text["upcase"] = _g($p,"now_playing_text_block_title_upcase") == "true";
	$now_playing_text["color"] = "#" . _g($p,"now_playing_text_block_title_color");

	$now_playing_text = &$sceneData["live"]["subtitle"];
	$now_playing_text["display"] = _g($p,"now_playing_text_block_subtitle_visible") == "true";
	$now_playing_text["upcase"] = _g($p,"now_playing_text_block_subtitle_upcase") == "true";
	$now_playing_text["color"] = "#" . _g($p,"now_playing_text_block_subtitle_color");
	$now_playing_text = &$sceneData["playlist"]["subtitle"];
	$now_playing_text["display"] = _g($p,"now_playing_text_block_subtitle_visible") == "true";
	$now_playing_text["upcase"] = _g($p,"now_playing_text_block_subtitle_upcase") == "true";
	$now_playing_text["color"] = "#" . _g($p,"now_playing_text_block_subtitle_color");
	
	$now_playing_text = &$sceneData["live"]["description"];
	$now_playing_text["display"] = _g($p,"now_playing_text_block_description_visible") == "true";
	$now_playing_text["upcase"] = _g($p,"now_playing_text_block_description_upcase") == "true";
	$now_playing_text["color"] = "#" . _g($p,"now_playing_text_block_description_color");
	$now_playing_text = &$sceneData["playlist"]["description"];
	$now_playing_text["display"] = _g($p,"now_playing_text_block_description_visible") == "true";
	$now_playing_text["upcase"] = _g($p,"now_playing_text_block_description_upcase") == "true";
	$now_playing_text["color"] = "#" . _g($p,"now_playing_text_block_description_color");

	//must be done three times each time (for rss/twitter/facebook)

	$news_text = &$sceneData["news"]["rss"]["title"];
	$news_text["display"] = _g($p,"news_text_block_title_visible") == "true";
	$news_text["upcase"] = _g($p,"news_text_block_title_upcase") == "true";
	$news_text["color"] = "#" . _g($p,"news_text_block_title_color");
	$news_text = &$sceneData["news"]["twitter"]["title"];
	$news_text["display"] = _g($p,"news_text_block_title_visible") == "true";
	$news_text["upcase"] = _g($p,"news_text_block_title_upcase") == "true";
	$news_text["color"] = "#" . _g($p,"news_text_block_title_color");
	$news_text = &$sceneData["news"]["facebook"]["title"];
	$news_text["display"] = _g($p,"news_text_block_title_visible") == "true";
	$news_text["upcase"] = _g($p,"news_text_block_title_upcase") == "true";
	$news_text["color"] = "#" . _g($p,"news_text_block_title_color");
	
	$news_text = &$sceneData["news"]["rss"]["subtitle"];
	$news_text["display"] = _g($p,"news_text_block_subtitle_visible") == "true";
	$news_text["upcase"] = _g($p,"news_text_block_subtitle_upcase") == "true";
	$news_text["color"] = "#" . _g($p,"news_text_block_subtitle_color");
	$news_text = &$sceneData["news"]["twitter"]["subtitle"];
	$news_text["display"] = _g($p,"news_text_block_subtitle_visible") == "true";
	$news_text["upcase"] = _g($p,"news_text_block_subtitle_upcase") == "true";
	$news_text["color"] = "#" . _g($p,"news_text_block_subtitle_color");
	$news_text = &$sceneData["news"]["facebook"]["subtitle"];
	$news_text["display"] = _g($p,"news_text_block_subtitle_visible") == "true";
	$news_text["upcase"] = _g($p,"news_text_block_subtitle_upcase") == "true";
	$news_text["color"] = "#" . _g($p,"news_text_block_subtitle_color");
	
	$news_text = &$sceneData["news"]["rss"]["description"];
	$news_text["display"] = _g($p,"news_text_block_description_visible") == "true";
	$news_text["upcase"] = _g($p,"news_text_block_description_upcase") == "true";
	$news_text["color"] = "#" . _g($p,"news_text_block_description_color");
	$news_text = &$sceneData["news"]["twitter"]["description"];
	$news_text["display"] = _g($p,"news_text_block_description_visible") == "true";
	$news_text["upcase"] = _g($p,"news_text_block_description_upcase") == "true";
	$news_text["color"] = "#" . _g($p,"news_text_block_description_color");
	$news_text = &$sceneData["news"]["facebook"]["description"];
	$news_text["display"] = _g($p,"news_text_block_description_visible") == "true";
	$news_text["upcase"] = _g($p,"news_text_block_description_upcase") == "true";
	$news_text["color"] = "#" . _g($p,"news_text_block_description_color");
	
	$news_text = &$sceneData["news"]["rss"]["footer"];
	$news_text["display"] = _g($p,"news_text_block_footer_visible") == "true";
	$news_text["upcase"] = _g($p,"news_text_block_footer_upcase") == "true";
	$news_text["color"] = "#" . _g($p,"news_text_block_footer_color");
	$news_text = &$sceneData["news"]["twitter"]["footer"];
	$news_text["display"] = _g($p,"news_text_block_footer_visible") == "true";
	$news_text["upcase"] = _g($p,"news_text_block_footer_upcase") == "true";
	$news_text["color"] = "#" . _g($p,"news_text_block_footer_color");
	$news_text = &$sceneData["news"]["facebook"]["footer"];
	$news_text["display"] = _g($p,"news_text_block_footer_visible") == "true";
	$news_text["upcase"] = _g($p,"news_text_block_footer_upcase") == "true";
	$news_text["color"] = "#" . _g($p,"news_text_block_footer_color");

	$twitter_entities = &$sceneData["news"]["twitter"]["title"]["highlight"];
	$twitter_entities["upcase"] = _g($p,"news_text_block_twitter_entities_upcase") == "true";
	$twitter_entities["color"] = "#" . _g($p,"news_text_block_twitter_entities_color");
	$twitter_entities = &$sceneData["news"]["twitter"]["subtitle"]["highlight"];
	$twitter_entities["upcase"] = _g($p,"news_text_block_twitter_entities_upcase") == "true";
	$twitter_entities["color"] = "#" . _g($p,"news_text_block_twitter_entities_color");
	$twitter_entities = &$sceneData["news"]["twitter"]["description"]["highlight"];
	$twitter_entities["upcase"] = _g($p,"news_text_block_twitter_entities_upcase") == "true";
	$twitter_entities["color"] = "#" . _g($p,"news_text_block_twitter_entities_color");
	$twitter_entities = &$sceneData["news"]["twitter"]["footer"]["highlight"];
	$twitter_entities["upcase"] = _g($p,"news_text_block_twitter_entities_upcase") == "true";
	$twitter_entities["color"] = "#" . _g($p,"news_text_block_twitter_entities_color");
	
	$facebook_entities = &$sceneData["news"]["facebook"]["title"]["highlight"];
	$facebook_entities["upcase"] = _g($p,"news_text_block_facebook_entities_upcase") == "true";
	$facebook_entities["color"] = "#" . _g($p,"news_text_block_facebook_entities_color");
	$facebook_entities = &$sceneData["news"]["facebook"]["subtitle"]["highlight"];
	$facebook_entities["upcase"] = _g($p,"news_text_block_facebook_entities_upcase") == "true";
	$facebook_entities["color"] = "#" . _g($p,"news_text_block_facebook_entities_color");
	$facebook_entities = &$sceneData["news"]["facebook"]["description"]["highlight"];
	$facebook_entities["upcase"] = _g($p,"news_text_block_facebook_entities_upcase") == "true";
	$facebook_entities["color"] = "#" . _g($p,"news_text_block_facebook_entities_color");
	$facebook_entities = &$sceneData["news"]["facebook"]["footer"]["highlight"];
	$facebook_entities["upcase"] = _g($p,"news_text_block_facebook_entities_upcase") == "true";
	$facebook_entities["color"] = "#" . _g($p,"news_text_block_facebook_entities_color");

	$_POST["sceneData"] = $sceneData;
}

updateScene($sceneId, $sceneData);

prettyEchoJson($_POST);

?>
