<?php

error_reporting(E_ERROR | E_PARSE);

header('Content-Type: application/json; charset=utf-8');


define ('TOKEN', '1736478d752e3d28d9b8f4e897edb25a');
define ('APP_ID', '234420790375467');
define ('APP_SECRET', '136398edcf84b2c84d26442822b7ae75');
define ('GRAPH_VERSION', 'v2.5');

define ('CACHE_FILE', '_facebook_cache.json');
define('CACHE_DURATION_SECONDS', 240);

define("ID_FILE", "_facebookId.txt");
define("DEFAULT_ID", "14892757589");
define("LAST_ID_FILE", "_fb_lastId.txt");

define("DEFAULT_IMAGE", "");


$facebookId = DEFAULT_ID;
if(file_exists(ID_FILE) && filesize(ID_FILE)>0){
	$facebookId = trim(file_get_contents(ID_FILE));
}

//will check if must clear cache
$oldId = $facebookId;
if(file_exists(LAST_ID_FILE)){
	$oldId = trim(file_get_contents(LAST_ID_FILE));
}

if($oldId != $facebookId){
	unlink(CACHE_FILE);
}

$url = 'https://graph.facebook.com/' 
		. $facebookId 
		. '/posts?&access_token=' 
		. APP_ID. '|' 
		. APP_SECRET 
		. '&fields=attachments,id,message,created_time,type,object_id,from';

$now = time();

if(file_exists(CACHE_FILE) && ($now - filemtime(CACHE_FILE) < CACHE_DURATION_SECONDS) && filesize(CACHE_FILE) > 100){
	$result = file_get_contents(CACHE_FILE);
}
else{
	$result = file_get_contents($url);
	file_put_contents(CACHE_FILE, $result);
	chmod(CACHE_FILE, 0777); 
	file_put_contents(LAST_ID_FILE, $facebookId);


}

$resultArray = json_decode($result, true);
$fbData = $resultArray["data"];


$outData = array();
$fbArray = array();

//print_r($fbData);

foreach($fbData as $postData){

	$data = array();

	$data["createdAt"] = $postData["created_time"];
	$data["from"] = $postData["from"]["name"];
	
	$message = $postData["message"];
	$message = cleanField($message);
	$message = removeEmoji($message);
	$message = cleaner($message);
	$data["message"] = $message;
	$data["image"] = DEFAULT_IMAGE;
	$postImage = $postData["attachments"]["data"][0]["media"]["image"]["src"];
	if($postImage) $data["image"] = $postImage;
	
	if(!$message) continue;
	
	array_push($fbArray, $data);
	
}

$outData["facebook"] = $fbArray;
if(defined("JSON_PRETTY_PRINT") && defined("JSON_UNESCAPED_UNICODE"))
	echo(json_encode($outData, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
else
	echo(json_encode($outData));

/**
 * Misc functions
 */

function removeEmoji($text) {

    $clean_text = "";

    // Match Emoticons
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clean_text = preg_replace($regexEmoticons, '', $text);

    // Match Miscellaneous Symbols and Pictographs
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clean_text = preg_replace($regexSymbols, '', $clean_text);

    // Match Transport And Map Symbols
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clean_text = preg_replace($regexTransport, '', $clean_text);

    // Match Miscellaneous Symbols
    $regexMisc = '/[\x{2600}-\x{26FF}]/u';
    $clean_text = preg_replace($regexMisc, '', $clean_text);

    // Match Dingbats
    $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
    $clean_text = preg_replace($regexDingbats, '', $clean_text);

    return $clean_text;
}

function cleanField($field){
	
	$field = preg_replace('!\s+!', ' ', $field);
	$field = str_replace("\n", " ", $field);
	
	$field = str_replace('►', ">", $field);
	$field = str_replace('♫', " ", $field);
	$field = str_replace('♪', " ", $field);
	$field = str_replace('…', "...", $field);
	$field = str_replace('&icirc;', "î", $field);
	$field = str_replace('&acirc;', "â", $field);
	$field = str_replace('&ucirc;', "û", $field);
	$field = str_replace('&ecirc;', "ê", $field);
	$field = str_replace("&ocirc;", "ô", $field);
	$field = str_replace('&egrave;', "è", $field);
	$field = str_replace('&agrave;', "à", $field);
	$field = str_replace('&eacute;', "é", $field);
	$field = str_replace('&hellip;', "...", $field);
	$field = str_replace('&amp;', "&", $field);
	$field = str_replace("&#39;", "'", $field);
	$field = str_replace("&#039;", "'", $field);
	$field = str_replace('&rsquo;', "'", $field);
	$field = str_replace('&lsquo;', "'", $field);
	$field = str_replace("&nbsp;", " ", $field);
	$field = str_replace("&quot;", "\"", $field);
	$field = str_replace("&laquo;", "\"", $field);
	$field = str_replace("&raquo;", "\"", $field);
	$field = str_replace("&ldquo;", "\"", $field);
	$field = str_replace("&rdquo;", "\"", $field);
	$field = str_replace("&lsaquo;", "<", $field);
	$field = str_replace("&rsaquo;", ">", $field);
	$field = str_replace("&ccedil;", "ç", $field);
	$field = str_replace("&iuml;", "ï", $field);
	$field = str_replace("&auml;", "ä", $field);
	$field = str_replace("&euml;", "ë", $field);
	$field = str_replace("&ouml;", "ö", $field);
	$field = str_replace("&uuml;", "ü", $field);
	$field = str_replace("&oelig;", "œ", $field);
	$field = str_replace("&ndash;", "–", $field);
	$field = str_replace('&ugrave;', "ù", $field);
	$field = str_replace("œ", "oe", $field);
	$field = cleaner($field);
	$field = preg_replace('!\s+!', ' ', $field);
	$field = trim(preg_replace('/\s+/', ' ', $field));

	
	return $field;
}


function startsWith($haystack, $needle)
{
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}
function cleaner($url) {
//	return $url;
//	echo "\ntext: " .$url;
	$clean = preg_replace('/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', "", $url);
	//echo "\nclean: " . $clean;
	//echo "\ntrimmed: " . trim($clean);

	return trim($clean);
}

?>
