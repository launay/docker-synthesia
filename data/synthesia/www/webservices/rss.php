<?php

error_reporting(E_ERROR | E_PARSE);


header('Content-Type: application/json; charset=utf-8');

define("CACHE_FILE", "_rss_cache.json");
define("LAST_URL_FILE", "_rss_lastUrl.txt");
define('CACHE_DURATION_SECONDS', 240);


define("RSS_FILE", "_rssURL.txt");
define("DEFAULT_IMAGE", "");
define("DEFAULT_RSS_URL", "http://www.lemonde.fr/rss/une.xml");
define("DEFAULT_RSS_SOURCE", "Le Monde.fr - Actualités et Infos en France et dans le monde");

$rssUrl= $_GET['rssUrl'];
if(!$defaultImage) $defaultImage = DEFAULT_IMAGE;
if(!$source)$source = "";

if(!$rssUrl){
	$rssUrl = DEFAULT_RSS_URL;
	$source = DEFAULT_RSS_SOURCE;
}

if(file_exists(RSS_FILE) && filesize(RSS_FILE)>0){
	$rssUrl = trim(file_get_contents(RSS_FILE));
	$source = "";
}

//will check if must clear cache
$oldUrl = $rssUrl;
if(file_exists(LAST_URL_FILE)){
	$oldUrl = trim(file_get_contents(LAST_URL_FILE));
}

if($oldUrl != $rssUrl){
	unlink(CACHE_FILE);
}


$data = getRSSData($rssUrl, $defaultImage, $source);
echo(json_encode($data));

function getRSSData($rssUrl, $defaultImage, $source){

	$now = time();
	if(file_exists(CACHE_FILE) && ($now - filemtime(CACHE_FILE) < CACHE_DURATION_SECONDS) && filesize(CACHE_FILE) > 100){
		$rssXMLData = file_get_contents(CACHE_FILE);
	}
	else{
		$rssXMLData = file_get_contents($rssUrl);
		file_put_contents(CACHE_FILE, $rssXMLData);
		file_put_contents(LAST_URL_FILE, $rssUrl);
	}
	
	$rssData = array();
	$xml = simplexml_load_string($rssXMLData);

	if(!$source){
		$sources = $xml->xpath("//channel/title");
		$source = (string)$sources[0];
	}

	$items = $xml->xpath("//item");	

	$rss = array();
	foreach($items as $target){
		$item = array();
		$title = (string) $target->title;
		$title = cleaner(cleanField(removeEmoji($title)));
		$description = (string) $target->description;
		$description = cleaner(cleanField(removeEmoji($description)));
		$item["title"] = $title;
		$item["pubDate"] = (string) $target->pubDate;
		$item["description"] = $description;
		$item["link"] = (string) $target->link;
		$item["enclosure"] = $defaultImage;
		$item["source"] = $source;
		$enclosure = $target->enclosure;
		if($enclosure){
			$attrz = $enclosure->attributes();			
			$item["enclosure"] = (string)$attrz["url"];
		}
		if(!$item["enclosure"]){
			$enclosure = $target->children("http://search.yahoo.com/mrss/");
			$enclosure = $enclosure->content;
			$attrz = $enclosure->attributes();
			$item["enclosure"] = (string)$attrz["url"];
		}
		
		if(!$item["enclosure"]){
			$item["enclosure"] = DEFAULT_IMAGE;
		}
		
		array_push($rss, $item);
	}
	$rssData["rss"]=$rss;


	return $rssData;	
}


function removeEmoji($text) {

    $clean_text = "";

    // Match Emoticons
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clean_text = preg_replace($regexEmoticons, '', $text);

    // Match Miscellaneous Symbols and Pictographs
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clean_text = preg_replace($regexSymbols, '', $clean_text);

    // Match Transport And Map Symbols
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clean_text = preg_replace($regexTransport, '', $clean_text);

    // Match Miscellaneous Symbols
    $regexMisc = '/[\x{2600}-\x{26FF}]/u';
    $clean_text = preg_replace($regexMisc, '', $clean_text);

    // Match Dingbats
    $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
    $clean_text = preg_replace($regexDingbats, '', $clean_text);

    return $clean_text;
}

function cleanField($field){
	
	$field = strip_tags($field);
	$field = preg_replace('!\s+!', ' ', $field);
	$field = str_replace("\n", " ", $field);
	
	$field = str_replace('►', ">", $field);
	$field = str_replace('♫', " ", $field);
	$field = str_replace('♪', " ", $field);
	$field = str_replace('…', "...", $field);
	$field = str_replace('&icirc;', "î", $field);
	$field = str_replace('&acirc;', "â", $field);
	$field = str_replace('&ucirc;', "û", $field);
	$field = str_replace('&ecirc;', "ê", $field);
	$field = str_replace("&ocirc;", "ô", $field);
	$field = str_replace('&egrave;', "è", $field);
	$field = str_replace('&agrave;', "à", $field);
	$field = str_replace('&eacute;', "é", $field);
	$field = str_replace('&hellip;', "...", $field);
	$field = str_replace('&amp;', "&", $field);
	$field = str_replace("&#39;", "'", $field);
	$field = str_replace("&#039;", "'", $field);
	$field = str_replace('&rsquo;', "'", $field);
	$field = str_replace('&lsquo;', "'", $field);
	$field = str_replace("&nbsp;", " ", $field);
	$field = str_replace("&quot;", "\"", $field);
	$field = str_replace("&laquo;", "\"", $field);
	$field = str_replace("&raquo;", "\"", $field);
	$field = str_replace("&ldquo;", "\"", $field);
	$field = str_replace("&rdquo;", "\"", $field);
	$field = str_replace("&lsaquo;", "<", $field);
	$field = str_replace("&rsaquo;", ">", $field);
	$field = str_replace("&ccedil;", "ç", $field);
	$field = str_replace("&iuml;", "ï", $field);
	$field = str_replace("&auml;", "ä", $field);
	$field = str_replace("&euml;", "ë", $field);
	$field = str_replace("&ouml;", "ö", $field);
	$field = str_replace("&uuml;", "ü", $field);
	$field = str_replace("&oelig;", "œ", $field);
	$field = str_replace("&ndash;", "–", $field);
	$field = str_replace('&ugrave;', "ù", $field);

	
	return $field;
}


function cleaner($url) {
	return $url;
//	echo "\ntext: " .$url;
//	$clean = preg_replace('/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', "\n", $url);
	//echo "\nclean: " . $clean;
	//echo "\ntrimmed: " . trim($clean);

	return trim($clean);
}

?> 
