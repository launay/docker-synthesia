<?php

include("liveHiderUtils.php");


header('Content-Type: application/json; charset=utf-8');

$liveHiderConfig = getLiveHider();

if(defined("JSON_PRETTY_PRINT") && defined("JSON_UNESCAPED_UNICODE"))
	echo(json_encode($liveHiderConfig, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
else
	echo(json_encode($liveHiderConfig));


?>
