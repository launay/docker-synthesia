<?php


header('Content-Type: application/json; charset=utf-8');

define("RSS_URL", "http://localhost/synthesia/common/webservices/rss.php");
define("TWITTER_URL", "http://localhost/synthesia/common/webservices/twitter.php");
define("FACEBOOK_URL", "http://localhost/synthesia/common/webservices/facebook.php");

define("NEWS_DURATION_FILE", "_news_duration.json");
define("NEWS_NUMBER_FILE", "_news_number.json");
define("NEWS_ACTIVE_FILE", "_news_active.json");

define("DEFAULT_NEWS_DURATION", 15);
define("DEFAULT_NEWS_NUMBER", 10);
define("DEFAULT_ACTIVE_RSS", true);
define("DEFAULT_ACTIVE_TWITTER", false);
define("DEFAULT_ACTIVE_FACEBOOK", false);

define("RSS", "RSS");
define("TWITTER", "TWITTER");
define("FACEBOOK", "FACEBOOK");

//set default values
$newsDuration = DEFAULT_NEWS_DURATION;

$rssNumber = DEFAULT_NEWS_NUMBER;
$twitterNumber = DEFAULT_NEWS_NUMBER;
$facebookNumber = DEFAULT_NEWS_NUMBER;

$rssActive = DEFAULT_ACTIVE_RSS;
$twitterActive = DEFAULT_ACTIVE_TWITTER;
$facebookActive = DEFAULT_ACTIVE_FACEBOOK;

//read input config
if(file_exists(NEWS_DURATION_FILE)){
	$numberDataJson = file_get_contents(NEWS_DURATION_FILE);
	if($numberDataJson){
		$numberData = json_decode($numberDataJson, true);
		if($numberData){
			$newsDuration = $numberData["newsDuration"];
		}
	}
}
if(file_exists(NEWS_NUMBER_FILE)){
	$numberDataJson = file_get_contents(NEWS_NUMBER_FILE);
	if($numberDataJson){
		$numberData = json_decode($numberDataJson, true);
		if($numberData){
			$rssNumber = $numberData["rssNumber"];
			$twitterNumber = $numberData["twitterNumber"];
			$facebookNumber = $numberData["facebookNumber"];
		}
	}
}
if(file_exists(NEWS_ACTIVE_FILE)){
	$activeDataJson = file_get_contents(NEWS_ACTIVE_FILE);
	if($activeDataJson){
		$activeData = json_decode($activeDataJson, true);
		if($activeData){
			$rssActive = $activeData["rssActive"];
			$twitterActive = $activeData["twitterActive"];
			$facebookActive = $activeData["facebookActive"];
		}
	}
}

if($rssNumber<0) $rssNumber = 99999;
if($twitterNumber<0) $twitterNumber = 99999;
if($facebookNumber<0) $facebookNumber = 99999;

//get data for active sources
$itemsCount = 0;
$rssCount = 0;
$twitterCount = 0;
$facebookCount = 0;
$allData = array();
if($rssActive){
	$rssData = file_get_contents(RSS_URL);
	$rssDataJson = json_decode($rssData,true);

	$count = 0;
	$rssDataArrayIn = $rssDataJson["rss"];
	$rssDataArrayOut = array();
	foreach($rssDataArrayIn as $newsItem){
		if(++$count > $rssNumber) break;
		array_push($rssDataArrayOut, $newsItem);
		$itemsCount++;
		$rssCount++;
	}
	$allData[RSS] = $rssDataArrayOut;
}
if($twitterActive){
	$twitterData = file_get_contents(TWITTER_URL);
	$twitterDataJson = json_decode($twitterData,true);

	$count = 0;
	$twitterDataArrayIn = $twitterDataJson["twitter"]["data"];
	$twitterDataArrayOut = array();
	foreach($twitterDataArrayIn as $newsItem){
		if(++$count > $twitterNumber) break;
		array_push($twitterDataArrayOut, $newsItem);
		$itemsCount++;
		$twitterCount++;
	}
	$allData[TWITTER] = $twitterDataArrayOut;
}
if($facebookActive){
	$facebookData = file_get_contents(FACEBOOK_URL);
	$facebookDataJson = json_decode($facebookData,true);

	$count = 0;
	$facebookDataArrayIn = $facebookDataJson["facebook"];
	$facebookDataArrayOut = array();
	foreach($facebookDataArrayIn as $newsItem){
		if(++$count > $facebookNumber) break;
		array_push($facebookDataArrayOut, $newsItem);
		$itemsCount++;
		$facebookCount++;
	}
	$allData[FACEBOOK] = $facebookDataArrayOut;

}

$order = array();
if($rssActive) array_push($order, RSS);
if($twitterActive) array_push($order, TWITTER);
if($facebookActive) array_push($order, FACEBOOK);

$maxNumber = max($rssCount, $twitterCount, $facebookCount) * count($order);


$rssIdx = 0;
$twitterIdx = 0;
$facebookIdx = 0;
$allDataOut = array();
for($i = 0 ; $i < $maxNumber ; $i++){
	$currentOrderIdx = $i % count($order);
	$currentOrder = $order[$currentOrderIdx];
	
	if($currentOrder == RSS  && $rssCount > 0){
		$item = $allData[RSS][$rssIdx];
		$item["kind"] = RSS;
		array_push($allDataOut, $item);
		$rssIdx = ($rssIdx + 1 ) % $rssCount;
	}
	if($currentOrder == TWITTER  && $twitterCount > 0){
		$item = $allData[TWITTER][$twitterIdx];
		$item["kind"] = TWITTER;
		array_push($allDataOut, $item);
		$twitterIdx = ($twitterIdx + 1 ) % $twitterCount;
	}
	if($currentOrder == FACEBOOK && $facebookCount > 0){
		$item = $allData[FACEBOOK][$facebookIdx];
		$item["kind"] = FACEBOOK;
		array_push($allDataOut, $item);
		$facebookIdx = ($facebookIdx + 1 ) % $facebookCount;
	}

}

$now = time();
$period = ($now / $newsDuration) % $maxNumber;

$target = $allDataOut[$period];
//$target["message"] = "L'@unistra accueille les premiers \"Rendez-vous de la mobilité inter-fonction publique - #GrandEst\" : accompagner la mobilité dans un parcours professionnel !";
$result = $target;

if(defined("JSON_PRETTY_PRINT") && defined("JSON_UNESCAPED_UNICODE"))
	echo(json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
else
	echo(json_encode($result));
?> 
