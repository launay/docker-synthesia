<?php

define("DEFAULT_NEWS_NUMBER", 10);
define("NEWS_NUMBER_FILE", "_news_number.json");

$rssNumber = DEFAULT_NEWS_NUMBER;
$twitterNumber = DEFAULT_NEWS_NUMBER;
$facebookNumber = DEFAULT_NEWS_NUMBER;

//first reads from input config
if(file_exists(NEWS_NUMBER_FILE)){
	$numberDataJson = file_get_contents(NEWS_NUMBER_FILE);
	if($numberDataJson){
		$numberData = json_decode($numberDataJson, true);
		if($numberData){
			$rssNumber = $numberData["rssNumber"];
			$twitterNumber = $numberData["twitterNumber"];
			$facebookNumber = $numberData["facebookNumber"];
		}
	}
}

if(isset($_GET["rssNumber"])){	$rssNumber = trim($_GET["rssNumber"]);}
if(isset($_GET["twitterNumber"])){	$twitterNumber = trim($_GET["twitterNumber"]);}
if(isset($_GET["facebookNumber"])){	$facebookNumber = trim($_GET["facebookNumber"]);}

$result = array(
	"rssNumber" => $rssNumber,
	"twitterNumber" => $twitterNumber,
	"facebookNumber" => $facebookNumber 
);

$resultJson = json_encode($result);
file_put_contents(NEWS_NUMBER_FILE, $resultJson);

?>
