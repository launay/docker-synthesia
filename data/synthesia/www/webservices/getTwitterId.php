<?php
header('Content-Type: application/json; charset=utf-8');


$url = "";
$id = "";
if(isset($_GET["url"])){
	$url = trim($_GET["url"]);
}


if($url){

	$pageContents = file_get_contents($url);
	$pattern = '/href="android-app:\/\/com\.twitter\.android\/twitter\/user\?screen_name=([^&]+)/';
	$pattern2 = '/"screen_name":"([^"])+"/';
	preg_match($pattern, $pageContents, $matches, PREG_OFFSET_CAPTURE);
	if(count($matches)>1){
		$id = $matches[1][0];
	}else{
		preg_match($pattern2, $pageContents, $matches2, PREG_OFFSET_CAPTURE);
		if(count($matches2)>1){
			$id = $matches2[1][0];
		}

	}
}



$data = array("url"=>$url, "id"=>$id);

if(defined("JSON_PRETTY_PRINT") && defined("JSON_UNESCAPED_UNICODE"))
	echo(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
else
	echo(json_encode($data));
		
?>
