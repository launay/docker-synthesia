<?php

define("SOURCE_FILE", "../source/source.txt");
define("DEFAULT_SOURCE", "0");

header('Content-Type: application/json; charset=utf-8');


$source = DEFAULT_SOURCE;
if(file_exists(SOURCE_FILE)){
	$source = file_get_contents(SOURCE_FILE);
}

echo json_encode(array("source"=>$source));
?>
