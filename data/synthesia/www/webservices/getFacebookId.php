<?php
header('Content-Type: application/json; charset=utf-8');

define ('TOKEN', '1736478d752e3d28d9b8f4e897edb25a');
define ('APP_ID', '234420790375467');
define ('APP_SECRET', '136398edcf84b2c84d26442822b7ae75');
define ('GRAPH_VERSION', 'v2.5');


$url = "";
$id = "";
if(isset($_GET["url"])){
	$url = trim($_GET["url"]);
}


if($url){

	while(endsWith($url, "/")){
		$url = substr($url, 0, strlen($url)-1);
		$url = trim($url);
	}

	$name = end(explode("/", $url));

	$wsUrl = 'https://graph.facebook.com/' . $name . '?&access_token='. APP_ID. '|' . APP_SECRET;
	$result = file_get_contents($wsUrl);
	if($result){
		$resultJson = json_decode($result,true);
		if($resultJson){
			$id = $resultJson["id"];
		}
	}
}



$data = array("url"=>$url, "id"=>$id);

if(defined("JSON_PRETTY_PRINT") && defined("JSON_UNESCAPED_UNICODE"))
	echo(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
else
	echo(json_encode($data));
	
	
function startsWith($haystack, $needle)
{
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}	
?>
