<?php

error_reporting(E_ERROR | E_PARSE);

require_once(dirname(__FILE__) . '/scenario/authoring/conf/config.php');
require_once(dirname(__FILE__) . '/scenario/conf/config.php');
include("autofit.php");

header('Content-Type: application/json; charset=utf-8');

define("NEWS_URL", "http://localhost/synthesia/common/webservices/news.php");
define("SCENARIO_URL", "http://localhost/synthesia/common/webservices/scenario/getScenarioOrScene.php");
define("TITLE_URL", "http://localhost/synthesia/common/webservices/getTitle.php");
define("PLAYLIST_URL", "http://localhost/synthesia/common/playlist/manager/getCurrentStatus.php");
define("SOURCE_STATE_URL", "http://localhost/synthesia/common/webservices/getSource.php");
define("LIVE_META_URL", "http://localhost/synthesia/common/webservices/getLiveMeta.php");
define("LIVE_HIDER_URL", "http://localhost/synthesia/common/webservices/getLiveHider.php");
define("CLOCK_URL", "http://localhost/synthesia/common/webservices/getCurrentTime.php");

define("RSS", "RSS");
define("TWITTER", "TWITTER");
define("FACEBOOK", "FACEBOOK");

define("AUTOFIT", true);

define("AUTOFIT_MIN_SIZE", 16);
define("AUTOFIT_MAX_SIZE", 40);

define("LEFT_ALIGN", 2);
define("DEFAULT_FONT", "itcfranklingothic-bold.ttf");
define("DEFAULT_FONT_SIZE", 8);

define("NEWS_ELEMENT_ID", "element_301");
define("PLAYLIST_ELEMENT_ID", "element_201");
define("PLAYLIST_NEXT_ELEMENT_ID", "element_402");
define("LIVE_ELEMENT_ID", "element_10000");
define("COMING_NEXT_LIVE_ELEMENT_ID", "element_500");
define("SHOW_TITLE_ELEMENT_ID", "element_101");
define("CLOCK_ELEMENT_ID", "element_1");


//get live hider
$liveHider = 0;
$liveHiderJson = file_get_contents(LIVE_HIDER_URL);
if($liveHiderJson){
	$liveHiderData = json_decode($liveHiderJson, true);
	if($liveHiderData) $liveHider = $liveHiderData;
}

//get clock
$clockTime = "";
$clockTimeJson = file_get_contents(CLOCK_URL);
if($clockTimeJson){
	$clockTimeData = json_decode($clockTimeJson, true);
	if($clockTimeData) $clockTime = $clockTimeData["currentTime"];
}

//get source
$source = 0;
$sourceJson = file_get_contents(SOURCE_STATE_URL);
if($sourceJson){
	$sourceData = json_decode($sourceJson, true);
	if($sourceData) $source = $sourceData["source"];
}

//get live data
$liveMetaJson = file_get_contents(LIVE_META_URL);
$liveMeta = json_decode($liveMetaJson, true);


//get title
$showTitleJson = json_decode(file_get_contents(TITLE_URL),true);
$showTitle = $showTitleJson["showTitle"];

//get news
$news = json_decode(file_get_contents(NEWS_URL),true);

//get scenario
$scenario = json_decode(file_get_contents(SCENARIO_URL), true);

//formats news
$scenarioNews = $scenario["data"]["news"];

$rssNews = $scenarioNews["rss"];
$twitterNews = $scenarioNews["twitter"];
$facebookNews = $scenarioNews["facebook"];

//format clock time
$clockFormat = $scenario["data"]["clock"];
$clockFormatted = array();
{

	$fullDescription="";
	$attributes="attributes=";	
	$from = 0;
	$to=0;
	
	
	$title = cleanupField($clockTime);
	$title_upcase = $clockFormat["upcase"];
	if($title_upcase) $title = strtoupperFr($title);


	$title = applyMaxLen($title, $clockFormat["title"]);
	
	if($title){

		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $title;
		$to = mb_strlen($fullDescription, 'utf8');			
		

		$font = $clockFormat["font"];
		$size = $clockFormat["size"];
		$color = $clockFormat["color"];
		
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
	}
	
	
	
	$clockFormatted["full_description"] = $fullDescription;
	$clockFormatted["full_description_attributes"] = $attributes;

}

//format show title
$showTitleFormat = $scenario["data"]["show_title"];
$showTitleFormatted = array();
{

	$fullDescription="";
	$attributes="attributes=";	
	$from = 0;
	$to=0;
	
	
	$title = cleanupField($showTitle);
	$title_upcase = $showTitleFormat["upcase"];
	if($title_upcase) $title = strtoupperFr($title);


	$title = applyMaxLen($title, $showTitleFormat["title"]);
	
	if($title){

		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $title;
		$to = mb_strlen($fullDescription, 'utf8');			
		

		$font = $showTitleFormat["font"];
		$size = $showTitleFormat["size"];
		$color = $showTitleFormat["color"];
		
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
	}
	
	
	
	$showTitleFormatted["full_description"] = $fullDescription;
	$showTitleFormatted["full_description_attributes"] = $attributes;

}

//get current news
$currentNewsKind = $news["kind"];
$newsFormatted = array();
if($currentNewsKind == RSS){

	$fullDescription="";
	$attributes="attributes=";	
	$from = 0;
	$to=0;
	
	$image = $news["enclosure"];
	
	$title = cleanupField($news["title"]);
	$title_upcase = $rssNews["title"]["upcase"];
	if($title_upcase) $title = strtoupperFr($title);

	$subtitle = $news["pubDate"];
	$subtitle = date('d/m/Y H:i', strtotime($subtitle));
	$subtitle_upcase = $rssNews["subtitle"]["upcase"];
	if($subtitle_upcase) $subtitle = strtoupperFr($subtitle);

	$description = cleanupField($news["description"]);
	$description_upcase = $rssNews["description"]["upcase"];
	if($description_upcase) $description = strtoupperFr($description);
	
	$footer = $news["source"];
	$footer_upcase = $rssNews["footer"]["upcase"];
	if($footer_upcase) $footer = strtoupperFr($footer);

	$title_display = $rssNews["title"]["display"];
	$subtitle_display = $rssNews["subtitle"]["display"];
	$description_display = $rssNews["description"]["display"];
	$footer_display = $rssNews["footer"]["display"];

	$title = applyMaxLen($title, $rssNews["title"]);
	$subtitle = applyMaxLen($subtitle, $rssNews["subtitle"]);
	$description = applyMaxLen($description, $rssNews["description"]);
	
	if($title && $title_display){

		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $title;
		$to = mb_strlen($fullDescription, 'utf8');			
		

		$font = $rssNews["title"]["font"];
		$size = $rssNews["title"]["size"];
		$color = $rssNews["title"]["color"];
		
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
		$fullDescription .=  "\n";
	}
	
	if($subtitle && $subtitle_display){
		
		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $subtitle;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $rssNews["subtitle"]["font"];
		$size = $rssNews["subtitle"]["size"];
		$color = $rssNews["subtitle"]["color"];
		
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);

		//$fullDescription .=  "\n";
		$fullDescription .=  "\n";
	}
	
	if($description && $description_display){
	
		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $description;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $rssNews["description"]["font"];
		$size = $rssNews["description"]["size"];
		$color = $rssNews["description"]["color"];
				
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
		$fullDescription .=  "\n";
		//$fullDescription .=  "\n";
	}

	if($footer && $footer_display){
		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $footer;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $rssNews["footer"]["font"];
		$size = $rssNews["footer"]["size"];
		$color = $rssNews["footer"]["color"];
				
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
	}
	
	$newsFormatted["full_description"] = $fullDescription;
	$newsFormatted["full_description_attributes"] = $attributes;
	$newsFormatted["image"] = $image;
}
if($currentNewsKind == TWITTER){

	$fullDescription="";
	$attributes="attributes=";	
	$from = 0;
	$to=0;
	
	$image = $news["messageImage"];
	//if(!$image){
	//	$image = $news["profileImage"];
	//}
	
	$name = cleanupField($news["name"]);
	$userName = cleanupField("@" . $news["userName"]);
	$title_upcase = $twitterNews["title"]["upcase"];
	if($title_upcase){
		$name = strtoupperFr($name);
		$userName = strtoupperFr($userName);
	}


	$subtitle = $news["createdAt"];
	$subtitle = date('d/m/Y H:i', strtotime($subtitle));
	$subtitle_upcase = $twitterNews["subtitle"]["upcase"];
	if($subtitle_upcase) $subtitle = strtoupperFr($subtitle);

	$description = cleanupField($news["message"]);
	$description_upcase = $twitterNews["description"]["upcase"];
	if($description_upcase) $description = strtoupperFr($description);
	
	$title_display = $twitterNews["title"]["display"];
	$subtitle_display = $twitterNews["subtitle"]["display"];
	$description_display = $twitterNews["description"]["display"];
	$footer_display = $twitterNews["footer"]["display"];

	//$title = applyMaxLen($title, $twitterNews["title"]);
	$subtitle = applyMaxLen($subtitle, $twitterNews["subtitle"]);
	$description = applyMaxLen($description, $twitterNews["description"]);
	
	$footer = "TWITTER";
	$footer_upcase = $twitterNews["footer"]["upcase"];
	if($footer_upcase) $footer = strtoupperFr($footer);
	
	if($name && $userName && $title_display){

		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $name;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $twitterNews["title"]["font"];
		$size = $twitterNews["title"]["size"];
		$color = $twitterNews["title"]["color"];
		
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
		$fullDescription .=  "     ";

		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $userName;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $twitterNews["title"]["highlight"]["font"];
		$size = $twitterNews["title"]["highlight"]["size"];
		$color = $twitterNews["title"]["highlight"]["color"];
		
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
		$fullDescription .=  "\n";


	}
	
	if($subtitle && $subtitle_display){
		
		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $subtitle;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $twitterNews["subtitle"]["font"];
		$size = $twitterNews["subtitle"]["size"];
		$color = $twitterNews["subtitle"]["color"];
		
		$attributes.=createFontAttribute($from, $to, $font, $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);

		$fullDescription .=  "\n";
		//$fullDescription .=  "\n";
	}
	
	if($description && $description_display){

		$hashtags = locateEntity($description, "#");
		$accounts = locateEntity($description, "@");

		$fromText = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $description;
		$to = mb_strlen($fullDescription, 'utf8');	

		$font = $twitterNews["description"]["font"];
		$size = $twitterNews["description"]["size"];
		$color = $twitterNews["description"]["color"];

		$attributes.=createFontAttribute($fromText, $to, $font, $size);
		$attributes.=createForegroundAttribute($fromText, $to, $color);
		
		$font = $twitterNews["description"]["highlight"]["font"];
		$size = $twitterNews["description"]["highlight"]["size"];
		$color = $twitterNews["description"]["highlight"]["color"];
		
		foreach($hashtags as $from=>$to){
			$attributes.=createFontAttribute($from + $fromText, $to + $fromText, $font, $size);
			$attributes.=createForegroundAttribute($from + $fromText, $to + $fromText, $color);
		}
		foreach($accounts as $from=>$to){
			$attributes.=createFontAttribute($from + $fromText, $to + $fromText, $font, $size);
			$attributes.=createForegroundAttribute($from + $fromText, $to + $fromText, $color);
		}

		$fullDescription .=  "\n";
		//$fullDescription .=  "\n";
	}
	if($footer && $footer_display){
		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $footer;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $twitterNews["footer"]["font"];
		$size = $twitterNews["footer"]["size"];
		$color = $twitterNews["footer"]["color"];
				
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
	}

	
	$newsFormatted["full_description"] = $fullDescription;
	$newsFormatted["full_description_attributes"] = $attributes;
	$newsFormatted["image"] = $image;
}
if($currentNewsKind == FACEBOOK){

	$fullDescription="";
	$attributes="attributes=";	
	$from = 0;
	$to=0;
	
	$image = $news["image"];
	
	$name = cleanupField($news["from"]);
	$title_upcase = $facebookNews["title"]["upcase"];
	if($title_upcase){
		$name = strtoupperFr($name);
	}

	$subtitle = $news["createdAt"];
	$subtitle = date('d/m/Y H:i', strtotime($subtitle));
	$subtitle_upcase = $facebookNews["subtitle"]["upcase"];
	if($subtitle_upcase) $subtitle = strtoupperFr($subtitle);

	$description = cleanupField($news["message"]);
	$description_upcase = $facebookNews["description"]["upcase"];
	if($description_upcase) $description = strtoupperFr($description);
	
	$title_display = $facebookNews["title"]["display"];
	$subtitle_display = $facebookNews["subtitle"]["display"];
	$description_display = $facebookNews["description"]["display"];
	$footer_display = $facebookNews["footer"]["display"];
	
	$name = applyMaxLen($name, $facebookNews["title"]);
	$subtitle = applyMaxLen($subtitle, $facebookNews["subtitle"]);
	$description = applyMaxLen($description, $facebookNews["description"]);
	
	$footer = "FACEBOOK";
	$footer_upcase = $facebookNews["footer"]["upcase"];
	if($footer_upcase) $footer = strtoupperFr($footer);
	
	if($name && $title_display){

		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $name;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $facebookNews["title"]["font"];
		$size = $facebookNews["title"]["size"];
		$color = $facebookNews["title"]["color"];
				
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
	
		$fullDescription .=  "\n";


	}
	
	if($subtitle && $subtitle_display){
		
		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $subtitle;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $facebookNews["subtitle"]["font"];
		$size = $facebookNews["subtitle"]["size"];
		$color = $facebookNews["subtitle"]["color"];
		
		$attributes.=createFontAttribute($from, $to, $font, $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);

		$fullDescription .=  "\n";
		//$fullDescription .=  "\n";
	}
	
	if($description && $description_display){
	
		$hashtags = locateEntity($description, "#");
		$accounts = locateEntity($description, "@");

		$fromText = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $description;
		$to = mb_strlen($fullDescription, 'utf8');
		
		$font = $facebookNews["description"]["font"];
		$size = $facebookNews["description"]["size"];
		$color = $facebookNews["description"]["color"];

		$attributes.=createFontAttribute($fromText, $to, $font, $size);
		$attributes.=createForegroundAttribute($fromText, $to, $color);
				
		$font = $facebookNews["description"]["highlight"]["font"];
		$size = $facebookNews["description"]["highlight"]["size"];
		$color = $facebookNews["description"]["highlight"]["color"];		
		
		foreach($hashtags as $from=>$to){
			$attributes.=createFontAttribute($from + $fromText, $to + $fromText, $font, $size);
			$attributes.=createForegroundAttribute($from + $fromText, $to + $fromText, $color);
		}
		foreach($accounts as $from=>$to){
			$attributes.=createFontAttribute($from + $fromText, $to + $fromText, $font, $size);
			$attributes.=createForegroundAttribute($from + $fromText, $to + $fromText, $color);
		}

		$fullDescription .=  "\n";
		//$fullDescription .=  "\n";
	}
	if($footer && $footer_display){
		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $footer;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $facebookNews["footer"]["font"];
		$size = $facebookNews["footer"]["size"];
		$color = $facebookNews["footer"]["color"];
				
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
	}

	
	$newsFormatted["full_description"] = $fullDescription;
	$newsFormatted["full_description_attributes"] = $attributes;
	$newsFormatted["image"] = $image;
}

//gets live status
$scenarioLive = $scenario["data"]["live"];
$liveFormatted = array();
if($liveMeta){
	
	$fullDescription="";
	$attributes="attributes=";	
	$from = 0;
	$to=0;
		
	$title = cleanupField($liveMeta["liveTitle"]);
	$title_upcase = $scenarioLive["title"]["upcase"];
	if($title_upcase) $title = strtoupperFr($title);


	$subtitle = cleanupField($liveMeta["liveSubtitle"]);
	$subtitle_upcase = $scenarioLive["subtitle"]["upcase"];
	if($subtitle_upcase) $subtitle = strtoupperFr($subtitle);

	$description = cleanupField($liveMeta["liveDescription"]);
	$description_upcase = $scenarioLive["description"]["upcase"];
	if($description_upcase) $description = strtoupperFr($description);
	
	$title_display = $scenarioLive["title"]["display"];
	$subtitle_display = $scenarioLive["subtitle"]["display"];
	$description_display = $scenarioLive["description"]["display"];

	$title = applyMaxLen($title, $scenarioLive["title"]);
	$subtitle = applyMaxLen($subtitle, $scenarioLive["subtitle"]);
	$description = applyMaxLen($description, $scenarioLive["description"]);

	
	if($title && $title_display){

		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $title;
		$to = mb_strlen($fullDescription, 'utf8');			
		

		$font = $scenarioLive["title"]["font"];
		$size = $scenarioLive["title"]["size"];
		$color = $scenarioLive["title"]["color"];
		
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
		$fullDescription .=  "\n";
	}
	
	if($subtitle && $subtitle_display){
		
		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $subtitle;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $scenarioLive["subtitle"]["font"];
		$size = $scenarioLive["subtitle"]["size"];
		$color = $scenarioLive["subtitle"]["color"];
		
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);

		$fullDescription .=  "\n";
		$fullDescription .=  "\n";
	}
	
	if($description && $description_display){
	
		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $description;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $scenarioLive["description"]["font"];
		$size = $scenarioLive["description"]["size"];
		$color = $scenarioLive["description"]["color"];
				
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
		$fullDescription .=  "\n";
		$fullDescription .=  "\n";
	}

	
	$liveFormatted["full_description"] = $fullDescription;
	$liveFormatted["full_description_attributes"] = $attributes;
	$liveFormatted["image"] = "";
	
}



//get playlist status
$scenarioPlaylist = $scenario["data"]["playlist"];
$playlistStatus = json_decode(file_get_contents(PLAYLIST_URL),true);
$playlistCurrent = $playlistStatus["current"];
$playlistFormatted = array();
if($playlistCurrent){

	$duration = $playlistCurrent["duration"];
	$info = $playlistCurrent["info"];
	
	$fullDescription="";
	$attributes="attributes=";	
	$from = 0;
	$to=0;
	
	$image = $info["image"];
	
	$title = cleanupField($info["title"]);
	$title_upcase = $scenarioPlaylist["title"]["upcase"];
	if($title_upcase) $title = strtoupperFr($title);


	$subtitle = cleanupField($info["subtitle"]);
	$subtitle_upcase = $scenarioPlaylist["subtitle"]["upcase"];
	if($subtitle_upcase) $subtitle = strtoupperFr($subtitle);

	$description = cleanupField($info["description"]);
	$description_upcase = $scenarioPlaylist["description"]["upcase"];
	if($description_upcase) $description = strtoupperFr($description);
	
	$title_display = $scenarioPlaylist["title"]["display"];
	$subtitle_display = $scenarioPlaylist["subtitle"]["display"];
	$description_display = $scenarioPlaylist["description"]["display"];

	$title = applyMaxLen($title, $scenarioPlaylist["title"]);
	$subtitle = applyMaxLen($subtitle, $scenarioPlaylist["subtitle"]);
	$description = applyMaxLen($description, $scenarioPlaylist["description"]);

	
	if($title && $title_display){

		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $title;
		$to = mb_strlen($fullDescription, 'utf8');			
		

		$font = $scenarioPlaylist["title"]["font"];
		$size = $scenarioPlaylist["title"]["size"];
		$color = $scenarioPlaylist["title"]["color"];
		
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
		$fullDescription .=  "\n";
	}
	
	if($subtitle && $subtitle_display){
		
		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $subtitle;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $scenarioPlaylist["subtitle"]["font"];
		$size = $scenarioPlaylist["subtitle"]["size"];
		$color = $scenarioPlaylist["subtitle"]["color"];
		
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);

		$fullDescription .=  "\n";
	}
	
	if($description && $description_display){
	
		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $description;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $scenarioPlaylist["description"]["font"];
		$size = $scenarioPlaylist["description"]["size"];
		$color = $scenarioPlaylist["description"]["color"];
				
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
		$fullDescription .=  "\n";
		$fullDescription .=  "\n";
	}

	
	$playlistFormatted["full_description"] = $fullDescription;
	$playlistFormatted["full_description_attributes"] = $attributes;
	$playlistFormatted["image"] = $image;
}

//get playlist status
$scenarioPlaylistNext = $scenario["data"]["playlist_next"];
$playlistNextStatus = json_decode(file_get_contents(PLAYLIST_URL),true);
$playlistNextCurrent = $playlistStatus["next"];
$playlistNextFormatted = array();
if($playlistNextCurrent){

	$remaining = $playlistStatus["current"]["duration"] - $playlistStatus["current"]["elapsed"];
	$remainingMinutes = ceil($remaining / 60000);

	{
		$fullDescription="";
		$attributes="attributes=";	
		$from = 0;
		$to=0;
		
		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $remainingMinutes . "min";
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $scenarioPlaylistNext["title"]["font"];
		$size = $scenarioPlaylistNext["title"]["size"];
		$color = $scenarioPlaylistNext["title"]["color"];
		
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
	
		$playlistNextFormatted["remaining"]["full_description"] = $fullDescription;
		$playlistNextFormatted["remaining"]["full_description_attributes"] = $attributes;
	}

	$info = $playlistNextCurrent["info"];
	
	$fullDescription="";
	$attributes="attributes=";	
	$from = 0;
	$to=0;
	
	$image = $info["image"];
	
	$title = cleanupField($info["title"]);
	$title_upcase = $scenarioPlaylistNext["title"]["upcase"];
	if($title_upcase) $title = strtoupperFr($title);


	$subtitle = cleanupField($info["subtitle"]);
	$subtitle_upcase = $scenarioPlaylistNext["subtitle"]["upcase"];
	if($subtitle_upcase) $subtitle = strtoupperFr($subtitle);

	$description = cleanupField($info["description"]);
	$description_upcase = $scenarioPlaylistNext["description"]["upcase"];
	if($description_upcase) $description = strtoupperFr($description);
	
	$title_display = $scenarioPlaylistNext["title"]["display"];
	$subtitle_display = $scenarioPlaylistNext["subtitle"]["display"];
	$description_display = $scenarioPlaylistNext["description"]["display"];

	$title = applyMaxLen($title, $scenarioPlaylistNext["title"]);
	$subtitle = applyMaxLen($subtitle, $scenarioPlaylistNext["subtitle"]);
	$description = applyMaxLen($description, $scenarioPlaylistNext["description"]);

	
	if($title && $title_display){

		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $title;
		$to = mb_strlen($fullDescription, 'utf8');			
		

		$font = $scenarioPlaylistNext["title"]["font"];
		$size = $scenarioPlaylistNext["title"]["size"];
		$color = $scenarioPlaylistNext["title"]["color"];
		
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
		$fullDescription .=  "\n";
	}
	
	if($subtitle && $subtitle_display){
		
		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $subtitle;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $scenarioPlaylistNext["subtitle"]["font"];
		$size = $scenarioPlaylistNext["subtitle"]["size"];
		$color = $scenarioPlaylistNext["subtitle"]["color"];
		
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);

		$fullDescription .=  "\n";
	}
	
	if($description && $description_display){
	
		$from = mb_strlen($fullDescription, 'utf8');
		$fullDescription .=  $description;
		$to = mb_strlen($fullDescription, 'utf8');			
		
		$font = $scenarioPlaylistNext["description"]["font"];
		$size = $scenarioPlaylistNext["description"]["size"];
		$color = $scenarioPlaylistNext["description"]["color"];
				
		$attributes.=createFontAttribute($from, $to, $font,  $size);	
		$attributes.=createForegroundAttribute($from, $to, $color);
		$fullDescription .=  "\n";
		$fullDescription .=  "\n";
	}

	
	$playlistNextFormatted["full_description"] = $fullDescription;
	$playlistNextFormatted["full_description_attributes"] = $attributes;
	$playlistNextFormatted["image"] = $image;
}


//looks for no image adjustements
$finalNewsPosition = $scenario["data"]["elements"][NEWS_ELEMENT_ID]["position"];
$finalNewsSize = $scenario["data"]["elements"][NEWS_ELEMENT_ID]["size"];
if(!$newsFormatted["image"] && $scenario["data"]["elements"][NEWS_ELEMENT_ID]["no_image"]){
	$finalNewsPosition = $scenario["data"]["elements"][NEWS_ELEMENT_ID]["no_image"]["position"];
	$finalNewsSize = $scenario["data"]["elements"][NEWS_ELEMENT_ID]["no_image"]["size"];	
}

$scenario["data"]["elements"][NEWS_ELEMENT_ID]["final_position"] = $finalNewsPosition;
$scenario["data"]["elements"][NEWS_ELEMENT_ID]["final_size"] = $finalNewsSize;

//performs autofit
if(AUTOFIT){
	
	$clockDimension = $scenario["data"]["elements"][CLOCK_ELEMENT_ID]["size"];
	$fitDataClock = fits(
		$clockFormatted["full_description"], 
		$clockFormatted["full_description_attributes"], 
		LEFT_ALIGN, 
		$clockDimension, 
		true, //auto-adjust 
		AUTOFIT_MIN_SIZE, 
		AUTOFIT_MAX_SIZE, 
		DEFAULT_FONT, 
		DEFAULT_FONT_SIZE);
		
	$clockFits = $fitDataClock["result"][AUTOFIT_LABEL]["fits"];
	$clockFormatted["autofit"] = $fitDataClock;
	if($clockFits){
		$clockFormatted["full_description_attributes"] = $fitDataClock["result"][AUTOFIT_LABEL]["outAttributes"];
	}
	
	$showTitleDimension = $scenario["data"]["elements"][SHOW_TITLE_ELEMENT_ID]["size"];
	$fitDataShowTitle = fits(
		$showTitleFormatted["full_description"], 
		$showTitleFormatted["full_description_attributes"], 
		LEFT_ALIGN, 
		$showTitleDimension, 
		true, //auto-adjust 
		AUTOFIT_MIN_SIZE, 
		AUTOFIT_MAX_SIZE, 
		DEFAULT_FONT, 
		DEFAULT_FONT_SIZE);
		
	$showTitleFits = $fitDataShowTitle["result"][AUTOFIT_LABEL]["fits"];
	$showTitleFormatted["autofit"] = $fitDataShowTitle;
	if($showTitleFits){
		$showTitleFormatted["full_description_attributes"] = $fitDataShowTitle["result"][AUTOFIT_LABEL]["outAttributes"];
	}	
	
	$newsDimension = $finalNewsSize;

	$fitDataNews = fits(
		$newsFormatted["full_description"], 
		$newsFormatted["full_description_attributes"], 
		LEFT_ALIGN, 
		$newsDimension, 
		true, //auto-adjust 
		AUTOFIT_MIN_SIZE, 
		AUTOFIT_MAX_SIZE, 
		DEFAULT_FONT, 
		DEFAULT_FONT_SIZE);
		
	$newsFits = $fitDataNews["result"][AUTOFIT_LABEL]["fits"];
	$newsFormatted["autofit"] = $fitDataNews;
	if($newsFits){
		$newsFormatted["full_description_attributes"] = $fitDataNews["result"][AUTOFIT_LABEL]["outAttributes"];
	}
	
	$playlistDimension = $scenario["data"]["elements"][PLAYLIST_ELEMENT_ID]["size"];

	$fitDataPlaylist = fits(
		$playlistFormatted["full_description"], 
		$playlistFormatted["full_description_attributes"], 
		LEFT_ALIGN, 
		$playlistDimension, 
		true, //auto-adjust 
		AUTOFIT_MIN_SIZE, 
		AUTOFIT_MAX_SIZE, 
		DEFAULT_FONT, 
		DEFAULT_FONT_SIZE);
		
	$playlistFits = $fitDataPlaylist["result"][AUTOFIT_LABEL]["fits"];
	$playlistFormatted["autofit"] = $fitDataPlaylist;
	if($playlistFits){
		$playlistFormatted["full_description_attributes"] = $fitDataPlaylist["result"][AUTOFIT_LABEL]["outAttributes"];
	}
	
	$playlistNextDimension = $scenario["data"]["elements"][PLAYLIST_NEXT_ELEMENT_ID]["size"];

	$fitDataPlaylistNext = fits(
		$playlistNextFormatted["full_description"], 
		$playlistNextFormatted["full_description_attributes"], 
		LEFT_ALIGN, 
		$playlistNextDimension, 
		true, //auto-adjust 
		AUTOFIT_MIN_SIZE, 
		AUTOFIT_MAX_SIZE, 
		DEFAULT_FONT, 
		DEFAULT_FONT_SIZE);
		
	$playlistNextFits = $fitDataPlaylistNext["result"][AUTOFIT_LABEL]["fits"];
	$playlistNextFormatted["autofit"] = $fitDataPlaylistNext;
	if($playlistNextFits){
		$playlistNextFormatted["full_description_attributes"] = $fitDataPlaylistNext["result"][AUTOFIT_LABEL]["outAttributes"];
	}
	
	$liveDimension = $scenario["data"]["elements"][PLAYLIST_ELEMENT_ID]["size"];

	$fitDataLive = fits(
		$liveFormatted["full_description"], 
		$liveFormatted["full_description_attributes"], 
		LEFT_ALIGN, 
		$liveDimension, 
		true, //auto-adjust 
		AUTOFIT_MIN_SIZE, 
		AUTOFIT_MAX_SIZE, 
		DEFAULT_FONT, 
		DEFAULT_FONT_SIZE);
		
	$liveFits = $fitDataLive["result"][AUTOFIT_LABEL]["fits"];
	$liveFormatted["autofit"] = $fitDataLive;
	if($liveFits){
		$liveFormatted["full_description_attributes"] = $fitDataLive["result"][AUTOFIT_LABEL]["outAttributes"];
	}
	
}

$playlistOrLive = $source == 0 ? $playlistFormatted : $liveFormatted;

//will hide coming next if source is live
$comingNextLiveImageVisible = $scenario["data"]["elements"][COMING_NEXT_LIVE_ELEMENT_ID]["visible"];
if($comingNextLiveImageVisible == "true" && $source == 1){
	$comingNextLiveImageVisible = true;
}else{
	$comingNextLiveImageVisible = false;
}
$scenario["data"]["elements"][COMING_NEXT_LIVE_ELEMENT_ID]["visible"] = $comingNextLiveImageVisible;


//handles live hider
$liveVisible = $scenario["data"]["elements"][LIVE_ELEMENT_ID]["visible"] ? true : false;
$switchTime = $liveHider["switchTime"];
$now = time();
$elapsedSinceSwitch = $now - $switchTime;
$liveHider["elapsedSinceSwitch"] = $elapsedSinceSwitch;
/* does not work from now*/
/*
  if($liveHider["mode"] != 0){	
	if($elapsedSinceSwitch < ($source == 0 ? 2 : 5)){
		$liveVisible = false;
	} else {
		$liveVisible = true;
		$liveHider["visible"] = false;
	}
}
* */
//FIXME
$liveVisible = true;
$liveHider["visible"] = false;



$result = array(
	//"playlist"=>$playlistStatus, 
	"title"=>$showTitle, 
	"showTitle_formatted"=>$showTitleFormatted,
	"clock_formatted"=>$clockFormatted,
	//"news"=>$news, 
	"scenario"=>$scenario, 
	"news_formatted"=>$newsFormatted, 
	"playlist_formatted"=>$playlistFormatted, 
	"playlist_next_formatted"=>$playlistNextFormatted,
	"live_formatted"=>$liveFormatted, 
	"playlistOrLive"=>$playlistOrLive,
	"liveHider"=>$liveHider,
	"liveVisible"=>$liveVisible
	);

if(defined("JSON_PRETTY_PRINT") && defined("JSON_UNESCAPED_UNICODE"))
	echo(json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
else
	echo(json_encode($result));
	

/**
 * MISC METHODS
 */

function strtoupperFr($string) {
	if(!$string) return $string;
   $string = strtoupper($string);
   $string = str_replace(
      array('é', 'è', 'ê', 'ë', 'à', 'â', 'î', 'ï', 'ô', 'ù', 'û', 'ç'),
      array('É', 'È', 'Ê', 'Ë', 'À', 'Â', 'Î', 'Ï', 'Ô', 'Ù', 'Û', 'Ç'),
      $string
   );
   return $string;
}

function createFontAttribute($from, $to, $font, $size){
	$result = "font:$from:$to:$font:$size::";
	return $result;
}

function createForegroundAttribute($from, $to, $color){
	
	$result = "foreground:$from:$to:$color::";
	return $result;

}

function locateEntity($text, $entity){
	$result = array();
	$words = preg_split("/\s+/", $text);
	$newText = "";
	$idxFrom=0;
	$idxTo=0;
	foreach($words as $word){
		$len = mb_strlen($newText, 'utf8');
		if($len > 0){
			$newText .= " ";
			$len = mb_strlen($newText, 'utf8');
		}
		$idxFrom = $len;
		$newText .= $word;
		if (mb_strpos($word, $entity, 0, "utf-8") !== false) {
			$entityPos = mb_strpos($word, $entity, 0, "utf-8");
			$idxFrom = $len + $entityPos;
			if (preg_match_all('/[\p{L}\p{N}_]/u', $word, $matches, PREG_OFFSET_CAPTURE) > 0) {			
				$wLen = 0;
				foreach($matches[0] as $match){
					$wLen += mb_strlen($match[0], 'utf8');
				}				
				$idxTo = $len + $wLen + 1 + $entityPos;
			}else{
				$idxTo = mb_strlen($newText, 'utf8');
			}			
			$result[$idxFrom] = $idxTo;
		}
	}
	return $result;
}

function urlCleaner($url) {
//	echo "\ntext: " .$url;
	$clean = preg_replace('/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', '', $url);   
//	echo "\nclean: " . $clean;
	return trim($clean);
}

function removeEmoji($text) {

    $clean_text = "";

    // Match Emoticons
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clean_text = preg_replace($regexEmoticons, ' ', $text);

    // Match Miscellaneous Symbols and Pictographs
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clean_text = preg_replace($regexSymbols, ' ', $clean_text);

    // Match Transport And Map Symbols
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clean_text = preg_replace($regexTransport, ' ', $clean_text);

    // Match Miscellaneous Symbols
    $regexMisc = '/[\x{2600}-\x{26FF}]/u';
    $clean_text = preg_replace($regexMisc, ' ', $clean_text);

    // Match Dingbats
    $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
    $clean_text = preg_replace($regexDingbats, ' ', $clean_text);

    return $clean_text;
}

function removeNonLatin1($field){
	$field = preg_replace('/[^\x20-\x7e^\x0a^\x0d^\xa0-\xff]/u', ' ', $field);	
	return $field;
}

function cleanField($field){
	
	$field = trim($field);
	$field = strip_tags($field);
	$field = urlCleaner($field);
	$field = removeEmoji($field);
	$field = preg_replace('/\s+/', ' ', $field); // needed for entities to be ok
	$field = str_replace("’", "'", $field);
	$field = str_replace('►', ">", $field);
	$field = str_replace('►', ">", $field);
	$field = str_replace('♫', " ", $field);
	$field = str_replace('♪', " ", $field);
	$field = str_replace('…', "...", $field);
	$field = str_replace('&icirc;', "î", $field);
	$field = str_replace('&acirc;', "â", $field);
	$field = str_replace('&ucirc;', "û", $field);
	$field = str_replace('&ecirc;', "ê", $field);
	$field = str_replace("&ocirc;", "ô", $field);
	$field = str_replace('&egrave;', "è", $field);
	$field = str_replace('&agrave;', "à", $field);
	$field = str_replace('&eacute;', "é", $field);
	$field = str_replace('&hellip;', "...", $field);
	$field = str_replace('&amp;', "&", $field);
	$field = str_replace("&#39;", "'", $field);
	$field = str_replace("&#039;", "'", $field);
	$field = str_replace('&rsquo;', "'", $field);
	$field = str_replace('&lsquo;', "'", $field);
	$field = str_replace("&nbsp;", " ", $field);
	$field = str_replace("&quot;", "\"", $field);
	$field = str_replace("&laquo;", "\"", $field);
	$field = str_replace("&raquo;", "\"", $field);
	$field = str_replace("&ldquo;", "\"", $field);
	$field = str_replace("&rdquo;", "\"", $field);
	$field = str_replace("&lsaquo;", "<", $field);
	$field = str_replace("&rsaquo;", ">", $field);
	$field = str_replace("&ccedil;", "ç", $field);
	$field = str_replace("&iuml;", "ï", $field);
	$field = str_replace("&auml;", "ä", $field);
	$field = str_replace("&euml;", "ë", $field);
	$field = str_replace("&ouml;", "ö", $field);
	$field = str_replace("&uuml;", "ü", $field);
	$field = str_replace("&oelig;", "œ", $field);
	$field = str_replace("&ndash;", "-", $field);
	$field = str_replace('&ugrave;', "ù", $field);
	$field = str_replace('’', "'", $field);
	$field = removeNonLatin1($field);
	$field = str_replace("œ", "oe", $field);
	$field = preg_replace('/\s+/', ' ', $field); // needed for entities to be ok
	
	return $field;
}

function applyMaxLen($text, $item){
	$maxLen = isset($item["max_len"]) ? $item["max_len"] : -1;
	if($maxLen>=0){
		return cutString($text, $maxLen);
	}
	return $text;
}

function cutString($string, $maxchars){
//	$string = str_replace("\n", " ", $string);
	$modified = false;
	while(strlen($string)>$maxchars){
		$pos = strrpos($string, " ");
		$pos2 = strrpos($string, "\n");
		if($pos === false && $pos2 === false) break;
		$realpos = max($pos, $pos2);
		$string = substr($string, 0, $realpos);
		$modified = true;
	}

	if($modified) 
		return substr($string, 0, $maxchars) . "...";
	else return $string;
}

function cleanupField($field){
	//$field = utf8_decode($field);
	$field = cleanField($field);
	$field = removeNonLatin1($field);
	$field = removeEmoji($field);
	$field = cleanField($field); //a second time to remove extra spaces
	return $field;
}
	
?> 
