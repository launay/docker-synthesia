<?php

include("liveMetaUtils.php");

header('Content-Type: application/json; charset=utf-8');

$liveMeta = getLiveMeta();

if(defined("JSON_PRETTY_PRINT") && defined("JSON_UNESCAPED_UNICODE"))
	echo(json_encode($liveMeta, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
else
	echo(json_encode($liveMeta));

?>
