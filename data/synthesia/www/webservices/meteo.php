<?php

require_once "simple_html_dom.php";

header('Content-Type: application/json; charset=utf-8');

define("CACHE_FILE", "_meteo_cache.json");
define('CACHE_DURATION_SECONDS', 600);


define("DEFAULT_CITY_CODE", "france/clichy/clichy-55863454");
define("WS_URL_PFX", "https://fr.news.yahoo.com/meteo/");

$cityCode= $_GET['code'];

if(!$cityCode){
	$cityCode = DEFAULT_CITY_CODE;
}

$meteoUrl = WS_URL_PFX . $cityCode;

$data = getMeteoData($meteoUrl);
echo(json_encode($data));

function getMeteoData($meteoUrl){

	$now = time();
	if(file_exists(CACHE_FILE) && ($now - filemtime(CACHE_FILE) < CACHE_DURATION_SECONDS) && filesize(CACHE_FILE) > 100){
		$meteoHtmlData = file_get_contents(CACHE_FILE);
	}
	else{
		$meteoHtmlData = file_get_contents($meteoUrl);
		file_put_contents(CACHE_FILE, $meteoHtmlData);
	}
	
	$meteoData = array();
	$html = str_get_html($meteoHtmlData);
	$tempDiv = $html->find("div[class=temperature]");
	$tempDiv = $tempDiv[0];
	
	$tempMinMax = $tempDiv->find("div[class=high-low]/span[class=Va(m)]");
	$tempMin = $tempMinMax[1];
	$tempMax = $tempMinMax[0];
	$tempMin = (string)$tempMin->plaintext;
	$tempMax = (string)$tempMax->plaintext;

	$tempNow = $tempDiv->find("div[class=now]");
	$tempNow = (string)$tempNow[0]->plaintext;
	$tempNow = preg_replace('/\s+/', '',$tempNow);
	$tempNow = str_replace('CF', '',$tempNow);

	$description = $tempDiv->find("span[class=description]");
	$description = $description[0];
	$description = (string)$description->plaintext;

	$img = $tempDiv->find("img");
	$img = $img[0];
	$img = (string)$img->src;

	
	$meteoData["temp_min"] = trim($tempMin);
	$meteoData["temp_max"] = trim($tempMax);
	$meteoData["temp_minmax"] = "min : " . trim($tempMin) . "\nmax : " . trim($tempMax);

	$meteoData["now"] = trim($tempNow);
	$meteoData["description"] = trim($description);
	$meteoData["image"] = trim($img);

	return $meteoData;	
}


function removeEmoji($text) {

    $clean_text = "";

    // Match Emoticons
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clean_text = preg_replace($regexEmoticons, '', $text);

    // Match Miscellaneous Symbols and Pictographs
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clean_text = preg_replace($regexSymbols, '', $clean_text);

    // Match Transport And Map Symbols
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clean_text = preg_replace($regexTransport, '', $clean_text);

    // Match Miscellaneous Symbols
    $regexMisc = '/[\x{2600}-\x{26FF}]/u';
    $clean_text = preg_replace($regexMisc, '', $clean_text);

    // Match Dingbats
    $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
    $clean_text = preg_replace($regexDingbats, '', $clean_text);

    return $clean_text;
}

function cleanField($field){
	
	$field = strip_tags($field);
	$field = preg_replace('!\s+!', ' ', $field);
	$field = str_replace("\n", " ", $field);
	
	$field = str_replace('►', ">", $field);
	$field = str_replace('♫', " ", $field);
	$field = str_replace('♪', " ", $field);
	$field = str_replace('…', "...", $field);
	$field = str_replace('&icirc;', "î", $field);
	$field = str_replace('&acirc;', "â", $field);
	$field = str_replace('&ucirc;', "û", $field);
	$field = str_replace('&ecirc;', "ê", $field);
	$field = str_replace("&ocirc;", "ô", $field);
	$field = str_replace('&egrave;', "è", $field);
	$field = str_replace('&agrave;', "à", $field);
	$field = str_replace('&eacute;', "é", $field);
	$field = str_replace('&hellip;', "...", $field);
	$field = str_replace('&amp;', "&", $field);
	$field = str_replace("&#39;", "'", $field);
	$field = str_replace("&#039;", "'", $field);
	$field = str_replace('&rsquo;', "'", $field);
	$field = str_replace('&lsquo;', "'", $field);
	$field = str_replace("&nbsp;", " ", $field);
	$field = str_replace("&quot;", "\"", $field);
	$field = str_replace("&laquo;", "\"", $field);
	$field = str_replace("&raquo;", "\"", $field);
	$field = str_replace("&ldquo;", "\"", $field);
	$field = str_replace("&rdquo;", "\"", $field);
	$field = str_replace("&lsaquo;", "<", $field);
	$field = str_replace("&rsaquo;", ">", $field);
	$field = str_replace("&ccedil;", "ç", $field);
	$field = str_replace("&iuml;", "ï", $field);
	$field = str_replace("&auml;", "ä", $field);
	$field = str_replace("&euml;", "ë", $field);
	$field = str_replace("&ouml;", "ö", $field);
	$field = str_replace("&uuml;", "ü", $field);
	$field = str_replace("&oelig;", "œ", $field);
	$field = str_replace("&ndash;", "–", $field);
	$field = str_replace('&ugrave;', "ù", $field);

	
	return $field;
}


function cleaner($url) {
	return $url;
//	echo "\ntext: " .$url;
//	$clean = preg_replace('/\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i', "\n", $url);
	//echo "\nclean: " . $clean;
	//echo "\ntrimmed: " . trim($clean);

	return trim($clean);
}

?> 
