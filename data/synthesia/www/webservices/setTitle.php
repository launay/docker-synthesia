<?php

define("DEFAULT_TITLE", "Synthesia");
define("TITLE_FILE", "_showTitle.json");

$showTitle = DEFAULT_TITLE;

//first reads from input config
if(file_exists(TITLE_FILE)){
	$showTitleJson = file_get_contents(TITLE_FILE);
	if($showTitleJson){
		$showTitleData = json_decode($showTitleJson, true);
		if($showTitleData){
			$showTitle = $showTitleData["showTitle"];
		}
	}
}

if(isset($_GET["showTitle"])){	$showTitle = trim($_GET["showTitle"]);}

$result = array(
	"showTitle" => $showTitle,
);

$resultJson = json_encode($result);
file_put_contents(TITLE_FILE, $resultJson);

?>
