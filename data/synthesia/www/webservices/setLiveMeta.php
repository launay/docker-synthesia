<?php

include("liveMetaUtils.php");

$liveTitle = LIVE_TITLE_DEFAULT;
$liveSubtitle = LIVE_SUBTITLE_DEFAULT;
$liveDescription = LIVE_DESCRIPTION_DEFAULT;

if(isset($_GET["liveTitle"])){
	$liveTitle = trim($_GET["liveTitle"]);
}
if(isset($_GET["liveSubtitle"])){
	$liveSubtitle = trim($_GET["liveSubtitle"]);
}
if(isset($_GET["liveDescription"])){
	$liveDescription = trim($_GET["liveDescription"]);
}

setLiveMeta($liveTitle, $liveSubtitle, $liveDescription);


?>
