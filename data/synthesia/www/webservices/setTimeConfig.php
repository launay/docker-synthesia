<?php

include('timeUtils.php');

$timezone = DEFAULT_TIMEZONE;
$timeDisplayMode = DEFAULT_TIME_DISPLAY_MODE;
$hourFormat = DEFAULT_HOUR_FORMAT;

if(isset($_GET["timezone"])){
	$timezone = trim($_GET["timezone"]);
}
if(isset($_GET["timeDisplayMode"])){
	$timeDisplayMode = trim($_GET["timeDisplayMode"]);
}
if(isset($_GET["hourFormat"])){
	$hourFormat = trim($_GET["hourFormat"]);
}

setTimeConfig($timezone, $timeDisplayMode, $hourFormat);


?>
