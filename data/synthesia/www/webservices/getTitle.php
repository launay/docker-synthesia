<?php

define("DEFAULT_TITLE", "Synthesia");
define("TITLE_FILE", "_showTitle.json");

$showTitle = DEFAULT_TITLE;

//first reads from input config
if(file_exists(TITLE_FILE)){
	$showTitleJson = file_get_contents(TITLE_FILE);
	if($showTitleJson){
		$showTitleData = json_decode($showTitleJson, true);
		if($showTitleData){
			$showTitle = $showTitleData["showTitle"];
		}
	}
}

$result = array(
	"showTitle" => $showTitle,
);

if(defined("JSON_PRETTY_PRINT") && defined("JSON_UNESCAPED_UNICODE"))
	echo(json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
else
	echo(json_encode($result));

?>
