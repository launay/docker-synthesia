<?php

error_reporting(E_ERROR | E_PARSE);

require_once "php/info.php";

define("MAX_EXEC_TIME", 600);
define("MAX_ATTEMPTS", 10);

define("API_URL_PFX", "https://api.dailymotion.com/user/");
define("API_URL_SFX", "/videos?");
define("DEFAULT_CHANNEL_ID", "franceinter");
define("DEFAULT_HOURS", 4);
define("DEFAULT_MAX_DURATION", 300);
define("DEFAULT_ADD_TO_PLAYLIST", 1);
define("DEFAULT_AUTOREMOVE", 1);

define("DOWNLOAD_FOLDER", "dm_import/");
define("CACHE_FILE_PFX", DOWNLOAD_FOLDER . "_dm_cache_");
define("CACHE_DURATION", 300);
define("CACHE_VALID_SIZE", 500);

define("WATCH_VIDEO_PFX", "http://www.dailymotion.com/video/");

define("LAST_DL_FILE", DOWNLOAD_FOLDER . "_last_dl_time");
define("LAST_PARAMS_FILE", DOWNLOAD_FOLDER . "_last_params");

define("ONE_DAY", 24*3600);


$hours = DEFAULT_HOURS;
$maxDuration = DEFAULT_MAX_DURATION;
$channelId = DEFAULT_CHANNEL_ID;
$addToPlaylist = DEFAULT_ADD_TO_PLAYLIST;
$autoRemove = DEFAULT_ADD_TO_PLAYLIST;
if(isset($_GET["addToPlaylist"])){
	$addToPlaylist = $_GET["addToPlaylist"];
}
if(isset($_GET["autoRemove"])){
	$autoRemove = $_GET["autoRemove"];
}
if(isset($_GET["channelId"])){
	$channelId = $_GET["channelId"];
}
if(isset($_GET["maxDuration"])){
	$maxDuration = $_GET["maxDuration"];
}
if(isset($_GET["hours"])){
	$hours = $_GET["hours"];
}

$params = $hours . "_" . $maxDuration . "_" . $channelId . "_" . $addToPlaylist . "_" . $autoRemove;
$paramsChanged = true;
if(file_exists(LAST_PARAMS_FILE)){
	$lastParams = file_get_contents(LAST_PARAMS_FILE);
	if($lastParams == $params){
		$paramsChanged = false;
	}
}
if($paramsChanged){
	file_put_contents(LAST_PARAMS_FILE, $params);
	unlink(LAST_DL_FILE);

}

$now = time();
$from = time();
$from -= 3600*$hours;

$url = buildUrl($channelId, $from);
echo "Primary URL: " . $url . "\n";


$cacheFile = CACHE_FILE_PFX . $channelId . ".json";
$cacheIsValid = !$paramsChanged && file_exists($cacheFile) && ($now - filemtime($cacheFile) < CACHE_DURATION) && filesize($cacheFile) > CACHE_VALID_SIZE;
if(!$cacheIsValid){
	echo "renewing dm cache\n";
	$pageData = file_get_contents($url);
	file_put_contents($cacheFile, $pageData);
	
}

$dmData = file_get_contents($cacheFile);
$dmDataJson = json_decode($dmData, true);
$items = $dmDataJson["list"];

$items = array_reverse($items); //oldest elements first

$itemInfos = array();

foreach($items as $item){
			
	$videoId = $item["id"];
	$title = cleanField($item["title"]);
	$description = cleanField($item["description"]);
	$image = $item["thumbnail_url"];
	$duration = $item["duration"];
	$publishTime = $item["updated_time"];
	$subtitle=date("d/m/Y", $publishTime);

	$itemInfo = array(
		"videoId" => $videoId,
		"title" => $title,
		"description" => $description,
		"subtitle" => $subtitle,
		"image" => $image,
		"duration" => $duration,
		"publishTime" => $publishTime,
	);
	
	array_push($itemInfos, $itemInfo);
	
	$lastPubTime = 0;
	if(file_exists(LAST_DL_FILE)){
		$lastPubTime = intval(file_get_contents(LAST_DL_FILE));
	}

	downloadVideo($itemInfo, $lastPubTime, $maxDuration);
	
	
	//DEBUG
	//break;
}

//DEBUG
//print_r($itemInfos);
//return;

if($addToPlaylist){

	//views all items to update playlist
	$plFiles = getPlaylistFiles(CURRENT_PLAYLIST_FILE);
	$newPlFiles = array();

	//echo "Old playlist: \n";
	//print_r($plFiles);

	//echo "New item infos:\n";
	//print_r($itemInfos);

	foreach($itemInfos as $itemInfo){
		$fileName = getVideoName($itemInfo);
		//echo $fileName . "\n";
		if(checkName($fileName)){
			array_push($newPlFiles, $fileName);
		}
	}
	echo "New playlist: \n";
	print_r($newPlFiles);

	//check if playlist has changed
	if(0 == count(array_diff($plFiles, $newPlFiles)) && 0 == count(array_diff($newPlFiles, $plFiles))){
		echo "Playlist unchanged. Nothing to do.\n";
	}
	else{
		setCurrentPlaylist($newPlFiles);
		echo "Playlist updated.\n";
	}
}

if($autoRemove){
	//remove old files
	$allVideos = getAllVideos();
	//print_r($allVideos);
	$limit = time() - ( $hours + 1 ) * 3600 - $maxDuration;
	foreach($allVideos as $guid=>$video){
		$fileLocation = MASTER_FOLDER . $guid;
		if(file_exists($fileLocation)){
			$fileTime = $video["date"];
			$source = $video["source"];
			if($source != SOURCE_DM_IMPORT) continue;
			//echo "$guid -> $fileTime / $limit \n";
			//echo "time: " . time() . "\n";
			if($fileTime < $limit){
				$fName = $guid;
				//echo "$fName \n";
				if(!isInPlaylist($fName, CURRENT_PLAYLIST_FILE)){
					echo "Removing too old file: $guid \n";
					removeVideo($guid);
				}
			}
		}
		
	}
}

/**
 * Utils
 */

function getVideoName($itemInfo){
	$publishTime = $itemInfo["publishTime"];
	$namePfx =  date("Y/m/d_H_i_s_", $publishTime);
	$videoId = $itemInfo["videoId"];
	$fileName = preg_replace('/[^A-Za-z0-9_\-\.]/', '_', $namePfx . $videoId) . ".mp4";
	return $fileName;
}

function downloadVideo($itemInfo, $lastPubTime, $maxDuration){


	$publishTime = $itemInfo["publishTime"];
	$namePfx =  date("Y/m/d_H_i_s_", $publishTime);

	$videoId = $itemInfo["videoId"];

	$duration = $itemInfo["duration"];
	$dlLimit = max($duration + MAX_EXEC_TIME, $duration);
	//echo "Set time limit to $dlLimit \n";
	set_time_limit($dlLimit);

	//print_r($itemInfo);	
	echo "Handling item: $videoId \n";

	$duration = $itemInfo["duration"];
	if($duration > $maxDuration){
		echo "Ignoring too long video: $videoId $duration > $maxDuration \n";
		return;
	}

	if($lastPubTime >= $publishTime){
		echo "Ignoring video published after last stored time: $publishTime <= $lastPubTime \n";
		return;
	}
	
	$fileName = getVideoName($itemInfo);
	$targetFile = DOWNLOAD_FOLDER . $fileName;
	$targetFileTmp = $targetFile . ".tmp";
	
	//check if exists in DB
	if(checkName($fileName)){
		echo "Local file '" . $videoId . "' already exists.\n";
		return;
	}
	
	echo "downloading from video URL: " . $videoId . "\n";
		
	//downloads
	$attempts = 0;
	while($attempts < MAX_ATTEMPTS){
		$attempts++;
	
		$dmData = file_get_contents(WATCH_VIDEO_PFX . $videoId);
		$REG_EXP = "/{\"type\":\"video\\\\\/mp4\",\"url\":\"([^\"]*)\"}/";

		$ret = preg_match_all($REG_EXP, $dmData, $matches, PREG_PATTERN_ORDER);

		$dmVideoUrl = null;
		if($ret){
			$matches1 = $matches[1];
			if($matches1 && count($matches1)>0){
				$lastUrl = $matches1[count($matches1)-1];
				$dmVideoUrl = str_replace('\\', '', $lastUrl);
			}
		}
		
		//debug
		//return;
	
		echo "Attempt $attempts to download $videoId \n";
		unlink($targetFile);
		unlink($targetFileTmp);
		$src = fopen($dmVideoUrl, 'r');
		$dest = fopen($targetFileTmp, 'w');
		
		$written = stream_copy_to_stream($src, $dest);
		if($written < 1024*512){
			unlink($targetFileTmp);
			unlink($targetFile);
			echo "Download failed for: " . $dmVideoUrl . "\n";
			continue;
		}
		
		chmod($targetFileTmp, 0777);
		rename($targetFileTmp, $targetFile);
		chmod($targetFile, 0777);
		unlink($targetFileTmp);

		echo "Download succeeded for: " . $dmVideoUrl . "\n";
		
		//handles upload
		
		$toAdd = $fileName;
		$toAddTmp = $targetFile;
		$isUpload = false;
		$guid = $toAdd;
		
		$error = false;
		$error = !addVideo($toAdd, $toAddTmp, $itemInfo["image"], $itemInfo["title"], $itemInfo["subtitle"], $itemInfo["description"], $isUpload, $publishTime, SOURCE_DM_IMPORT);
		if($error){
			echo "Impossible to import video $toAdd \n";
			return;
		}
		$message = "Video $toAdd was succesfully imported into DB.\n";

		unlink($toAddTmp);
								
		//handles profiles
		$PROFILES = getAllProfiles();
		$profiles = array();
		$currentProfile = getCurrentProfile();
		$currentProfile = str_replace(".", "_", $currentProfile);

		foreach($PROFILES as $profile){
			$profile2 = str_replace(".", "_", $profile);	
			if($profile2 == $currentProfile){
				$profiles[$profile] = "1";
				break;
			}
		}

		if(!updateProfiles($guid, $profiles)){
			echo "Impossible to update profiles for video $toAdd \n";
			return;
		}

		//OK, updates last dl time
		file_put_contents(LAST_DL_FILE, $publishTime);

		break;

		
	}
	

}


function buildUrl($channelId, $fromDate=0, $maxResults=50){
	return API_URL_PFX . $channelId . API_URL_SFX . "limit=" . $maxResults . "&fields=duration,thumbnail_url,updated_time,title,id,url,description&created_after=" . $fromDate;
}

function cleanField($field){
	$field = strip_tags($field);
	$field = html_entity_decode($field);
	$field = str_replace('&icirc;', "î", $field);
	$field = str_replace('&acirc;', "â", $field);
	$field = str_replace('&ucirc;', "û", $field);
	$field = str_replace('&ecirc;', "ê", $field);
	$field = str_replace("&ocirc;", "ô", $field);
	
	$field = str_replace('&egrave;', "è", $field);
	$field = str_replace('&agrave;', "à", $field);

	$field = str_replace('&eacute;', "é", $field);
	
	$field = str_replace('&hellip;', "...", $field);
	
	$field = str_replace('&amp;', "&", $field);
	$field = str_replace("&#39;", "'", $field);
	$field = str_replace("&#039;", "'", $field);
	
	$field = str_replace('&rsquo;', "'", $field);
	$field = str_replace('&lsquo;', "'", $field);

	$field = str_replace("&nbsp;", " ", $field);

	$field = str_replace("&quot;", "\"", $field);
	$field = str_replace("&laquo;", "\"", $field);
	$field = str_replace("&raquo;", "\"", $field);
	$field = str_replace("&ldquo;", "\"", $field);
	$field = str_replace("&rdquo;", "\"", $field);

	$field = str_replace("&lsaquo;", "<", $field);
	$field = str_replace("&rsaquo;", ">", $field);

	$field = str_replace("&ccedil;", "ç", $field);



	
	$field = str_replace("&iuml;", "ï", $field);
	$field = str_replace("&auml;", "ä", $field);
	$field = str_replace("&euml;", "ë", $field);
	$field = str_replace("&ouml;", "ö", $field);
	$field = str_replace("&uuml;", "ü", $field);

	$field = str_replace("&oelig;", "œ", $field);
	$field = str_replace("&ndash;", "–", $field);

	$field = str_replace('&ugrave;', "ù", $field);

	
	return trim($field);
}

?>
