<?php
require_once "php/info.php";

define("KIND_DM", "DM");
define("KIND_YT", "YT");
define("DEFAULT_CHANNEL", "franceinter");
define("DEFAULT_HOURS", 24);
define("DEFAULT_MAXDURATION", 300);
define("DEFAULT_ACTIVE", 0);
define("DEFAULT_ADDTOPLAYLIST", 1);
define("DEFAULT_AUTOREMOVE", 1);

$logs= file_get_contents("../log/import.log.0");
if(!$logs) $logs= "No log file found";

$message = "";
$error = false;

         $cmdType = "";
         if(isset($_POST['startImport'])){
                 $cmdType = "start";
         }
         if(isset($_POST['stopImport'])){
                 $cmdType = "stop";
         }

         if($cmdType){
                 $command = 'sudo -u synthesia /home/synthesia/synthesia/common/import/' . $cmdType . 'Import.sh';
                 exec($command, $output, $result);

                 if($result == 0){
                         if($cmdType == "start") $message = "Auto-import was succesfully (re)started";
                         if($cmdType == "stop") $message = "Auto-import was succesfully stopped";
																				$error = false;
                 }
                 else{
                         if($cmdType == "start") $message = "Error while (re)starting auto-import";
                         if($cmdType == "stop") $message = "Error while stopping auto-import";
																				$error = true;
                 }

         }

	$kind = KIND_DM;
	$channel = DEFAULT_CHANNEL;
	$hours = DEFAULT_HOURS;
	$maxDuration = DEFAULT_MAXDURATION;
	$active = DEFAULT_ACTIVE;
	$addToPlaylist = DEFAULT_ADDTOPLAYLIST;
	$autoRemove = DEFAULT_AUTOREMOVE;

	$paramsFile = "../import.properties";
	if(!file_exists($paramsFile)){
		$error = true;
		$message = "Impossible to find auto-import parameters file";
	}
	else{
		$paramsData = file_get_contents($paramsFile);
		if(!$paramsData){
			$error = true;
			$message = "Impossible to read auto-import parameters file";		
		}
		else{
			$params = explode("\n", trim($paramsData));
			$paramsArray = array();
			foreach($params as $param){
				$param = trim($param);
				$keyVal = explode("=", $param,2);
				$key = trim($keyVal[0]);
				$val = trim($keyVal[1]);
				$paramsArray[$key] = $val;
			}
			{
				
				
			if(isset($_POST['updateParameters'])){				
				$paramsArray['kind'] = $_POST['kind'];
				$paramsArray['channel'] = $_POST['channel'];
				$paramsArray['hours'] = intval($_POST['hours']);
				$paramsArray['maxDuration'] = intval($_POST['maxDuration']);
				$paramsArray['active'] = (isset($_POST['active']) && $_POST['active']==1 ) ? 1:0;
				$paramsArray['addToPlaylist'] = (isset($_POST['addToPlaylist']) && $_POST['addToPlaylist']==1 ) ? 1:0;
				$paramsArray['autoRemove'] = (isset($_POST['autoRemove']) && $_POST['autoRemove']==1 ) ? 1:0;
				
				
				//write them back
				$toWrite = "";
				foreach($paramsArray as $k=>$v){
					$toWrite .= "\n" . $k . "=" . $v;
				}
				$toWrite = trim($toWrite);
				$res = file_put_contents($paramsFile, $toWrite);

				if(!$res){
					$error = true;
					$message = "Impossible to update auto-import parameters";
				}else{
					$error = false;
					$message = "Succesfully updated auto-import parameters";
				}

			 }
			 
			$kind = $paramsArray['kind'];
			$channel = $paramsArray['channel'];
			$hours = $paramsArray['hours'];
			$maxDuration = $paramsArray['maxDuration'];
			$active = $paramsArray['active'];
			$addToPlaylist = $paramsArray['addToPlaylist'];
			$autoRemove = $paramsArray['autoRemove'];

			}
		}

	}
	
?>


<html>
	<head>
		<?php createHeader();?>

	</head>

	<body>

		<?php createNavBar("import",""); ?>

<?php
if(strlen($message)>0){
	$class="label label-default";
	if($error)
		$class="label label-danger";
	echo '<div id="errorMsg" class="' . $class . '" style="display:block">' . $message .'</div></br>';
}
?>

<center>
	<div id="pendingON" style="display:none">
		<img class="pendingImage" src="images/wait.gif"/>
		<div class="pendingText">... Please wait while (re)starting auto-import</div>
	</div>	
</center>
<center>
	<div id="pendingOFF" style="display:none">
		<img class="pendingImage" src="images/wait.gif"/>
		<div class="pendingText">... Please wait while stopping auto-import</div>
	</div>	
</center>

<div class="panel panel-default">

	<div class="panel-heading">
	  <h3 class="panel-title">Auto-import commands</h3>
	</div>

	<div class="panel-body">
		<div class="container">
			<table><tr><td>
			 <form method="POST" onsubmit="if (!confirm('Are you sure you want to (re)start auto-import?')) return false; displayPendingON();">
				 <input type="hidden" name="startImport" value="1"></input>
				 <input type="submit" value="(re)start auto-import"></input>
			 </form></td><td>
			 <form method="POST" onsubmit="if (!confirm('Are you sure you want to stop auto-import?')) return false; displayPendingOFF();">
				 <input type="hidden" name="stopImport" value="1"></input>
				 <input type="submit" value="stop auto-import"></input>
			 </form>
		</td></tr></table>
		</div>
	</div>
</div>
<div class="panel panel-default">

	<div class="panel-heading">
	  <h3 class="panel-title">Auto-import parameters</h3>
	</div>

	<div class="panel-body">
		<div class="container">
			 <form method="POST" onsubmit="return confirm('Auto-import will be restarted if configuration is changed.\nProceed anyway?');">
				<input type="hidden" name="updateParameters" value="1"></input>
				<table>
					<tr>
						<td>Active</td>
						<td>
							<input type="checkbox" value="1" name="active" <?php if($active) echo "checked" ;?>></input>
						</td>
					</tr>
					<tr>
						<td>Import kind</td>
						<td>
							<select name="kind" id="kind">
								<option value="<?php echo KIND_DM?>" <?php if($kind == KIND_DM) echo "selected";?>>Dailymotion</option>
								<option value="<?php echo KIND_YT?>" <?php if($kind == KIND_YT) echo "selected";?>>YouTube</option>
  							</select>
						</td>
					</tr>
					<tr>
						<td>Channel ID</td>
						<td>
							<input type="text" name="channel" id="channel" size="12" value="<?php echo htmlspecialchars($channel) ;?>"></input>
							&nbsp;<button id="channelIdSearchButton" onclick="$('#channelIdSearchTR').show();return false;">I can't find this ID</button>
						</td>
					</tr>
					<tr id="channelIdSearchTR" style="display:none">
										<td>&nbsp;</td>
										<td><input type="text" size="32" name="channelUrl" id="channelUrl" placeholder="Paste the page URL"></td>
										<td><button id="channelIdButton" onclick="findChannelId();return false;">find ID</button><button onclick="$('#channelIdSearchTR').hide();return false;">hide</button></td>
									</tr>
					<tr>
						<td>Import videos published within the last</td>
						<td>
							<input type="text" name="hours" size="6" value="<?php echo htmlspecialchars($hours) ;?>"></input>&nbsp;hours
						</td>
					</tr>
					<tr>
						<td>Import videos whose duration is less than</td>
						<td>
							<input type="text" name="maxDuration" size="12" value="<?php echo htmlspecialchars($maxDuration) ;?>"></input>&nbsp;seconds
						</td>
					</tr>
					<tr>
						<td>Update current playlist automatically</td>
						<td>
							<input type="checkbox" value="1" name="addToPlaylist" <?php if($addToPlaylist) echo "checked" ;?>></input>
						</td>
					</tr>
					<tr>
						<td>Remove old video files automatically</td>
						<td>
							<input type="checkbox" value="1" name="autoRemove" <?php if($autoRemove) echo "checked" ;?>></input>
						</td>
					</tr>
					<tr>
						<td><input type="submit" value="Save configuration"></input></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>

<div class="panel panel-default">

	<div class="panel-heading">
	  <h3 class="panel-title">Auto-import Log</h3>
	</div>



	<div class="panel-body">
		<div class="container">
			 <form method="POST">
				 <input type="hidden" name="refresh" value="1"></input>
				 <input type="submit" value="Refresh"></input>
			 </form>
		</div>
		<div class="container">
			<textarea id="logsArea" style="width:100%;height:50%;resize: both;overflow:auto;"><?php echo $logs;?></textarea>
		</div>
	</div>
</div>

<script>
		function displayPendingON(){
			$("#pendingON").show();
		}
		function displayPendingOFF(){
			$("#pendingOFF").show();
		}
		
		setTimeout(function(){
			$("#errorMsg").hide(); 
		}, 5000);
</script>
<script>
	var textarea = document.getElementById('logsArea');
	textarea.scrollTop = textarea.scrollHeight;
</script>
<script>

function findChannelId(){
	var selectedKind = $("#kind").val();
	if(selectedKind == 'YT'){
		findYouTubeId();
	}else{
		findDailymotionId();
	}
}

function findYouTubeId(){
	var channelUrl = $("#channelUrl").val().trim();
	if(channelUrl.length == 0) return;
	
	var wsUrl = "../../webservices/getYouTubeId.php?url=" + encodeURIComponent(channelUrl);
	$.get( wsUrl , function( data ) {
		$('#channel').val(data["id"]);
	});

}
function findDailymotionId(){
	var channelUrl = $("#channelUrl").val().trim();
	if(channelUrl.length == 0) return;
	
	var wsUrl = "../../webservices/getDailymotionId.php?url=" + encodeURIComponent(channelUrl);
	$.get( wsUrl , function( data ) {
		$('#channel').val(data["id"]);
	});

}
</script>
</body>


</html>
