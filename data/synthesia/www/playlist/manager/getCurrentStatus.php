<?php
require_once "php/info.php";

$json = file_get_contents(STATUS_FILE);
$jsonData = json_decode($json,true);

$current = $jsonData["current"];
$duration = $jsonData["duration"];
$elapsed = $jsonData["elapsed"];
$next = $jsonData["next"];

$fullInfo = $jsonData;

$result = array();
$currentData = array();
$nextData = array();

$currentData["duration"] = $duration;
$currentData["elapsed"] = $elapsed;

if($current){
	$currentData["info"] = getMediaData(OUT_FOLDER, $current);
}
if($next){
	$nextData["info"] = getMediaData(OUT_FOLDER, $next);
}

$result["current"] = $currentData;
$result["next"] = $nextData;
$result["full"] = $fullInfo;

echo json_encode($result);

?>
