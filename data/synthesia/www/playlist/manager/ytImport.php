<?php

error_reporting(E_ERROR | E_PARSE);

require_once "php/info.php";

define("MAX_EXEC_TIME", 600);
define("MAX_ATTEMPTS", 10);

define("API_KEY", "AIzaSyABE1q_MY97BYpHfU8xXRMrfJsrB0uQXDA");
define("API_URL", "https://www.googleapis.com/youtube/v3/search");
define("API_URL2", "https://www.googleapis.com/youtube/v3/videos");
define("DEFAULT_CHANNEL_ID", "UCQKmfKnJ9ZpNLGxjt0RiMJA");
define("DEFAULT_HOURS", 24);
define("DEFAULT_MAX_DURATION", 300);
define("DEFAULT_ADD_TO_PLAYLIST", 1);
define("DEFAULT_AUTOREMOVE", 1);

define("DOWNLOAD_FOLDER", "yt_import/");
define("CACHE_FILE_PFX", DOWNLOAD_FOLDER . "_youtube_cache_");
define("CACHE_FILE_PFX_2", DOWNLOAD_FOLDER . "_youtube_cache2_");
define("CACHE_DURATION", 300);
define("CACHE_VALID_SIZE", 500);

define("WATCH_VIDEO_PFX", "https://www.youtube.com/watch?v=");

define("LAST_DL_FILE", DOWNLOAD_FOLDER . "_last_dl_time");
define("LAST_PARAMS_FILE", DOWNLOAD_FOLDER . "_last_params");

define("ONE_DAY", 24*3600);


$hours = DEFAULT_HOURS;
$maxDuration = DEFAULT_MAX_DURATION;
$channelId = DEFAULT_CHANNEL_ID;
$addToPlaylist = DEFAULT_ADD_TO_PLAYLIST;
$autoRemove = DEFAULT_ADD_TO_PLAYLIST;
if(isset($_GET["addToPlaylist"])){
	$addToPlaylist = $_GET["addToPlaylist"];
}
if(isset($_GET["autoRemove"])){
	$autoRemove = $_GET["autoRemove"];
}
if(isset($_GET["channelId"])){
	$channelId = $_GET["channelId"];
}
if(isset($_GET["maxDuration"])){
	$maxDuration = $_GET["maxDuration"];
}
if(isset($_GET["hours"])){
	$hours = $_GET["hours"];
}

$params = $hours . "_" . $maxDuration . "_" . $channelId . "_" . $addToPlaylist . "_" . $autoRemove;
$paramsChanged = true;
if(file_exists(LAST_PARAMS_FILE)){
	$lastParams = file_get_contents(LAST_PARAMS_FILE);
	if($lastParams == $params){
		$paramsChanged = false;
	}
}
if($paramsChanged){
	file_put_contents(LAST_PARAMS_FILE, $params);
	unlink(LAST_DL_FILE);
}

$now = time();
$from = time();
$from -= 3600*$hours;
$fromDate = date(DATE_ATOM, $from);

$url = buildUrl($channelId, urlencode($fromDate));
echo "Primary URL: " . $url . "\n";

$cacheFile = CACHE_FILE_PFX . $channelId . ".json";
$cacheFile2 = CACHE_FILE_PFX_2 . $channelId . ".json";
$cacheIsValid = !$paramsChanged && file_exists($cacheFile) && ($now - filemtime($cacheFile) < CACHE_DURATION) && filesize($cacheFile) > CACHE_VALID_SIZE;
if(!$cacheIsValid){
	echo "renewing youtube cache\n";
	unlink($cacheFile2);
	$pageData = file_get_contents($url);
	file_put_contents($cacheFile, $pageData);
	
}

$youtubeData = file_get_contents($cacheFile);
$youtubeDataJson = json_decode($youtubeData, true);
$items = $youtubeDataJson["items"];

$ids = array();
foreach($items as $item){
	$videoId = $item["id"]["videoId"];
	array_push($ids, $videoId);
}

$ids = join(",", $ids);
echo "Found all ids: " . $ids . "\n";

$url2 = buildUrl2($ids);
echo "Secondary URL: " . $url2 . "\n";

$cacheIsValid2 = file_exists($cacheFile2) && ($now - filemtime($cacheFile2) < CACHE_DURATION) && filesize($cacheFile2) > CACHE_VALID_SIZE;
if(!$cacheIsValid2){
	$pageData = file_get_contents($url2);
	file_put_contents($cacheFile2, $pageData);
}

$youtubeData = file_get_contents($cacheFile2);
$youtubeDataJson = json_decode($youtubeData, true);
$items = $youtubeDataJson["items"];
$items = array_reverse($items); //oldest elements first

$itemInfos = array();

foreach($items as $item){
			
	$videoId = $item["id"];
	$snippet = $item["snippet"];
	$title = $snippet["title"];
	$description = $snippet["description"];
	$publishedAt = $snippet["publishedAt"];
	$thumbnails = $snippet["thumbnails"];
	$image = $thumbnails["high"]["url"];
	if(!$image) $image = $thumbnails["medium"]["url"];
	if(!$image) $image = $thumbnails["default"]["url"];
	
	$contentDetails = $item["contentDetails"];
	$duration = $contentDetails["duration"];
	$interval = new DateInterval($duration);
	$duration = $interval->days*86400 + $interval->h*3600 + $interval->i*60 + $interval->s;
	
	$publishTime = strtotime($publishedAt);
	$date = substr($publishedAt, 0, 10);
	$subtitle = substr($date, 8, 2) . "/" . substr($date, 5, 2) . "/" . substr($date, 0, 4) ;


	$itemInfo = array(
		"videoId" => $videoId,
		"title" => $title,
		"description" => $description,
		"publishedAt" => $publishedAt,
		"subtitle" => $subtitle,
		"image" => $image,
		"duration" => $duration,
		"publishTime" => $publishTime,
	);
	
	array_push($itemInfos, $itemInfo);
	
	$lastPubTime = 0;
	if(file_exists(LAST_DL_FILE)){
		$lastPubTime = intval(file_get_contents(LAST_DL_FILE));
	}
	downloadVideo($itemInfo, $lastPubTime, $maxDuration);
	
	//DEBUG
	//break;
}

if($addToPlaylist){
	//views all items to update playlist
	$plFiles = getPlaylistFiles(CURRENT_PLAYLIST_FILE);
	$newPlFiles = array();

	//echo "Old playlist: \n";
	//print_r($plFiles);

	//echo "New item infos:\n";
	//print_r($itemInfos);

	foreach($itemInfos as $itemInfo){
		$fileName = getVideoName($itemInfo);
		//echo $fileName . "\n";
		if(checkName($fileName)){
			array_push($newPlFiles, $fileName);
		}
	}
	//echo "New playlist: \n";
	//print_r($newPlFiles);

	//check if playlist has changed
	if(0 == count(array_diff($plFiles, $newPlFiles)) && 0 == count(array_diff($newPlFiles, $plFiles))){
		echo "Playlist unchanged. Nothing to do.\n";
	}
	else{
		setCurrentPlaylist($newPlFiles);
		echo "Playlist updated.\n";
	}
}

if($autoRemove){
	//remove old files
	$allVideos = getAllVideos();
	//print_r($allVideos);
	$limit = time() - ( $hours + 1 ) * 3600 - $maxDuration;
	foreach($allVideos as $guid=>$video){
		$fileLocation = MASTER_FOLDER . $guid;
		if(file_exists($fileLocation)){
			$fileTime = $video["date"];
			$source = $video["source"];
			if($source != SOURCE_YT_IMPORT) continue;		
			//echo "$guid -> $fileTime / $limit \n";
			//echo "time: " . time() . "\n";
			if($fileTime < $limit){
				$fName = $guid;
				//echo "$fName \n";
				if(!isInPlaylist($fName, CURRENT_PLAYLIST_FILE)){
					echo "Removing too old file: $guid \n";
					removeVideo($guid);
				}
			}
		}
		
	}
}
/**
 * Utils
 */

function getVideoName($itemInfo){
	$publishTime = $itemInfo["publishTime"];
	$namePfx =  date("Y/m/d_H_i_s_", $publishTime);
	$videoId = $itemInfo["videoId"];
	$fileName = preg_replace('/[^A-Za-z0-9_\-\.]/', '_', $namePfx . $videoId) . ".mp4";
	return $fileName;
}

function downloadVideo($itemInfo, $lastPubTime, $maxDuration){


	$publishTime = $itemInfo["publishTime"];
	$namePfx =  date("Y/m/d_H_i_s_", $publishTime);

	$duration = $itemInfo["duration"];
	$dlLimit = max($duration + MAX_EXEC_TIME, $duration);
	//echo "Set time limit to $dlLimit \n";
	set_time_limit($dlLimit);

	//print_r($itemInfo);	

	$duration = $itemInfo["duration"];
	if($duration > $maxDuration){
		echo "Ignoring too long video: $videoId $duration > $maxDuration \n";
		return;
	}

	if($lastPubTime >= $publishTime){
		echo "Ignoring video published after last stored time: $publishTime <= $lastPubTime \n";
		return;
	}
		
	$videoId = $itemInfo["videoId"];
	$fileName = getVideoName($itemInfo);
	$targetFile = DOWNLOAD_FOLDER . $fileName;
	$targetFileTmp = $targetFile . ".tmp";
	
	//todo: check if exists in DB !!!
	if(checkName($fileName)){
		echo "Local file '" . $videoId . "' already exists.\n";
		return;
	}
	
	echo "downloading from video URL: " . $videoId . "\n";
	//downloads
	$attempts = 0;
	while($attempts < MAX_ATTEMPTS){
		$attempts++;
	
		$ytData = file_get_contents(WATCH_VIDEO_PFX . $videoId);

		$ytVideoUrl = null;
		
		$removeLocalAtEnd = false;

				//first tries with adaptive_fmts to get highest quality
				if(IMPORT_YOUTUBE_HIGHEST_QUALITY){ //bug : only downloads video !

					$pattern = '/"adaptive_fmts":"[^"]*"/';
					preg_match($pattern, $ytData, $matches);
					$urlListDebug = array();
					$urlListMP4_video = array();
					$urlListWEBM_video = array();
					$urlListMP4_audio = array();
					$urlListWEBM_audio = array();
					$urlListUnknown = array();
					$currentData = array();
					$targetList = array();
					if($matches && count($matches)>0){
						$match = $matches[0];
						$match = str_replace('"adaptive_fmts":"', "", $match);

						$urlItems = explode(",", $match);
						foreach($urlItems as $urlItem){
							$urlData = array();
							$items = explode("\\u0026", $urlItem);
							//print_r($items);
							foreach($items as $item){
								$item = trim($item);
								$itemComps = explode("=", $item, 2);
								if(count($itemComps) != 2) continue;
								$key = trim($itemComps[0]);
								$val = trim($itemComps[1]);
								$urlData[$key] = urldecode($val);
								
								array_push($urlListDebug, array($key=>$val));
																
								if("url" == $key ){
									$urlData["videoUrl"] = urldecode($val);
								}

							}
							$type = trim($urlData["type"]);
							
							$isOk = false;
							$isAudio = false;

							//echo "<br/>type is: " . $type ;

							if(startsWith($type, "video/mp4")) {
								$targetList = &$urlListMP4_video;
								$isOk = true;
								$isAudio = false;
							}
							else if(startsWith($type, "audio/mp4")) {
								$targetList = &$urlListMP4_audio;
								$isOk = true;
								$isAudio = true;
							}
							else if(startsWith($type, "video/webm")) {
								$targetList = &$urlListWEBM_video;
								$isOk = true;
								$isAudio = false;

							}
							else if(startsWith($type, "audio/webm")) {
								$targetList = &$urlListWEBM_audio;
								$isOk = true;
								$isAudio = true;
							}
							
							
							if($urlData["url"] 
								&& ($urlData["size"] && !$isAudio) || ($urlData["bitrate"] && $isAudio)
								&& $isOk){
								
								if($isAudio){
									$bitrate = $urlData["bitrate"];
									$targetList[$bitrate] = $urlData;
								}
								else{
									$size = $urlData["size"];
									$sizeComps = explode("x", $size, 2);
									$sizeVal = $sizeComps[0] * $sizeComps[1];
									$targetList[$sizeVal] = $urlData;
								}
								
								krsort($targetList);
							}
							else{
								array_push($urlListUnknown, $urlData);
							}
												
						}
						
					}
					$urlList["unknown"] = $urlListUnknown;
					$urlList["MP4_video"] = $urlListMP4_video;
					$urlList["MP4_audio"] = $urlListMP4_audio;
					$urlList["WEBM_video"] = $urlListWEBM_video;
					$urlList["WEBM_audio"] = $urlListWEBM_audio;
					
					//first will try with MP4
					$mp4Urls = array();
					$webmUrls = array();
					$hasBestMP4 = false;
					$hasBestWebm = false;
					if(count($urlListMP4_video)>0 && count($urlListMP4_audio)>0){
						$bestVideoUrl = array_shift($urlListMP4_video);
						$bestAudioUrl = array_shift($urlListMP4_audio);
						$mp4Urls = array("video"=>$bestVideoUrl["videoUrl"], "audio"=>$bestAudioUrl["videoUrl"]);
						$hasBestMP4 = true;
					}
					if(count($urlListWEBM_video)>0 && count($urlListWEBM_audio)>0){
						$bestVideoUrl = array_shift($urlListWEBM_video);
						$bestAudioUrl = array_shift($urlListWEBM_audio);
						$webmUrls = array("video"=>$bestVideoUrl["videoUrl"], "audio"=>$bestAudioUrl["videoUrl"]);
						$hasBestWebm = true;
					}

					$urlList["MP4_best"] = $mp4Urls;
					$urlList["WEBM_best"] = $webmUrls;
					
					$ytVideoUrlConcat = null;
					
					//will concatenate
					$concatFile = "/tmp/concat-" . uniqid() ;
					if($hasBestMP4){
						$videoFile = $mp4Urls["video"];
						$audioFile = $mp4Urls["audio"];
						$concatFile .= ".mp4";
						
						$videoLocal = "/tmp/yt-video-" . uniqid() . ".mp4" ;
						$audioLocal = "/tmp/yt-audio-" . uniqid() . ".mp4" ;
						
						$audioDLCommand = "wget \"$audioFile\" -O $audioLocal";
						$videoDLCommand = "wget \"$videoFile\" -O $videoLocal";
						
						//echo $audioDLCommand . "<br/><br/><br/><br/><br/>";
						
						$output = array();
						$return = -1;
						exec($audioDLCommand, $output, $return);

						//echo $audioDLCommand . "<br/><br/><br/><br/><br/>";						
						//print_r($output);
						//echo "return code is $return";
						
						if($return == 0){
							$output = array();
							$return = -1;
							exec($videoDLCommand, $output, $return);

							//echo $videoDLCommand . "<br/><br/><br/><br/><br/>";						
							//print_r($output);
							//echo "return code is $return";

							if($return == 0){
								$concatCommand = FFMPEG_EXE . " -y -i \"$videoLocal\" -i \"$audioLocal\" -c copy -f mp4 $concatFile";

								$output = array();
								$return = -1;
								exec($concatCommand, $output, $return);

								//echo $concatCommand . "<br/><br/><br/><br/><br/>";						
								//print_r($output);
								//echo "return code is $return";
								
								if($return == 0){
									$ytVideoUrlConcat = $concatFile;
								}

								
							}

						}
						
						unlink($audioLocal);
						unlink($videoLocal);

					}
					else if($hasBestWebm){
						$videoFile = $webmUrls["video"];
						$audioFile = $webmUrls["audio"];
						$concatFile .= ".webm";
						
						$videoLocal = "/tmp/yt-video-" . uniqid() . ".webm" ;
						$audioLocal = "/tmp/yt-audio-" . uniqid() . ".webm" ;
						
						$audioDLCommand = "wget \"$audioFile\" -O $audioLocal";
						$videoDLCommand = "wget \"$videoFile\" -O $videoLocal";
						
						//echo $audioDLCommand . "<br/><br/><br/><br/><br/>";
						
						$output = array();
						$return = -1;
						exec($audioDLCommand, $output, $return);

						//echo $audioDLCommand . "<br/><br/><br/><br/><br/>";						
						//print_r($output);
						//echo "return code is $return";
						
						if($return == 0){
							$output = array();
							$return = -1;
							exec($videoDLCommand, $output, $return);

							//echo $videoDLCommand . "<br/><br/><br/><br/><br/>";						
							//print_r($output);
							//echo "return code is $return";

							if($return == 0){
								$concatCommand = FFMPEG_EXE . " -y -i \"$videoLocal\" -i \"$audioLocal\" -c copy -f webm $concatFile";

								$output = array();
								$return = -1;
								exec($concatCommand, $output, $return);

								//echo $concatCommand . "<br/><br/><br/><br/><br/>";						
								//print_r($output);
								//echo "return code is $return";
																
								if($return == 0){
									$ytVideoUrlConcat = $concatFile;
								}

							}

						}
						
						unlink($audioLocal);
						unlink($videoLocal);


					}


					if($ytVideoUrlConcat){
						$ytVideoUrl = $ytVideoUrlConcat;
						$removeLocalAtEnd = true;
					}
					//print_r($urlListDebug);
					//print_r($urlList);
					
					
					//DEBUG
					//$error = true;
					//$message = "Just debugging !";
					//return;
				}
	
		
		$initialYtUrl = $ytVideoUrl;
		$pattern = '/"url_encoded_fmt_stream_map":"[^"]*"/';

		//then tries with url_encoded_fmt_stream_map
		if(!$ytVideoUrl){
			preg_match($pattern, $ytData, $matches);
			if($matches && count($matches)>0){
				$match = $matches[0];
				$items = explode("\\u0026", $match);
				//echo $match . "\n";
				//print_r($items);
				foreach($items as $item){
					$item = trim($item);
					$urls = explode("url=", $item);
					if(count($urls)==2){
						$ytVideoUrl = $urls[1];
						$ytVideoUrl = urldecode($ytVideoUrl);
						break;
					}
				}
			}
		}

		if(!$ytVideoUrl){
			echo "No valid video URL was found for this id !\n";
			continue;
		}

		//debug
		//return;
	
		echo "Attempt $attempts to download $videoId \n";
		unlink($targetFile);
		unlink($targetFileTmp);
		$src = fopen($ytVideoUrl, 'r');
		$dest = fopen($targetFileTmp, 'w');
		
		$written = stream_copy_to_stream($src, $dest);
		if($written < 1024*512){
			unlink($targetFileTmp);
			unlink($targetFile);
			echo "Download failed for: " . $ytVideoUrl . "\n";
			continue;
		}
		
		chmod($targetFileTmp, 0777);
		rename($targetFileTmp, $targetFile);
		chmod($targetFile, 0777);
		unlink($targetFileTmp);

		echo "Download succeeded for: " . $ytVideoUrl . "\n";
		
		//then removes local cache if any
		if($removeLocalAtEnd && $initialYtUrl){
			unlink($initialYtUrl);
		}
		
		//handles upload
		
		$toAdd = $fileName;
		$toAddTmp = $targetFile;
		$isUpload = false;
		$guid = $toAdd;
		
		$error = false;
		$error = !addVideo($toAdd, $toAddTmp, $itemInfo["image"], $itemInfo["title"], $itemInfo["subtitle"], $itemInfo["description"], $isUpload, $publishTime, SOURCE_YT_IMPORT);
		if($error){
			echo "Impossible to import video $toAdd \n";
			return;
		}
		$message = "Video $toAdd was succesfully imported into DB.\n";

		unlink($toAddTmp);
								
		//handles profiles
		$PROFILES = getAllProfiles();
		$profiles = array();
		$currentProfile = getCurrentProfile();
		$currentProfile = str_replace(".", "_", $currentProfile);

		foreach($PROFILES as $profile){
			$profile2 = str_replace(".", "_", $profile);	
			if($profile2 == $currentProfile){
				$profiles[$profile] = "1";
				break;
			}
		}

		if(!updateProfiles($guid, $profiles)){
			echo "Impossible to update profiles for video $toAdd \n";
			return;
		}

		//OK, updates last dl time
		file_put_contents(LAST_DL_FILE, $publishTime);

		break;

		
	}
	

}


function buildUrl($channelId, $fromDate="1970-01-01T00:00:00Z", $maxResults=50){
	return API_URL . "?type=video&key=" . API_KEY . "&channelId=" . $channelId . "&order=date&maxResults=" . $maxResults . "&part=id,snippet&publishedAfter=" . $fromDate;
}

function buildUrl2($ids){
	return API_URL2 . "?key=" . API_KEY . "&id=" . $ids . "&part=id,snippet,contentDetails";
}

?>
