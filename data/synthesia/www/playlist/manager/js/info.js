function prettyDuration(duration){
	var totalSec = duration/1000;
	var hours = Math.round(( totalSec / 3600 ) % 24);
	var minutes = Math.round(( totalSec / 60 ) % 60);
	var seconds = Math.round(totalSec) % 60;

	var result = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);

	return result;
}
