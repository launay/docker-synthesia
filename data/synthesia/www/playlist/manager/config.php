<?php
require_once "php/info.php";

$message = "";
$error = false;

         $cmdType = "";
         if(isset($_POST['startPlaylist'])){
                 $cmdType = "start";
         }
         if(isset($_POST['stopPlaylist'])){
                 $cmdType = "stop";
         }

         if($cmdType){
                 $command = 'sudo -u synthesia /home/synthesia/synthesia/common/playlist/' . $cmdType . 'Playlist.sh';
                 exec($command, $output, $result);

                 if($result == 0){
                         if($cmdType == "start") $message = "Playlist manager succesfully (re)started";
                         if($cmdType == "stop") $message = "Playlist manager succesfully stopped";
																				$error = false;
                 }
                 else{
                         if($cmdType == "start") $message = "Error while (re)starting playlist manager";
                         if($cmdType == "stop") $message = "Error while stopping playlist manager";
																				$error = true;
                 }

         }

		$sampling = 48000;
	$fps = 25;
	$host = "localhost";
	$size = "sd";
	$normalizeAudio = false;

	$paramsFile = "../playlist.properties";
	if(!file_exists($paramsFile)){
		$error = true;
		$message = "Impossible to find parameters file";
	}
	else{
		$paramsData = file_get_contents($paramsFile);
		if(!$paramsData){
			$error = true;
			$message = "Impossible to read parameters file";
		}
		else{
			$params = explode("\n", trim($paramsData));
			$paramsArray = array();
			foreach($params as $param){
				$param = trim($param);
				$keyVal = explode("=", $param,2);
				$key = trim($keyVal[0]);
				$val = trim($keyVal[1]);
				$paramsArray[$key] = $val;
			}
			{
				
				
			if(isset($_POST['updateParameters'])){
				$size = $_POST['size'];
				if("sd" == $size){
					$width=848;
					$height=480;
				}
				if("hd" == $size){
					$width=1280;
					$height=720;
				}
				if("full_hd" == $size){
					$width=1920;
					$height=1080;
				}
				$paramsArray['samplingRate'] = $_POST['sampling'];
				$paramsArray['fps'] = $_POST['fps'];
				$paramsArray['width'] = $width;
				$paramsArray['height'] = $height;
				$paramsArray['host'] = $_POST['host'];
				$paramsArray['port'] = $_POST['port'];
				$paramsArray['app'] = $_POST['app'];
				$paramsArray['path'] = $_POST['path'];
				$paramsArray['normalizeAudio'] = $_POST['normalizeAudio'] == "true" ? "true" : "false";

				
				//write them back
				$toWrite = "";
				foreach($paramsArray as $k=>$v){
					$toWrite .= "\n" . $k . "=" . $v;
				}
				$toWrite = trim($toWrite);
				$res = file_put_contents($paramsFile, $toWrite);

				if(!$res){
					$error = true;
					$message = "Impossible to update parameters";
				}else{
					$error = false;
					$message = "Succesfully updated parameters";
				}

			 }

			$sampling = $paramsArray['samplingRate'];
			$fps = $paramsArray['fps'];
			$width = $paramsArray['width'];
			$height = $paramsArray['height'];
			$host=$paramsArray['host'];
			$port=$paramsArray['port'];
			$app=$paramsArray['app'];
			$path=$paramsArray['path'];
			$normalizeAudio=$paramsArray['normalizeAudio'] == "true" ? true : false;
			
			if($height == 480) $size="sd";
			if($height == 720) $size="hd";
			if($height == 1080) $size="full_hd";

			}
		}

	}
	
	
	
?>


<html>
	<head>
		<?php createHeader();?>

	</head>

	<body>

		<?php createNavBar("config",""); ?>

<?php
if(strlen($message)>0){
	$class="label label-default";
	if($error)
		$class="label label-danger";
	echo '<div id="errorMsg" class="' . $class . '" style="display:block">' . $message .'</div></br>';
}
?>

<center>
	<div id="pendingON" style="display:none">
		<img class="pendingImage" src="images/wait.gif"/>
		<div class="pendingText">... Please wait while playlist manager is restarting ...</div>
	</div>	
</center>
<center>
	<div id="pendingOFF" style="display:none">
		<img class="pendingImage" src="images/wait.gif"/>
		<div class="pendingText">... Please wait while playlist manager is stopping ...</div>
	</div>	
</center>

<div class="panel panel-default">

	<div class="panel-heading">
	  <h3 class="panel-title">Playlist manager commands</h3>
	</div>

	<div class="panel-body">
		<div class="container">
			<table><tr><td>
			 <form method="POST" onsubmit="if (!confirm('Are you sure you want to (re)start playlist manager?')) return false; displayPendingON();">
				 <input type="hidden" name="startPlaylist" value="1"></input>
				 <input type="submit" value="(re)start playlist manager"></input>
			 </form></td><td>
			 <form method="POST" onsubmit="if (!confirm('Are you sure you want to stop playlist manager?')) return false; displayPendingOFF();">
				 <input type="hidden" name="stopPlaylist" value="1"></input>
				 <input type="submit" value="stop playlist manager"></input>
			 </form>
		</td></tr></table>
		</div>
	</div>
</div>
<div class="panel panel-default">

	<div class="panel-heading">
	  <h3 class="panel-title">Playlist parameters</h3>
	</div>

	<div class="panel-body">
		<div class="container">
			 <form method="POST" onsubmit="return confirm('Playlist manager will be restart if configuration if modified.\nProceed anyway?');">
				<input type="hidden" name="updateParameters" value="1"></input>
				<table>
					<!--tr>
						<td>Audio sampling rate</td>
						<td>
							<select name="sampling" id="sampling">
								<option value="44100" <?php if($sampling == 44100) echo "selected";?>>44.1 KHz</option>
								<option value="48000" <?php if($sampling == 48000) echo "selected";?>>48 KHz</option>
  							</select>
						</td>
					</tr>
					<tr>
						<td>Video size</td>
						<td>
							<select name="size" id="size">
								<option value="sd" <?php if($size == "sd") echo "selected";?>>SD (480p)</option>
								<option value="hd" <?php if($size == "hd") echo "selected";?>>HD (720p)</option>
								<option value="full_hd" <?php if($size == "full_hd") echo "selected";?>>Full HD (1080p)</option>
  							</select>
						</td>
					</tr>
					<tr>
						<td>Frame rate (FPS)</td>
						<td>
							<select name="fps" id="fps">
								<option value="25" <?php if($fps == 25) echo "selected";?>>25</option>
								<option value="30" <?php if($fps == 30) echo "selected";?>>30</option>
  							</select>
						</td>
					</tr>
					<tr>
						<td>RTMP output</td>
						<td>rtmp://
							<input type="text" name="host" size="12" value="<?php echo htmlspecialchars($host) ;?>"></input>:
							<input type="text" name="port" size="4" value="<?php echo htmlspecialchars($port) ;?>"></input>/
							<input type="text" name="app" size="12" value="<?php echo htmlspecialchars($app) ;?>"></input>/
							<input type="text" name="path" size="12" value="<?php echo htmlspecialchars($path) ;?>"></input>
						</td>
					</tr-->
					<tr>
						<td>Normalize audio</td>
						<td>
							<select name="normalizeAudio" id="normalizeAudio">
								<option value="false" <?php if(!$normalizeAudio) echo "selected";?>>no</option>
								<option value="true" <?php if($normalizeAudio) echo "selected";?>>yes</option>
  							</select>
						</td>
					</tr>
					<tr>
						<td><input type="submit" value="Save configuration"></input></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>

<script>
		function displayPendingON(){
			$("#pendingON").show();
		}
		function displayPendingOFF(){
			$("#pendingOFF").show();
		}
		
		setTimeout(function(){
			$("#errorMsg").hide(); 
		}, 5000);
</script>

</body>


</html>
