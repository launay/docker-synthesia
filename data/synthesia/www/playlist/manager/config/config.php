<?php

define( "IMPORT_YOUTUBE_HIGHEST_QUALITY", false); //import highest quality is slower...


define( "MAIN_TITLE", "Playlist Manager");
define( "MASTER_FOLDER", "../master/");
define( "IN_FOLDER", "../in/");
define( "CONVERT_FOLDER", "../convert/");
define( "OUT_FOLDER", "../out/");
define( "IMAGES_UPLOAD_FOLDER", "../images/");
define( "IMAGES_LINK_FOLDER", "/var/www/synthesia/" . "common" . "/playlist/images/");

define( "PLAYLIST_PROPERTIES_FILE", "../playlist.properties");

define( "IMAGE_SFX", ".IMAGE");

define( "DEFAULT_BITRATE", -1);

define( "STATUS_FILE", "../../status/playlist_status.json");

define( "CURRENT_PLAYLIST_FILE", "../playlist/current.m3u");
define( "NEXT_PLAYLIST_FILE", "../playlist/next.m3u");

define( "STATUS", "status");
define( "PROFILE", "profile");
define( "AVAILABLE", "available");
define( "CONVERTING", "converting");
define( "WAITING", "waiting");
define( "CAN_REMOVE", "canRemove");
define( "IN_CURRENT_PLAYLIST", "inCurrentPlaylist");
define( "IN_NEXT_PLAYLIST", "inNextPlaylist");

define( "DURATION", "duration");
define( "IMAGE", "image");
define( "TITLE", "title");
define( "SUBTITLE", "subtitle");
define( "DESCRIPTION", "description");

define( "DATE", "date");
define( "SOURCE", "source");

define( "SD", "848x480");
define( "HD", "1280x720");
define( "FULLHD", "1920x1080");

define( "AUDIO_48000", 48000);
define( "AUDIO_44100", 44100);

define( "VIDEO_25", 25);
define( "VIDEO_30", 30);

define( "SD_25_48000", "SD@25@48k");
define( "SD_30_48000", "SD@30@48k");
define( "SD_25_44100", "SD@25@44.1k");
define( "SD_30_44100", "SD@30@44.1k");

define( "HD_25_48000", "HD@25@48k");
define( "HD_30_48000", "HD@30@48k");
define( "HD_25_44100", "HD@25@44.1k");
define( "HD_30_44100", "HD@30@44.1k");

define( "FULLHD_25_48000", "FULLHD@25@48k");
define( "FULLHD_30_48000", "FULLHD@30@48k");
define( "FULLHD_25_44100", "FULLHD@25@44.1k");
define( "FULLHD_30_44100", "FULLHD@30@44.1k");

define( "UNKNOWN_PROFILE", "[unknown]");

define( "SOURCE_LOCAL", "Local");
define( "SOURCE_REMOTE", "Link");
define( "SOURCE_DM", "Dailymotion");
define( "SOURCE_YT", "YouTube");
define( "SOURCE_DM_IMPORT", "Dailymotion (auto)");
define( "SOURCE_YT_IMPORT", "YouTube (auto)");

?>
