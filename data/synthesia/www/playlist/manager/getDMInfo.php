<?php

require_once "php/simple_html_dom.php";

$url = $_GET['url'];
if(!$url) $url="http://www.dailymotion.com/video/x4aagrx_psg-ca-se-discute-le-depart-de-zlatan-est-il-une-bonne-nouvelle_sport";
$localTitle="";
$title="";
$description="";
$image="";
$dateFr="";

$maxSize = 1000;
if(isset($_GET['max_size'])){
	$maxSize = $_GET['max_size'];
}

if($url){

	$html = file_get_html($url);
	$titles = $html->find('meta[property=og:title]');
	if($titles){
		$title = $titles[0];
		$title = (string)$title->content;
		$title = cleanField($title);
		$title = str_replace(" - vidéo Dailymotion", "", $title);
	}
	$descriptions = $html->find('meta[property=og:description]');
	if($descriptions){
		$description = $descriptions[0];
		$description = (string)$description->content;
		$description = cleanField($description);
		$description = cleanSpaces($description);

		if(strlen($description)>$maxSize){
			$regexp = "/(?<=[.?!;])\s+(?=\p{Lu})/";
			while(strlen($description)>$maxSize){
				//echo "turn";
				$sentences = preg_split($regexp, $description);
				//print_r ($sentences);
				array_pop($sentences);
				$description = join(" ", $sentences);
				//echo $description;
			}
		}
		
	}
	$images = $html->find('meta[property=og:image]');
	if($images){
		$image = $images[0];
		$image = (string)$image->content;
	}

	$dates = $html->find('meta[property=video:release_date]');
	if($dates){
		$date = $dates[0];
		$date = (string)$date->content;
		$date = substr($date, 0, 10);
		$dateFr = substr($date, 8, 2) . "/" . substr($date, 5, 2) . "/" . substr($date, 0, 4) ;
	}

	$ids = $html->find('meta[property=og:url]');
	if($ids){
		$id = $ids[0];
		$id = (string)$id->content;
		$ids = explode("/", $id);
		$id = $ids[count($ids)-1];
	}
	$localTitle = $date . "_" . $id;
}

$result = array("localTitle"=>$localTitle, "title"=>$title, "description"=>$description, "image"=>$image, "date"=>$dateFr);
$resultJson = json_encode($result);
echo $resultJson;

function cleanSpaces($field){
	$field = str_replace(array("\n", "\t", "\r"), ' ', $field);
	$field = preg_replace('/\s+/', ' ',$field);
	return $field;
}

function cleanField($field){
	$field = html_entity_decode($field);



	$field = str_replace('&icirc;', "î", $field);
	$field = str_replace('&acirc;', "â", $field);
	$field = str_replace('&ucirc;', "û", $field);
	$field = str_replace('&ecirc;', "ê", $field);
	$field = str_replace("&ocirc;", "ô", $field);
	
	$field = str_replace('&egrave;', "è", $field);
	$field = str_replace('&agrave;', "à", $field);

	$field = str_replace('&eacute;', "é", $field);
	
	$field = str_replace('&hellip;', "...", $field);
	
	$field = str_replace('&amp;', "&", $field);
	$field = str_replace("&#39;", "'", $field);
	$field = str_replace("&#039;", "'", $field);

	$field = str_replace('&rsquo;', "'", $field);
	$field = str_replace('&lsquo;', "'", $field);

	$field = str_replace("&nbsp;", " ", $field);

	$field = str_replace("&quot;", "\"", $field);
	$field = str_replace("&laquo;", "\"", $field);
	$field = str_replace("&raquo;", "\"", $field);
	$field = str_replace("&ldquo;", "\"", $field);
	$field = str_replace("&rdquo;", "\"", $field);

	$field = str_replace("&lsaquo;", "<", $field);
	$field = str_replace("&rsaquo;", ">", $field);

	$field = str_replace("&ccedil;", "ç", $field);

	$field = str_replace("&iuml;", "ï", $field);
	$field = str_replace("&auml;", "ä", $field);
	$field = str_replace("&euml;", "ë", $field);
	$field = str_replace("&ouml;", "ö", $field);
	$field = str_replace("&uuml;", "ü", $field);

	$field = str_replace("&oelig;", "œ", $field);
	$field = str_replace("&ndash;", "–", $field);

	$field = str_replace('&ugrave;', "ù", $field);
	$field = str_replace('&gt;', ">", $field);
	$field = str_replace('&lt;', "<", $field);
	$field = str_replace('&amp;', "&", $field);

	
	return $field;
}

?>
