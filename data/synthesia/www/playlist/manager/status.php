<?php
require_once "php/info.php";


?>

<html>
	<head>
		<?php createHeader();?>

<script>
	
	
function getJSONValueOrDefault(jsonObject, key, def){
	var value = jsonObject[key];
	if(!value) return def;
	return value;
	
}
refreshPeriod = 5000;
lm = 0;
function refreshStatus(){
		var statusFile = "getCurrentStatus.php";
		$.getJSON( statusFile + "?rand=" + Math.random(), function( data ) {
		
			var engineRunning = true;
			var lastModified = getJSONValueOrDefault(data.full, "lastPublish", 0);
			var engineUptime = getJSONValueOrDefault(data.full, "uptime", "00:00:00.000");

			if(lastModified == lm){
				engineRunning = false;
			}
			
			var oldLm = lm;

			lm = lastModified;
			
			//first turn
			if(oldLm == 0) return;
			
			$("#engineRunning").removeClass("ok");
			$("#engineRunning").removeClass("nok");
			$("#engineRunning").addClass( engineRunning  ? "ok" : "nok");
			$("#engineRunning").html( engineRunning ? "Running" : "Stopped");
			$("#engineUptime").html(engineRunning ? engineUptime : "");

			var cdnRunning = getJSONValueOrDefault(data.full, "VZR_RTMP_RUNNING", false);
			var cdnBR = Math.max(0, getJSONValueOrDefault(data.full, "VZR_RTMP_BR_OUT", 0));
			var cdnUptime = getJSONValueOrDefault(data.full, "VZR_RTMP_UPTIME", "00:00:00.000");
			var cdnUrl = getJSONValueOrDefault(data.full, "VZR_RTMP_URL", "");
			
			$("#cdnRunning").removeClass("ok");
			$("#cdnRunning").removeClass("nok");
			$("#cdnRunning").addClass( cdnRunning == "true" ? "ok" : "nok");
			$("#cdnRunning").html( engineRunning ? (cdnRunning == "true" ? "connected" : "not connected") : "");
			$("#cdnBR").html(engineRunning ? (cdnBR / 1000) + " kbps" : "");
			$("#cdnUptime").html(engineRunning ? cdnUptime : "");
			$("#cdnUrl").html(engineRunning ? cdnUrl : "");


			if(data.current){
				var current=data.current.info.guid;
				var currentTitle=data.current.info.title;
				var duration=Math.round(data.current.duration);
				var elapsed=Math.round(data.current.elapsed);
				var next=data.next.info.guid;
				var nextTitle=data.next.info.title;
					
				$("#current").html(engineRunning ? current : "");
				$("#currentTitle").html(engineRunning ? currentTitle : "");
				$("#duration").html(engineRunning ? prettyDuration(duration) : "");
				$("#elapsed").html(engineRunning ? prettyDuration(elapsed) : "");
				$("#next").html(engineRunning ? next : "");
				$("#nextTitle").html(engineRunning ? nextTitle : "");
			}
		
		 
		});
   }
var timer=setInterval("refreshStatus()", refreshPeriod);
refreshStatus();
setTimeout("refreshStatus()", 1000);

</script>

<style>
 .bordered_table td{
	border: 1px solid black;
 }
 .ok{
	color: green;
	font-weight: bold;
 }
 .nok{
	color: red;
	font-weight: bold;
 }
</style>

	</head>

	<body>

		<?php createNavBar("status",""); ?>

<div class="panel panel-default">


<div class="panel-heading">
	  <h3 class="panel-title">Playlist status</h3>
	</div>


<div class="panel-body">
		<div class="container">
			
<table width="100%">

			<tr class="bordered_table">
					<td><b>Streamer status</b></td>
					<td><span id="engineRunning"></span></td>
				</tr>
				<tr class="bordered_table">
					<td><b>Uptime</b></td>
					<td><span id="engineUptime"></span></td>
				</tr>
</table>
		</div>
	</div>

<div class="panel-heading">
	  <h3 class="panel-title">RTMP output status</h3>
	</div>


<div class="panel-body">
		<div class="container">
			
<table width="100%">

			<tr class="bordered_table">
					<td><b>Output connection</b></td>
					<td><span id="cdnRunning"></span></td>
				</tr>
				<tr class="bordered_table">
					<td><b>URL</b></td>
					<td><span id="cdnUrl"></span></td>
				</tr>
				<tr class="bordered_table">
					<td><b>Bitrate</b></td>
					<td><span id="cdnBR"></span></td>
				</tr>
				<tr class="bordered_table">
					<td><b>Uptime</b></td>
					<td><span id="cdnUptime"></span></td>
				</tr>
</table>
		</div>
	</div>

   

<div class="panel-heading">
	  <h3 class="panel-title">Current playlist status</h3>
	</div>


<div class="panel-body">
		<div class="container">
			
<table width="100%">

		<tr class="bordered_table">
			<td><b>Current video file</b></td>
			<td><span id="current"></span></td>
		</tr>
		<tr class="bordered_table">
			<td><b>Video title</b></td>
			<td><span id="currentTitle"></span></td>
		</tr>
		<tr class="bordered_table">
			<td><b>Duration</b></td>
			<td><span id="duration"></span></td>
		</tr>
		<tr class="bordered_table">
			<td><b>Elapsed time</b></td>
			<td><span id="elapsed"></span></td>
		</tr>
		<tr><td></td></tr>
		<tr class="bordered_table">
			<td><b>Next video file</b></td>
			<td><span id="next"></span></td>
		</tr>
		<tr class="bordered_table">
			<td><b>Video title</b></td>
			<td><span id="nextTitle"></span></td>
		</tr>

</table>
		</div>
	</div>

            
  </div>

</body>


</html>
