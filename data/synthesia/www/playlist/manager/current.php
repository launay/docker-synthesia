<?php
require_once "php/info.php";


$currentProfile = getCurrentProfile();

//print_r($_POST);

$message = "";
$error = false;

//sorts playlist if required
if(isset($_POST["sortBy"])  ){
	$sortBy = $_POST["sortBy"];
	$sortOrder = $_POST["sortOrder"];

	if("name" == $sortBy){
		$videos = getAllVideos();
		$playlistFiles = getPlaylistFiles(CURRENT_PLAYLIST_FILE);
		if("asc"==$sortOrder){
			sort($playlistFiles);
		}else{
			rsort($playlistFiles);
		}

		setCurrentPlaylist($playlistFiles);

	}elseif("date" == $sortBy){
		
		$videos = getAllVideos();
		$playlistFiles = getPlaylistFiles(CURRENT_PLAYLIST_FILE);
		$playIds = array();
		foreach($videos as $guid=>$data){
			if(in_array($guid, $playlistFiles)){
				$playIds[$guid] = $data["date"];
			}
		}
		if("asc"==$sortOrder){
			asort($playIds);
		}else{
			arsort($playIds);
		}

		setCurrentPlaylist(array_keys($playIds));

	}else{
		
		
		$videos = getAllVideos();
		$playlistFiles = getPlaylistFiles(CURRENT_PLAYLIST_FILE);
		$playIds = array();
		foreach($videos as $guid=>$data){
			if(in_array($guid, $playlistFiles)){
				$playIds[$guid] = $data["duration"];
			}
		}
		if("asc"==$sortOrder){
			asort($playIds);
		}else{
			arsort($playIds);
		}

		setCurrentPlaylist(array_keys($playIds));

	}
		
	$error = false;
	$message = "Playlist succesfully sorted";
	

}

//shuffles playlist if required
if(isset($_POST["shuffle"])  ){

	$videos = getAllVideos();
	$playlistFiles = getPlaylistFiles(CURRENT_PLAYLIST_FILE);
	$playIds = array();
	foreach($videos as $guid=>$data){
		if(in_array($guid, $playlistFiles)){
			array_push($playIds, $guid);
		}
	}
	
	shuffle($playIds);
	//print_r($playIds);
	setCurrentPlaylist($playIds);
	
	$error = false;
	$message = "Playlist succesfully shuffled";
	

}

//clears playlist if required
if(isset($_POST["clear"])){

	clearCurrentPlaylist();
	$error = false;
	$message = "Playlist was cleared";
}


//updates playlist if required
if(isset($_POST["ids"])){
	$ids = $_POST["ids"];
	if(strlen($ids)>5){
		$newIds = explode(",", $ids);
		setCurrentPlaylist($newIds);
	}
	$error = false;
	$message = "Playlist was updated";
}

//removes file from playlist if required
if(isset($_POST["remove_file"])){
	$guid = $_POST["remove_file"];
	if($guid){
		removeFromCurrentPlaylist($guid);
	}
	$error = false;
	$message = "Video file was removed from the playlist";
}


//adds file to playlist if required
if((isset($_POST["add_file"]) && isset($_POST["guid"])) || isset($_POST["addAll"])  ){

	if(isset($_POST["addAll"]) && $_POST["addAll"] == 1){
		$videos = getAllVideos();
		$videoNumber = count($videos);
		$playlistFiles = getPlaylistFiles(CURRENT_PLAYLIST_FILE);
		
		foreach($videos as $guid=>$data){
			if(!in_array($guid, $playlistFiles)){
				addToCurrentPlaylist($guid);
			}
		}
				
		$error = false;
		$message = "All video files were added to the playlist";
	}
	else{
		$guid = $_POST["guid"];
		if($guid){
			addToCurrentPlaylist($guid);
		}
		$error = false;
		$message = "Video file was added to the playlist";
	}
}

$videos = getAllVideos();
$videoNumber = count($videos);

$playlistFiles = getPlaylistFiles(CURRENT_PLAYLIST_FILE);
$playlistNumber = count($playlistFiles);

//print_r ($playlistFiles);
//print_r ($videos);

//gets playlist duration
$duration = 0;
foreach($playlistFiles as $guid){
	$itemData = $videos[$guid];
	if($itemData["duration"])
	$duration += $itemData["duration"];
}
$durationPretty = printDuration($duration);

$plFiles = array();
$unusedVideos = array();

$count = 0;
foreach($playlistFiles as $guid){
	$itemData = $videos[$guid];
	$itemData["guid"] = $guid;
	if(in_array($guid, $playlistFiles)){
		$plFiles[$count++] = $itemData;
	}
}

foreach($videos as $guid=>$data){
	if(!in_array($guid, $playlistFiles)){
		$data["guid"] = $guid;
		$unusedVideos[$guid] = $data;
	}
}


//print_r($plFiles);
//echo "<br/><br/>\n\n";
//print_r($unusedVideos);
?>
<html>
	<head>
		<?php createHeader();?>

  <style>
  #sortable { cursor: pointer; list-style-type: none; margin: 0; padding: 0; width: 60%; }
  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
  #sortable li span { position: absolute; margin-left: -1.3em; }

.vzr_centered {
   text-align: center;
   align:center;
}

  </style>

	<script>
		function doConfirm(video) {
			return confirm("Are you sure you want to remove video " +video + " from the playlist?");
		}
		function doConfirmClear() {
			return confirm("Are you sure you want to clear the playlist?");
		}
		function doConfirmShuffle() {
			return confirm("Are you sure you want to shuffle the playlist?");
		}

		function doConfirmSort() {
			return confirm("Are you sure you want to sort the playlist?");
		}
	</script>

	</head>

	<body>

		<?php createNavBar("current",""); ?>

<?php
if(strlen($message)>0){
	$class="label label-default";
	if($error)
		$class="label label-danger";
	echo '<div class="' . $class . '" style="display:block">' . $message .'</div></br>';
}
?>

<div class="panel panel-default">

 <div class="panel-heading">
	  <h3 class="panel-title">Add a video to the <b>current</b> playlist</h3>
	</div>
	
	<div class="panel-body">
		<div class="container">
			<form method="POST" id="add_file" enctype="multipart/form-data" name="add_file">
				<table>
					<tr>
						<td>Select a video file to add to the playlist</td>
						<td><select name="guid">
						<?php foreach($unusedVideos as $key=>$vid){
								echo "\n<option value='" . $vid["guid"]."'>" . $vid["guid"] . " (" . htmlspecialchars(cutString($vid["title"], 64), ENT_QUOTES).")" . "</option>";
						}?>
						</select></td>
						<td colspan="1"><input type="submit" name="add_file" value="Add to playlist"></input></td>
						<td colspan="1"><input type="button" value="Add all" onclick="$('#addAll').val('1');$('#add_file').submit();"></input></td>
					</tr>
				</table>
				<input type="hidden" id="addAll" name="addAll" value="0"></input>
			</form>
		</div>
	</div>

	 <div class="panel-heading">
	  <h3 class="panel-title"><b>Current</b> playlist management (<?php echo $playlistNumber;?> video(s) : <?php echo $durationPretty;?>)</b></h3>
	</div>

	<div class="panel-body">
	<div class="container">
		
			<table>
			<tr>
			<td align="left">
			<h4 class="font_80">
			<div align="left">
				<button onclick="reloadPage();">Refresh</button>
				<button id="clear_button" onclick="if(doConfirmClear()){$('#clear_playlist').submit();}">Clear playlist</button>
				<button id="shuffle_button" onclick="if(doConfirmShuffle()){$('#shuffle').submit();}">Shuffle</button>
			</div>
			</h4>
			</td>
			<td align="right">
			<h4 class="font_80">
			<div align="right">
				<form method="POST" enctype="multipart/form-data" id="sort" name="sort" onsubmit="return doConfirmSort();">		
					
					Sort by
					<select id="sortBy" name="sortBy">
						<option name="name" value="name">Name</option>
						<option name="duration" value="duration">Duration</option>
						<option name="date" value="date">Date</option>
					</select>
					<select id="sortOrder" name="sortOrder">
						<option name="asc" value="asc">Ascending</option>
						<option name="desc" value="desc">Descending</option>
					</select>
					<input type="submit" value="Sort"></input>
				</form>			
			</div>
			</h4>
			</td>
			</tr>
			</table>
					
		<form method="POST" enctype="multipart/form-data" id="clear_playlist" name="clear_playlist">
			<input type="hidden" name="clear" value="1"></input>
		</form>
		<form method="POST" enctype="multipart/form-data" id="shuffle" name="shuffle">
			<input type="hidden" name="shuffle" value="1"></input>
		</form>
	  <button id="applyChangesButton" onclick="applyChanges();" style="display:none;">Save new positions</button>
	  <button id="revertChangesButton" onclick="revertChanges();" style="display:none;">Cancel new positions</button>
	</div>
	</div>	
	
		<div class="panel-body">
		<div class="container">
			<table class="font_80 evenOdd" id="plTable" style="white-space: nowrap;">
			  <thead>
					<tr>
						<th>Video file&nbsp;</td>
						<th class="vzr_centered">Duration&nbsp;</th>
						<th class="vzr_centered">Play status&nbsp;</th>
						<th class="vzr_centered">Date&nbsp;</th>
						<th class="vzr_centered">Source&nbsp;</th>
						<th class="vzr_centered">Image&nbsp;</th>
						<th>Title / SubTitle / Description&nbsp;</th>
						<th class="vzr_centered">Video Status</th>
					</tr>
			  </thead>
			  <tbody id="sortable">
<?php

$count=0;
foreach($plFiles as $key=>$video){
	$count++;
	
	$date = date("d/m/Y H:i:s", $video[DATE]);
	$source = $video[SOURCE];
	
	$guid = $video["guid"];
	$duration = $video[DURATION];
	$image = $video[IMAGE];
	$title = $video[TITLE];
	$subtitle = $video[SUBTITLE];
	$description = $video[DESCRIPTION];
	$canRemove = $video[STATUS][CAN_REMOVE];
	$avProfiles = array();
	$convProfiles = array();
	$waitProfiles = array();
	foreach($video[STATUS][AVAILABLE] as $md5=>$profile){
		array_push($avProfiles, $profile[PROFILE]);
	}
	foreach($video[STATUS][CONVERTING] as $md5=>$profile){
		array_push($convProfiles, $profile[PROFILE]);
	}
	foreach($video[STATUS][WAITING] as $md5=>$profile){
		if(in_array($profile[PROFILE], $convProfiles)) continue;
		array_push($waitProfiles, $profile[PROFILE]);
	}
	
	$hasProfile = in_array($currentProfile, $avProfiles);
	$isConverting = in_array($currentProfile, $convProfiles);
	$isWaiting = in_array($currentProfile, $waitProfiles);

	echo "<tr " . (!$hasProfile ? "style='background-color:#ffd6cc;'" : "") . " class='rowItem' id='" . $guid . "'>";
	echo "<td><a class='font_80' href='". (MASTER_FOLDER  . rawurlencode($guid))."' target='_blank'>" . $guid ."</a></td>";
	echo "<td class='vzr_centered'>" . printDuration($duration) ."</td>";
	echo "<td class='vzr_centered' id='" . $guid . ".span'></td>";

	echo "<td class='vzr_centered'>" . $date ."</td>";
	echo "<td class='vzr_centered'>" . $source ."</td>";


	echo "<td class='vzr_centered'><img height='40' src='" . trimImageUrl($image)  . "'/></td>";
	echo "<td>" . htmlspecialchars(cutString($title, 48), ENT_QUOTES)  . "<br/>";
	echo "" . htmlspecialchars(cutString($subtitle, 48), ENT_QUOTES)  . "<br/>";
	echo "" . htmlspecialchars(cutString($description,48), ENT_QUOTES) . "</td>";
	echo "<td class='vzr_centered'>";
	if($hasProfile){
		echo '<span style="width:100%" class="label label-success">OK</span>';
	}else if($isConverting){
		echo '<span class="label label-default">PROCESSING</span>';
	}else{
		echo '<span class="label label-default">QUEUED</span>';
	}
	"</td>";

	$submitText = "return doConfirm(\"" . $guid ."\")";

	echo "<td>
			<form method='post' name='remove_" . $count ."' onsubmit='" . $submitText ."'>
				<input type='submit' name='remove' value='Remove'></input>
				<input type='hidden' name='remove_file' value='" . $guid . "'></input>
			</form>
		</td>";

	echo "</tr>\n";
}

?>
				</tbody>
			</table>
	
		</div>
	</div>
		   
	     
  </div>

<script>

	oldPlaying = null;
	oldPlayingNext = null;
	duration = -1;
	elapsed = -1;
	
	
	function refreshPlaying() {
		
		var statusFile = "getCurrentStatus.php";
		$.getJSON( statusFile + "?rand=" + Math.random(), function( data ) {
		
			var engineRunning = true;
					
			if(oldPlaying != null){
					var oldPlayingElt = $('span[id="' + oldPlaying + '"]');
					oldPlayingElt.remove();
			}
			if(oldPlayingNext != null){
					var oldPlayingElt = $('span[id="' + oldPlayingNext + '"]');
					oldPlayingElt.remove();
			}

		    var current=data.current.info.guid;
		    var duration=Math.round(data.current.duration);
			var elapsed=Math.round(data.current.elapsed);
			var next=data.next.info.guid;
				
			if(current.length>0){
				var id = current+".span";
				var playing = $('td[id="' + id+'"]');
				
				var newPlaying = "nowplaying_" + Math.random();
				
				var percents = 0;
				if(duration>-1 && elapsed >-1){
					percents = Math.round(100 * elapsed / duration);
				}
				
				var text = "<span id=\"" + newPlaying +"\"><span class=\"label label-success\">PLAYING ("+percents+"%)</span>";
				text+='<div class="progress"><div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="'+percents+'" aria-valuemin="0" aria-valuemax="100" style="width:'+percents+'%"><span class="sr-only"></span> </div></div>';
				text+="</span>";
				
				playing.prepend( text );
				oldPlaying = newPlaying;
			}
			
			if(next.length>0){
				var id = next+".span";
				var playing = $('td[id="' + id+'"]');
				
				var newPlaying = "nowplaying_" + Math.random();
				playing.prepend( "<span id=\"" + newPlaying +"\"><span class=\"label label-default\">COMING NEXT</span>&nbsp;</span>" );
				oldPlayingNext = newPlaying;
			}
			
		 
		});
   }
	var timer=setInterval("refreshPlaying()", 3000);
	refreshPlaying();

</script>

		  <script>

			  initIds = getAllIds("");
			  
			  function getAllIds(sep){
				var allIds = "";
				$(".rowItem").each(function() {
					allIds += this.id + sep;
				});
				
				return allIds.trim();

			  }
			  
			
		  $(function() {
			$( "#sortable" ).sortable();
			$( "#sortable" ).disableSelection();
			
			});
		  
		  function checkPositions(){
				var allIds = getAllIds("");
				
				if(initIds.trim() == allIds.trim()){
					$("#applyChangesButton").hide();
					$("#revertChangesButton").hide();
				}
				else{					
				  $("#applyChangesButton").show();
				  $("#revertChangesButton").show();
				}
		  }
		  
		  function revertChanges(){
			window.location = window.location.href;
		  }
		  
		   function applyChanges(){
			   var allIds = getAllIds(",");
			   $("#ids").val(allIds);
			   $("#setPlaylist").submit();		
			   	
		  }
		  
		  setTimeout(function() { doCheckPositions(); }, 500);
			function doCheckPositions(arr, num) {
			  checkPositions();
			  setTimeout(function() { doCheckPositions(); }, 500);
			}
		  
		  <?php if($playlistNumber ==0){echo '$("#plTable").hide();';} ?>

		 		function reloadPage(){
			window.location = window.location.href;
		}
		  
		  </script>

<div style="display:none">
	<form id="setPlaylist" method="POST">
		<input type="hidden" id="ids" name="ids" value=""></input>
		<input type="submit" name="setPlaylist" value="1"></input>
	</form>
</div>

</body>

</html>

