<?php
require_once "php/info.php";
include_once "php/simple_html_dom.php";

$PROFILES = getAllProfiles();

$currentProfile = getCurrentProfile();

// DEBUG
//print_r($_FILES);
//echo "<br/>";
//print_r($_POST);
//print_r($_GET);

$isLocal = 0;
$remoteLink = "";
$remoteName = "";
$message = "";
$error = false;

$tit = "";
$desc = "";

handlePost($error, $message, $im, $dat, $tit, $sub, $desc, $isLocal, $remoteLink, $remoteName, $imageLink);
function handlePost(&$error, &$message, &$im, &$dat, &$tit, &$sub, &$desc, &$isLocal, &$remoteLink, &$remoteName, &$imageLink ){

	//adds if required
	if(isset($_POST["add_file"])){

		$title = trim($_POST["title"]);
		$description = trim($_POST["description"]);
		$subtitle = trim($_POST["subtitle"]);
		$date = trim($_POST["date"]);
		$tit = $title;
		$dat = $date;
		$sub = $subtitle;
		$desc = $description;
		
		$date = time();
		$source = SOURCE_LOCAL;
			

		$guid = "";
		$im = $image;
		$kind = $_POST["optradio"];
		
		$toAdd=null;
		$toAddTmp=null;
		$isUpload = false;
		
		if(isset($_POST["imageLink"]) && strlen(trim($_POST["imageLink"]))>0){
			$imageLink = $_POST["imageLink"];
		}
		if(isset($_POST["description"]) && strlen(trim($_POST["description"]))>0){
			$description = $_POST["description"];
		}
		
		if($kind == "local"){
			$date = time();
			$source = SOURCE_LOCAL;
				
			if(!isset($_FILES["file"])){
				$error = true;
				$message = "No video file specified";
				return;
			}

			$file = $_FILES["file"];
			if(!isset($file["size"]) || $file["size"] == 0){
				$error = true;
				$message = "No video file specified";
				return;
			}
			if(!isset($file["name"]) || !$file["name"]){
				$error = true;
				$message = "No video file specified";
				return;
			}
			if($file["error"]){
				$error = true;
				$message = "An error occured while uploading file";
				return;
			}		
			
			$toAdd = $file["name"];
			$toAdd = preg_replace('/[^A-Za-z0-9_\-\.]/', '_', $toAdd);
			$toAddTmp = $file["tmp_name"];
			$isUpload = true;
			$guid = $toAdd;

			if(checkName($toAdd)){
				$error = true;
				$message = "File " . $toAdd . " already exists";
				return;
			}

			$path_parts = pathinfo($toAdd);
			$extension = $path_parts['extension'];

			if(strtoupper($extension) != "MP4" && strtoupper($extension) != "MOV"){
				$error = true;
				$message = "You must specify a MP4 file (*.mp4)";
				return;
			}		
				
		}
		
		else if ($kind == "remote"){

			$date = time();
			$source = SOURCE_REMOTE;
			
			$isLocal = 1;

			if(!$_POST["fileRemote"]){
				$error = true;
				$message = "You must specify a link to a video file";
				return;
			}
			
			if(!$_POST["fileRemoteName"]){
				$error = true;
				$message = "You must specify a name for the video";
				return;
			}
			
			$remoteLink = trim($_POST["fileRemote"]);
			$remoteName = trim($_POST["fileRemoteName"]);
			if(!$remoteLink){
				$error = true;
				$message = "Video link is empty";
				return;
			}
			if(!$remoteName){
				$error = true;
				$message = "Video name is empty";
				return;
			}
			
			$remoteName = preg_replace('/[^A-Za-z0-9_\-\.]/', '_', $remoteName);
			$usedRemoteName = $remoteName . ".mp4";
			if(checkName($usedRemoteName)){
				$error = true;
				$message = "Local file '" . $usedRemoteName . "' already exists";
				return;
			}
			
			$localPath = "/tmp/" . uniqid("tmp-vid-",true);

			//downloads
			$src = fopen($remoteLink, 'r');
			$dest = fopen($localPath, 'w');
			
			$written = stream_copy_to_stream($src, $dest);
			if($written < 1024*1024){
				unlink($dest);
				$error = true;
				$message = "Video download failed. Please check the link or retry...";
				return;
			}
			
			//echo "<br/>downloaded " . $written . " bytes";
			chmod($dest, 0777); 

			$toAdd = $usedRemoteName;
			$toAddTmp = $localPath;
			$isUpload = false;
			$guid = $toAdd;
			
		}
				
		else if ($kind == "dm"){

			$date = time();
			$source = SOURCE_DM;

			$isLocal = 2;

			if(!$_POST["fileDM"]){
				$error = true;
				$message = "You must specify a link to a video";
				return;
			}
			
			if(!$_POST["fileDMName"]){
				$error = true;
				$message = "You must specify a name for the video";
				return;
			}
			
			$remoteLink = trim($_POST["fileDM"]);
			$remoteName = trim($_POST["fileDMName"]);
			if(!$remoteLink){
				$error = true;
				$message = "Video link is empty";
				return;
			}
			if(!$remoteName){
				$error = true;
				$message = "Video name is empty";
				return;
			}
			
			$remoteName = preg_replace('/[^A-Za-z0-9_\-\.]/', '_', $remoteName);
			$usedRemoteName = $remoteName . ".mp4";
			if(checkName($usedRemoteName)){
				$error = true;
				$message = "Local file '" . $usedRemoteName . "' already exists";
				return;
			}
			
			$localPath = "/tmp/" . uniqid("tmp-vid-",true);

			//gets date
			$html = file_get_html($remoteLink);
			$dates = $html->find('meta[property=video:release_date]');
			if($dates){
				$date = $dates[0];
				$date = (string)$date->content;
				$date = strtotime($date);
			}
			
			//gets duration
			$durations = $html->find('meta[property=video:duration]');
			if($durations){
				$duration = $durations[0];
				$duration = (string)$duration->content;
				$duration = 600 + intval($duration);
				set_time_limit($duration);
			}


			$dmData = file_get_contents($remoteLink);

			$REG_EXP = "/{\"type\":\"video\\\\\/mp4\",\"url\":\"([^\"]*)\"}/";

			$ret = preg_match_all($REG_EXP, $dmData, $matches, PREG_PATTERN_ORDER);

			if(!$ret){
				$error = true;
				$message = "No valid video file was found for this link";
				return;
			}

			$matches1 = $matches[1];
			if(!$matches1 || count($matches1)==0){
				$error = true;
				$message = "No valid video file was found for this link";
				return;
			}

			$lastUrl = $matches1[count($matches1)-1];
			$lastUrl = str_replace('\\', '', $lastUrl);

			//downloads
			$src = fopen($lastUrl, 'r');
			$dest = fopen($localPath, 'w');
			
			$written = stream_copy_to_stream($src, $dest);
			if($written < 1024*512){
				unlink($dest);
				$error = true;
				$message = "Video download failed. Please check the link or retry...";
				return;
			}
			
			//echo "<br/>downloaded " . $written . " bytes";
			chmod($dest, 0777); 

			$toAdd = $usedRemoteName;
			$toAddTmp = $localPath;
			$guid = $toAdd;
			
			$isUpload = false;
			
		}

		else if ($kind == "yt"){

			$date = time();
			$source = SOURCE_YT;

			$isLocal = 3;

			if(!$_POST["fileYT"]){
				$error = true;
				$message = "You must specify a link to a video";
				return;
			}
			
			if(!$_POST["fileYTName"]){
				$error = true;
				$message = "You must specify a name for the video";
				return;
			}
			
			$remoteLink = trim($_POST["fileYT"]);
			$remoteName = trim($_POST["fileYTName"]);
			if(!$remoteLink){
				$error = true;
				$message = "Video link is empty";
				return;
			}
			if(!$remoteName){
				$error = true;
				$message = "Video name is empty";
				return;
			}
			
			$remoteName = preg_replace('/[^A-Za-z0-9_\-\.]/', '_', $remoteName);
			$usedRemoteName = $remoteName . ".mp4";
			if(checkName($usedRemoteName)){
				$error = true;
				$message = "Local file '" . $usedRemoteName . "' already exists";
				return;
			}
			
			$localPath = "/tmp/" . uniqid("tmp-vid-",true);

			//gets date
			$html = file_get_html($remoteLink);
			$dates = $html->find('meta[itemprop=datePublished]');
			if($dates){
				$date = $dates[0];
				$date = (string)$date->content;
				$date = strtotime($date);
			}
			
			//gets duration
			$durations = $html->find('meta[itemprop=duration]');
			if($durations){
				$duration = $durations[0];
				$duration = (string)$duration->content;
				$interval = new DateInterval($duration);
				$duration = 600 + $interval->days*86400 + $interval->h*3600 + $interval->i*60 + $interval->s;
				set_time_limit($duration);
			}
			
			$attempts = 0;
			$success = false;
			while($attempts < 10){
//				echo "Attempt: " . $attempts;
				$attempts++;
				$ytData = file_get_contents($remoteLink);
			
	//			$pattern = '/"adaptive_fmts":"[^"]*"/';

				$ytVideoUrl = null;
				$removeLocalAtEnd = false;


				//first tries with adaptive_fmts to get highest quality
				if(IMPORT_YOUTUBE_HIGHEST_QUALITY){ //bug : only downloads video !

					$pattern = '/"adaptive_fmts":"[^"]*"/';
					preg_match($pattern, $ytData, $matches);
					$urlListDebug = array();
					$urlListMP4_video = array();
					$urlListWEBM_video = array();
					$urlListMP4_audio = array();
					$urlListWEBM_audio = array();
					$urlListUnknown = array();
					$currentData = array();
					$targetList = array();
					if($matches && count($matches)>0){
						$match = $matches[0];
						$match = str_replace('"adaptive_fmts":"', "", $match);

						$urlItems = explode(",", $match);
						foreach($urlItems as $urlItem){
							$urlData = array();
							$items = explode("\\u0026", $urlItem);
							//print_r($items);
							foreach($items as $item){
								$item = trim($item);
								$itemComps = explode("=", $item, 2);
								if(count($itemComps) != 2) continue;
								$key = trim($itemComps[0]);
								$val = trim($itemComps[1]);
								$urlData[$key] = urldecode($val);
								
								array_push($urlListDebug, array($key=>$val));
																
								if("url" == $key ){
									$urlData["videoUrl"] = urldecode($val);
								}

							}
							$type = trim($urlData["type"]);
							
							$isOk = false;
							$isAudio = false;

							//echo "<br/>type is: " . $type ;

							if(startsWith($type, "video/mp4")) {
								$targetList = &$urlListMP4_video;
								$isOk = true;
								$isAudio = false;
							}
							else if(startsWith($type, "audio/mp4")) {
								$targetList = &$urlListMP4_audio;
								$isOk = true;
								$isAudio = true;
							}
							else if(startsWith($type, "video/webm")) {
								$targetList = &$urlListWEBM_video;
								$isOk = true;
								$isAudio = false;

							}
							else if(startsWith($type, "audio/webm")) {
								$targetList = &$urlListWEBM_audio;
								$isOk = true;
								$isAudio = true;
							}
							
							
							if($urlData["url"] 
								&& ($urlData["size"] && !$isAudio) || ($urlData["bitrate"] && $isAudio)
								&& $isOk){
								
								if($isAudio){
									$bitrate = $urlData["bitrate"];
									$targetList[$bitrate] = $urlData;
								}
								else{
									$size = $urlData["size"];
									$sizeComps = explode("x", $size, 2);
									$sizeVal = $sizeComps[0] * $sizeComps[1];
									$targetList[$sizeVal] = $urlData;
								}
								
								krsort($targetList);
							}
							else{
								array_push($urlListUnknown, $urlData);
							}
												
						}
						
					}
					$urlList["unknown"] = $urlListUnknown;
					$urlList["MP4_video"] = $urlListMP4_video;
					$urlList["MP4_audio"] = $urlListMP4_audio;
					$urlList["WEBM_video"] = $urlListWEBM_video;
					$urlList["WEBM_audio"] = $urlListWEBM_audio;
					
					//first will try with MP4
					$mp4Urls = array();
					$webmUrls = array();
					$hasBestMP4 = false;
					$hasBestWebm = false;
					if(count($urlListMP4_video)>0 && count($urlListMP4_audio)>0){
						$bestVideoUrl = array_shift($urlListMP4_video);
						$bestAudioUrl = array_shift($urlListMP4_audio);
						$mp4Urls = array("video"=>$bestVideoUrl["videoUrl"], "audio"=>$bestAudioUrl["videoUrl"]);
						$hasBestMP4 = true;
					}
					if(count($urlListWEBM_video)>0 && count($urlListWEBM_audio)>0){
						$bestVideoUrl = array_shift($urlListWEBM_video);
						$bestAudioUrl = array_shift($urlListWEBM_audio);
						$webmUrls = array("video"=>$bestVideoUrl["videoUrl"], "audio"=>$bestAudioUrl["videoUrl"]);
						$hasBestWebm = true;
					}

					$urlList["MP4_best"] = $mp4Urls;
					$urlList["WEBM_best"] = $webmUrls;
					
					$ytVideoUrlConcat = null;
					
					//will concatenate
					$concatFile = "/tmp/concat-" . uniqid() ;
					if($hasBestMP4){
						$videoFile = $mp4Urls["video"];
						$audioFile = $mp4Urls["audio"];
						$concatFile .= ".mp4";
						
						$videoLocal = "/tmp/yt-video-" . uniqid() . ".mp4" ;
						$audioLocal = "/tmp/yt-audio-" . uniqid() . ".mp4" ;
						
						$audioDLCommand = "wget \"$audioFile\" -O $audioLocal";
						$videoDLCommand = "wget \"$videoFile\" -O $videoLocal";
						
						//echo $audioDLCommand . "<br/><br/><br/><br/><br/>";
						
						$output = array();
						$return = -1;
						exec($audioDLCommand, $output, $return);

						//echo $audioDLCommand . "<br/><br/><br/><br/><br/>";						
						//print_r($output);
						//echo "return code is $return";
						
						if($return == 0){
							$output = array();
							$return = -1;
							exec($videoDLCommand, $output, $return);

							//echo $videoDLCommand . "<br/><br/><br/><br/><br/>";						
							//print_r($output);
							//echo "return code is $return";

							if($return == 0){
								$concatCommand = FFMPEG_EXE . " -y -i \"$videoLocal\" -i \"$audioLocal\" -c copy -f mp4 $concatFile";

								$output = array();
								$return = -1;
								exec($concatCommand, $output, $return);

								//echo $concatCommand . "<br/><br/><br/><br/><br/>";						
								//print_r($output);
								//echo "return code is $return";
								
								if($return == 0){
									$ytVideoUrlConcat = $concatFile;
								}

								
							}

						}
						
						unlink($audioLocal);
						unlink($videoLocal);

					}
					else if($hasBestWebm){
						$videoFile = $webmUrls["video"];
						$audioFile = $webmUrls["audio"];
						$concatFile .= ".webm";
						
						$videoLocal = "/tmp/yt-video-" . uniqid() . ".webm" ;
						$audioLocal = "/tmp/yt-audio-" . uniqid() . ".webm" ;
						
						$audioDLCommand = "wget \"$audioFile\" -O $audioLocal";
						$videoDLCommand = "wget \"$videoFile\" -O $videoLocal";
						
						//echo $audioDLCommand . "<br/><br/><br/><br/><br/>";
						
						$output = array();
						$return = -1;
						exec($audioDLCommand, $output, $return);

						//echo $audioDLCommand . "<br/><br/><br/><br/><br/>";						
						//print_r($output);
						//echo "return code is $return";
						
						if($return == 0){
							$output = array();
							$return = -1;
							exec($videoDLCommand, $output, $return);

							//echo $videoDLCommand . "<br/><br/><br/><br/><br/>";						
							//print_r($output);
							//echo "return code is $return";

							if($return == 0){
								$concatCommand = FFMPEG_EXE . " -y -i \"$videoLocal\" -i \"$audioLocal\" -c copy -f webm $concatFile";

								$output = array();
								$return = -1;
								exec($concatCommand, $output, $return);

								//echo $concatCommand . "<br/><br/><br/><br/><br/>";						
								//print_r($output);
								//echo "return code is $return";
																
								if($return == 0){
									$ytVideoUrlConcat = $concatFile;
								}

							}

						}
						
						unlink($audioLocal);
						unlink($videoLocal);


					}


					if($ytVideoUrlConcat){
						$ytVideoUrl = $ytVideoUrlConcat;
						$removeLocalAtEnd = true;
					}
					//print_r($urlListDebug);
					//print_r($urlList);
					
					
					//DEBUG
					//$error = true;
					//$message = "Just debugging !";
					//return;
				}
	
				$initialYtUrl = $ytVideoUrl;
				$pattern = '/"url_encoded_fmt_stream_map":"[^"]*"/';

				//then tries with url_encoded_fmt_stream_map
				if(!$ytVideoUrl){
					preg_match($pattern, $ytData, $matches);
					if($matches && count($matches)>0){
						$match = $matches[0];
						$items = explode("\\u0026", $match);
						foreach($items as $item){
							$item = trim($item);
							$urls = explode("url=", $item);
							if(count($urls)==2){
								$ytVideoUrl = $urls[1];
								$ytVideoUrl = urldecode($ytVideoUrl);
								break;
							}
						}
					}
				}

				if(!$ytVideoUrl){
					$error = true;
					$message = "No valid video was found for this link";
					continue;
				}


				$lastUrl = $ytVideoUrl;

				//downloads
				$src = fopen($lastUrl, 'r');
				$dest = fopen($localPath, 'w');
				
				$written = stream_copy_to_stream($src, $dest);
				if($written < 1024*512){
					unlink($dest);
					$error = true;
				$message = "Video download failed. Please check the link or retry...";
					continue;
				}
				
				//echo "<br/>downloaded " . $written . " bytes";
				chmod($dest, 0777); 
				
				
				//then removes local cache if any
				if($removeLocalAtEnd && $initialYtUrl){
					unlink($initialYtUrl);
				}

				$toAdd = $usedRemoteName;
				$toAddTmp = $localPath;
				$isUpload = false;
				$guid = $toAdd;
				$success = true;
				$error = false;
				$message = "Video download was succesful";
				break;
			}
			
			if(!$success){
				$error = true;
				return;
			}

		}
		
		//handles image
		$imageUp = $_FILES["image"];
		$hasImage = $imageUp["size"] >0;
		if($hasImage && $imageUp["error"]){
			$error = true;
			$message = "An error occured while uploading file";
		}

		if(!$error && $hasImage){

			$name =  $imageUp["name"];
			$nameTmp =  $imageUp["tmp_name"];
			$ext = "." . pathinfo($name, PATHINFO_EXTENSION);
			$outName = $guid . IMAGE_SFX . $ext;

			$imageFullName = IMAGES_LINK_FOLDER . $outName;
			
			if(!move_uploaded_file($nameTmp, $imageFullName)){
				$error = true;
				$message = "An error occured while getting image file";
				$image = IMAGES_LINK_FOLDER . "synthesia.png";
			}
			else{
				chmod($fullName, 0777);	
				$image = IMAGES_LINK_FOLDER . $outName;
			}

		}else if(isset($_POST["imageLink"]) && strlen(trim($_POST["imageLink"]))>0){
			$imageLink = $_POST["imageLink"];
			$image = $imageLink;
		}
		else{
			$image = IMAGES_LINK_FOLDER . "synthesia.png";
		}
		
		//handles upload
		if(!$error){
			$error = !addVideo($toAdd, $toAddTmp, $image, $title, $subtitle, $description, $isUpload, $date, $source);
			if($error){
				$message = "An error occured while adding video file";
				return;
			}else{
				$message = "Video file was succesfully added";
			}
			if(!$isUpload)
				unlink($toAddTmp);
								
		}
		
		//handles profiles
		if(!$error){
			$PROFILES = getAllProfiles();
			$profiles = array();


			foreach($PROFILES as $profile){
				$profile2 = str_replace(".", "_", $profile);
				
				$newValue = isset($_POST[$profile2]) && $_POST[$profile2];
				$mustUpdate = $newValue;
										
				if($mustUpdate){
					$profiles[$profile] = $newValue ? "1" : "0";
				}
			}

			if(!updateProfiles($guid, $profiles)){
				$error = true;
				$message = "An error occured while updating video profiles";
				return;
			}
		}

	}

}

?>
<html>
	<head>
		<?php createHeader();?>

  <!--style>
  #sortable { cursor: pointer; list-style-type: none; margin: 0; padding: 0; width: 60%; }
  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
  #sortable li span { position: absolute; margin-left: -1.3em; }

  </style-->


	</head>

	<body>

		<?php createNavBar("add",""); ?>

<?php
if(strlen($message)>0){
	$class="label label-default";
	if($error)
		$class="label label-danger";
	echo '<div id="errorMsg" class="' . $class . '" style="display:block">' . $message .'</div></br>';
}
?>

<center>
	<div id="pending" style="display:none">
		<img class="pendingImage" src="images/wait.gif"/>
		<div class="pendingText">... Please wait while video file is being processed ...</div>
	</div>	
</center>

<div class="panel panel-default">

 <div class="panel-heading">
	  <h3 class="panel-title">Import a new video file</h3>
	</div>
		   
	<div class="panel-body" >
		<div class="container">
		<form method="POST" enctype="multipart/form-data" name="add_file" onsubmit="displayPending();">
			<table class="font_80">
				<tr><td>
				 <div class="radio font_80">
				  <label class=".radio-inline"><input value="local" onchange="setLocal(0);" type="radio" name="optradio" <?php if($isLocal==0) echo "checked";?>>Local</label>
				  <label class=".radio-inline"><input value="remote" onchange="setLocal(1);" type="radio" name="optradio" <?php if($isLocal==1) echo "checked";?>>Link</label>
				  <label class=".radio-inline"><input value="dm" onchange="setLocal(2);" type="radio" name="optradio" <?php if($isLocal==2) echo "checked";?>>Dailymotion URL</label>
				  <label class=".radio-inline"><input value="yt" onchange="setLocal(3);" type="radio" name="optradio" <?php if($isLocal==3) echo "checked";?>>YouTube URL</label>
				</div></td>
				</tr>
				<tr class="isLocal">
					<td>Local video file</td>
					<td><input type="file" name="file" size="16"></input></td>
				</tr>
				<tr class="isRemote" >
					<td>Remote video link</td>
					<td><input type="text" name="fileRemote" size="64" value="<?php echo htmlspecialchars($remoteLink, ENT_QUOTES);?>" placeholder="Paste the video direct URL"></input></td>
				</tr>
				<tr class="isDM" >
					<td>Dailymotion URL</td>
					<td><input type="text" name="fileDM" id="fileDM" size="64" value="<?php echo htmlspecialchars($remoteLink, ENT_QUOTES);?>" placeholder="Paste the Dailymotion video URL"></input></td>
					<td><input type="button" onclick="setDM();" name="fillDM" value="autofill fields"></input></td>
				</tr>
				<tr class="isYT" >
					<td>YouTube URL</td>
					<td><input type="text" name="fileYT" id="fileYT" size="64" value="<?php echo htmlspecialchars($remoteLink, ENT_QUOTES);?>" placeholder="Paste the YouTube video URL"></input></td>
					<td><input type="button" onclick="setYT();" name="fillYT" value="autofill fields"></input></td>
				</tr>
				<tr class="isRemote" >
					<td>Video local name</td>
					<td><input type="text" name="fileRemoteName" size="32" value="<?php echo htmlspecialchars($remoteName, ENT_QUOTES);?>" placeholder="Enter the local video name"></input>.mp4</td>
				</tr>
				<tr class="isDM" >
					<td>Video local name</td>
					<td><input type="text" id="fileDMName" name="fileDMName" size="32" value="<?php echo htmlspecialchars($remoteName, ENT_QUOTES);?>" placeholder="Enter the local video name"></input>.mp4</td>
				</tr>
				<tr class="isYT" >
					<td>Video local name</td>
					<td><input type="text" id="fileYTName" name="fileYTName" size="32" value="<?php echo htmlspecialchars($remoteName, ENT_QUOTES);?>" placeholder="Enter the local video name"></input>.mp4</td>
				</tr>
				<tr>
					<td>Title</td>
					<td><input type="text" id="title" name="title" size="64" value="<?php echo htmlspecialchars($tit, ENT_QUOTES);?>" placeholder="Enter the video title"></input></td>
				</tr>
				<tr>
					<td>SubTitle</td>
					<td><input type="text" id="subtitle" name="subtitle" size="64" value="<?php if($sub) echo htmlspecialchars($sub, ENT_QUOTES); else echo "";?>" placeholder="Enter the video subtitle"></input></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><textarea id="description" name="description" cols="64" rows="3" placeholder="Enter the video description"><?php echo htmlspecialchars($desc, ENT_QUOTES);?></textarea></td>
				</tr>
				<tr>
					<td>Image</td>
					<td><input type="file" name="image"><?php echo $im ;?></td>
				</tr>
								
				<tr>
					<td></td>
					<td><input type="text" id="imageLink" name="imageLink" size="64" value="<?php echo $imageLink ;?>" placeholder="... or paste the image URL"></td>
				</tr>
				<tr style="display:none">
					<td>Profiles</td>
					<td>
					
						<table style="_font-size:90%"><tr>
						
						<?php 

							foreach($PROFILES as $profile){
								if(SD_25_44100 == $profile) echo "<td>";
								if(HD_25_44100 == $profile) echo "</td><td>";
								if(FULLHD_25_44100 == $profile) echo "</td><td>";
									
								echo '<div class="checkbox">' . "\n";
								echo '<label>' . "\n";
								echo'<input type="checkbox" name="' . $profile .'" value="1" '. ($profile == $currentProfile ? "checked" : "").'>';
								echo'</label>' . "\n";
								echo '<label class="label label-default">'. $profile  . "</label>\n";
								echo '</div>' . "\n";
							}
						?></td>
						</table>
					
					</td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" name="add_file" value="Upload video file"></textarea></td>
				</tr>
			</table>
		</form>
		</div>
	</div>



            
  </div>

		  <!--script>

		  $(function() {
			$( "#sortable" ).sortable();
			$( "#sortable" ).disableSelection();
		  });
		  </script-->
		<script>
		

		function setLocal(local){
			if(local==0){
				$(".isLocal").show();
				$(".isRemote").hide();
				$(".isDM").hide();
				$(".isYT").hide(); 
			}if(local==1){
				$(".isLocal").hide(); 
				$(".isRemote").show();
				$(".isDM").hide();
				$(".isYT").hide(); 
			}if(local==2){
				$(".isLocal").hide(); 
				$(".isRemote").hide();
				$(".isDM").show();
				$(".isYT").hide(); 
			}if(local==3){
				$(".isLocal").hide(); 
				$(".isRemote").hide();
				$(".isDM").hide();
				$(".isYT").show();
			}

		}
		
			function setDM(){
				var url = $("#fileDM").val();
				if(url.length == 0){
					alert("No URL provided");
					return;
				}

				  var phpUrl = "./getDMInfo.php?url=" + encodeURIComponent(url) + "&rand=" + Math.random();
				  $.getJSON( phpUrl, function( data ) {
				  $.each( data, function( key, val ) {

						if(key=="title"){
							$("#title").val(val);
						}
						if(key=="date"){
							$("#subtitle").val(val);
						}
						if(key=="image"){
							$("#imageLink").val(val);
						}
						if(key=="description"){
							$("#description").val(val);
						}
						if(key=="localTitle"){
							$("#fileDMName").val(val);
						}
						
						
				  });

			});
		}
		function setYT(){
				var url = $("#fileYT").val();
				if(url.length == 0){
					alert("No URL provided");
					return;
				}

				  var phpUrl = "./getYTInfo.php?url=" + encodeURIComponent(url) + "&rand=" + Math.random();
				  $.getJSON( phpUrl, function( data ) {
				  $.each( data, function( key, val ) {

						if(key=="title"){
							$("#title").val(val);
						}
						if(key=="image"){
							$("#imageLink").val(val);
						}
						if(key=="date"){
							$("#subtitle").val(val);
						}
						if(key=="description"){
							$("#description").val(val);
						}
						if(key=="localTitle"){
							$("#fileYTName").val(val);
						}
						
						
				  });

			});
		}
		

<?php
	echo "setLocal(" . $isLocal . ");";
?>
		 
		 
		 setTimeout(function(){
			$("#errorMsg").hide(); 
		}, 5000);

		function displayPending(){
			$("#pending").show();
		}


		function reloadPage(){
			window.location = window.location.href;
		}
		

		  </script>



</body>

</html>

