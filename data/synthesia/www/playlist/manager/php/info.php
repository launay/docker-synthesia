<?php

require_once "config/config.php";

define("FFMPEG_EXE", "bin/ffmpeg");

function importCurrentPlaylist(){
	return copy(CURRENT_PLAYLIST_FILE, NEXT_PLAYLIST_FILE);
	
}

function switchPlaylist(){

	chmod(CURRENT_PLAYLIST_FILE, 0777); 
	chmod(NEXT_PLAYLIST_FILE, 0777); 

	rename(NEXT_PLAYLIST_FILE, CURRENT_PLAYLIST_FILE);

	chmod(CURRENT_PLAYLIST_FILE, 0777); 
	chmod(NEXT_PLAYLIST_FILE, 0777); 

}

function clearCurrentPlaylist($ids){
	return doClearPlaylist($ids, true);
}
function clearNextPlaylist($ids){
	return doClearPlaylist($ids, false);
}

function doClearPlaylist($ids, $isCurrent){
	$newPlaylist = "";
		
	chmod($isCurrent ? CURRENT_PLAYLIST_FILE : NEXT_PLAYLIST_FILE, 0777); 
	file_put_contents($isCurrent ? CURRENT_PLAYLIST_FILE : NEXT_PLAYLIST_FILE, $newPlaylist);
	chmod($isCurrent ? CURRENT_PLAYLIST_FILE : NEXT_PLAYLIST_FILE, 0777); 
}

function setCurrentPlaylist($ids){
	return doSetPlaylist($ids, true);
}
function setNextPlaylist($ids){
	return doSetPlaylist($ids, false);
}

function doSetPlaylist($ids, $isCurrent){
	
	$newPlaylist = join("\n", $ids);
	$newPlaylist = trim($newPlaylist);
		
	chmod($isCurrent ? CURRENT_PLAYLIST_FILE : NEXT_PLAYLIST_FILE, 0777); 
	file_put_contents($isCurrent ? CURRENT_PLAYLIST_FILE : NEXT_PLAYLIST_FILE, $newPlaylist);
	chmod($isCurrent ? CURRENT_PLAYLIST_FILE : NEXT_PLAYLIST_FILE, 0777); 
	
	
}

function addToCurrentPlaylist($theguid){
	doAddToPlaylist($theguid, true);
}
function addToNextPlaylist($theguid){
	doAddToPlaylist($theguid, false);
}


function doAddToPlaylist($theguid, $isCurrent){
	
	$currentPlaylistFiles = getPlaylistFiles($isCurrent ? CURRENT_PLAYLIST_FILE : NEXT_PLAYLIST_FILE);

	$newFiles = array();
	foreach($currentPlaylistFiles as $guid){
		array_push($newFiles, $guid);
	}
	array_push($newFiles, $theguid);

	$newPlaylist = join("\n", $newFiles);
	$newPlaylist = trim($newPlaylist);
		
	chmod($isCurrent ? CURRENT_PLAYLIST_FILE : NEXT_PLAYLIST_FILE, 0777); 
	file_put_contents($isCurrent ? CURRENT_PLAYLIST_FILE : NEXT_PLAYLIST_FILE, $newPlaylist);
	chmod($isCurrent ? CURRENT_PLAYLIST_FILE : NEXT_PLAYLIST_FILE, 0777); 
	
}

function removeFromCurrentPlaylist($guid){
	doRemoveFromPlaylist($guid, true);

}
function removeFromNextPlaylist($guid){
	doRemoveFromPlaylist($guid, false);

}

function doRemoveFromPlaylist($theguid, $isCurrent){
	
	$currentPlaylistFiles = getPlaylistFiles($isCurrent ? CURRENT_PLAYLIST_FILE : NEXT_PLAYLIST_FILE);

	$newFiles = array();
	foreach($currentPlaylistFiles as $guid){
		if($guid != $theguid){
			array_push($newFiles, $guid);
		}
	}
	$newPlaylist = join("\n", $newFiles);
	$newPlaylist = trim($newPlaylist);
		
	chmod($isCurrent ? CURRENT_PLAYLIST_FILE : NEXT_PLAYLIST_FILE, 0777); 
	file_put_contents($isCurrent ? CURRENT_PLAYLIST_FILE : NEXT_PLAYLIST_FILE, $newPlaylist);
	chmod($isCurrent ? CURRENT_PLAYLIST_FILE : NEXT_PLAYLIST_FILE, 0777); 
	
}

function getPlaylistFiles($playlistFile){
	$files = array();

	if(file_exists($playlistFile)){
		
		$plData = file_get_contents($playlistFile);
		$plFiles = explode("\n", trim($plData));
		foreach($plFiles as $plFile){
			$plFile = trim($plFile);
			if(strlen($plFile) == 0) continue;
			$file = MASTER_FOLDER . $plFile;
			if(file_exists($file)){
				array_push($files, $plFile);
			}
		}

	}
	return $files;
}

function getCurrentProfile($prettyVersion = true){

	$paramsData = file_get_contents(PLAYLIST_PROPERTIES_FILE);	
	$params = explode("\n", trim($paramsData));
	$paramsArray = array();
	foreach($params as $param){
		$param = trim($param);
		$keyVal = explode("=", $param,2);
		$key = trim($keyVal[0]);
		$val = trim($keyVal[1]);
		$paramsArray[$key] = $val;
	}

	$sampling = $paramsArray['samplingRate'];
	$fps = $paramsArray['fps'];
	$width = $paramsArray['width'];
	$height = $paramsArray['height'];
	$normalizeAudio = $paramsArray['normalizeAudio'] == "true" ? true : false;
	
	$prof = array("audio"=>$sampling, "video"=>$fps, "size"=>($width . "x" . $height), "normalizeAudio"=>$normalizeAudio);
	if(!$prettyVersion) return $prof;
	$profile = getPrettyProfile($prof);
	
	return $profile;
	
}

function getAllProfiles(){
	$PROFILES = array(
		SD_25_44100, SD_25_48000, SD_30_44100, SD_30_48000,
		HD_25_44100, HD_25_48000, HD_30_44100, HD_30_48000,
		FULLHD_25_44100, FULLHD_25_48000, FULLHD_30_44100, FULLHD_30_48000
	);
	return $PROFILES;

}

function checkName($name){
	
	$path_parts = pathinfo($name);
	$base = $path_parts['filename'];	
	$mp4name = $base . ".mp4";
	
	return file_exists(MASTER_FOLDER . $mp4name);
}

function addVideo($toAdd, $toAddTmp, $image, $title, $subtitle, $description, $isUpload=true, $date=null, $source=null){
	$path_parts = pathinfo($toAdd);
	$base = $path_parts['filename'];
	$mp4name = $base . ".mp4";

	$mp4Path = MASTER_FOLDER . $mp4name;
	$descriptionName = $mp4Path . ".json";
	
	$ok = true;

	if($isUpload)
		$ok = move_uploaded_file($toAddTmp, $mp4Path);
	else
		$ok = rename($toAddTmp, $mp4Path);

	if(!$ok) return false;

	$guid = $mp4Path;
	
	chmod($mp4Path, 0777); 	
	updateMeta($guid, $title, $subtitle, $description, $image, true, $date, $source);	
	
	//force setting duration
	getVideoDescription($guid);

	return $ok;
}

function removeVideo($guid){
	
	$masterVideo = MASTER_FOLDER . $guid;
	if(file_exists($masterVideo)){
		unlink($masterVideo);
	}
	$masterVideoDescription = MASTER_FOLDER . $guid . ".json";
	if(file_exists($masterVideoDescription)){
		unlink($masterVideoDescription);
	}
	
	$glob = IMAGES_UPLOAD_FOLDER . $guid . IMAGE_SFX . "*";
	$images = glob($glob);
	foreach($images as $key=>$image){
		$im = IMAGES_UPLOAD_FOLDER . $image;
		unlink($im);
	}

	$PROFILES = array(
		SD_25_44100, SD_25_48000, SD_30_44100, SD_30_48000,
		HD_25_44100, HD_25_48000, HD_30_44100, HD_30_48000,
		FULLHD_25_44100, FULLHD_25_48000, FULLHD_30_44100, FULLHD_30_48000
	);
	foreach($PROFILES as $profile){
		removeProfile($guid, $profile);
	}
	
	return true;
}

function updateProfiles($guid, $profiles){

	foreach($profiles as $profile=>$update){
		if(!$update){
			removeProfile($guid, $profile);
		}else{
			createProfile($guid, $profile);
		}

	}

	return true;
}

function createProfile($guid, $profile){

	$file = IN_FOLDER . $guid . "." . rand() . ".json"; 
	
	$jsonData = array();
	$jsonData["guid"] = $guid;
	$jsonData["bitrate"] = DEFAULT_BITRATE;
	$jsonData["audio"] = getProfileAudio($profile);
	$jsonData["video"] = getProfileVideo($profile);
	$jsonData["size"] = getProfileSize($profile);
	
	$currentProf = getCurrentProfile(false);
	$jsonData["normalizeAudio"] = $currentProf["normalizeAudio"];

	$json = json_encode($jsonData);
	
	file_put_contents($file, $json);
	chmod($file, 0777);	

}

function removeProfile($guid, $profile){
	doRemoveProfile($guid, $profile, IN_FOLDER);
	doRemoveProfile($guid, $profile, OUT_FOLDER);
}

function doRemoveProfile($guid, $profile, $folder){

	$glob = $folder . "*.json";
	$descrs = glob($glob);
	foreach($descrs as $key=>$descrJson){
		$json = file_get_contents($descrJson);
		$descr = json_decode($json,true);
		
		$id = $descr["guid"];
		if($id==$guid){
			
			$prof = getPrettyProfile($descr);
			if($prof != $profile) continue;
			
			unlink($descrJson);
			
			if($folder == OUT_FOLDER){
				$mediaFile = pathinfo($descrJson);
				$mediaFileName = $mediaFile["filename"];
				$toRemove = OUT_FOLDER . $mediaFileName;			
				if(file_exists($toRemove)){
					unlink($toRemove);
				}
			}
		}
	}
}

function getProfileAudio($profile){

	if(	SD_25_44100 == $profile
		|| SD_30_44100 == $profile
		|| HD_25_44100 == $profile
		|| HD_30_44100 == $profile
		|| FULLHD_25_44100 == $profile
		|| FULLHD_30_44100 == $profile
	){
		return AUDIO_44100;
	}
	
	if(	SD_25_48000 == $profile
		|| SD_30_48000 == $profile
		|| HD_25_48000 == $profile
		|| HD_30_48000 == $profile
		|| FULLHD_25_48000 == $profile
		|| FULLHD_30_48000 == $profile
	){
		return AUDIO_48000;
	}
}

function getProfileVideo($profile){
	
	if(	SD_25_44100 == $profile
		|| SD_25_48000 == $profile
		|| HD_25_44100 == $profile
		|| HD_25_48000 == $profile
		|| FULLHD_25_44100 == $profile
		|| FULLHD_25_48000 == $profile
	){
		return VIDEO_25;
	}
	
	if(	SD_30_44100 == $profile
		|| SD_30_48000 == $profile
		|| HD_30_44100 == $profile
		|| HD_30_48000 == $profile
		|| FULLHD_30_44100 == $profile
		|| FULLHD_30_48000 == $profile
	){
		return VIDEO_30;
	}
	
}
function getProfileSize($profile){

	if(	SD_25_44100 == $profile
		|| SD_25_48000 == $profile
		|| SD_30_44100 == $profile
		|| SD_30_48000 == $profile
	){
		return SD;
	}

	if(	HD_25_44100 == $profile
		|| HD_25_48000 == $profile
		|| HD_30_44100 == $profile
		|| HD_30_48000 == $profile
	){
		return HD;
	}

	if(	FULLHD_25_44100 == $profile
		|| FULLHD_25_48000 == $profile
		|| FULLHD_30_44100 == $profile
		|| FULLHD_30_48000 == $profile
	){
		return FULLHD;
	}

}

function updateMeta($guid, $title, $subtitle, $descr, $image, $create=false, $date=null, $source=null){

	$descrFile = MASTER_FOLDER . $guid . ".json";

	$description = array();
	if($create){
		$description["guid"] = $guid;
	}else{
		if(!file_exists($descrFile)){
			return false;
		}
		$json = file_get_contents($descrFile);
		$description = json_decode($json,true);
	}
	if(count($description)==0) return false;

	$description["title"] = $title;
	$description["subtitle"] = $subtitle;
	$description["description"] = $descr;
	$description["image"] = $image;
	if($date){
		$description["date"] = $date;
	}
	else{
		$description["date"] = time();
	}
	if($source){
		$description["source"] = $source;
	}
	else{
		$description["source"] = SOURCE_LOCAL;
	}

	$newData = json_encode($description);
	if(!file_put_contents($descrFile, $newData)){
		return false;
	}
	
	chmod($descrFile, 0777); 

	return true;

}

function isInPlaylist($file, $playlistFile, $folder=MASTER_FOLDER){

	$guid = getGuid($file);
	if(file_exists($playlistFile)){
		$plData = file_get_contents($playlistFile);
		if($plData){
			$plData = trim($plData);
			$plData = explode("\n", $plData);
			if(in_array($guid, $plData))
				return true;
		}
	}
	return false;
}


function isMediaFile($file){

	$MEDIA_EXTENSIONS = array(".MP4", ".MP3", ".AAC", ".ADTS");
	foreach($MEDIA_EXTENSIONS as $ext){
		if(endsWith(strtoupper(basename($file)), $ext)){
			return true;
		}
	}
	return false;
}

function getVideoDescription($file, $folder=MASTER_FOLDER){
	$description = array();
	$descrFile = $folder . $file . ".json";
	if(file_exists($descrFile)){
		$json = file_get_contents($descrFile);
		$description = json_decode($json,true);
		if($folder == MASTER_FOLDER && !$description[DURATION]){
			$size = getMediaDuration($folder . $file);
			if($size){
				$description[DURATION] = $size;
				$jsonBack = json_encode($description);
				file_put_contents($descrFile, $jsonBack);
			}
		}
		if($folder == MASTER_FOLDER && !$description[SOURCE]){
			$description[SOURCE] = SOURCE_LOCAL;
			$jsonBack = json_encode($description);
			file_put_contents($descrFile, $jsonBack);
		}
		if($folder == MASTER_FOLDER && !$description[DATE]){
			$date = filemtime($folder . $file);
			$description[DATE] = $date;
			$jsonBack = json_encode($description);
			file_put_contents($descrFile, $jsonBack);
		}
	}
	
	return $description;
	
}

function getGuid($file){
	return basename($file);
}


function getPrettyProfile($descr){
	
	$audio = abs($descr["audio"]);
	$video = abs($descr["video"]);
	$size = abs($descr["size"]);
	
	if(SD == $size){
		if(VIDEO_25 == $video && AUDIO_44100 == $audio) return SD_25_44100;
		if(VIDEO_25 == $video && AUDIO_48000 == $audio) return SD_25_48000;		
		if(VIDEO_30 == $video && AUDIO_44100 == $audio) return SD_30_44100;
		if(VIDEO_30 == $video && AUDIO_48000 == $audio) return SD_30_48000;		
	}
	if(HD == $size){
		if(VIDEO_25 == $video && AUDIO_44100 == $audio) return HD_25_44100;
		if(VIDEO_25 == $video && AUDIO_48000 == $audio) return HD_25_48000;		
		if(VIDEO_30 == $video && AUDIO_44100 == $audio) return HD_30_44100;
		if(VIDEO_30 == $video && AUDIO_48000 == $audio) return HD_30_48000;		
	}
	if(FULLHD == $size){
		if(VIDEO_25 == $video && AUDIO_44100 == $audio) return FULLHD_25_44100;
		if(VIDEO_25 == $video && AUDIO_48000 == $audio) return FULLHD_25_48000;		
		if(VIDEO_30 == $video && AUDIO_44100 == $audio) return FULLHD_30_44100;
		if(VIDEO_30 == $video && AUDIO_48000 == $audio) return FULLHD_30_48000;		
	}
	
	return UNKNOWN_PROFILE;
}

function getProfiles($file, $kind, $folder=MASTER_FOLDER){
	$profiles = array();

	$guid = getGuid($file);

	$outFolder = OUT_FOLDER;	
	if(AVAILABLE == $kind){
		$outFolder = OUT_FOLDER;
	}
	if(CONVERTING == $kind){
		$outFolder = CONVERT_FOLDER;
	}
	if(WAITING == $kind){
		$outFolder = IN_FOLDER;
	}
	
	$glob = $outFolder . "*.json";
	$descrs = glob($glob);
	foreach($descrs as $key=>$descrJson){
		$json = file_get_contents($descrJson);
		$descr = json_decode($json,true);
		
		$descr[PROFILE] = getPrettyProfile($descr);
		
		$id = $descr["guid"];
		if($id==$guid){
			$md5 = md5_file($descrJson);
			$profiles[$md5] = $descr;
		}
	}


	return $profiles;
}

function getVideoStatus($file, $folder=MASTER_FOLDER){
	$status=array();

	$avProfiles = getProfiles($file, AVAILABLE);
	$status[AVAILABLE] = $avProfiles;

	$convProfiles = getProfiles($file, CONVERTING);
	$status[CONVERTING] = $convProfiles;
	
	$waitProfiles = getProfiles($file, WAITING);
	$status[WAITING] = $waitProfiles;


	$inCurrentPlaylist = isInPlaylist($file, CURRENT_PLAYLIST_FILE);
	$inNextPlaylist = isInPlaylist($file, NEXT_PLAYLIST_FILE);
	$canRemove = 
		(!$inCurrentPlaylist) && 
		(!$inNextPlaylist) && 
		(count($waitProfiles) == 0) && 
		(count($convProfiles) == 0);
	$status[IN_CURRENT_PLAYLIST] = $inCurrentPlaylist;
	$status[IN_NEXT_PLAYLIST] = $inNextPlaylist;
	$status[CAN_REMOVE] = $canRemove;

	return $status;

}

function getAllVideos($folder=MASTER_FOLDER){

	$videos = array();
	$result = array();

	$vids = scandir($folder);
	asort($vids);
	foreach($vids as $key=>$vid){
		if(isMediaFile($vid)){
			$guid = getGuid($vid);
			$descr = getVideoDescription($vid);
			$videos[$guid] = $descr;
			$status = getVideoStatus($vid);
			$videos[$guid]["status"] = $status;
		}
	}

	return $videos;


	$currentPlaylistFiles = getPlaylistFiles(CURRENT_PLAYLIST_FILE);
	$nextPlaylistFiles = getPlaylistFiles(NEXT_PLAYLIST_FILE);

	
	
	foreach($videos as $key=>$vid){
		$status = 0;

		$inCurrent = in_array($vid, $nextPlaylistFiles);
		$inNext = in_array($vid, $currentPlaylistFiles);
		if($inCurrent)
			$status += STATUS_IN_NEXT_PLAYLIST;
		if($inNext)
			$status += STATUS_IN_CURRENT_PLAYLIST;
			
		if(!$inCurrent && !$inNext)
			$status = STATUS_UNUSED;
			
		if($folder == UPLOAD_FOLDER) $status = STATUS_WAITING_FOR_CONVERSION;
		if($folder == CONVERT_FOLDER) $status = STATUS_CONVERTING;
		
		$title= "";
		$subtitle= "";
		$description = "";
		$descriptionFile = $folder . "/" . $vid . ".json";
		if(!file_exists($descriptionFile))continue;
		
		$size=0;
		$guid="";
		$jsonData = file_get_contents($descriptionFile);
		if($jsonData){
			$json = json_decode($jsonData);
			if($json){
				$image = $json->image;
				$title = $json->title;
				$subtitle = $json->subtitle;
				$description = $json->description;
				$guid = $json->guid;
				
				$size = $json->size;
				$sizeSet = true;
				if(!isset($json->size) || $size == 0){
					$sizeSet = false;
					$sz = exec("../getDuration.sh " . $folder . "/" . $vid, $output, $res);
					if(!$res){
						$size = intval($sz);
					}
				}
				$guidSet = true;
				if(!$guid || strlen($guid)==0){
					$guidSet = false;
					$md5 = md5_file($folder . "/" . $vid);
					$guid = $md5;
					
				}
				
				if(!$sizeSet || !$guidSet){
					$newData = array("image"=>$image, "title"=>$title, "subtitle"=>$subtitle, "description"=>$description, "guid"=>$guid, "duration"=>$size);
					$newJson = json_encode($newData);
					file_put_contents($descriptionFile, $newJson);
				}
			}
		}
		
		
		$data = array("file"=>$vid, "status"=>$status, "image"=>$image, "title"=>$title, "subtitle"=>$subtitle, "description"=>$description, "guid"=>$guid, "duration"=>$size);
		array_push($result, $data);
	}

	usort($result, "vidSort");

	return $result;

}

function getMediaDuration($file){
	$size = 0;

	$sz = exec("./getDuration.sh " . $file, $output, $res);
	if(!$res){
		$size = intval($sz);
	}

	return $size;
}

function getMediaData($folder, $filePath){

	$path =  pathinfo($filePath); 
	$name = $path['basename'];	
	$descriptionFile = $folder . $name . ".json";
	if(file_exists($descriptionFile)){
		$jsonDescr = file_get_contents($descriptionFile);
		$jsonDescrData = json_decode($jsonDescr, true);
		$guid = $jsonDescrData["guid"];
		if($guid){
			$masterDescriptionFile = MASTER_FOLDER . $guid . ".json";
			if(file_exists($masterDescriptionFile)){
				$jsonMaster = file_get_contents($masterDescriptionFile);
				$jsonMasterDescrData = json_decode($jsonMaster, true);
				$jsonMasterDescrData["guid"] = $guid;
				return $jsonMasterDescrData;
			}
			
		}
	}

	return array();

}

function trimImageUrl($image){
	$lm = 0;
	if(file_exists($image))
		$lm = filemtime($image);
	$image= str_replace(IMAGES_LINK_FOLDER, IMAGES_UPLOAD_FOLDER, $image);
	return $image . "?lm=" . $lm;
}

function cutString($string, $maxchars){
	$string = str_replace("\n", " ", $string);
	$modified = false;
	while(strlen($string)>$maxchars){
		$pos = strrpos($string, " ");
		if($pos === false) break;
		$string = substr($string, 0, $pos);
		$modified = true;
	}

	if($modified) 
		return substr($string, 0, $maxchars) . "[...]";
	else return $string;
}

function printDuration($duration){
	if(!$duration) return "(inconnue)";


	$ms = $duration;
	$hours = 0;
	$minutes = 0;
	$seconds = 0;


	if ( $ms > 3600000 ){
		$hours = floor( $ms / 3600000 );
		$ms = $ms % 3600000;
	}
	if ( $ms > 60000 ){
		$minutes = floor( $ms / 60000 );
		$ms = $ms % 60000;
	}
	$seconds = floor( $ms / 1000 );


	return str_pad( $hours, 2, '0', STR_PAD_LEFT ) . "h"
		. str_pad( $minutes, 2, '0', STR_PAD_LEFT ) . "m"
		. str_pad( $seconds, 2, '0', STR_PAD_LEFT ) . "s";

	
}

function startsWith($haystack, $needle) {
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

function createHeader(){
	echo '
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>' . MAIN_TITLE . '</title>
		<script src="js/info.js"></script>
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery-ui/jquery-ui.js"></script>
				
		<link rel="icon" type="image/png" href="images/synthesia.png" />
		<link rel="stylesheet" type="text/css" href="js/jquery-ui/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
		<style>
		body {
			padding-top: 70px;	
		}
		.pad_10{
			padding: 10px;
		}
		
		.evenOdd tr:nth-child(even) {background: #EEE}
		.evenOdd tr:nth-child(odd) {background: #FFF}
		
		td { 
			padding: 10px;
		}
		
		.pad_4{
			padding: 4px;
		}
		.bordered_nok{
			border: 3px solid #DDD;
		}
		.bordered_ok{
			border: 3px solid red;
		}
		.font_80 {
			font-size: 85%;	
		}
		.btn-warning{
			background-color:#ff3d00;
		}
		.lbl-warning{
			background-color:#ff3d00;
		}
		.panel-heading a:after {
			font-family:"Glyphicons Halflings";
			content:"\e114";
			float: right;
			color: grey;
		}
		.panel-heading a.collapsed:after {
			content:"\e080";
		}
		.clickable{
			cursor: pointer;
		}
		.table-no-border td {
			border-top: none !important;
		}
		.table-label{
			font-weight:bold;
		}
		.row{
			margin-top:40px;
			padding: 0 10px;
		}
		.panel-heading span {
			margin-top: -20px;
			font-size: 15px;
		}
		</style>'
	;

}


function createNavBar($selectedOne, $selectedTwo){

	echo '

	<nav class="navbar navbar-default navbar-fixed-top">
		  <div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			<a class="navbar-brand" href="index.php"><span style="color:black;"><b>' . MAIN_TITLE . '</b></span></a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">
			  				<li><img src="images/synthesia.png" height="50"/>&nbsp;&nbsp;&nbsp;</li>

			  ';

				echo '				
					<li class="dropdown' . (($selectedOne == "current" || $selectedOne == "next") ? " active" : "") .'">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Playlist<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li ' . ($selectedOne == "current" ? 'class="active"' : '') . '><a href="current.php">Current Playlist</a></li>
							<li ' . ($selectedOne == "next" ? 'class="active"' : '') . '><a href="next.php">Next Playlist</a></li>
						</ul>
					</li>
					<li class="dropdown' . (($selectedOne == "add" || $selectedOne == "video") ? " active" : "") .'">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Videos<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li ' . ($selectedOne == "add" ? 'class="active"' : '') . '><a href="add.php">Add a video</a></li>
							<li ' . ($selectedOne == "video" ? 'class="active"' : '') . '><a href="video.php">Video database</a></li>
						</ul>				
					</li>
					<li ' . ($selectedOne == "import" ? 'class="active"' : '') . '><a href="import.php">Automation</a></li>
					<li class="dropdown' . (($selectedOne == "config" || $selectedOne == "status" || $selectedOne == "log") ? " active" : "") .'">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li ' . ($selectedOne == "config" ? 'class="active"' : '') . '><a href="config.php">Configuration</a></li>
							<li ' . ($selectedOne == "status" ? 'class="active"' : '') . '><a href="status.php">Status</a></li>
							<li ' . ($selectedOne == "log" ? 'class="active"' : '') . '><a href="log.php">Log</a></li>
						</ul>				
					</li>


				';			
			  echo'
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li><a href="../../main/">MAIN MENU</a></li>
			  </ul>
			</div>
		  </div>
		</nav>


	';

}
?>
