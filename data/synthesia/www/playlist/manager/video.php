<?php
require_once "php/info.php";

$PROFILES = getAllProfiles();

$message = "";
$error = false;

//handles remove request
if(isset($_POST["remove"])){
	$guid = $_POST["guid"];
	$error = false;
	$message = "Video file " . $guid . " was succesfully deleted";
	
	if(!removeVideo($guid)){
		$error = false;
		$message = "Video file " . $guid . " could not be deleted";

	}

}


//print_r($_POST);
//print_r($_FILES);


//handles modify request
$showModify = false;
if(isset($_POST["updateGuid"])){

	$error = false;
	$message = "Video description was succesfully updated";

	$guid = $_POST["updateGuid"];
	$videoData = getVideoDescription($guid);
	$videoData[STATUS] = getVideoStatus($guid);

	$updateTitle = $_POST["updateTitle"];
	$updateSubtitle = $_POST["updateSubtitle"];
	$updateDescription = $_POST["updateDescription"];
	$updateImage = $_POST["oldImage"];
		
	$imageFullName = $updateImage;
	if(isset($_FILES["updateImage"])){

		$imageUp = $_FILES["updateImage"];
		$hasImage = $imageUp["size"] >0;
		if($hasImage && $imageUp["error"]){
			$error = true;
			$message = "An error occurred while uploading image";
		}

		if(!$error && $hasImage){

			$name =  $imageUp["name"];
			$nameTmp =  $imageUp["tmp_name"];
			$ext = "." . pathinfo($name, PATHINFO_EXTENSION);
			$outName = $guid . IMAGE_SFX . $ext;

			$imageFullName = IMAGES_LINK_FOLDER . $outName;
			
			if(!move_uploaded_file($nameTmp, $imageFullName)){
				$error = true;
				$message = "An error occurred while moving image";
			}
			else{
				chmod($fullName, 0777);	
				$updateImage = IMAGES_UPLOAD_FOLDER . $outName;
			}

		}
		
		if(!updateMeta($guid, $updateTitle, $updateSubtitle, $updateDescription, $imageFullName)){
			$error = true;
			$message = "An error occurred while updating video metadata";
		}

	}
	
	
	//finally updates in the data
	$videos[$guid]["title"] = $updateTitle;
	$videos[$guid]["subtitle"] = $updateSubtitle;
	$videos[$guid]["description"] = $updateDescription;
	$videos[$guid]["image"] = $updateImage;
	
	
	//updates profiles
	if(true || !$_POST["profilesDisabled"]){
		$profiles = array();
		foreach($PROFILES as $profile){
			$profile2 = str_replace(".", "_", $profile);
			$oldValueLabel = "old_" . $profile2;
			$disValueLabel = "dis_" . $profile2;
			
			$oldValue = isset($_POST[$oldValueLabel]) && $_POST[$oldValueLabel];
			$disValue = isset($_POST[$disValueLabel]) && $_POST[$disValueLabel];
			$newValue = isset($_POST[$profile2]) && $_POST[$profile2];
			$mustUpdate = ($oldValue != $newValue) && ! $disValue;
					
			if($mustUpdate){
				$profiles[$profile] = $newValue ? "1" : "0";
			}
		
		}
				
		if(!updateProfiles($guid, $profiles)){
			$error = true;
			$message = "An error occurred while updating profiles";
		}
	}

}
//handles modify request
$showModify = false;
if(isset($_POST["modify"])){


	$showModify = true;

	$guid = $_POST["guid"];
	$videoData = getVideoDescription($guid);
	$videoData[STATUS] = getVideoStatus($guid);

	$modif_guid = $guid;
	$modif_title = $videoData["title"];
	$modif_subtitle = $videoData["subtitle"];
	$modif_description = $videoData["description"];
	$modif_image = $videoData["image"];
	$oldImage = $videoData["image"];
	
	$modify_in_playlist = $videoData[STATUS][IN_CURRENT_PLAYLIST] || $videoData[STATUS][IN_NEXT_PLAYLIST];
	
	$modify_profiles = array();
	$modify_avProfiles = array();
	$modify_convProfiles = array();
	$modify_waitProfiles = array();
	foreach($videoData[STATUS][AVAILABLE] as $md5=>$profile){
		array_push($modify_profiles, $profile[PROFILE]);
		array_push($modify_avProfiles, $profile[PROFILE]);
	}
	foreach($videoData[STATUS][CONVERTING] as $md5=>$profile){
		array_push($modify_profiles, $profile[PROFILE]);
		array_push($modify_convProfiles, $profile[PROFILE]);
	}
	foreach($videoData[STATUS][WAITING] as $md5=>$profile){
		if(in_array($profile[PROFILE], $modify_convProfiles)) continue;
		array_push($modify_profiles, $profile[PROFILE]);
		array_push($modify_waitProfiles, $profile[PROFILE]);
	}

}

$videos = getAllVideos();
$videoNumber = count($videos);

//print_r($videos);


?>
<html>
	<head>
		<?php createHeader();?>

	</head>
	<style>
	.vzr_centered {
	   text-align: center;   
	}
	</style>

	<body>

	<script>
		function doConfirm(video) {
			return confirm("Are you sure you want to delete video " +video + "?");
		}
	</script>


		<?php createNavBar("video",""); ?>

<?php
if(strlen($message)>0){
	$class="label label-default";
	if($error)
		$class="label label-danger";
	echo '<div class="' . $class . '" style="display:block">' . $message .'</div></br>';
}
?>

<!-- Modal -->
<div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                   data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="modalTitle">
                    Editing video <b><?php echo $modif_guid; ?></b>
                </h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                <?php
						$disabled = $modify_in_playlist;
                ?>
                <form role="form" id="update" method="POST" enctype="multipart/form-data" >
				  <input type="hidden" id="updateGuid" name="updateGuid" value="<?php echo htmlspecialchars($modif_guid); ?>"/>
				  <input type="hidden" id="profilesDisabled" name="profilesDisabled" value="<?php echo $disabled ? "1" : "0"; ?>"/>
				 <input value="<?php echo htmlspecialchars($modif_image); ?>" type="hidden" class="form-control" id="oldImage" name="oldImage" />
                  <div class="form-group">
						<label for="updateTitle">Title</label>
						<input value="<?php echo htmlspecialchars($modif_title); ?>" type="text" class="form-control" id="updateTitle" name="updateTitle" placeholder="Enter title"/>
                  </div>
                  <div class="form-group">
						<label for="updateSubtitle">SubTitle</label>
						<input value="<?php echo htmlspecialchars($modif_subtitle); ?>" type="text" class="form-control" id="updateSubtitle" name="updateSubtitle" placeholder="Enter subtitle"/>
                  </div>
                  <div class="form-group">
						<label for="updateDescription">Description</label>
						<textarea class="form-control" id="updateDescription" name="updateDescription" placeholder="Enter description"><?php echo htmlspecialchars($modif_description); ?></textarea>
                  </div>
                   <div class="form-group">
						<label>Image</label>
						<table><tr>
						<td><img id="image_preview" src="<?php echo htmlspecialchars(trimImageUrl($oldImage));?>" height="80"/></td>
						<td><input value="<?php echo htmlspecialchars($modif_image); ?>" type="file" id="updateImage" name="updateImage"/></td>
						</tr></table>
				   </div>
                   <div class="form-group" style="display:none">
						<label>Profiles</label>
						<?php
							if($disabled){
								echo "<br/><span><i>You cannot remove profiles for a video that belongs to a playlist</i></span>";
							}
						?>
						<table style="_font-size:90%"><tr>
						
						<?php 

							foreach($PROFILES as $profile){
								if(SD_25_44100 == $profile) echo "<td>";
								if(HD_25_44100 == $profile) echo "</td><td>";
								if(FULLHD_25_44100 == $profile) echo "</td><td>";
								$profileClass= "label label-default";								
								
								$isAv = false;
								if(in_array($profile, $modify_avProfiles)) {
									$profileClass= "label label-success";
									$isAv=true;
								};
								if(in_array($profile, $modify_convProfiles)) {
									 $profileClass= "label label-warning";
									$isAv=true;
								 }
								if(in_array($profile, $modify_waitProfiles)) {
									$profileClass= "label label-danger";
									$isAv=true;
								}

								if(in_array($profile, $modify_convProfiles)){
									$disabled= true;
								}
									
								echo '<div class="checkbox">' . "\n";
								echo '<label>' . "\n";
								echo '<input type="hidden" name="dis_' . $profile .'" value="' . (($disabled && $isAv) ? "1" : "0") .'">' . "\n";
								echo'<input type="hidden" name="old_' . $profile .'" value="' . (in_array($profile, $modify_profiles) ? "1" : "0") .'">' . "\n";
								echo'<input ' . ($disabled && $isAv ? "disabled" : "") .' type="checkbox" name="' . $profile .'" value="1" ' . (in_array($profile, $modify_profiles) ? "checked" : "") . '>';
								echo'</label>' . "\n";
								echo '<label class="' . $profileClass. '">'. $profile  . "</label>\n";
								echo '</div>' . "\n";
							}
						?></td></tr>
						</table>
                  </div>
                  <button type="submit" onclick="" class="btn btn-default">Update video metadata</button>
                </form>
                
                
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default">


	<div class="panel-heading">
	  <h3 class="panel-title">Video Database (<?php echo $videoNumber;?> video(s))</h3>
	</div>
	<div class="panel-body">
	  <h4 class="font_80"><i>(This page is not refreshed automatically. You must <button onclick="reloadPage();">reload page</button> to follow video processing status)</i></h4>
	</div>
		
	<div class="panel-body" _style="max-height: 400;overflow-y: scroll;">
		<div class="container">
			<table class="font_80 evenOdd" id="plTable" style="white-space: nowrap;">
			  <thead>
					<tr>
						<th>Video file&nbsp;</th>
						<th class="vzr_centered">Duration&nbsp;</th>
						<th class="vzr_centered">Date&nbsp;</th>
						<th class="vzr_centered">Source&nbsp;</th>
						<th class="vzr_centered">Image&nbsp;</th>
						<th>Title / SubTitle / Description&nbsp;</th>
						<th class="vzr_centered">Status</th>
					</tr>
			  </thead>
			  <tbody id="sortable">
<?php

$count=0;
foreach($videos as $key=>$video){
	$count++;
	
	$date = date("d/m/Y H:i:s", $video[DATE]);
	$source = $video[SOURCE];

	$guid = $key;
	$duration = $video[DURATION];
	$image = $video[IMAGE];
	$title = $video[TITLE];
	$subtitle = $video[SUBTITLE];
	$description = $video[DESCRIPTION];
	$canRemove = $video[STATUS][CAN_REMOVE];
	$avProfiles = array();
	$convProfiles = array();
	$waitProfiles = array();
	foreach($video[STATUS][AVAILABLE] as $md5=>$profile){
		array_push($avProfiles, $profile[PROFILE]);
	}
	foreach($video[STATUS][CONVERTING] as $md5=>$profile){
		array_push($convProfiles, $profile[PROFILE]);
	}
	foreach($video[STATUS][WAITING] as $md5=>$profile){
		if(in_array($profile[PROFILE], $convProfiles)) continue;
		array_push($waitProfiles, $profile[PROFILE]);
	}
	
	echo "<tr id='" . $guid . "'>";
	echo "<td><a class='font_80' href='". (MASTER_FOLDER  . rawurlencode($guid))."' target='_blank'>" . $guid ."</a></td>";
	echo "<td class='vzr_centered'>" . printDuration($duration) ."</td>";

	echo "<td class='vzr_centered'>" . $date ."</td>";
	echo "<td class='vzr_centered'>" . $source ."</td>";

	echo "<td class='vzr_centered'><img height='40' src='" . trimImageUrl($image)  . "'/></td>";
	echo "<td>" . htmlspecialchars(cutString($title, 48), ENT_QUOTES)  . "<br/>";
	echo "" . htmlspecialchars(cutString($subtitle, 48), ENT_QUOTES)  . "<br/>";
	echo "" . htmlspecialchars(cutString($description,48), ENT_QUOTES) . "</td>";
	echo "<td class='vzr_centered'>";
	$count1=0;
	foreach($avProfiles as $profile){
		echo '<span class="label label-success">OK</span>';
		if(($count1)%4 == 3) echo '<br/><br/>';
		$count1++;
	}
	foreach($convProfiles as $profile){
		echo '<span class="label label-warning">PROCESSING</span>';
		if(($count1)%4 == 3) echo '<br/><br/>';
		$count1++;
	}
	foreach($waitProfiles as $profile){
		echo '<span class="label label-danger">QUEUED</span>';
		if(($count1)%4 == 3) echo '<br/><br/>';
		$count1++;
	}
	"</td>";

	$submitText = "return doConfirm(\"" . $guid ."\")";

	echo "<td>
			<form method='post' name='modify_" . $count . "'>
				<input type='submit' name='modify' value='Edit'></input>
				<input type='hidden' name='guid' value='" . $guid . "'></input>
			</form>
		</td>
		<td>
			<form method='post' name='remove_" . $count . "' onsubmit='" . $submitText ."'>
				<input type='submit' name='remove' value='Delete' " . ($canRemove ? "" : ("disabled")) ."></input>
				<input type='hidden' name='guid' value='" . $guid . "'></input>
			</form>
		</td>";

	echo "</tr>\n";
}

?>
				</tbody>
			</table>
	
		</div>
	</div>
		               
  </div>


<script>
  <?php if($videoNumber == 0){echo '$("#plTable").hide();';} ?>

<?php if($showModify) echo '$("#myModalNorm").modal();' ?>

		function reloadPage(){
			window.location = window.location.href;
		}

</script>



</body>

</html>

