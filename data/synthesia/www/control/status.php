<?php
require_once "php/info.php";

$numInputs = 1;
$numOutputs = 0;

//reads number of inputs/outputs
{
	$paramsFile = "../config/startup.json";
		if(!file_exists($paramsFile)){
			$error = true;
			$mesage = "Impossible to find Synthesia parameters file";
		}
		else{
			$paramsData = file_get_contents($paramsFile);
			if(!$paramsData){
				$error = true;
				$mesage = "Impossible to read Synthesia parameters file";
			}
			else{
				$paramsJson = json_decode($paramsData,true);
				if(!$paramsJson){
					$error = true;
					$mesage = "Impossible de decode Synthesia parameters";
				}
				else{
					$ins = $paramsJson['setup']['videoUrl'];
					$ins = trim($ins);
					$numInputs = count(explode(";",$ins));
					$outs = $paramsJson['setup']['vzrRtmpOut'];
					$outs = trim($outs);
					$numOutputs = count(explode(";",$outs));
				}
			}
	
		}
	}

?>

<html>
	<head>
		<?php createHeader();?>

<script>
	
	
function getJSONValueOrDefault(jsonObject, key, def){
	var value = jsonObject[key];
	if(!value) return def;
	return value;
	
}

lm = 0;
function refreshStatus(){
		var statusFile = "../status/status.json";
		$.getJSON( statusFile + "?rand=" + Math.random(), function( data ) {


			var engineRunning = true;
			var lastModified = getJSONValueOrDefault(data.status, "lastPublish", 0);
			var engineUptime = getJSONValueOrDefault(data.status, "LIVE_PROVIDER_UPTIME", "00:00:00.000");

			if(lastModified == lm){
				engineRunning = false;
			}
			
			var oldLm = lm;

			lm = lastModified;
			
			//first turn
			if(oldLm == 0) return;

			$("#engineRunning").removeClass("ok");
			$("#engineRunning").removeClass("nok");
			$("#engineRunning").addClass( engineRunning  ? "ok" : "nok");
			$("#engineRunning").html( engineRunning ? "Running" : "Stopped");
			$("#engineUptime").html(engineRunning ? engineUptime : "");

			var numInputs = <?php echo $numInputs?>;
			var numOutputs = <?php echo $numOutputs?>;
		
			var serverURLArray = new Array();
			var serverRunningArray = new Array();
			var serverBRArray = new Array();
			var serverUptimeArray = new Array();
			var cdnRunningArray = new Array();
			var cdnBRArray = new Array();
			var cdnUptimeArray = new Array();
			var cdnURLArray = new Array();			
			
			for (i = 0; i < numInputs; i++) {
				var sfx = numInputs < 2 ? "" : i;
				serverRunningArray[i] = getJSONValueOrDefault(data.status, "VZR_RTMP_SERVER_RUNNING"+sfx, false);
				serverBRArray[i] = Math.max(0, getJSONValueOrDefault(data.status, "VZR_RTMP_SERVER_BR_IN"+sfx, 0));
				serverUptimeArray[i] = getJSONValueOrDefault(data.status, "VZR_RTMP_SERVER_UPTIME"+sfx, "00:00:00.000");
				serverURLArray[i] = getJSONValueOrDefault(data.status, "VZR_RTMP_SERVER_URL"+sfx, "");
				
				$("#server"+i+"running").removeClass("ok");
				$("#server"+i+"running").removeClass("nok");
				$("#server"+i+"running").addClass( serverRunningArray[i] == "true" ? "ok" : "nok");
				$("#server"+i+"running").html(engineRunning ? (serverRunningArray[i] == "true" ? "connected" : "not connected") : "");
				$("#server"+i+"BR").html(engineRunning ? (serverBRArray[i] / 1000) + " kbps" : "");
				$("#server"+i+"uptime").html(engineRunning ?serverUptimeArray[i] : "");
				$("#server"+i+"url").html(engineRunning ? serverURLArray[i] : "");

			} 
			
			for (i = 0; i < numOutputs; i++) {
				var sfx = numOutputs < 2 ? "" : i;
				cdnRunningArray[i] = getJSONValueOrDefault(data.status, "VZR_RTMP_RUNNING"+sfx, false);
				cdnBRArray[i] = Math.max(0, getJSONValueOrDefault(data.status, "VZR_RTMP_BR_OUT"+sfx, 0));
				cdnUptimeArray[i] = getJSONValueOrDefault(data.status, "VZR_RTMP_UPTIME"+sfx, "00:00:00.000");
				cdnURLArray[i] = getJSONValueOrDefault(data.status, "VZR_RTMP_URL"+sfx, "");

				$("#cdn"+i+"running").removeClass("ok");
				$("#cdn"+i+"running").removeClass("nok");
				$("#cdn"+i+"running").addClass( cdnRunningArray[i] == "true" ? "ok" : "nok");
				$("#cdn"+i+"running").html(engineRunning ?  (cdnRunningArray[i] == "true" ? "connected" : "not connected") : "");
				$("#cdn"+i+"BR").html(engineRunning ?  (cdnBRArray[i] / 1000) + " kbps" : "");
				$("#cdn"+i+"uptime").html(engineRunning ? cdnUptimeArray[i] : "");
				$("#cdn"+i+"url").html(engineRunning ? cdnURLArray[i] : "");

			} 
		 
		  var procRunning = getJSONValueOrDefault(data.status, "VIDEO_PROCESSOR_RUNNING", false);
		  var compRunning = getJSONValueOrDefault(data.status, "LIVE_COMPOSITOR_RUNNING", false);
		  var procUptime = getJSONValueOrDefault(data.status, "VIDEO_PROCESSOR_UPTIME", "00:00:00.000");
		  var compUptime = getJSONValueOrDefault(data.status, "LIVE_COMPOSITOR_UPTIME", "00:00:00.000");
		  
		  $("#procRunning").removeClass("ok");
		  $("#procRunning").removeClass("nok");
		  $("#compRunning").removeClass("ok");
		  $("#compRunning").removeClass("nok");
		  $("#procRunning").addClass( procRunning == "true" ? "ok" : "nok");
		  $("#compRunning").addClass( compRunning == "true" ? "ok" : "nok");
		  $("#procRunning").html(engineRunning ?  (procRunning == "true" ? "OK" : "stopped") : "");
		  $("#compRunning").html(engineRunning ?  (compRunning == "true" ? "OK": "stopped" ) : "");
		  $("#procUptime").html(engineRunning ? procUptime : "");
		  $("#compUptime").html(engineRunning ? compUptime : "");
		 
		});
   }
var timer=setInterval("refreshStatus()", 5000);
refreshStatus();
setTimeout("refreshStatus()", 1000);

</script>

	</head>

	<body>

		<?php createNavBar("status",""); ?>

<div class="panel panel-default">

<div class="panel-heading">
	  <h3 class="panel-title">Synthesia status overview</h3>
	</div>

<style>
 .bordered_table td{
	border: 1px solid black;
 }
 .ok{
	color: green;
	font-weight: bold;
 }
 .nok{
	color: red;
	font-weight: bold;
 }
</style>
<div class="panel-body">
		<div class="container">
			
<table width="100%">
	
		<tr class="bordered_table">
					<td><b>Synthesia status</b></td>
					<td><span id="engineRunning"></span></td>
				</tr>
				<tr class="bordered_table">
					<td>Uptime</td>
					<td><span id="engineUptime"></span></td>
				</tr>
				<tr><td></td></tr>
	
<?php 
for ($i = 0; $i < $numInputs; $i++) {
	$label = 'Connexion Entrée ' . ($i+1);
	if($i == 0) $label = "Playlist Input";
	if($i == 1) $label = "Live Input";
	
	echo '
				<tr class="bordered_table">
					<td><b>' . $label . '</b></td>
					<td><span id="server' . $i .'running"></span></td>
				</tr>
				<tr class="bordered_table">

					<td>URL</td>
					<td><span id="server' . $i .'url"></span></td>
				</tr>
				<tr class="bordered_table">

					<td>Bitrate</td>
					<td><span id="server' . $i .'BR"></span></td>
				</tr>
				<tr class="bordered_table">

					<td>Uptime</td>
					<td><span id="server' . $i .'uptime"></span></td>
				</tr>
				<tr><td></td></tr>
	';
}?>
				
				<tr class="bordered_table">
					<td><b>Video processor</b></td>
					<td><span id="procRunning"></span></td>
				</tr>
				<tr class="bordered_table">
					<td>Uptime</td>
					<td><span id="procUptime"></span></td>
				</tr>
				<tr><br/></tr>
				<tr class="bordered_table">
					<td>Video Compositor</td>
					<td><span id="compRunning"></span></td>
				</tr>
				<tr class="bordered_table">
					<td>Uptime</td>
					<td><span id="compUptime"></span></td>
				</tr>
				<tr><td></td></tr>
<?php 
for ($i = 0; $i < $numOutputs; $i++) {
	echo '
				<tr class="bordered_table">
					<td><b>Output Connexion ' . ($i+1) . '</b></td>
					<td><span id="cdn' . $i .'running"></span></td>
				</tr>
				<tr class="bordered_table">
					<td>URL</td>
					<td><span id="cdn' . $i .'url"></span></td>
				</tr>
				<tr class="bordered_table">
					<td>Bitrate</td>
					<td><span id="cdn' . $i .'BR"></span></td>
				</tr>
				<tr class="bordered_table">
					<td>Uptime</td>
					<td><span id="cdn' . $i .'uptime"></span></td>
				</tr>
				<tr><td></td></tr>
	';
}?>
</table>
		</div>
	</div>

            
  </div>

</body>


</html>
