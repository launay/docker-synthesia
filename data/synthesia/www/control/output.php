<?php
require_once "php/info.php";

$message = "";
$error = false;

$userOutputServerURL="";
$userOutputStreamKey="";
$outputs = array();

$paramsFile = "../config/startup.json";
if(!file_exists($paramsFile)){
	$error = true;
	$message = "Impossible to find Synthesia parameters file";
}
else{
	$paramsData = file_get_contents($paramsFile);
	if(!$paramsData){
		$error = true;
		$message = "Impossible de read Synthesia parameters";
	}
	else{
		$paramsJson = json_decode($paramsData,true);
		if(!$paramsJson){
			$error = true;
			$message = "Impossible de decode Synthesia parameters";
		}
		else{
			$outs = $paramsJson['setup']['vzrRtmpOut'];
			$outputs = explode(";", $outs);

			$firstOut = $outputs[0];
			
			if(count($outputs)>1){
				$userOut = $outputs[1];
				$parts = explode('/', $userOut);
				$userOutputStreamKey = end($parts);

				for($i=0;$i<count($parts)-1 ; $i++){
					$userOutputServerURL .= $parts[$i]."/";
				}
				
				
			}
			
			
			if(isset($_POST['updateParameters'])){
						
				if(isset($_POST['serverUrl']) && isset($_POST['streamKey'])){
					$serverUrl = trim($_POST['serverUrl']);
					$streamKey = trim($_POST['streamKey']);
					$newOut = $firstOut;
					if(strlen($serverUrl)>0 && strlen($streamKey)>0){
						if(!endsWith($serverUrl, "/")) $serverUrl .= "/";
						$newOut .= ";" . $serverUrl . $streamKey;
						$userOutputStreamKey = $streamKey;
						$userOutputServerURL = $serverUrl;
						
					}
					else{
						$userOutputStreamKey = "";
						$userOutputServerURL = "";
					}
					$outputs = $newOut;
					$paramsJson['setup']['vzrRtmpOut'] = $outputs;
				}
									

				//writes config back
				$newJson = json_encode($paramsJson);
				$res = file_put_contents($paramsFile, $newJson);
				if(!$res){
					$error = true;
					$message = "Impossible to update Synthesia configuration";
				}else{
					$error = false;
					$message = "Synthesia destination was succesfully updated";
				}

			}
				

				
			
		 }
	}
}


?>
<html>
	<head>
		<?php createHeader();?>

  <style>
  #sortable { cursor: pointer; list-style-type: none; margin: 0; padding: 0; width: 60%; }
  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
  #sortable li span { position: absolute; margin-left: -1.3em; }

  </style>


	</head>

	<body>

		<?php createNavBar("output",""); ?>

<?php
if(strlen($message)>0){
	$class="label label-default";
	if($error)
		$class="label label-danger";
	echo '<div id="errorMsg" class="' . $class . '" style="display:block">' . $message .'</div></br>';
}
?>

<div class="panel panel-default">

 <div class="panel-heading">
	  <h3 class="panel-title">Destinations Management</h3>
	</div>
	
		<div class="panel-body">
		<div class="container">
			 <form method="POST" id="updateForm" onsubmit="return confirm('Destination change will force Synthesia to restart.\nAre you sure you want to apply this new destination?');">
				<input type="hidden" name="updateParameters" id="updateParameters" value="1"></input>
				<table>
					<tr>
						<td>Server URL:&nbsp;</td>
						<td><input type="text" size="64" id="serverUrl" name="serverUrl" value="<?php echo $userOutputServerURL;?>"></td>
					</tr>
					<tr>
						<td>Stream Key:&nbsp;</td>
						<td><input type="text" size="64" id="streamKey" name="streamKey" value="<?php echo $userOutputStreamKey;?>"></td>
					</tr>
					<tr>
						<td><input type="submit" id="destSubmit" value="Save destination"></input></td>
						<td><input type="button" onClick="clearDestination();" value="Clear destination"></input></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
 <div class="panel-heading">
	  <h3 class="panel-title">Configure my CDN live stream</h3>
	</div>
	<div class="container"><br/><h4>Click one of the buttons below to configure your live stream on your favorite CDN</h4></div>
	
		<div class="panel-body">
		<div class="container">
			<button onclick=" window.open('https://www.youtube.com/my_live_events?action_create_live_event=1', '_blank'); ">
				<img src="images/youtube.png" height="48"/>
			</button>
			<button onclick=" window.open('http://www.dailymotion.com/', '_blank'); ">
				<img src="images/dailymotion.png" height="48"/>
			</button>
			<button onclick=" window.open('https://www.facebook.com/', '_blank'); ">
				<img src="images/facebook.png" height="48"/>
			</button>
		</div>
	</div>

	<script>
	function clearDestination(){
		$('#serverUrl').val("");
		$('#streamKey').val("");
		$('#updateParameters').val("1");
		$('#updateForm').submit();
	}
	</script>
	
 </div>


</body>

</html>
