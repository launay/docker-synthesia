<?php

require_once "config/config.php";

define("WEB_UI_PORT", "DOCKER_SYNTHESIA_HOST_80");
define("RTMP_IN_PORT", "DOCKER_SYNTHESIA_HOST_1935");
define("RTMP_OUT_PORT", "DOCKER_SYNTHESIA_HOST_1940");

define("WEB_UI_PORT_DEFAULT", 80);
define("RTMP_IN_PORT_DEFAULT", 1935);
define("RTMP_OUT_PORT_DEFAULT", 8080);

define("ENVARS_FILE", "/home/synthesia/envars.txt");

function getMappedPort($kind){

        $envars = explode("\n", file_get_contents(ENVARS_FILE));
        foreach($envars as $envar){
                $keyValue = explode("=", $envar, 2);
                if(count($keyValue) != 2) continue; 
                $key = $keyValue[0]; 
                $value = $keyValue[1]; 
                if($key == $kind) return $value;
        }

        if($kind == WEB_UI_PORT) return WEB_UI_PORT_DEFAULT;
        if($kind == RTMP_IN_PORT) return RTMP_IN_PORT_DEFAULT;
        if($kind == RTMP_OUT_PORT) return RTMP_OUT_PORT_DEFAULT;


}


function startsWith($haystack, $needle) {
    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
}

function endsWith($haystack, $needle) {
    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
}

function createHeader(){
	echo '
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>' . MAIN_TITLE . '</title>
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery-ui/jquery-ui.js"></script>
		<script src="js/jscolor/jscolor.js"></script>
				
		<link rel="icon" type="image/png" href="images/synthesia.png" />
		<link rel="stylesheet" type="text/css" href="js/jquery-ui/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
		<style>
		body {
			padding-top: 70px;	
		}
		.pad_10{
			padding: 10px;
		}
		
		.evenOdd tr:nth-child(even) {background: #EEE}
		.evenOdd tr:nth-child(odd) {background: #FFF}
		
		td { 
			padding: 10px;
		}
		
		.pad_4{
			padding: 4px;
		}
		.bordered_nok{
			border: 3px solid #DDD;
		}
		.bordered_ok{
			border: 3px solid red;
		}
		.font_80 {
			font-size: 85%;	
		}
		.btn-warning{
			background-color:#ff3d00;
		}
		.lbl-warning{
			background-color:#ff3d00;
		}
		.panel-heading a:after {
			font-family:"Glyphicons Halflings";
			content:"\e114";
			float: right;
			color: grey;
		}
		.panel-heading a.collapsed:after {
			content:"\e080";
		}
		.clickable{
			cursor: pointer;
		}
		.table-no-border td {
			border-top: none !important;
		}
		.table-label{
			font-weight:bold;
		}
		.row{
			margin-top:40px;
			padding: 0 10px;
		}
		.panel-heading span {
			margin-top: -20px;
			font-size: 15px;
		}
		</style>'
	;

}


function createNavBar($selectedOne, $selectedTwo){

	echo '

	<nav class="navbar navbar-default navbar-fixed-top">
		  <div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			<a class="navbar-brand" href="output.php"><span style="color:black;"><b>' . MAIN_TITLE . '</b></span></a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
			  <ul class="nav navbar-nav">
			  				<li><img src="images/synthesia.png" height="50"/>&nbsp;&nbsp;&nbsp;</li>

			  ';

				echo '				
					  <li ' . ($selectedOne == "overview" ? 'class="active"' : '') . '><a href="overview.php">Main</a></li>
					  <li ' . ($selectedOne == "input" ? 'class="active"' : '') . '><a href="input.php">Sources</a></li>
					  <li ' . ($selectedOne == "output" ? 'class="active"' : '') . '><a href="output.php">Destinations</a></li>
					  <li ' . ($selectedOne == "scenes" ? 'class="active"' : '') . '><a href="scenario_maker.php">Scenes</a></li>

					<li class="dropdown' . (($selectedOne == "config" || $selectedOne == "status" || $selectedOne == "log") ? " active" : "") .'">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration<span class="caret"></span></a>
					<ul class="dropdown-menu">
					  <li ' . ($selectedOne == "config" ? 'class="active"' : '') . '><a href="config.php">Configuration</a></li>
				 <li ' . ($selectedOne == "status" ? 'class="active"' : '') . '><a href="status.php">Status</a></li>
					  <li ' . ($selectedOne == "log" ? 'class="active"' : '') . '><a href="log.php">Log</a></li>
					</ul>				
					</li>


				';			
			  echo'
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li><a href="../main/">MAIN MENU</a></li>
			  </ul>
			</div>
		  </div>
		</nav>


	';

}
?>
