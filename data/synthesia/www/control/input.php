<?php
require_once "php/info.php";

$numInputs = 1;


$message = "";
$error = false;

define("SOURCE_FILE", "../source/source.txt");

//reads number of inputs
{
	$paramsFile = "../config/startup.json";
		if(!file_exists($paramsFile)){
			$error = true;
			$mesage = "Impossible to find Synthesia parameters file";
		}
		else{
			$paramsData = file_get_contents($paramsFile);
			if(!$paramsData){
				$error = true;
				$mesage = "Impossible to read Synthesia parameters file";
			}
			else{
				$paramsJson = json_decode($paramsData,true);
				if(!$paramsJson){
					$error = true;
					$mesage = "Impossible to decode synthesia parameters file";
				}
				else{
					$ins = $paramsJson['setup']['videoUrl'];
					$ins = trim($ins);
					$numInputs = count(explode(";",$ins));
				}
			}
		}
	}

$liveServerUrl = "rtmp://" . $_SERVER['SERVER_NAME'] . ":" . getMappedPort(RTMP_IN_PORT) . "/live";
$liveStreamKey = "";
$liveIn = explode(";",$ins);
$liveIn = $liveIn[1];
$liveStreamKey=$end = end((explode('/', $liveIn)));

$mode= "0";

//first reads input config (source)
if(file_exists(SOURCE_FILE)){
	$mode = trim(file_get_contents(SOURCE_FILE));
}

?>
<html>
	<head>
		<?php createHeader();?>

  <style>
  #sortable { cursor: pointer; list-style-type: none; margin: 0; padding: 0; width: 60%; }
  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
  #sortable li span { position: absolute; margin-left: -1.3em; }

  </style>


<script>
	
	
function getJSONValueOrDefault(jsonObject, key, def){
	var value = jsonObject[key];
	if(!value) return def;
	return value;
	
}
function refreshStatus(){
		var statusFile = "../status/status.json";
		$.getJSON( statusFile + "?rand=" + Math.random(), function( data ) {

			var numInputs = <?php echo $numInputs?>;
		
			var serverURLArray = new Array();
			var serverRunningArray = new Array();
			var serverBRArray = new Array();
			var serverUptimeArray = new Array();
					
			
			for (i = 0; i < numInputs; i++) {
				var sfx = numInputs < 2 ? "" : i;
				serverRunningArray[i] = getJSONValueOrDefault(data.status, "VZR_RTMP_SERVER_RUNNING"+sfx, false);
				serverBRArray[i] = Math.max(0, getJSONValueOrDefault(data.status, "VZR_RTMP_SERVER_BR_IN"+sfx, 0));
				serverUptimeArray[i] = getJSONValueOrDefault(data.status, "VZR_RTMP_SERVER_UPTIME"+sfx, "00:00:00.000");
				serverURLArray[i] = getJSONValueOrDefault(data.status, "VZR_RTMP_SERVER_URL"+sfx, "");
				
				$("#server"+i+"running").removeClass("ok");
				$("#server"+i+"running").removeClass("nok");
				$("#server"+i+"running").addClass( serverRunningArray[i] == "true" ? "ok" : "nok");
				$("#server"+i+"running").html( serverRunningArray[i] == "true" ? "connected" : "not connected");
				$("#server"+i+"BR").html( (serverBRArray[i] / 1000) + " kbps");
				$("#server"+i+"uptime").html(serverUptimeArray[i]);
				$("#server"+i+"url").html(serverURLArray[i]);

			} 
			
		 
		});
   }
var timer=setInterval("refreshStatus()", 5000);
refreshStatus();

</script>

	</head>

	<body>

		<?php createNavBar("input",""); ?>

<?php
	$display =  strlen($message)>0 ? "block" : "none";
	$class="label label-default";
	if($error) $class="label label-danger";
	echo '<div id="messageLabel" class="' . $class . '" style="display:' . $display . '">' . $message .'</div><div id="messageSep" style="display:' . $display . '"><br/></div>';

?>

<div class="panel panel-default">

<div class="panel-heading">
	  <h3 class="panel-title">Inputs Management</h3>
	</div>

<style>
 .bordered_table td{
	border: 1px solid black;
 }
 .ok{
	color: green;
	font-weight: bold;
 }
 .nok{
	color: red;
	font-weight: bold;
 }
</style>

<div class="panel-body">
		<div class="container">

		  <fieldset>
			<legend>Active source selection :</legend>
			<div class="btn-group-horizontal" data-toggle="buttons" id ="source_selection">
			<label onclick="selectInput(0);" class="btn btn-sm btn-default btnSelect <?php if($mode == 0) echo "active";?>" for="radio12" style="font-size:100%">
				<input type="radio" name="radio12" id="radio12">Playlist
			</label>
			<label onclick="selectInput(1);" class="btn btn-sm btn-default btnSelect <?php if($mode == 1) echo "active";?>" for="radio13" style="font-size:100%">
				<input type="radio" name="radio12" id="radio13">Live
			</label>
			</div>
		  </fieldset>
		  <br/>
		  <fieldset>
			<legend>Sources status :</legend>
		  </fieldset>

			<table width="100%">
<tr class="bordered_table"><td></td>
<?php 
for ($i = 0; $i < $numInputs; $i++) {
	$label = $i == 0 ? "Playlist" : "Live";
	echo '					
					<td><b>' . $label . '</b></td>	
	';
}
?>
</tr><tr class="bordered_table"><td><b>Connection status</b></td>
<?php
for ($i = 0; $i < $numInputs; $i++) {
	echo '
				
					
					<td><span id="server' . $i .'running"></span></td>	
	';
}?>
</tr><tr class="bordered_table">
					<td><b>URL</b></td>
<?php
for ($i = 0; $i < $numInputs; $i++) {
	echo '
					<td><span id="server' . $i .'url"></span></td>
';
}?>
</tr><tr class="bordered_table">
					<td><b>Bitrate</b></td>
<?php
for ($i = 0; $i < $numInputs; $i++) {
	echo '
				

					<td><span id="server' . $i .'BR"></span></td>
';
}?>
</tr><tr class="bordered_table">
					<td><b>Uptime</b></td>
<?php
for ($i = 0; $i < $numInputs; $i++) {
	echo '				
					<td><span id="server' . $i .'uptime"></span></td>

	';
}?>
</tr>			
</table>

	<br/>
	 <fieldset>
		<legend>User Live connection :</legend>
	  </fieldset>
	  
	  <label>To connect your <b>live</b> stream to Synthesia, use the credentials below:</label>
		<table>
			<tr>
				<td>Server URL:&nbsp;</td>
				<td><input type="text" size="64" value="<?php echo $liveServerUrl;?>" readonly></td>
			</tr>
			<tr>
				<td>Stream Key:&nbsp;</td>
				<td><input type="text" size="64" value="<?php echo $liveStreamKey;?>" readonly></td>
			</tr>
			<tr>
				<td>Full URL:&nbsp;</td>
				<td><input type="text" size="64" value="<?php echo $liveServerUrl."/".$liveStreamKey;?>" readonly></td>
			</tr>


		</table>

		</div>
		
		
<script>
$("input[type='text']").click(function () {
	if (navigator.userAgent.match(/ipad|ipod|iphone/i)) {
	  var el = $(this).get(0);
	  var editable = el.contentEditable;
	  var readOnly = el.readOnly;
	  el.contentEditable = true;
	  el.readOnly = false;
	  var range = document.createRange();
	  range.selectNodeContents(el);
	  var sel = window.getSelection();
	  sel.removeAllRanges();
	  sel.addRange(range);
	  el.setSelectionRange(0, 999999);
	  el.contentEditable = editable;
	  el.readOnly = readOnly;
	} else {
	  $(this).select();
	}
	document.execCommand('copy');
	 $(this).blur();

});
</script>
		
	</div>



            
  </div>

<form style="display:none" method="POST" enctype="multipart/form-data" name="changeDiffusion" id="changeDiffusionForm">
	<input type="hidden" name="mode" id="mode" value="0"/>
</form>

</body>

<script>
function selectInput(inputNumber){
	
	var wsUrl = "../webservices/setSource.php?source=" + encodeURIComponent(inputNumber);
	$.get( wsUrl , function( data ) {
		$("#messageLabel").html("Source was succesfully switched to " + (inputNumber == 0 ? "Playlist" : "Live"));
		$("#messageLabel").css( "display", "block" );
		$("#messageSep").css( "display", "block" );

	});
	
}
</script>

</html>

