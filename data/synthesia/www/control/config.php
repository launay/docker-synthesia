<?php
require_once "php/info.php";

$message = "";
$error = false;

         $cmdType = "";
         if(isset($_POST['startSynthesia'])){
                 $cmdType = "start";
         }
         if(isset($_POST['stopSynthesia'])){
                 $cmdType = "stop";
         }

         if($cmdType){
                 $command = 'sudo -u synthesia /home/synthesia/synthesia/common/'  . $cmdType . 'Synthesia.sh';
                 exec($command, $output, $result);

                 if($result == 0){
                         if($cmdType == "start") $message = "Synthesia was succesfully restarted";
                         if($cmdType == "stop") $message = "Synthesia was succesfully stopped";
																				$error = false;
                 }
                 else{
                         if($cmdType == "start") $message = "Error while restarting Synthesia";
                         if($cmdType == "stop") $message = "Error while stopping Synthesia";
																				$error = true;
                 }

         }

	$sampling = 48000;
	$bitrate = -1;	
	$fps = 25;
	$inputs = array();
	$outputs = array();

	$paramsFile = "../config/startup.json";
	if(!file_exists($paramsFile)){
		$error = true;
		$message = "Impossible to find Synthesia parameters file";
	}
	else{
		$paramsData = file_get_contents($paramsFile);
		if(!$paramsData){
			$error = true;
			$message = "Impossible de read Synthesia parameters";
		}
		else{
			$paramsJson = json_decode($paramsData,true);
			if(!$paramsJson){
				$error = true;
				$message = "Impossible de decode Synthesia parameters";
			}
			else{
				$sampling = $paramsJson['setup']['audioSamplingRate'];
				$bitrate = $paramsJson['setup']['bitrateKbps'];
				$fps = $paramsJson['setup']['fps'];
				$ins = $paramsJson['setup']['videoUrl'];
				$outs = $paramsJson['setup']['vzrRtmpOut'];
				$inputs = explode(";", $ins);
				$outputs = explode(";", $outs);
				
				if(isset($_POST['updateParameters'])){
							
					if(isset($_POST['inputs'])){
						$inputs = preg_split("/[\s,;]+/", trim($_POST['inputs']));
						$paramsJson['setup']['videoUrl'] = join(";", $inputs);
					}
					if(isset($_POST['outputs'])){
						$outputs = preg_split("/[\s,;]+/", trim($_POST['outputs']));
						$paramsJson['setup']['vzrRtmpOut'] = join(";", $outputs);
					}
					if(isset($_POST['sampling'])){
						$sampling = intval($_POST['sampling']);
						$paramsJson['setup']['audioSamplingRate'] = $sampling;
					}
					if(isset($_POST['bitrate'])){
						$bitrate = intval($_POST['bitrate']);
						$paramsJson['setup']['bitrateKbps'] = $bitrate;
					}
					if(isset($_POST['fps'])){
						$fps = intval($_POST['fps']);
						$paramsJson['setup']['fps'] = $fps;
					}
					
					//writes config back
					$newJson = json_encode($paramsJson);
					$res = file_put_contents($paramsFile, $newJson);
					if(!$res){
						$error = true;
						$message = "Impossible to update Synthesia configuration";
					}else{
						$error = false;
						$message = "Synthesia configuration was succesfully updated";
					}
					
				
				 }
			}
		}

	}
	
	
	
?>



<html>
	<head>
		<?php createHeader();?>

	</head>

	<body>

		<?php createNavBar("config",""); ?>

<?php
if(strlen($message)>0){
	$class="label label-default";
	if($error)
		$class="label label-danger";
	echo '<div id="errorMsg" class="' . $class . '" style="display:block">' . $message .'</div></br>';
}
?>

<center>
	<div id="pendingON" style="display:none">
		<img class="pendingImage" src="images/wait.gif"/>
		<div class="pendingText">... Please wait while Synthesia is restarting ...</div>
	</div>	
</center>
<center>
	<div id="pendingOFF" style="display:none">
		<img class="pendingImage" src="images/wait.gif"/>
		<div class="pendingText">... Please wait while Synthesia is stopping ...</div>
	</div>	
</center>

<div class="panel panel-default">

	<div class="panel-heading">
	  <h3 class="panel-title">Engine commands</h3>
	</div>

	<div class="panel-body">
		<div class="container">
			<table><tr><td>
			 <form method="POST" onsubmit="if (!confirm('Are you sure you want to restart Synthesia?')) return false; displayPendingON();">
				 <input type="hidden" name="startSynthesia" value="1"></input>
				 <input type="submit" value="Restart Synthesia"></input>
			 </form></td><td>
			 <form method="POST" onsubmit="if (!confirm('Are you sure you want to stop Synthesia?')) return false; displayPendingOFF();">
				 <input type="hidden" name="stopSynthesia" value="1"></input>
				 <input type="submit" value="Stop Synthesia"></input>
			 </form>
		</td></tr></table>
		</div>
	</div>
</div>
<div class="panel panel-default">

	<div class="panel-heading">
	  <h3 class="panel-title">Global parameters</h3>
	</div>

	<div class="panel-body">
		<div class="container">
			 <form method="POST" onsubmit="return confirm('Configuration changes will force Synthesia to restart.\nAre you sure you want to apply this new configuration?');">
				<input type="hidden" name="updateParameters" value="1"></input>
				<table>
					<tr>
						<td>Audio sampling rate</td>
						<td>
							<select name="sampling" id="sampling">
								<option value="44100" <?php if($sampling == 44100) echo "selected";?>>44.1 KHz</option>
								<option value="48000" <?php if($sampling == 48000) echo "selected";?>>48 KHz</option>
  							</select>
						</td>
					</tr>
					<tr>
						<td>Video output bitrate</td>
						<td>
							<select name="bitrate" id="bitrate">
								<option value="-1" <?php if($bitrate == -1) echo "selected";?>>Auto</option>
								<option value="128" <?php if($bitrate == 128) echo "selected";?>>128 Kbps</option>
								<option value="256" <?php if($bitrate == 256) echo "selected";?>>256 Kbps</option>
								<option value="512" <?php if($bitrate == 512) echo "selected";?>>512 Kbps</option>
								<option value="640" <?php if($bitrate == 640) echo "selected";?>>640 Kbps</option>
								<option value="768" <?php if($bitrate == 768) echo "selected";?>>768 Kbps</option>
								<option value="1024" <?php if($bitrate == 1024) echo "selected";?>>1024 Kbps</option>
								<option value="1500" <?php if($bitrate == 1500) echo "selected";?>>1500 Kbps</option>
								<option value="2000" <?php if($bitrate == 2000) echo "selected";?>>2000 Kbps</option>
								<option value="2500" <?php if($bitrate == 2500) echo "selected";?>>2500 Kbps</option>
								<option value="3000" <?php if($bitrate == 3000) echo "selected";?>>3000 Kbps</option>
								<option value="3500" <?php if($bitrate == 3500) echo "selected";?>>3500 Kbps</option>
								<option value="4000" <?php if($bitrate == 4000) echo "selected";?>>4000 Kbps</option>
								<option value="4500" <?php if($bitrate == 4500) echo "selected";?>>4500 Kbps</option>
								<option value="5000" <?php if($bitrate == 5000) echo "selected";?>>5000 Kbps</option>
  							</select>
						</td>
					</tr>
					<tr>
						<td>Frames per second</td>
						<td>
							<select name="fps" id="fps">
								<option value="25" <?php if($fps == 25) echo "selected";?>>25</option>
								<option value="30" <?php if($fps == 30) echo "selected";?>>30</option>
  							</select>
						</td>
					</tr>
					<!--tr>
						<td>Sources RTMP</td>
						<td>
							<textarea name="inputs" rows="3" cols="64"><?php foreach($inputs as $input) echo $input . "\n";?></textarea>
						</td>
					</tr>
					<tr>
						<td>Sorties RTMP</td>
						<td>
							<textarea name="outputs" rows="3" cols="64"><?php foreach($outputs as $output) echo $output . "\n";?></textarea>
						</td>
					</tr-->
					<tr>
						<td><input type="submit" value="Save configuration"></input></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>

<script>
		function displayPendingON(){
			$("#pendingON").show();
		}
		function displayPendingOFF(){
			$("#pendingOFF").show();
		}
		
		setTimeout(function(){
			$("#errorMsg").hide(); 
		}, 5000);
</script>

</body>


</html>
