<?php
require_once "php/info.php";
require_once "../webservices/scenario/authoring/php/utils.php";
require_once "../webservices/scenario/php/utils.php";

include_once("../webservices/timeUtils.php");
include_once("../webservices/liveMetaUtils.php");

define("RSS_FILE", "../webservices/_rssURL.txt");
define("TWITTER_ID_FILE", "../webservices/_twitterId.txt");
define("FACEBOOK_ID_FILE", "../webservices/_facebookId.txt");

define("DEFAULT_RSS", "http://rss.nytimes.com/services/xml/rss/nyt/World.xml");
define("DEFAULT_TWITTER_ID", "lemondefr");
define("DEFAULT_FACEBOOK_ID", "14892757589");

define("NEWS_ACTIVE_FILE", "../webservices/_news_active.json");
define("NEWS_NUMBER_FILE", "../webservices/_news_number.json");
define("NEWS_DURATION_FILE", "../webservices/_news_duration.json");
define("DEFAULT_NEWS_NUMBER", 10);
define("DEFAULT_NEWS_DURATION", 15);

define("DEFAULT_TITLE", "Synthesia");
define("TITLE_FILE", "../webservices/_showTitle.json");

//get live meta
$liveTitle = LIVE_TITLE_DEFAULT;
$liveSubtitle = LIVE_SUBTITLE_DEFAULT;
$liveDescription = LIVE_DESCRIPTION_DEFAULT;

//get time configuration
$timeConfig = getTimeConfig();
$timezone = $timeConfig["timezone"];
$timeDisplayMode = $timeConfig["timeDisplayMode"];
$hourFormat = $timeConfig["hourFormat"];

//handles show title
$showTitle = DEFAULT_TITLE;

//first reads from input config
if(file_exists(TITLE_FILE)){
	$showTitleJson = file_get_contents(TITLE_FILE);
	if($showTitleJson){
		$showTitleData = json_decode($showTitleJson, true);
		if($showTitleData){
			$showTitle = $showTitleData["showTitle"];
		}
	}
}


$rssActive=true;
$twitterActive=false;
$facebookActive=false;
if(file_exists(NEWS_ACTIVE_FILE)){
	$activeDataJson = file_get_contents(NEWS_ACTIVE_FILE);
	if($activeDataJson){
		$activeData = json_decode($activeDataJson,true);
		if($activeData){
			$rssActive = $activeData["rssActive"];
			$twitterActive = $activeData["twitterActive"];
			$facebookActive = $activeData["facebookActive"];
		}
	}
}

$rssNumber=DEFAULT_NEWS_NUMBER;
$twitterNumber=DEFAULT_NEWS_NUMBER;
$facebookNumber=DEFAULT_NEWS_NUMBER;
if(file_exists(NEWS_NUMBER_FILE)){
	$numberDataJson = file_get_contents(NEWS_NUMBER_FILE);
	if($numberDataJson){
		$numberData = json_decode($numberDataJson, true);
		if($numberData){
			$rssNumber = $numberData["rssNumber"];
			$twitterNumber = $numberData["twitterNumber"];
			$facebookNumber = $numberData["facebookNumber"];
		}
	}
}

$newsDuration = DEFAULT_NEWS_DURATION;

//first reads from input config
if(file_exists(NEWS_DURATION_FILE)){
	$numberDataJson = file_get_contents(NEWS_DURATION_FILE);
	if($numberDataJson){
		$numberData = json_decode($numberDataJson, true);
		if($numberData){
			$newsDuration = $numberData["newsDuration"];
		}
	}
}

define("SOURCE_FILE", "../source/source.txt");

$message = "";
$error = false;

$mode= "0";

$allScenarios = listScenarios();
$currentScenario = getCurrentScenario();

$allScenes = listScenes();

//first reads input config (source)
if(file_exists(SOURCE_FILE)){
	$mode = trim(file_get_contents(SOURCE_FILE));
}


$rssURL=DEFAULT_RSS;
if(file_exists(RSS_FILE) && filesize(RSS_FILE)>0){
	$rssURL = trim(file_get_contents(RSS_FILE));
}

$twitterId=DEFAULT_TWITTER_ID;
if(file_exists(TWITTER_ID_FILE) && filesize(TWITTER_ID_FILE)>0){
	$twitterId = trim(file_get_contents(TWITTER_ID_FILE));
}

$facebookId=DEFAULT_FACEBOOK_ID;
if(file_exists(FACEBOOK_ID_FILE) && filesize(FACEBOOK_ID_FILE)>0){
	$facebookId = trim(file_get_contents(FACEBOOK_ID_FILE));
}

?>
<html>
	<head>
		<?php createHeader();?>

  <style>
  #sortable { cursor: pointer; list-style-type: none; margin: 0; padding: 0; width: 60%; }
  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
  #sortable li span { position: absolute; margin-left: -1.3em; }

	a:active {
		outline:none;
	}
	:focus { -moz-outline-style:none; }

	.palert {
		padding: 12px;
		color: black;
		background-color: #ededed;
		box-shadow: none;
	}

  </style>

    <!-- flowplayer javascript component -->
    <script src="js/flowplayer-3.2.13.min.js"></script>


	</head>

	<body>

		<?php createNavBar("overview",""); ?>

<?php
	$display =  strlen($message)>0 ? "block" : "none";
	$class="label label-default";
	if($error) $class="label label-danger";
	echo '<div id="messageLabel" class="' . $class . '" style="display:' . $display . '">' . $message .'</div><div id="messageSep" style="display:' . $display . '"><br/></div>';

?>

<div class="panel panel-default">

 <div class="panel-heading">
	  <h3 class="panel-title">Overview (picture is refreshed every 10s)</h3>
	</div>
		
	<div class="panel-body">
		<div class="container">
			<table>

				<!-- SOURCE SELECTION -->
				<tr>
					<td>
						  <fieldset>
							<legend>Active source selection:</legend>
							<div class="btn-group-horizontal" data-toggle="buttons" id ="source_selection">
							<label onclick="selectInput(0);" class="btn btn-sm btn-default btnSelect <?php if($mode == 0) echo "active";?>" for="radio12" style="font-size:100%">
								<input type="radio" name="radio12" id="radio12">Playlist
							</label>
							<label onclick="selectInput(1);" class="btn btn-sm btn-default btnSelect <?php if($mode == 1) echo "active";?>" for="radio13" style="font-size:100%">
								<input type="radio" name="radio12" id="radio13">Live
							</label>
							</div>
						  </fieldset>
					</td>
				</tr>

				<!-- SCENARIO SELECTION -->
				<tr>
					<td>
						 <fieldset>
							<legend>Scene selection:</legend>								
						  </fieldset>				
						<div id="accordionScenes">
							<h3>Framed Video</h3>
							<div>
								<div class="btn-group-horizontal" data-toggle="buttons" id ="scenarios_selection">
										
									<?php foreach($allScenarios as $scenarioId=>$scenarioData){
										$scenarioName = $scenarioData["scenario_name"];
										$kind = $scenarioData["kind"];
										if($kind != VALUE_SCENARIO_KIND_FRAMED) continue;
										$checked = $currentScenario == $scenarioId ? " active" : "";
										echo 	
'<label id="label_scenario_' . $scenarioId . '" class="btn btn-sm btn-default btnSelect' . $checked .'" onclick="selectScenario(\'' . $scenarioId . '\',\'' . $scenarioName . '\');">
	<input type="radio" name="scenario_group" id="input_scenario_' . $scenarioId . '"' . '>' . $scenarioName . '
</label> ';
									}
									?>										
								</div>
							</div>
							<h3>Fullscreen Video</h3>
							<div>
								<div class="btn-group-horizontal" data-toggle="buttons" id ="scenarios_selection">
										
									<?php foreach($allScenarios as $scenarioId=>$scenarioData){
										$scenarioName = $scenarioData["scenario_name"];
										$kind = $scenarioData["kind"];
										if($kind != VALUE_SCENARIO_KIND_FULLSCREEN) continue;
										$checked = $currentScenario == $scenarioId ? " active" : "";
										echo 	
'<label id="label_scenario_' . $scenarioId . '" class="btn btn-sm btn-default btnSelect' . $checked .'" onclick="selectScenario(\'' . $scenarioId . '\',\'' . $scenarioName . '\');">
	<input type="radio" name="scenario_group" id="input_scenario_' . $scenarioId . '"' . '>' . $scenarioName . '
</label> ';
									}
									?>										
								</div>
							</div>
							<h3>User Scenes</h3>
							<div>
								<div class="btn-group-horizontal" data-toggle="buttons" id ="scenarios_selection">
										
									<?php foreach($allScenes as $scenarioId=>$scenarioName){
										if($scenarioId == DEFAULT_SCENE) continue;
										$checked = $currentScenario == $scenarioId ? " active" : "";
										echo 	
'<label id="label_scenario_' . $scenarioId . '" class="btn btn-sm btn-default btnSelect' . $checked .'" onclick="selectScene(\'' . $scenarioId . '\',\'' . $scenarioName . '\');">
	<input type="radio" name="scenario_group" id="input_scenario_' . $scenarioId . '"' . '>' . $scenarioName . '
</label> ';
									}
									?>										
								</div>
							</div>

					</td>
				</tr>

				<!-- MAIN CONFIGURATION -->
				<tr>
					<td>
						<fieldset>
							<legend>Main Configuration:</legend>					
						</fieldset>
						<div id="accordionMain">
							<h3>Show Title</h3>
							<div>
								<input type="text" size="64" name="showTitle" id="showTitle" placeholder="Enter the show Title here" value="<?php echo htmlspecialchars($showTitle);?>">
								<input type="button" value="apply" onclick="setShowTitle();">
							</div>
							<h3>Time/Timezone parameters</h3>
							<div>
								<table>
									<tr>
										<td>Timezone</td>
										<td>
											<select name="timezone" id="timezone">
												<?php
													foreach($TIMEZONES as $timezoneId=>$timezoneLabel){
													$tzSelected = $timezoneId == $timezone ? ' selected' : '';
													echo '<option value="' . $timezoneId . '"' . $tzSelected . '>' . $timezoneLabel .'</option>';
												}?>
											</select>
										</td>
									</tr>
									<tr>
										<td>Display mode</td>
										<td>
											<select name="timeDisplayMode" id="timeDisplayMode">
												<option value="0" <?php if($timeDisplayMode == 0) echo "selected";?>>24h</option>
												<option value="1" <?php if($timeDisplayMode == 1) echo "selected";?>>AM/PM</option>
											</select>
										</td>
									</tr>
									<tr>
										<td>Hour format</td>
										<td>
											<select name="hourFormat" id="hourFormat">
												<option value="0" <?php if($hourFormat == 0) echo "selected";?>>hour:min (06:27)</option>
												<option value="1" <?php if($hourFormat == 1) echo "selected";?>>hour(h)min (06h27)</option>
												<!--option value="2" <?php if($hourFormat == 2) echo "selected";?>>hour:min:sec (06:27:54)</option>
												<option value="3" <?php if($hourFormat == 3) echo "selected";?>>hour(h)min(m)sec(s) (06h27m54s)</option-->
											</select>
										</td>
									</tr>
									<tr><td><input type="button" value="apply" onclick="setTime();"></td></tr>
								</table>
							</div>
							<h3>Live metadata</h3>
							<div>
								<table>
									<tr>
										<td>Title</td>
										<td><input type="text" size="40" name="liveTitle" id="liveTitle" placeholder="Enter the live Title here" value="<?php echo htmlspecialchars($liveTitle);?>"></td>
									</tr>
									<tr>
										<td>SubTitle</td>
										<td><input type="text" size="40" name="liveSubtitle" id="liveSubtitle" placeholder="Enter the live Subtitle here" value="<?php echo htmlspecialchars($liveSubtitle);?>"></td>
									</tr>
									<tr>
										<td>Description</td>
										<td><textarea type="text" cols="40" rows="5" name="liveDescription" id="liveDescription" placeholder="Enter the live Description here"><?php echo htmlspecialchars($liveDescription);?></textarea>
									</tr>
									<tr><td><input type="button" value="apply" onclick="setLiveMeta();"></td></tr>
								</table>
							</div>

						</div>
					</td>
				</tr>				



				<!-- NEWS CONFIGURATION -->
				<tr>
					<td>
						 <fieldset>
							<legend>News configuration:</legend>
						  </fieldset>
						  <div id="accordionNews">
							  <h3>Global</h3>
							  <div>
								  <table>
									<tr>
										<td>News items display duration:&nbsp;</td>
										<td>
											<select name="newsDuration" id="newsDuration">
												<option value="5" <?php if($newsDuration == 5) echo "selected"?>>5</option>
												<option value="10" <?php if($newsDuration == 10) echo "selected"?>>10</option>
												<option value="15" <?php if($newsDuration == 15) echo "selected"?>>15</option>
												<option value="20" <?php if($newsDuration == 20) echo "selected"?>>20</option>
												<option value="25" <?php if($newsDuration == 25) echo "selected"?>>25</option>
												<option value="30" <?php if($newsDuration == 30) echo "selected"?>>30</option>
											</select>
										</td>
										<td>seconds</td>
									</tr>
									<tr>
										<td><input type="button" value="apply" onclick="setNewsDuration();"></td>
									</tr>
								  </table>
							  </div>
							  <h3>RSS feed</h3>
							  <div>
								  <table>
									<tr>
										<td>Activate</td>
										<td><input type="checkbox" id="rssEnable" value="enable" <?php if($rssActive) echo "checked"?>></td>
									</tr>
									<tr>
										<td>RSS URL</td>
										<td><input type="text" size="64" name="rssURL" id="rssURL" placeholder="Paste the RSS URL" value="<?php echo htmlspecialchars($rssURL);?>"></td>
									</tr>
									<tr>
										<td>Number of items to display</td>
										<td>
											<select name="rssNumber" id="rssNumber">
												<option value="1" <?php if($rssNumber == 1) echo "selected"?>>1</option>
												<option value="5" <?php if($rssNumber == 5) echo "selected"?>>5</option>
												<option value="10" <?php if($rssNumber == 10) echo "selected"?>>10</option>
												<option value="15" <?php if($rssNumber == 15) echo "selected"?>>15</option>
												<option value="20" <?php if($rssNumber == 20) echo "selected"?>>20</option>
												<option value="25" <?php if($rssNumber == 25) echo "selected"?>>25</option>
												<option value="30" <?php if($rssNumber == 30) echo "selected"?>>30</option>
												<option value="-1" <?php if($rssNumber == -1) echo "checked"?>>MAX</option>
											</select>
										</td>
									</tr>
									<tr>
										<td><input type="button" value="apply" onclick="setRSS();"></td>
									</tr>
								  </table>
							  </div>
							  <h3>Twitter</h3>
								<div>
								  <table>
									<tr>
										<td>Activate</td>
										<td><input type="checkbox" id="twitterEnable" value="enable" <?php if($twitterActive) echo "checked"?>></td>
									</tr>
									<tr>
										<td>Twitter ID</td>
										<td><input type="text" size="32" name="twitterId" id="twitterId" placeholder="Paste the Twitter ID" value="<?php echo htmlspecialchars($twitterId);?>"></td>
										<td><button id="twSearchIdButton" onclick="$('#twSearchTR').show();">I can't find Twitter ID</button></td>
									</tr>
									<tr id="twSearchTR" style="display:none">
										<td>&nbsp;</td>
										<td><input type="text" size="32" name="twitterUrl" id="twitterUrl" placeholder="Paste the Twitter page URL"></td>
										<td><button id="twSearchIdButton" onclick="findTwitterId();">find ID</button><button onclick="$('#twSearchTR').hide();">hide</button></td>
									</tr>
									<tr>
										<td>Number of items to display</td>
										<td>
											<select name="twitterNumber" id="twitterNumber">
												<option value="1" <?php if($twitterNumber == 1) echo "selected"?>>1</option>
												<option value="5" <?php if($twitterNumber == 5) echo "selected"?>>5</option>
												<option value="10" <?php if($twitterNumber == 10) echo "selected"?>>10</option>
												<option value="15" <?php if($twitterNumber == 15) echo "selected"?>>15</option>
												<option value="20" <?php if($twitterNumber == 20) echo "selected"?>>20</option>
												<option value="25" <?php if($twitterNumber == 25) echo "selected"?>>25</option>
												<option value="30" <?php if($twitterNumber == 30) echo "selected"?>>30</option>
												<option value="-1" <?php if($twitterNumber == -1) echo "selected"?>>MAX</option>
											</select>
										</td>
									</tr>
									<tr>
										<td><input type="button" value="apply" onclick="setTwitter();"></td>
									</tr>
								  </table>
							  </div>
							  <h3>Facebook</h3>
								<div>
								  <table>
									<tr>
										<td>Activate</td>
										<td><input type="checkbox" id="facebookEnable" value="enable" <?php if($facebookActive) echo "checked"?>></td>
									</tr>
									<tr>
										<td>Facebook ID</td>
										<td><input type="text" size="32" name="facebookId" id="facebookId" placeholder="Paste the Facebook ID" value="<?php echo htmlspecialchars($facebookId);?>"></td>
										<td><button id="fbSearchIdButton" onclick="$('#fbSearchTR').show();">I can't find Facebook ID</button></td>
									</tr>
									<tr id="fbSearchTR" style="display:none">
										<td>&nbsp;</td>
										<td><input type="text" size="32" name="facebookUrl" id="facebookUrl" placeholder="Paste the Facebook page URL"></td>
										<td><button id="fbSearchIdButton" onclick="findFacebookId();">find ID</button><button onclick="$('#fbSearchTR').hide();">hide</button></td>
									</tr>
									<tr>
										<td>Number of items to display</td>
										<td>
											<select name="facebookNumber" id="facebookNumber">
												<option value="1" <?php if($facebookNumber == 1) echo "selected"?>>1</option>
												<option value="5" <?php if($facebookNumber == 5) echo "selected"?>>5</option>
												<option value="10" <?php if($facebookNumber == 10) echo "selected"?>>10</option>
												<option value="15" <?php if($facebookNumber == 15) echo "selected"?>>15</option>
												<option value="20" <?php if($facebookNumber == 20) echo "selected"?>>20</option>
												<option value="25" <?php if($facebookNumber == 25) echo "selected"?>>25</option>
												<option value="30" <?php if($facebookNumber == 30) echo "selected"?>>30</option>
												<option value="-1" <?php if($facebookNumber == -1) echo "selected"?>>MAX</option>
											</select>
										</td>
									</tr>
									<tr>
										<td><input type="button" value="apply" onclick="setFacebook();"></td>
									</tr>
								  </table>
							  </div>
						</div>		
					</td>
				</tr>
				<tr>					
					<td>
						  <fieldset>
							<legend>Overview:</legend>
							<label for="radio1">Player</label>
							<input onclick="showView('player');" type="radio" name="radio1" id="radio1" checked>
							<label for="radio2">Picture</label>
							<input onclick="showView('image');" type="radio" name="radio1" id="radio2">
						  </fieldset>
					   <img style="display:none;border: 1px solid black;" id="videoImage" height="480" src="../status/currentImage.png"/>
						<a id="player" class="player" style="display:block;border: 1px solid black;width:853px;height:480px;margin:10px auto" id="player"></a>
					</td>
				</tr>

			</table>

			
		</div>
	</div>

 </div>


  <script>
	  
  $('#accordionNews').accordion({
    active: false,
    collapsible: true            
	});
	  
  $( function() {
    $( "#accordionNews" ).accordion();
  } );
  
   $('#accordionMain').accordion({
    active: false,
    collapsible: true            
	});
   $( function() {
    $( "#accordionMain" ).accordion();
  } );
  
     $('#accordionScenes').accordion({
    active: false,
    collapsible: true            
	});
   $( function() {
    $( "#accordionScenes" ).accordion();
  } );
  
  </script>

<script>

getLiveMeta();

limitText("showTitle", 64);
function limitText(fieldId, maxChar){
    $('#' + fieldId).attr('maxlength',maxChar);
}

function setTime(){
	var timezone = $('#timezone').val().trim();
	var timeDisplayMode = $('#timeDisplayMode').val().trim();
	var hourFormat = $('#hourFormat').val().trim();
	
	var wsUrl = "../webservices/setTimeConfig.php?timezone=" + encodeURIComponent(timezone) + '&timeDisplayMode=' + encodeURIComponent(timeDisplayMode) + '&hourFormat=' + encodeURIComponent(hourFormat);
	$.get( wsUrl , function( data ) {
		$("#messageLabel").html("Time configuration was succesfully updated");
		$("#messageLabel").css( "display", "block" );
		$("#messageSep").css( "display", "block" );
	});
}

function getLiveMeta(){
	var liveTitle = $('#liveTitle').val().trim();
	var liveSubtitle = $('#liveSubtitle').val().trim();
	var liveDescription = $('#liveDescription').val().trim();
	
	var wsUrl = "../webservices/getLiveMeta.php";
	$.get( wsUrl , function( data ) {
		$('#liveTitle').val(data["liveTitle"]);
		$('#liveSubtitle').val(data["liveSubtitle"]);
		$('#liveDescription').val(data["liveDescription"]);
	});

}

function setLiveMeta(){
	var liveTitle = $('#liveTitle').val().trim();
	var liveSubtitle = $('#liveSubtitle').val().trim();
	var liveDescription = $('#liveDescription').val().trim();
	
	var wsUrl = "../webservices/setLiveMeta.php?liveTitle=" + encodeURIComponent(liveTitle) + '&liveSubtitle=' + encodeURIComponent(liveSubtitle) + '&liveDescription=' + encodeURIComponent(liveDescription);
	$.get( wsUrl , function( data ) {
		$("#messageLabel").html("Live description was succesfully updated");
		$("#messageLabel").css( "display", "block" );
		$("#messageSep").css( "display", "block" );
		
		getLiveMeta();
	});
}

function setShowTitle(){
	var showTitle = $('#showTitle').val().trim();
	
	var wsUrl = "../webservices/setTitle.php?showTitle=" + encodeURIComponent(showTitle);
	$.get( wsUrl , function( data ) {
		$("#messageLabel").html("Show Title was succesfully updated");
		$("#messageLabel").css( "display", "block" );
		$("#messageSep").css( "display", "block" );
	});
}

function findFacebookId(){
	var url = $('#facebookUrl').val().trim();
	if(url.length == 0) return;
	
	var wsUrl = "../webservices/getFacebookId.php?url=" + encodeURIComponent(url);
	$.get( wsUrl , function( data ) {
		$('#facebookId').val(data["id"]);
	});
}

function findTwitterId(){
	var url = $('#twitterUrl').val().trim();
	if(url.length == 0) return;
	
	var wsUrl = "../webservices/getTwitterId.php?url=" + encodeURIComponent(url);
	$.get( wsUrl , function( data ) {
		$('#twitterId').val(data["id"]);
	});
}


function setNewsDuration(){

	var newsDuration = $("#newsDuration").val();
	var wsUrl = "../webservices/setNewsDuration.php?newsDuration=" + newsDuration;
	$.get( wsUrl , function( data ) {
		$("#messageLabel").html("News global configuration was succesfully updated");
		$("#messageLabel").css( "display", "block" );
		$("#messageSep").css( "display", "block" );

	});

}

function activateNews(newsItem, value){

	var wsUrl = "../webservices/activateNews.php?" + newsItem + "=" + value;
	$.get( wsUrl , function( data ) {
		//noop

	});

}

function setNewsNumber(newsItem, value){

	var wsUrl = "../webservices/setNewsNumber.php?" + newsItem + "=" + value;
	$.get( wsUrl , function( data ) {
		//noop

	});

}

function setRSS(){
	var rssURL = $("#rssURL").val();
	var rssActive = $("#rssEnable").is(':checked') ? 1:0;
	var rssNumber = $("#rssNumber").val();
	var wsUrl = "../webservices/setRss.php?rssURL=" + encodeURIComponent(rssURL);
	$.get( wsUrl , function( data ) {
		
		activateNews("rssActive", rssActive);
		setNewsNumber("rssNumber", rssNumber);

		$("#messageLabel").html("RSS configuration was succesfully updated");
		$("#messageLabel").css( "display", "block" );
		$("#messageSep").css( "display", "block" );

	});
}

function setTwitter(){
	var twitterId = $("#twitterId").val();
	var twitterActive = $("#twitterEnable").is(':checked') ? 1:0;
	var twitterNumber = $("#twitterNumber").val();
	var wsUrl = "../webservices/setTwitterId.php?twitterId=" + encodeURIComponent(twitterId);
	$.get( wsUrl , function( data ) {

		activateNews("twitterActive", twitterActive);
		setNewsNumber("twitterNumber", twitterNumber);

		$("#messageLabel").html("Twitter configuration was succesfully updated");
		$("#messageLabel").css( "display", "block" );
		$("#messageSep").css( "display", "block" );

	});
}

function setFacebook(){
	var facebookId = $("#facebookId").val();
	var facebookActive = $("#facebookEnable").is(':checked') ? 1:0;
	var facebookNumber = $("#facebookNumber").val();
	var wsUrl = "../webservices/setFacebookId.php?facebookId=" + encodeURIComponent(facebookId);
	$.get( wsUrl , function( data ) {

		activateNews("facebookActive", facebookActive);
		setNewsNumber("facebookNumber", facebookNumber);

		$("#messageLabel").html("Facebook configuration was succesfully updated");
		$("#messageLabel").css( "display", "block" );
		$("#messageSep").css( "display", "block" );

	});
}
	
function showView(kind){
	if(kind == "player"){
		$('#player').show();
		$('#videoImage').hide();
	}
	if(kind == "image"){
		$('#player').hide();
		$('#videoImage').show();
	}
}
$('#radio1').attr('checked', true);

	
setInterval(function(){
	var randm = Math.random();
    $("#videoImage").attr("src", "../status/currentImage.png?rand=" +randm);
},10000);
</script>

<?php $server = $_SERVER["SERVER_NAME"]; ?>;

<script>
	$f("player", "./swf/flowplayer-3.2.18.swf", {
 
    clip: {
        url: 'rtmp://<?php echo $server; ?>:<?php echo getMappedPort(RTMP_OUT_PORT)?>/oflaDemo/synthesiaLocal',
        //url: 'rtmp://192.168.1.38:1935/oflaDemo/test',
        live: true,
        provider: 'synthesia'
    },
 
    plugins: {
        synthesia: {
            url: "flowplayer.rtmp-3.2.13.swf",
        }
    }
});
</script>

<script>
function selectInput(inputNumber){
	
	var wsUrl = "../webservices/setSource.php?source=" + encodeURIComponent(inputNumber);
	$.get( wsUrl , function( data ) {
		$("#messageLabel").html("Source was succesfully switched to " + (inputNumber == 0 ? "Playlist" : "Live"));
		$("#messageLabel").css( "display", "block" );
		$("#messageSep").css( "display", "block" );

	});
	
}
</script>

<script>
function selectScenario(scenarioId, scenarioName){
	var wsUrl = "../webservices/scenario/setScenario.php?scenario_id=" + encodeURIComponent(scenarioId);
	$.get( wsUrl , function( data ) {
		$("#messageLabel").html("Scenario was succesfully switched to " + scenarioName);
		$("#messageLabel").css( "display", "block" );
		$("#messageSep").css( "display", "block" );

	});
	
}
</script>

<script>
function selectScene(scenarioId, scenarioName){
	var wsUrl = "../webservices/scenario/authoring/setScene.php?&scene_id=" + encodeURIComponent(scenarioId);
	$.get( wsUrl , function( data ) {
		$("#messageLabel").html("Scenario was succesfully switched to " + scenarioName);
		$("#messageLabel").css( "display", "block" );
		$("#messageSep").css( "display", "block" );

	});
	
}
</script>


</body>

</html>

