<?php
   require_once "php/info.php";
   include "../webservices/scenario/authoring/php/utils.php";

//print_r($_POST);

$sceneId = DEFAULT_SCENE;
if(isset($_POST["current_scene_id"])){
	$sceneId = $_POST["current_scene_id"];
}
$updateImage = 0;
if(isset($_POST['updateImage'])){
	$updateImage = $_POST['updateImage'];
}
if($updateImage == 1){
	

		if($_FILES['background_image_modify']){
			$imageData = $_FILES['background_image_modify'];
			setPostImage($sceneId, $imageData);
		}

}

$createNewScene = 0;
if(isset($_POST["createNewScene"])){
	$createNewScene = $_POST["createNewScene"];
}

if($createNewScene == 1){
	$sceneName = trim($_POST["new_scene_title"]);
	if(!$sceneName){
		$error = true;
		$message = "You must give a name to your scene";
	}
	else{
		$sceneId = uniqid("_scene_");
		createNewScene($sceneId, $sceneName);
	}
}

?>


<html lang="en">
   <head>
      <?php createHeader();?>
      <style>
		 #scenario_panel {position:relative; background-image: url('images/background.png'); background-size: cover; background-repeat: no-repeat; background-position: center center;}
		 	 
		 .scene_block {display:block;filter:alpha(opacity=80); opacity:0.8; border:1px solid black; line-height: 1.1em;}

		 .block-span {font-size:80%;overflow:hidden;}

		.save-button{margin-left:10px; margin-bottom:5px;}

         #news_text_block {position:absolute;  left:16px; top:464px; width: 960px; height: 180px;}
         #news_image_block {position:absolute;  left:978px; top:464px; width: 280px; height: 180px;}
         #now_playing_text_block {position:absolute;  left:798px; top:16px; width: 466px; height: 420px;}
         #video_block {position:absolute;  left:10px; top:10px; width: 766px; height: 432px;}
         #show_title_block {position:absolute;  left:144px; top:654px; width: 800px; height: 61px;}
         #clock_block {position:absolute;  left:960px; top:654px; width: 250px; height: 61px;}


      </style>


   </head>
   <body onload="setAllParameters()">

<?php createNavBar("scenes",""); ?>

<?php
	$display =  strlen($message)>0 ? "block" : "none";
	$class="label label-default";
	if($error) $class="label label-danger";
	echo '<div id="messageLabel" class="' . $class . '" style="display:' . $display . '">' . $message .'</div><div id="messageSep" style="display:' . $display . '"><br/></div>';

?>


      <div class="panel panel-default">
         <div class="panel-heading">
            <h3 class="panel-title">Scene Creator</h3>
         </div>
      </div>
      <div class="panel-body">

		<div class="container">
			<fieldset>
			<legend>Create a New Scene:</legend>
		  </fieldset>
			<form method="post" name="newScene_form">
				<table>
					<tr>
						<td><input size="32" placeholder="Enter your scene name" name="new_scene_title" type="text"></td>
						<td><input type="submit" value="Create"></td>
					</tr>
				</table>
				<input type="hidden" name="createNewScene" value="1">
			</form>
		</div>  

		<br/>

		<div class="container">
			<fieldset>
			<legend>Load a Scene:</legend>
		  </fieldset>
			<table>
				<tr>
					<td><div id="scenes_div"><select id="scenes_select"></select></div></td>
					<td><button onclick="loadScene(null)">Load Scene</button></td>
					<td><button onclick="removeScene()">Remove Scene</button></td>
					<td><button onclick="loadScenes()">Refresh Scenes</button></td>
				</tr>
			</table>		
		</div>  

		<br/>

      <div class="container">
			<fieldset>
			<legend>Scene Editor:<button class="save-button btn btn-info" onclick="saveParameters();">Save Scene</button></legend>
		  </fieldset>


		  <div id="scenario_panel" style="width:1280px;height:720px;border:1px solid black;">
			  <div id="video_block" class="ui-widget-content draggable resizable scene_block">
				<center><p><b>VIDEO</b></p><span class="block-span"></span></center>
			 </div>
			 <div id="now_playing_text_block" class="ui-widget-content draggable resizable scene_block">
				<center><p><b>NOW PLAYING / LIVE</b></p><span class="block-span"></span></center>
			 </div>
			 <div id="news_text_block" class="ui-widget-content draggable resizable scene_block">
				<center><p><b>NEWS TEXT</b></p><span class="block-span"></span></center>
			 </div>
			 <div id="news_image_block" class="ui-widget-content draggable resizable scene_block">
				<center><p><b>NEWS IMAGE</b></p><span class="block-span"></span></center>
			 </div>
			  <div id="show_title_block" class="ui-widget-content draggable resizable scene_block">
				<center><p><b>SHOW TITLE</b></p><span class="block-span"></span></center>
			 </div>
			  <div id="clock_block" class="ui-widget-content draggable resizable scene_block">
				<center><p><b>CLOCK</b></p><span class="block-span"></span></center>
			 </div>

			</div>
      </div>
      
      <br/>
      <div class="container">
			<fieldset>
			<legend>Elements Editor:<button class="save-button btn btn-info" onclick="saveParameters();">Save Scene</button></legend>
		  </fieldset>
			<div id="elements_editor">			
				<div id="accordion_editor">
					<h3>Background Image</h3>
					<div id="background_image_edit">
					<form name="background_image_modify_form" method="post" enctype="multipart/form-data">
						<table>
							<tr>
								<td><label for="background_image_current">Current Image</label></td>
								<td><img height="128px" id="background_image_current" src=""></td>
							</tr>
							<tr>
								<td><label for="background_image_modify">Change Image</label></td>
								<td><input type="file" name="background_image_modify" id="background_image_modify"></td>
								<td><input type="submit" name="background_image_modify_submit" id="background_image_modify_submit" value="upload image"></td>
							</tr>
						</table>
						<input type="hidden" name="current_scene_id" id="current_scene_id" value="">
						<input type="hidden" name="updateImage" id="updateImage" value="1">
						</form>
					</div>
					<h3>Show Title</h3>
					<div id="show_title_block_edit">
						<table>
							<tbody>
								<tr>
									<td><label for="show_title_block_visible">Visible</label></td>
									<td><input onclick="applyEditorChanges();" id="show_title_block_visible" checked="" type="checkbox"></td>
								</tr>
								<tr>
									<td><label for="show_title_block_x">Position</label></td>
									<td><input onchange="applyEditorChanges();" size="4" value="144" id="show_title_block_x" type="number">px , <input onchange="applyEditorChanges();" size="4" value="660" id="show_title_block_y" type="number">px</td>
								</tr>
								<tr>
									<td><label for="show_title_block_width">Size</label></td>
									<td><input onchange="applyEditorChanges();" size="4" value="800" id="show_title_block_width" type="number">px , <input onchange="applyEditorChanges();" size="4" value="61" id="show_title_block_height" type="number">px</td>
								</tr>
								<tr>
									<td><label for="show_title_block_color">Color</label></td>
									<td><input onchange="applyEditorChanges();" class="jscolor" value="ffffff" id="show_title_block_color"></td>
								</tr>
								<tr>
									<td><label for="show_title_block_upcase">Upper Case</label></td>
									<td><input onclick="applyEditorChanges();" id="show_title_block_upcase" checked="" type="checkbox"></td>
								</tr>
							</tbody>
						</table>
					</div>
					<h3>Clock</h3>
					<div id="clock_block_edit">
						<table>
							<tbody>
								<tr>
									<td><label for="clock_block_visible">Visible</label></td>
									<td><input onclick="applyEditorChanges();" id="clock_block_visible" checked="" type="checkbox"></td>
								</tr>
								<tr>
									<td><label for="clock_block_x">Position</label></td>
									<td><input onchange="applyEditorChanges();" size="4" value="960" id="clock_block_x" type="number">px , <input onchange="applyEditorChanges();" size="4" value="660" id="clock_block_y" type="number">px</td>
								</tr>
								<tr>
									<td><label for="clock_block_width">Size</label></td>
									<td><input onchange="applyEditorChanges();" size="4" value="250" id="clock_block_width" type="number">px , <input onchange="applyEditorChanges();" size="4" value="68" id="clock_block_height" type="number">px</td>
								</tr>
								<tr>
									<td><label for="clock_block_color">Color</label></td>
									<td><input onchange="applyEditorChanges();" class="jscolor" value="ffffff" id="clock_block_color"></td>
								</tr>
								<tr>
									<td><label for="clock_block_visible">Upper Case</label></td>
									<td><input onclick="applyEditorChanges();" id="clock_block_upcase" checked="" type="checkbox"></td>
								</tr>
							</tbody>
						</table>
					</div>
					<h3>Video</h3>
					<div id="video_block_edit">
						<table>
							<tbody>
								<tr>
									<td><label for="video_block_visible">Visible</label></td>
									<td><input onclick="applyEditorChanges();" id="video_block_visible" checked="" type="checkbox"></td>
								</tr>
								<tr>
									<td><label for="video_block_x">Position</label></td>
									<td><input onchange="applyEditorChanges();" size="4" value="10" id="video_block_x" type="number">px , <input onchange="applyEditorChanges();" size="4" value="10" id="video_block_y" type="number">px</td>
								</tr>
								<tr>
									<td><label for="video_block_width">Size</label></td>
									<td><input onchange="applyEditorChanges();" size="4" value="766" id="video_block_width" type="number">px , <input onchange="applyEditorChanges();" size="4" value="432" id="video_block_height" type="number">px</td>
								</tr>
							</tbody>
						</table>
					</div>
					<h3>Now Playing / Live</h3>
					<div id="now_playing_text_block_edit">
						<table>
							<tbody>
								<tr>
									<td><label for="now_playing_text_block_visible">Visible</label></td>
									<td><input onclick="applyEditorChanges();" id="now_playing_text_block_visible" checked="" type="checkbox"></td>
								</tr>
								<tr>
									<td><label for="now_playing_text_block_x">Position</label></td>
									<td><input onchange="applyEditorChanges();" size="4" value="802" id="now_playing_text_block_x" type="number">px , <input onchange="applyEditorChanges();" size="4" value="10" id="now_playing_text_block_y" type="number">px</td>
								</tr>
								<tr>
									<td><label for="now_playing_text_block_width">Size</label></td>
									<td><input onchange="applyEditorChanges();" size="4" value="460" id="now_playing_text_block_width" type="number">px , <input onchange="applyEditorChanges();" size="4" value="432" id="now_playing_text_block_height" type="number">px</td>
								</tr>
								<tr>
									<td colspan="2">
										<div class="accordion_subeditor">
											<h3>Title</h3>
											<div>
												<table>

								<tr>
									<td><label for="now_playing_text_block_title_visible">Title Visible</label></td>
									<td><input onclick="applyEditorChanges();" id="now_playing_text_block_title_visible" checked="" type="checkbox"></td>
								</tr>
								<tr>
									<td><label for="now_playing_text_block_title_color">Title Color</label></td>
									<td><input onchange="applyEditorChanges();" class="jscolor" value="ffffff" id="now_playing_text_block_title_color"></td>
								</tr>
								<tr>
									<td><label for="now_playing_text_block_title_upcase">Title Upper Case</label></td>
									<td><input onclick="applyEditorChanges();" id="now_playing_text_block_title_upcase" checked="" type="checkbox"></td>
								</tr>
												</table>
											</div>
											<h3>SubTitle</h3>
											<div>
												<table>
								<tr>
									<td><label for="now_playing_text_block_subtitle_visible">SubTitle Visible</label></td>
									<td><input onclick="applyEditorChanges();" id="now_playing_text_block_subtitle_visible" checked="" type="checkbox"></td>
								</tr>
								<tr>
									<td><label for="now_playing_text_block_subtitle_color">SubTitle Color</label></td>
									<td><input onchange="applyEditorChanges();" class="jscolor" value="ffffff" id="now_playing_text_block_subtitle_color"></td>
								</tr>
								<tr>
									<td><label for="now_playing_text_block_subtitle_upcase">SubTitle Upper Case</label></td>
									<td><input onclick="applyEditorChanges();" id="now_playing_text_block_subtitle_upcase" checked="" type="checkbox"></td>
								</tr>

												</table>
											</div>
											<h3>Description</h3>
											<div>
												<table>
								<tr>
									<td><label for="now_playing_text_block_description_visible">Description Visible</label></td>
									<td><input onclick="applyEditorChanges();" id="now_playing_text_block_description_visible" checked="" type="checkbox"></td>
								</tr>
								<tr>
									<td><label for="now_playing_text_block_description_color">Description Color</label></td>
									<td><input onchange="applyEditorChanges();" class="jscolor" value="ffffff" id="now_playing_text_block_description_color"></td>
								</tr>
								<tr>
									<td><label for="now_playing_text_block_description_upcase">Description Upper Case</label></td>
									<td><input onclick="applyEditorChanges();" id="now_playing_text_block_description_upcase" checked="" type="checkbox"></td>
								</tr>

												</table>
											</div>
										</div>
									</td>
								</tr>
	
							</tbody>
						</table>
					</div>
					<h3>News Text</h3>
					<div id="news_text_block_edit">
						<table>
							<tbody>
								<tr>
									<td><label for="news_text_block_visible">Visible</label></td>
									<td><input onclick="applyEditorChanges();" id="news_text_block_visible" checked="" type="checkbox"></td>
								</tr>
								<tr>
									<td><label for="news_text_block_x">Position</label></td>
									<td><input onchange="applyEditorChanges();" size="4" value="20" id="news_text_block_x" type="number">px , <input onchange="applyEditorChanges();" size="4" value="477" id="news_text_block_y" type="number">px</td>
								</tr>
								<tr>
									<td><label for="news_text_block_width">Size</label></td>
									<td><input onchange="applyEditorChanges();" size="4" value="934" id="news_text_block_width" type="number">px , <input onchange="applyEditorChanges();" size="4" value="167" id="news_text_block_height" type="number">px</td>
								</tr>
								<tr>
									<td colspan="2">
										<div class="accordion_subeditor">
											<h3>Title</h3>
											<div>
												<table>

								<tr>
									<td><label for="news_text_block_title_visible">Title Visible</label></td>
									<td><input onclick="applyEditorChanges();" id="news_text_block_title_visible" checked="" type="checkbox"></td>
								</tr>
								<tr>
									<td><label for="news_text_block_title_color">Title Color</label></td>
									<td><input onchange="applyEditorChanges();" class="jscolor" value="ffffff" id="news_text_block_title_color"></td>
								</tr>
								<tr>
									<td><label for="news_text_block_title_upcase">Title Upper Case</label></td>
									<td><input onclick="applyEditorChanges();" id="news_text_block_title_upcase" checked="" type="checkbox"></td>
								</tr>
												</table>
											</div>
											<h3>SubTitle</h3>
											<div>
												<table>
								<tr>
									<td><label for="news_text_block_subtitle_visible">SubTitle Visible</label></td>
									<td><input onclick="applyEditorChanges();" id="news_text_block_subtitle_visible" checked="" type="checkbox"></td>
								</tr>
								<tr>
									<td><label for="news_text_block_subtitle_color">SubTitle Color</label></td>
									<td><input onchange="applyEditorChanges();" class="jscolor" value="ffffff" id="news_text_block_subtitle_color"></td>
								</tr>
								<tr>
									<td><label for="news_text_block_subtitle_upcase">SubTitle Upper Case</label></td>
									<td><input onclick="applyEditorChanges();" id="news_text_block_subtitle_upcase" checked="" type="checkbox"></td>
								</tr>

												</table>
											</div>
											<h3>Description</h3>
											<div>
												<table>
								<tr>
									<td><label for="news_text_block_description_visible">Description Visible</label></td>
									<td><input onclick="applyEditorChanges();" id="news_text_block_description_visible" checked="" type="checkbox"></td>
								</tr>
								<tr>
									<td><label for="news_text_block_description_color">Description Color</label></td>
									<td><input onchange="applyEditorChanges();" class="jscolor" value="ffffff" id="news_text_block_description_color"></td>
								</tr>
								<tr>
									<td><label for="news_text_block_description_upcase">Description Upper Case</label></td>
									<td><input onclick="applyEditorChanges();" id="news_text_block_description_upcase" checked="" type="checkbox"></td>
								</tr>

												</table>
											</div>
											<h3>Footer</h3>
											<div>
												<table>
								<tr>
									<td><label for="news_text_block_footer_visible">Footer Visible</label></td>
									<td><input onclick="applyEditorChanges();" id="news_text_block_footer_visible" checked="" type="checkbox"></td>
								</tr>
								<tr>
									<td><label for="news_text_block_footer_color">Footer Color</label></td>
									<td><input onchange="applyEditorChanges();" class="jscolor" value="ffffff" id="news_text_block_footer_color"></td>
								</tr>
								<tr>
									<td><label for="news_text_block_footer_upcase">Footer Upper Case</label></td>
									<td><input onclick="applyEditorChanges();" id="news_text_block_footer_upcase" checked="" type="checkbox"></td>
								</tr>

												</table>
											</div>
											<h3>Entities</h3>
											<div>
												<table>
								<tr>
									<td><label for="news_text_block_twitter_entities_color">Twitter Entities Color</label></td>
									<td><input onchange="applyEditorChanges();" class="jscolor" value="ffffff" id="news_text_block_twitter_entities_color"></td>
								</tr>
								<tr>
									<td><label for="news_text_block_twitter_entities_upcase">Twitter Entities Upper Case</label></td>
									<td><input onclick="applyEditorChanges();" id="news_text_block_twitter_entities_upcase" checked="" type="checkbox"></td>
								</tr>
								<tr>
									<td><label for="news_text_block_facebook_entities_color">Facebook Entities Color</label></td>
									<td><input onchange="applyEditorChanges();" class="jscolor" value="ffffff" id="news_text_block_facebook_entities_color"></td>
								</tr>
								<tr>
									<td><label for="news_text_block_facebook_entities_upcase">Facebook Entities Upper Case</label></td>
									<td><input onclick="applyEditorChanges();" id="news_text_block_facebook_entities_upcase" checked="" type="checkbox"></td>
								</tr>

												</table>
											</div>
										</div>
									</td>
								</tr>
	
							</tbody>
						</table>
					</div>
					<h3>News Image</h3>
					<div id="news_image_block_edit">
						<table>
							<tbody>
								<tr>
									<td><label for="news_image_block_visible">Visible</label></td>
									<td><input onclick="applyEditorChanges();" id="news_image_block_visible" checked="" type="checkbox"></td>
								</tr>
								<tr>
									<td><label for="news_image_block_x">Position</label></td>
									<td><input onchange="applyEditorChanges();" size="4" value="961" id="news_image_block_x" type="number">px , <input onchange="applyEditorChanges();" size="4" value="478" id="news_image_block_y" type="number">px</td>
								</tr>
								<tr>
									<td><label for="news_image_block_width">Size</label></td>
									<td><input onchange="applyEditorChanges();" size="4" value="300" id="news_image_block_width" type="number">px , <input onchange="applyEditorChanges();" size="4" value="167" id="news_image_block_height" type="number">px</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>



		</div>
      
     </div>
   </body>
   
<script>
	
 $('.accordion_subeditor').accordion({
    active: false,
    autoHeight:true,
    collapsible: true            
	});
	  
  $( function() {
    $( ".accordion_subeditor" ).accordion();
  } );
	
  $('#accordion_editor').accordion({
    active: false,
    autoHeight:true,
    collapsible: true            
	});
	  
  $( function() {
    $( "#accordion_editor" ).accordion();
  } );
	
	$( function() {
	   $( ".draggable" ).draggable({
		  containment: "parent",
		});
		$( ".resizable" ).resizable({
		  containment: "parent"   
		});

		$( ".draggable" ).on( "drag", 
			function( event, ui ) {
				setParameters($(this));
				
			} 
		);
		$( ".resizable" ).on( "resize", 
			function( event, ui ) {
				setParameters($(this));
				
			} 
		);

	 } );
	 
	borderSize = 1;	 
	 
	 function setParameters(target){
		//var parentLeftPos = target.parent().position().left;
		//var parentTopPos = target.parent().position().top;
		var leftPos = Math.floor(target.position().left);
		var topPos = Math.floor(target.position().top);
		var width = Math.floor(target.width() + 2 * borderSize);
		var height = Math.floor(target.height() + 2 * borderSize);

		
		$( "span", target ).html( "pos: " + leftPos + " x " + topPos + " px<br/>size: " +  width + " x " + height + " px");
		
		var targetId = target.attr("id");
	
		$('#' + targetId+"_x").val(leftPos);
		$('#' + targetId+"_y").val(topPos);
		$('#' + targetId+"_width").val(width);
		$('#' + targetId+"_height").val(height);
	 }

	function setAllParameters(){
		setParameters($('#news_text_block'));
		setParameters($('#news_image_block'));
		setParameters($('#now_playing_text_block'));
		setParameters($('#video_block'));
		setParameters($('#show_title_block'));
		setParameters($('#clock_block'));
		

	}
	
	function saveParameters(){
		var sceneId = $('#current_scene_id').val();
		
		
		postData = {
			sceneId: sceneId,
			show_title_block_visible: $('#show_title_block_visible').prop("checked"),
			show_title_block_x: $('#show_title_block_x').val(),
			show_title_block_y: $('#show_title_block_y').val(),
			show_title_block_width: $('#show_title_block_width').val(),
			show_title_block_height: $('#show_title_block_height').val(),
			show_title_block_color: $('#show_title_block_color').val(),
			show_title_block_upcase: $('#show_title_block_upcase').prop("checked"),
			
			clock_block_visible: $('#clock_block_visible').prop("checked"),
			clock_block_x: $('#clock_block_x').val(),
			clock_block_y: $('#clock_block_y').val(),
			clock_block_width: $('#clock_block_width').val(),
			clock_block_height: $('#clock_block_height').val(),
			clock_block_color: $('#clock_block_color').val(),
			clock_block_upcase: $('#clock_block_upcase').prop("checked"),
			
			video_block_visible: $('#video_block_visible').prop("checked"),
			video_block_x: $('#video_block_x').val(),
			video_block_y: $('#video_block_y').val(),
			video_block_width: $('#video_block_width').val(),
			video_block_height: $('#video_block_height').val(),
			
			now_playing_text_block_visible: $('#now_playing_text_block_visible').prop("checked"),
			now_playing_text_block_x: $('#now_playing_text_block_x').val(),
			now_playing_text_block_y: $('#now_playing_text_block_y').val(),
			now_playing_text_block_width: $('#now_playing_text_block_width').val(),
			now_playing_text_block_height: $('#now_playing_text_block_height').val(),
			
			now_playing_text_block_title_visible: $('#now_playing_text_block_title_visible').prop("checked"),
			now_playing_text_block_title_color: $('#now_playing_text_block_title_color').val(),
			now_playing_text_block_title_upcase: $('#now_playing_text_block_title_upcase').prop("checked"),
			
			now_playing_text_block_subtitle_visible: $('#now_playing_text_block_subtitle_visible').prop("checked"),
			now_playing_text_block_subtitle_color: $('#now_playing_text_block_subtitle_color').val(),
			now_playing_text_block_subtitle_upcase: $('#now_playing_text_block_subtitle_upcase').prop("checked"),
			
			now_playing_text_block_description_visible: $('#now_playing_text_block_description_visible').prop("checked"),
			now_playing_text_block_description_color: $('#now_playing_text_block_description_color').val(),
			now_playing_text_block_description_upcase: $('#now_playing_text_block_description_upcase').prop("checked"),

			news_text_block_visible: $('#news_text_block_visible').prop("checked"),
			news_text_block_x: $('#news_text_block_x').val(),
			news_text_block_y: $('#news_text_block_y').val(),
			news_text_block_width: $('#news_text_block_width').val(),
			news_text_block_height: $('#news_text_block_height').val(),
			
			news_text_block_title_visible: $('#news_text_block_title_visible').prop("checked"),
			news_text_block_title_color: $('#news_text_block_title_color').val(),
			news_text_block_title_upcase: $('#news_text_block_title_upcase').prop("checked"),
			
			news_text_block_subtitle_visible: $('#news_text_block_subtitle_visible').prop("checked"),
			news_text_block_subtitle_color: $('#news_text_block_subtitle_color').val(),
			news_text_block_subtitle_upcase: $('#news_text_block_subtitle_upcase').prop("checked"),
			
			news_text_block_description_visible: $('#news_text_block_description_visible').prop("checked"),
			news_text_block_description_color: $('#news_text_block_description_color').val(),
			news_text_block_description_upcase: $('#news_text_block_description_upcase').prop("checked"),

			news_text_block_footer_visible: $('#news_text_block_footer_visible').prop("checked"),
			news_text_block_footer_color: $('#news_text_block_footer_color').val(),
			news_text_block_footer_upcase: $('#news_text_block_footer_upcase').prop("checked"),

			news_text_block_twitter_entities_color: $('#news_text_block_twitter_entities_color').val(),
			news_text_block_twitter_entities_upcase: $('#news_text_block_twitter_entities_upcase').prop("checked"),

			news_text_block_facebook_entities_color: $('#news_text_block_facebook_entities_color').val(),
			news_text_block_facebook_entities_upcase: $('#news_text_block_facebook_entities_upcase').prop("checked"),

			news_image_block_visible: $('#news_image_block_visible').prop("checked"),
			news_image_block_x: $('#news_image_block_x').val(),
			news_image_block_y: $('#news_image_block_y').val(),
			news_image_block_width: $('#news_image_block_width').val(),
			news_image_block_height: $('#news_image_block_height').val(),

			
		};
		var wsUrl = "../webservices/scenario/authoring/saveScene.php";
		$.post( wsUrl, postData).done(function( data ) {
			console.log(data);
			$("#messageLabel").html("Scene was succesfully saved");
			$("#messageLabel").css( "display", "block" );
			$("#messageSep").css( "display", "block" );
			loadScenes();
			loadScene(sceneId);
		});

	}


</script>

<script>
	function loadScenes(){
		var wsUrl = "../webservices/scenario/authoring/listScenes.php";
		$.get( wsUrl , function( data ) {
			//clear all options
			$('#scenes_select').find('option').remove();
			for (var key in data) {		
				var sceneId = key;
				if(sceneId == "<?php echo DEFAULT_SCENE?>") continue;
				var sceneName = data[key];
				var option = $('<option/>');
				option.attr({ 'value': sceneId }).text(sceneName);
				$('#scenes_select').append(option);
    
			}

		});

	}

	function loadScene(sceneId){		
		if(sceneId == null){
			sceneId = $('#scenes_select').val();
		}
		
		doLoadScene(sceneId);
		$('#current_scene_id').val(sceneId);

	}
	
	function removeScene(){
		var defaultScene = "<?php echo DEFAULT_SCENE?>";	
		var sceneId = $('#scenes_select').val();		
		if(defaultScene == sceneId){
			alert("Impossible to remove default scene");
			return ;
		}
		if(!window.confirm("Are you sure you want to delete this scene ?")) return;

		doRemoveScene(sceneId);
	}

	function doLoadScene(sceneId){

		if(sceneId == "<?php echo DEFAULT_SCENE?>"){
			$('#scenario_panel').hide();
			$('#elements_editor').hide();
			$('.save-button').hide();
		}
		else{
			$('#scenario_panel').show();
			$('#elements_editor').show();
			$('.save-button').show();
		}
		var wsUrl = "../webservices/scenario/authoring/loadScene.php?scene_id=" + encodeURIComponent(sceneId);
		$.get( wsUrl , function( data ) {

			//load scene data
			//console.log(data);


			//load image
			var wsUrl2 = "../webservices/scenario/authoring/getImagePath.php?scene_id=" + encodeURIComponent(sceneId);
			$.get( wsUrl2 , function( data ) {
				var relativeUrl = data.relative;
				$('#scenario_panel').css("background-image", "url('" + relativeUrl + "')");
				$('#scenario_panel').css("background-size", "cover");
				$('#scenario_panel').css("background-repeat", "no-repeat");
				$('#scenario_panel').css("background-position", "center center");

				$('#background_image_current').attr("src", relativeUrl);

			});


			//load blocks
			updateVisualElement("video_block", 10000, data);
			updateVisualElement("now_playing_text_block", 201, data);
			updateVisualElement("news_text_block", 301, data);
			updateVisualElement("news_image_block", 300, data);
			updateVisualElement("show_title_block", 101, data);
			updateVisualElement("clock_block", 1, data);
			
			updateEditorElement("video_block", 10000, data);
			updateEditorElement("now_playing_text_block", 201, data);
			updateEditorElement("news_text_block", 301, data);
			updateEditorElement("news_image_block", 300, data);
			updateEditorElement("show_title_block", 101, data);
			updateEditorElement("clock_block", 1, data);
			
			setAllParameters();
			
			


		});
	}

	function updateVisualElement(targetId, sceneId, data){
		
		/*
		* loads wysiwyg
		*/
		var targetHtml = $('#' + targetId);
		var targetJson = data.elements['element_' + sceneId];
		var visible = targetJson.visible === "true";
		var position = targetJson.position;
		var positionComps = position.split(",");
		var size = targetJson.size;
		var sizeComps = size.split(",");
		
		$("#"+targetId).css({top: positionComps[1], left: positionComps[0]});
		$("#"+targetId).css({width: sizeComps[0], height: sizeComps[1]});
		
		if(visible) $("#"+targetId).show();
		else $("#"+targetId).hide();
				
	}


	function updateEditorElement(targetId, sceneId, data){
	
		var targetJson = data.elements['element_' + sceneId];
		var visible = targetJson.visible === "true";
		var position = targetJson.position;
		var positionComps = position.split(",");
		var size = targetJson.size;
		var sizeComps = size.split(",");

				
		// fills fields
		$('#' + targetId + '_visible').prop("checked", visible);
		$('#' + targetId + '_x').val(positionComps[0]);
		$('#' + targetId + '_y').val(positionComps[1]);
		$('#' + targetId + '_width').val(sizeComps[0]);
		$('#' + targetId + '_height').val(sizeComps[1]);

		//custom fields
		if(targetId == "show_title_block"){
			var color = data.show_title.color.substring(1);
			var upcase = data.show_title.upcase;
			$('#' + targetId + '_upcase').prop("checked", upcase);
			document.getElementById(targetId + '_color').jscolor.fromString(color);
		}
		if(targetId == "clock_block"){
			var color = data.clock.color.substring(1);
			var upcase = data.clock.upcase;
			$('#' + targetId + '_upcase').prop("checked", upcase);
			document.getElementById(targetId + '_color').jscolor.fromString(color);
		}
		if(targetId == "now_playing_text_block"){
			//title
			var item = data.playlist.title;
			var visible = item.display;
			var color = item.color.substring(1);
			var upcase = item.upcase;

			$('#' + targetId + '_title_visible').prop("checked", visible);
			$('#' + targetId + '_title_upcase').prop("checked", upcase);
			document.getElementById(targetId + '_title_color').jscolor.fromString(color);
			
			//subtitle
			var item = data.playlist.subtitle;
			var visible = item.display;
			var color = item.color.substring(1);
			var upcase = item.upcase;

			$('#' + targetId + '_subtitle_visible').prop("checked", visible);
			$('#' + targetId + '_subtitle_upcase').prop("checked", upcase);
			document.getElementById(targetId + '_subtitle_color').jscolor.fromString(color);
			
			//description
			var item = data.playlist.description;
			var visible = item.display;
			var color = item.color.substring(1);
			var upcase = item.upcase;
			
			$('#' + targetId + '_description_visible').prop("checked", visible);
			$('#' + targetId + '_description_upcase').prop("checked", upcase);
			document.getElementById(targetId + '_description_color').jscolor.fromString(color);
			

		}
		if(targetId == "news_text_block"){
			//title
			var item = data.news.rss.title;
			var visible = item.display;
			var color = item.color.substring(1);
			var upcase = item.upcase;

			$('#' + targetId + '_title_visible').prop("checked", visible);
			$('#' + targetId + '_title_upcase').prop("checked", upcase);
			document.getElementById(targetId + '_title_color').jscolor.fromString(color);
			
			//subtitle
			var item = data.news.rss.subtitle;
			var visible = item.display;
			var color = item.color.substring(1);
			var upcase = item.upcase;

			$('#' + targetId + '_subtitle_visible').prop("checked", visible);
			$('#' + targetId + '_subtitle_upcase').prop("checked", upcase);
			document.getElementById(targetId + '_subtitle_color').jscolor.fromString(color);
			
			//description
			var item = data.news.rss.description;
			var visible = item.display;
			var color = item.color.substring(1);
			var upcase = item.upcase;
			
			$('#' + targetId + '_description_visible').prop("checked", visible);
			$('#' + targetId + '_description_upcase').prop("checked", upcase);
			document.getElementById(targetId + '_description_color').jscolor.fromString(color);
			
			//footer
			var item = data.news.rss.footer;
			var visible = item.display;
			var color = item.color.substring(1);
			var upcase = item.upcase;
			
			$('#' + targetId + '_footer_visible').prop("checked", visible);
			$('#' + targetId + '_footer_upcase').prop("checked", upcase);
			document.getElementById(targetId + '_footer_color').jscolor.fromString(color);
			
			//twitter entities
			var item = data.news.twitter.title.highlight;
			var color = item.color.substring(1);
			var upcase = item.upcase;
			
			document.getElementById(targetId + '_twitter_entities_color').jscolor.fromString(color);
			$('#' + targetId + '_twitter_entities_upcase').prop("checked", upcase);
			
			//facebook entities
			var item = data.news.facebook.title.highlight;
			var color = item.color.substring(1);
			var upcase = item.upcase;
			
			document.getElementById(targetId + '_facebook_entities_color').jscolor.fromString(color);	
			$('#' + targetId + '_facebook_entities_upcase').prop("checked", upcase);


		}
		
				
		$('#accordion_editor').accordion("refresh"); 
		
	}
	
	function applyEditorChanges(){
		doApplyEditorChanges("video_block");
		doApplyEditorChanges("now_playing_text_block");
		doApplyEditorChanges("news_text_block");
		doApplyEditorChanges("news_image_block");
		doApplyEditorChanges("show_title_block");
		doApplyEditorChanges("clock_block");
	}

	function doApplyEditorChanges(targetId){
		var target = $('#' + targetId);
		var visible = $('#' + targetId + "_visible").prop('checked');
		var leftPos = $('#' + targetId + "_x").val();
		var topPos = $('#' + targetId + "_y").val();
		var sizeWidth = $('#' + targetId + "_width").val();
		var sizeHeight = $('#' + targetId + "_height").val();
		
		$( "span", target ).html( "pos: " + leftPos + " x " + topPos + " px<br/>size: " +  sizeWidth + " x " + sizeHeight + " px");
		target.css({top: topPos, left: leftPos});
		target.css({width: sizeWidth, height: sizeHeight});	
		if(visible) target.show();
		else target.hide();


	}
	
	function doRemoveScene(sceneId){
		var wsUrl = "../webservices/scenario/authoring/removeScene.php?scene_id=" + encodeURIComponent(sceneId);
		$.get( wsUrl , function( data ) {
			$("#messageLabel").html("Scene was succesfully removed");
			$("#messageLabel").css( "display", "block" );
			$("#messageSep").css( "display", "block" );
			loadScenes();
			loadScene("<?php echo DEFAULT_SCENE; ?>");
			
		});
	}

	loadScenes();
	loadScene("<?php echo $sceneId; ?>");
	
</script>
      
</html>

