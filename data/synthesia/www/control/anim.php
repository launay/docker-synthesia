<?php

define("SCENARIO_1", "SCENARIO_1");
define("SCENARIO_2", "SCENARIO_2");

define("SCENARIO_1_X", "10");
define("SCENARIO_1_Y", "10");
define("SCENARIO_1_W", "766");
define("SCENARIO_1_H", "432");

define("SCENARIO_2_W", "1280");
define("SCENARIO_2_H", "720");
define("SCENARIO_2_X", "0");
define("SCENARIO_2_Y", "0");

define("SHOW_SFX", "_SHOW");
define("HIDE_SFX", "_HIDE");

$scenario = SCENARIO_1;
if($_GET['scenario']){$scenario = $_GET['scenario'];}

$params = array();

$videoId= "10000";

$params["scenario"] = $scenario;
$params["videoId"] = "10000";

$params[SCENARIO_1 . SHOW_SFX] = array($videoId, "0", "1", "100", "101", "200", "201", "202", "203", "300", "301", "302", "303", "304");
$params[SCENARIO_1 . HIDE_SFX] = array();
$params[SCENARIO_2 . SHOW_SFX] = array($videoId);
$params[SCENARIO_2 . HIDE_SFX] = array("0", "1", "100", "101", "200", "201", "202", "203", "300", "301", "302", "303", "304");


$animFile = "../status/anim.txt";
$animStatefile = "../status/anim.state.json";

writeAnim($animFile, $params, $animStatefile);


function writeAnim($animFile, $params, $animStatefile){

$S1 = "SET 0 100 true " . SCENARIO_1_W . " " . SCENARIO_1_H . " " . SCENARIO_1_X . " " . SCENARIO_1_Y . " true";
$S2 = "SET 0 100 true " . SCENARIO_2_W . " " . SCENARIO_2_H . " " . SCENARIO_2_X . " " . SCENARIO_2_Y . " true";
$HIDE = "SET 0 100 true NULL NULL NULL NULL false";
$SHOW = "SET 0 100 true NULL NULL NULL NULL true";
$SEP = "\n\n\n";
$toWrite = "";


if($params["scenario"] == SCENARIO_1){
	$toWrite .= $params["videoId"] . "\n";
	$toWrite .= $S1.$SEP;
	
	foreach($params[SCENARIO_1 . HIDE_SFX] as $titId){
		$toWrite .= $titId . "\n";
		$toWrite .= $HIDE.$SEP;
	}
	foreach($params[SCENARIO_1 . SHOW_SFX] as $titId){
		$toWrite .= $titId . "\n";
		$toWrite .= $SHOW.$SEP;
	}
}
else if($params["scenario"] == SCENARIO_2){
	$toWrite .= $params["videoId"] . "\n";
	$toWrite .= $S2.$SEP;
	
	foreach($params[SCENARIO_2 . HIDE_SFX] as $titId){
		$toWrite .= $titId . "\n";
		$toWrite .= $HIDE.$SEP;
	}
	foreach($params[SCENARIO_2 . SHOW_SFX] as $titId){
		$toWrite .= $titId . "\n";
		$toWrite .= $SHOW.$SEP;
	}
}

$toWriteJson = json_encode($params);
	
file_put_contents($animFile, $toWrite);
file_put_contents($animStatefile, $toWriteJson);

}

?>
