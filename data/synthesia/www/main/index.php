<?php
require_once "php/info.php";

define ("ENGINE_ACTIVE", true);
define ("PLAYLIST_ACTIVE", true);
define ("PIGE_ACTIVE", true);

?>
<html>
	<head>
		<?php createHeader();?>

  <style>
  #sortable { cursor: pointer; list-style-type: none; margin: 0; padding: 0; width: 60%; }
  #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
  #sortable li span { position: absolute; margin-left: -1.3em; }

  </style>


	</head>

	<body>

    <div class="jumbotron">
      <div class="container">
        <h1><img height="100px" src="images/synthesia.png"/>&nbsp;Synthesia</h1>
        <p>Create your enhanced live stream in no time</p>
        <p>Enrich your video contents with metadata (RSS, social networks, etc.) and compose your own live stream with different visual scenarios.</p>
        <p>Stream your Live video to your favorite CDN (YouTube, Dailymotion, Facebook Live)</p>
      </div>
    </div>



    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h2>Live</h2>
          <ul>
			<li>Visual scenarios management</li>
			<li>News data sources configuration</li>
			<li>Administration and monitoring of the live engine</li>
          </ul>
          <p <?php if(!ENGINE_ACTIVE) echo 'style="display:none"';?>><a class="btn btn-default" href="../control/" role="button">Explore &raquo;</a></p>
          <p <?php if(ENGINE_ACTIVE) echo 'style="display:none"';?>><a class="btn btn-warning" href="mailto:contact@vizionr.fr" role="button">Contact Vizion'R</a></p>
        </div>
        <div class="col-md-6">
          <h2>Playlist</h2>
           <ul>
			<li>Playlist manager</li>
			<li>Video database management</li>
			<li>Administration et monitoring of the playlist manager</li>
          </ul>
          <p <?php if(!PLAYLIST_ACTIVE) echo 'style="display:none"';?>><a class="btn btn-default" href="../playlist/manager/" role="button">Explore &raquo;</a></p>
          <p <?php if(PLAYLIST_ACTIVE) echo 'style="display:none"';?>><a class="btn btn-warning" href="mailto:contact@vizionr.fr" role="button">Contact Vizion'R</a></p>
       </div>
        <!--div class="col-md-4">
          <h2>Pige</h2>
          <ul>
			<li>Configuration de l'enregistrement du flux Live</li>
			<li>Extraction de séquences vidéo</li>
			<li>Administration et monitoring du module de capture</li>
          </ul>
          <p <?php if(!PIGE_ACTIVE) echo 'style="display:none"';?>><a class="btn btn-default" href="../pige" role="button">Accéder &raquo;</a></p>
          <p <?php if(PIGE_ACTIVE) echo 'style="display:none"';?>><a class="btn btn-warning" href="mailto:contact@vizionr.fr" role="button">Contacter Vizion'R</a></p>
        </div-->
      </div>

      <hr>

      <footer>
        <p>Synthesia &copy; Vizion'R</p>
      </footer>
    </div> 



</body>

</html>

