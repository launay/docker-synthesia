<?php

require_once "config/config.php";


function createHeader(){
	echo '
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>' . MAIN_TITLE . '</title>
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery-ui/jquery-ui.js"></script>
				
		<link rel="icon" type="image/png" href="images/synthesia.png" />
		<link rel="stylesheet" type="text/css" href="js/jquery-ui/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
		<style>
		body {
			padding-top: 70px;	
		}
		.pad_10{
			padding: 10px;
		}
		
		.evenOdd tr:nth-child(even) {background: #EEE}
		.evenOdd tr:nth-child(odd) {background: #FFF}
		
		td { 
			padding: 10px;
		}
		
		.pad_4{
			padding: 4px;
		}
		.bordered_nok{
			border: 3px solid #DDD;
		}
		.bordered_ok{
			border: 3px solid red;
		}
		.font_80 {
			font-size: 85%;	
		}
		.btn-warning{
			background-color:#ff3d00;
		}
		.lbl-warning{
			background-color:#ff3d00;
		}
		.panel-heading a:after {
			font-family:"Glyphicons Halflings";
			content:"\e114";
			float: right;
			color: grey;
		}
		.panel-heading a.collapsed:after {
			content:"\e080";
		}
		.clickable{
			cursor: pointer;
		}
		.table-no-border td {
			border-top: none !important;
		}
		.table-label{
			font-weight:bold;
		}
		.row{
			margin-top:40px;
			padding: 0 10px;
		}
		.panel-heading span {
			margin-top: -20px;
			font-size: 15px;
		}
		</style>'
	;

}

?>
