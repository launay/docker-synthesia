## Execute this file as 'synthesia' to assign right properties on scripts and exes
echo 'Creation des dossiers'
mkdir -p log
mkdir -p images
mkdir -p fonts
mkdir -p proc
mkdir -p conf
mkdir -p bin
mkdir -p pige
mkdir -p pige/log
mkdir -p pige/proc
mkdir -p pige/conf
mkdir -p pige/bin
mkdir -p playlist
mkdir -p playlist/log
mkdir -p playlist/proc
mkdir -p playlist/conf
mkdir -p playlist/bin
mkdir -p import
mkdir -p import/log
mkdir -p import/proc
mkdir -p import/conf
mkdir -p import/bin
echo 'Attribution des droits aux repertoires et fichiers'
chmod u+x *.sh
chmod u+x playlist/*.sh
chmod u+x import/*.sh
chmod u+x pige/*.sh
chmod u+x bin/*
chmod u+x playlist/bin/*
chmod u+x import/bin/*
chmod u+x pige/bin/*
