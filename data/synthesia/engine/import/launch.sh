JAR_FILE="bin/CDNImport.jar";
SETUP_FILE="/var/www/synthesia/common/playlist/import.properties";
WATCHDOG_FILE="proc/wd.pid"
PIDS_FILE="proc/synthesia.pid"
WD_LOG_FILE="log/logs-wd.txt"

if test -f $WATCHDOG_FILE
then
echo "Stop watchdog"
WATCHDOG_PID=$(cat $WATCHDOG_FILE)
kill -15 $WATCHDOG_PID
 sleep 1
kill -9 $WATCHDOG_PID
rm $WATCHDOG_FILE
fi


if test -f $PIDS_FILE
then
echo "Stop running instance"
RUNNING_PIDS=$(cat $PIDS_FILE)
for PID in $RUNNING_PIDS; do
echo "Stop process: " $PID
kill -15 $PID
 sleep 5
kill -9 $PID
done
rm $PIDS_FILE
fi


if [ "$1" = "stop" ]
then
exit
fi

mv log/logs-synthesia.txt log/logs-synthesia.bak.txt;


java -Djava.util.logging.config.file=/home/synthesia/synthesia/common/import/conf/logging.properties -jar bin/CDNImport.jar /var/www/synthesia/common/playlist/import.properties http://localhost/synthesia/common/playlist/manager/dmImport.php http://localhost/synthesia/common/playlist/manager/ytImport.php >log/logs-synthesia.txt 2>&1 & echo $! > $PIDS_FILE



if test -f $WD_LOG_FILE
then
mv $WD_LOG_FILE $WD_LOG_FILE.bak
fi
echo "Start watchdog"
sh wd.sh >$WD_LOG_FILE 2>&1 & echo $! >> $WATCHDOG_FILE
