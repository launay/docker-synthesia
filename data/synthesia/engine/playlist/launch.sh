JAR_FILE="bin/RtmpStreamer.jar";
SETUP_FILE="/var/www/synthesia/common/playlist/playlist/playlist.properties";
WATCHDOG_FILE="proc/wd.pid"
PIDS_FILE="proc/synthesia.pid"
WD_LOG_FILE="log/logs-wd.txt"

if test -f $WATCHDOG_FILE
then
echo "Stop watchdog"
WATCHDOG_PID=$(cat $WATCHDOG_FILE)
kill -15 $WATCHDOG_PID
 sleep 1
kill -9 $WATCHDOG_PID
rm $WATCHDOG_FILE
fi


if test -f $PIDS_FILE
then
echo "Stop running instance"
RUNNING_PIDS=$(cat $PIDS_FILE)
for PID in $RUNNING_PIDS; do
echo "Stop process: " $PID
kill -15 $PID
 sleep 5
kill -9 $PID
done
rm $PIDS_FILE
fi


if [ "$1" = "stop" ]
then
exit
fi

mv log/logs-synthesia.txt log/logs-synthesia.bak.txt;


java -Djava.util.logging.config.file=/home/synthesia/synthesia/common/playlist/conf/logging.properties -Dvizionr.normalizer.ffmpeg=/home/synthesia/synthesia/common/playlist/bin/ffmpeg -jar bin/RtmpStreamer.jar /var/www/synthesia/common/playlist >log/logs-synthesia.txt 2>&1 & echo $! > $PIDS_FILE



if test -f $WD_LOG_FILE
then
mv $WD_LOG_FILE $WD_LOG_FILE.bak
fi
echo "Start watchdog"
sh wd.sh >$WD_LOG_FILE 2>&1 & echo $! >> $WATCHDOG_FILE
