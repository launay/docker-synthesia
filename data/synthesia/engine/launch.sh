UPDATE_FILE="bin/LiveProviderV2.jar.update";
JAR_FILE="bin/LiveProviderV2.jar";
UPDATE_SETUP_FILE="/var/www/synthesia/common/config/startup.json.update";
SETUP_FILE="/var/www/synthesia/common/config/startup.json";
WATCHDOG_FILE="proc/wd.pid"
PIDS_FILE="proc/synthesia.pid"
WD_LOG_FILE="log/logs-wd.txt"

if test -f $WATCHDOG_FILE
then
echo "Stop watchdog"
WATCHDOG_PID=$(cat $WATCHDOG_FILE)
kill -15 $WATCHDOG_PID
 sleep 1
kill -9 $WATCHDOG_PID
rm $WATCHDOG_FILE
fi


if test -f $PIDS_FILE
then
echo "Stop running instance"
RUNNING_PIDS=$(cat $PIDS_FILE)
for PID in $RUNNING_PIDS; do
echo "Stop process: " $PID
kill -15 $PID
 sleep 5
kill -9 $PID
done
rm $PIDS_FILE
fi


if [ "$1" = "stop" ]
then
exit
fi

if test -f $UPDATE_FILE
then
echo "Updating Synthesia version"
mv $UPDATE_FILE $JAR_FILE
fi

if test -f $UPDATE_SETUP_FILE
then
echo "Updating Synthesia setup"
mv $UPDATE_SETUP_FILE $SETUP_FILE
fi

mv log/logs-synthesia.txt log/logs-synthesia.bak.txt;

java -Xmx4G -Dvizionr.encoder.native=true -Dvizionr.decoder.native=true -Dvizionr.compositor.multithread=false -Dvizionr.rtmp.publisher.resendAVConfig=false -Dawt.useSystemAAFontSettings=on -Djava.util.logging.config.file=/home/synthesia/synthesia/common/conf/logging.properties -Dvizionr.processor.ffmpeg=/home/synthesia/synthesia/common/bin/ffmpeg -Dvizionr.compositor.x264.DISABLED=/home/synthesia/synthesia/common/bin/x264 -Dvizionr.rtmp.server.noAck=false -Dvizionr.live.rescale.optimize=true -Dvizionr.rendering.live=VALUE_RENDER_DEFAULT -Dvizionr.h264.preset=veryfast -Dvizionr.image.rescale.alignH=left -Dvizionr.image.rescale.alignV=top -Dvizionr.rtmp.pingpong.disable=true -Dvizionr.conversion.optimize=true -Dvizionr.image.rescale.smooth=false -Dhttp.agent="Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)" -Dvizionr.process.trace=false -jar bin/LiveProviderV2.jar -setupFile=/var/www/synthesia/common/config/startup.json >log/logs-synthesia.txt 2>&1 & echo $! > $PIDS_FILE


if test -f $WD_LOG_FILE
then
mv $WD_LOG_FILE $WD_LOG_FILE.bak
fi
echo "Start watchdog"
sh wd.sh >$WD_LOG_FILE 2>&1 & echo $! >> $WATCHDOG_FILE
