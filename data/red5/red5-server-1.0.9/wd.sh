#!/bin/bash

PIDS_FILE="proc/server.pid"
echo "$(date +"%Y-%m-%d %T") :  watchdog started. Watched pids are " $(cat $PIDS_FILE)

while [ 1 ]
do

sleep 5

if test -f $PIDS_FILE
then
RUNNING_PIDS=$(cat $PIDS_FILE)
for PID in $RUNNING_PIDS; do
if ! (ps -p $PID > /dev/null)
then
echo "$(date +"%Y-%m-%d %T") :  $PID was stopped. Restarting"
./launch.sh start
exit
fi
done
fi

done
