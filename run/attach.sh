INSTANCE_PID_FILE="proc/runningInstance1.txt";

if test -f $INSTANCE_PID_FILE
        then
                INSTANCE_PID=$(cat $INSTANCE_PID_FILE)
                echo "Attaching to running instance $INSTANCE_PID"
                sudo docker exec -i -t $INSTANCE_PID /bin/bash
        else
                echo "No running instance was found."

fi
