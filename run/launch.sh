HTTP_PORT=4000
RTMP_PUSH_PORT=4001
RTMP_PULL_PORT=4002
INSTANCE_PID_FILE="proc/runningInstance1.txt";

if test -f $INSTANCE_PID_FILE
        then
                INSTANCE_PID=$(cat $INSTANCE_PID_FILE)
                echo "Stopping running instance"
                STOPPED_INSTANCE=$(sudo docker stop $INSTANCE_PID)
                sudo docker rm $STOPPED_INSTANCE
                rm $INSTANCE_PID_FILE
                echo "Stopped instance $STOPPED_INSTANCE"
fi

if [ "$1" = "stop" ]
        then
                exit
fi


echo "Launching Docker Image for Synthesia"
sudo docker run -e "DOCKER_SYNTHESIA_HOST_80=$HTTP_PORT" -e "DOCKER_SYNTHESIA_HOST_1935=$RTMP_PUSH_PORT" -e "DOCKER_SYNTHESIA_HOST_1940=$RTMP_PULL_PORT" -p $HTTP_PORT:80 -p $RTMP_PUSH_PORT:1935 -p $RTMP_PULL_PORT:1940 -t -d jonathanlaunay/synthesia > $INSTANCE_PID_FILE
INSTANCE_PID=$(cat $INSTANCE_PID_FILE)
echo "Started Instance. Container pid stored in $INSTANCE_PID_FILE ($INSTANCE_PID)"
