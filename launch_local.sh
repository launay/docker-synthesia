
# engine startup
cd ~/synthesia/common/
./cleanup.sh
./configure.sh

# www cleanup
cd /var/www/synthesia/common/
./cleanup.sh

# demo stuff
cp ~/docker/synthesia/data/demo/playlist/master/* /var/www/synthesia/common/playlist/master/
cp ~/docker/synthesia/data/demo/playlist/out/* /var/www/synthesia/common/playlist/out/
cp ~/docker/synthesia/data/demo/playlist/images/* /var/www/synthesia/common/playlist/images/
cp ~/docker/synthesia/data/demo/playlist/current.m3u /var/www/synthesia/common/playlist/playlist/


# www rights config
cd /var/www/synthesia/common/
./configure.sh


# synthesia startup
cd ~/synthesia/common/
./launch.sh

# playlist startup
cd ~/synthesia/common/playlist/
./launch.sh
