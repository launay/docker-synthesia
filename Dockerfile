FROM ubuntu:16.04

MAINTAINER Jonathan Launay <jonathan.launay@vizionr.fr>

LABEL version="1.0"
LABEL description="Synthesia Vizion'R"

# updates packages
RUN apt-get -y update && apt-get install -y htop && apt-get install -y nano

# install needed web packages
RUN apt-get install -y apache2 php libapache2-mod-php php-xml php-mbstring curl libcurl3 php-curl

# install needed java packages
RUN apt-get -y install software-properties-common
RUN add-apt-repository -y ppa:webupd8team/java
RUN apt-get -y update
RUN echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
RUN apt-get install -y oracle-java8-installer

# install mediainfo
RUN apt-get install -y mediainfo

# install libav-tools
RUN apt-get -y install libav-tools

# install x264
RUN apt-get -y install x264

# install
RUN apt-get install -y libva-x11-1 libva-drm1

# get git
RUN apt-get install -y git

#cleanup
RUN apt-get clean

# install sudo
RUN apt-get -y install sudo
ADD data/system/sudoers /etc/sudoers
RUN chmod 440 /etc/sudoers

# create user
RUN useradd -m -p palM/ystThM06 -s /bin/bash synthesia
RUN adduser synthesia sudo
USER synthesia
WORKDIR /home/synthesia

# setup apache root
USER root
COPY data/apache/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY data/apache/php.ini /etc/php/7.0/apache2/php.ini
COPY data/apache/other-vhosts-access-log.conf /etc/apache2/conf-available/other-vhosts-access-log.conf

# startup script
COPY data/startup.sh /home/synthesia/startup.sh
RUN chmod a+rwx /home/synthesia/startup.sh

# get demo stuff
RUN mkdir -p -m 777 /home/synthesia/demo/
COPY data/demo/ /home/synthesia/demo/

# get bin stuff
RUN mkdir -p -m 777 /home/synthesia/bin/
COPY data/bin/ /home/synthesia/bin/

# setup logrotate
COPY data/logrotate/apache2 /etc/logrotate.d/apache2
RUN sudo apt-get install logrotate

# Make ports 80/1935(rtmpIn)/1940(rtmpOut) available
EXPOSE 80
EXPOSE 1935
EXPOSE 1940

# startup command
USER synthesia
CMD ./startup.sh
